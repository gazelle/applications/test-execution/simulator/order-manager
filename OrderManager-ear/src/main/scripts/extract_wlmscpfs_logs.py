#! /usr/bin/env python
import os.path
import sys
import re
import psycopg2

# method used to store the data in the database using the postgresql Python driver
def storeData(cursor, log_info):
    sql = "INSERT INTO om_scp_logger (id, sending_host, sending_ae_title, receiving_ae_title, request_parameters, message_id, processed_worklists, number_of_results, reason_for_error) VALUES ('" + str(log_info['index']) + "', '" + log_info['sendingHost']+"', '" + log_info['sendingAETitle'] + "', '" + log_info['receivingAETitle'] + "','" + log_info['request'] + "', '" + log_info['MessageID']+ "', '" +log_info['WorklistFileInfo']+"', '" +log_info['MatchingResultsNumber'] +"', '" +log_info['error']+ "');"
#    print sql
    cursor.execute(sql)
    connection.commit()
    

#bad usage
if len(sys.argv) < 2:
    print 'Usage: $>python extract_wlmscpfs_logs.py log_path'
    sys.exit();
elif os.path.exists(sys.argv[1]):
    filepath = sys.argv[1]
    # open the postgresql connection
    try:
        host = 'localhost'
        user = 'gazelle'
        password = 'gazelle'
        param = "host=%s dbname=order-manager user=%s password=%s" %(host, user, password)
        connection = psycopg2.connect(param)
        print "Successfully  connected to the database.\n"
    except StandardError, e:
        print "Error: Unable to connect to the database!", e
        sys.exit()
    
    # create a cursor
    cursor = connection.cursor()
    
    # get the last id used in database
    sql = "SELECT max(id) FROM om_scp_logger"
    index = cursor.execute(sql)
    if index == None:
        index = 0
    
    # open the log file
    logfile = open(filepath, "r")
    searchParams = True   
    # process each line of the log file
    for line in logfile:
        # look for the beginning of the log sequence
        if 'Association Received' in line:
           print "new log entry"
           #initialize a dico
           log_info = {}
           log_info['MatchingResultsNumber'] = "0"
           log_info['error'] = "none"
           index = index + 1
           log_info['index'] = index
           searchParams = True
           association = re.search('\(.+:[a-zA-Z-0-9].+ -> [a-zA-Z-0-9].+\)', line).group(0)
           association = re.sub('\(', '', association)
           association = re.sub('\)', '', association)
           firstPart = association.split(':')
           log_info['sendingHost'] = firstPart[0]
           secondPart = firstPart[1].split(' -> ')
           log_info['sendingAETitle'] = secondPart[0]
           log_info['receivingAETitle'] = secondPart[1]
        # look for the request type and message ID
        elif 'Message Type' in line:
            queryType = line.split(':')
            log_info['request'] = queryType[2]
        elif 'Message ID' in line:
            messageId = line.split(':')
            log_info['MessageID'] = messageId[2]
        # look for the request parameters
        elif searchParams and re.match('.[0-9]{4}.[0-9]{4}', line):
            if log_info.__contains__('Dicom-Data-Set'):
                log_info['Dicom-Data-Set'] = log_info['Dicom-Data-Set'] + line
            else:
                log_info['Dicom-Data-Set'] = line
        # the end of request parameters has been reached
        elif searchParams and re.match('[=]', line):
            searchParams = False
        # extract the information about the processed worklist files
        elif not (re.match('Matching results', line)) and re.search('[wW]{1}orklist file', line):
            if log_info.__contains__('WorklistFileInfo'):
                log_info['WorklistFileInfo'] = log_info['WorklistFileInfo'] + line
            else:
                log_info['WorklistFileInfo'] = line
        # extract the number of matching worklists
        elif 'Matching results' in line:
            log_info['MatchingResultsNumber'] = line
        # the end of the current log entry has been reach
        elif 'Cleaned up after child' in line:
            print "end of current log entry"
            # compute the next id for storing log
            storeData(cursor, log_info)
#            for key in log_info.keys():
#                print '%s : %s' %(key,log_info[key])
        elif "Refusing Association" in line:
            log_info['error'] = line
            storeData(cursor, log_info)
    
    #close the log file
    logfile.close() 
    connection.close()
else:
    print '%s does not exists' %(sys.argv[1])
    sys.exist()

        
    