@startuml
hide footbox
title Manage Order (KSA-TRO)
participant CREATOR as "TRO_CREATOR (Order Manager)" #99FF99
participant FORWARDER as "TRO_FORWARDER (infrastructure)" #FF99FF
participant FULFILLER as "TRO_FULFILLER (SUT)"
activate CREATOR
CREATOR -> FORWARDER : OMI^O23^OMI_O23 (NW, OC)
activate FORWARDER
FORWARDER --> FULFILLER : OMI^O23^OMI_O23 (NW, OC)
activate FULFILLER
FULFILLER -> FORWARDER : ACK^O23^ACK
deactivate FULFILLER
FORWARDER --> CREATOR : ACK^O23^ACK
deactivate FORWARDER
deactivate CREATOR
@enduml
