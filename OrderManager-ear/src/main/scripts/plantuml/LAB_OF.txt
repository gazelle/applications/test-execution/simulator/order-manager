@startuml
hide footbox
title Transactions supported by the Order Filler part of the tool
participant ORT as "ORT (SUT)"
participant AM as "AM (SUT)"
participant OP as "OP (SUT)"
participant OF as "OF (Order Manager)" #99FF99

group Order Management (LAB-1 & LAB-2)
OF -> OP : OML (O21, O33, O35)
activate OP
OP --> OF : ORL (O22, O34, O36)
deactivate OP
OP -> OF : OML (O21, O33, O35)
activate OF
OF --> OP : ORL (O22, O34, O36)
deactivate OF
end
group Work Order Management (LAB-4)
OF -> AM : OML (O21, O33, O35)
activate AM
AM --> OF : ORL (O22, O34, O36)
deactivate AM
end
group Order Results Management (LAB-3)
OF -> ORT : OUL^R22 or ORU^R01
activate ORT
ORT --> OF : ACK
deactivate ORT
end
group Test Results Management (LAB-5)
AM -> OF : OUL (R22, R23)
activate OF
OF --> AM : ACK
deactivate OF
end
@enduml
