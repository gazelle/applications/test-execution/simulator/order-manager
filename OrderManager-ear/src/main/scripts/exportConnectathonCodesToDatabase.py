#! /usr/bin/env python

import psycopg2
import os
import os.path
import sys
from xml.dom import minidom

initialdir = os.getcwd()

#bad usage
if len(sys.argv) < 2:
    print 'Usage: $>python exportConnectathonCodesToDatabase.py rootDirectory [dbhost dbuser dbpassword]'
    sys.exit();

#dbhost, dbuser and dbpassword missing
if len(sys.argv) < 4:
    host = raw_input('DB IP address: ')
    user = raw_input('DB user: ')
    password = raw_input('password: ')

else:
    host = sys.argv[2]
    user = sys.argv[3]
    password = sys.argv[4]

rootDirectory = sys.argv[1]

# get the list of files contained in the given directory
if (os.path.isdir(rootDirectory)):
    files = os.listdir(rootDirectory)
    os.chdir(rootDirectory)
else:
    print 'Error: %s is not a valid directory' %(rootDirectory)
    sys.exit()
    
#database connection
try:
    param = "host=%s dbname=order-manager user=%s password=%s" %(host, user, password)
    connection = psycopg2.connect(param)
    print 'Successfully  connected to the database.\n'
except StandardError, e:
    print 'Error: Unable to connect to the database!', e
    sys.exit()
    
# create a cursor
cursor = connection.cursor()

#get the last id used in database
sql = "SELECT max(id) FROM connectathon_code"
index = cursor.execute(sql)

if index != None:
    index = index + 1
else:
    index = 1

for filepath in files:
    print filepath
    
    if os.path.isdir(filepath):
        continue
    elif ".xml" not in filepath:
        continue
    else:
        document = minidom.parse(filepath)
        valueSetElement = document.getElementsByTagName('ValueSet')
        valueSetOID = valueSetElement[0].attributes["id"].value
        codeSet = valueSetElement[0].attributes["displayName"].value
        codeSet = codeSet.replace("'", "\'").replace('"', '\"')
        
        concepts = document.getElementsByTagName('Concept')
        for concept in concepts:
            code = concept.attributes["code"].value
            code = code.replace("'", "\'").replace('"', '\"')
            displayName = concept.attributes["displayName"].value
            displayName = displayName.replace("\'", "\\'").replace('"', '\"')
            codeSystem = concept.attributes["codeSystem"].value
            codeSystem = codeSystem.replace("\'", "\\'").replace('"', '\"')
            sql = "INSERT INTO connectathon_code (id, code, display_name, code_set_oid, code_system, code_set) VALUES (%s, '%s', '%s', '%s', '%s', '%s')" %(index, code, displayName, valueSetOID, codeSystem, codeSet)
            cursor.execute(sql)
            connection.commit()
            index = index + 1

os.chdir(initialdir)   
         
    