-- Create a new column for migrating enum string values
ALTER TABLE validation_parameters ADD COLUMN new_validator_type VARCHAR(64);

-- Migrate values from 'status' to 'new_status'
UPDATE validation_parameters SET new_validator_type='V2' WHERE validator_type=0;
UPDATE validation_parameters SET new_validator_type='V3' WHERE validator_type=1;
UPDATE validation_parameters SET new_validator_type='FHIR' WHERE validator_type=2;
UPDATE validation_parameters SET new_validator_type='FHIR' WHERE validator_type=3;

-- Drop the old 'status' column
ALTER TABLE validation_parameters DROP COLUMN validator_type ;

-- Rename the new column to match the entity declaration
ALTER TABLE validation_parameters RENAME COLUMN new_validator_type TO validator_type ;