UPDATE om_dicom_message set simulated_actor_id = 2;

INSERT INTO app_configuration (id, variable, value) VALUES (28, 'proxy_port_low_limit', '10001');
SELECT pg_catalog.setval('app_configuration_id_seq', 28, true);

INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (9, 'Aquisition Modality', 'MOD', 'Aquisition Modality', false);
SELECT pg_catalog.setval('tf_actor_id_seq', 9 , true);

