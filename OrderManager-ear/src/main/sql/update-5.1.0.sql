delete from app_configuration WHERE variable = 'cas_url';
INSERT INTO app_configuration(id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'restrict_access_to_messages', false);
ALTER TABLE cmn_transaction_instance RENAME COLUMN username to company_keyword;
ALTER TABLE cmn_transaction_instance ADD COLUMN new_standard VARCHAR(64);
UPDATE cmn_transaction_instance set new_standard = 'HL7V2' where standard = 0;
UPDATE cmn_transaction_instance set new_standard = 'HL7V3' where standard = 1;
UPDATE cmn_transaction_instance set new_standard = 'XDS' where standard = 2;
UPDATE cmn_transaction_instance set new_standard = 'FHIR_XML' where standard = 3;
UPDATE cmn_transaction_instance set new_standard = 'FHIR_JSON' where standard = 4;
UPDATE cmn_transaction_instance set new_standard = 'SVS' where standard = 5;
UPDATE cmn_transaction_instance set new_standard = 'HPD' where standard = 6;
UPDATE cmn_transaction_instance set new_standard = 'OTHER' where standard = 7;
ALTER TABLE cmn_transaction_instance DROP COLUMN standard;
ALTER TABLE cmn_transaction_instance RENAME new_standard TO standard;
ALTER TABLE cmn_message_instance ADD COLUMN issuer_ip_address character varying(255);
ALTER TABLE system_configuration ADD COLUMN owner_company varchar(255);

--
-- Name: cmn_company_details; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_company_details (
  id integer NOT NULL,
  company_keyword character varying(255)
);


ALTER TABLE cmn_company_details OWNER TO gazelle;

--
-- Name: cmn_company_details_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_company_details_id_seq
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;


ALTER TABLE cmn_company_details_id_seq OWNER TO gazelle;

--
-- Name: cmn_ip_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_ip_address (
  id integer NOT NULL,
  added_by character varying(255),
  added_on timestamp without time zone,
  value character varying(255),
  company_details_id integer
);


ALTER TABLE cmn_ip_address OWNER TO gazelle;

--
-- Name: cmn_ip_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_ip_address_id_seq
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;


ALTER TABLE cmn_ip_address_id_seq OWNER TO gazelle;

--
-- Name: cmn_company_details_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_company_details
  ADD CONSTRAINT cmn_company_details_pkey PRIMARY KEY (id);

--
-- Name: cmn_ip_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_ip_address
  ADD CONSTRAINT cmn_ip_address_pkey PRIMARY KEY (id);

--
-- Name: uk_r6osh086b3nqbuxpswcp1hcc2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_company_details
  ADD CONSTRAINT uk_r6osh086b3nqbuxpswcp1hcc2 UNIQUE (company_keyword);


--
-- Name: fk_8b3pqht8rw1vyiu1o0dm1ewtt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_ip_address
  ADD CONSTRAINT fk_8b3pqht8rw1vyiu1o0dm1ewtt FOREIGN KEY (company_details_id) REFERENCES cmn_company_details(id);
