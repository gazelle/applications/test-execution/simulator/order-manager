--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: link_messages_to_transaction(); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.link_messages_to_transaction() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE msg RECORD;
BEGIN
  FOR msg IN SELECT *
             FROM cmn_message_instance
             WHERE request = TRUE AND hl7_message_id IS NOT NULL
             LIMIT 10000
  LOOP
    UPDATE cmn_transaction_instance
    SET request_id = msg.id
    WHERE hl7_message_id = msg.hl7_message_id;
    UPDATE cmn_message_instance
    SET hl7_message_id = NULL
    WHERE id = msg.id;
  END LOOP;
  FOR msg IN SELECT *
             FROM cmn_message_instance
             WHERE request = FALSE AND hl7_message_id IS NOT NULL
             LIMIT 10000
  LOOP
    UPDATE cmn_transaction_instance
    SET response_id = msg.id
    WHERE hl7_message_id = msg.hl7_message_id;
    UPDATE cmn_message_instance
    SET hl7_message_id = NULL
    WHERE id = msg.id;
  END LOOP;
  RETURN;
END
$$;


ALTER FUNCTION public.link_messages_to_transaction() OWNER TO gazelle;

--
-- Name: sut_usages(); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.sut_usages() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE sys RECORD;
BEGIN
  FOR sys IN SELECT *
             FROM system_configuration
             WHERE dtype = 'hl7v2_responder'
  LOOP
    IF (sys.actor_id = (SELECT id
                        FROM tf_actor
                        WHERE keyword = 'OF'))
    THEN
      INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                 FROM usage_metadata
                                                                                                 WHERE keyword =
                                                                                                       'EYE_RAD2'));
      INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                 FROM usage_metadata
                                                                                                 WHERE keyword =
                                                                                                       'RAD_RAD2'));
      INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                 FROM usage_metadata
                                                                                                 WHERE keyword =
                                                                                                       'LAB_OF_LAB5'));
      INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                 FROM usage_metadata
                                                                                                 WHERE keyword =
                                                                                                       'LAB_OF_LAB1'));
      INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                 FROM usage_metadata
                                                                                                 WHERE keyword =
                                                                                                       'LAB_OF_LAB2'));
    ELSEIF (sys.actor_id = (SELECT id
                            FROM tf_actor
                            WHERE keyword = 'OP'))
      THEN
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'EYE_RAD3'));
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'RAD_RAD3'));
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'LAB_OP_LAB1'));
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'LAB_OP_LAB2'));
    ELSEIF (sys.actor_id = (SELECT id
                            FROM tf_actor
                            WHERE keyword = 'ANALYZER'))
      THEN
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'LAB_ANALYZER'));
    ELSEIF (sys.actor_id = (SELECT id
                            FROM tf_actor
                            WHERE keyword = 'ANALYZER_MGR'))
      THEN
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'LAB_AN_MGR_LAB27'));
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'LAB_AN_MGR_LAB29'));
    ELSEIF (sys.actor_id = (SELECT id
                            FROM tf_actor
                            WHERE keyword = 'ORT'))
      THEN
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'LAB_ORT'));
    ELSEIF (sys.actor_id = (SELECT id
                            FROM tf_actor
                            WHERE keyword = 'AM'))
      THEN
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'LAB_AM'));
    ELSEIF (sys.actor_id = (SELECT id
                            FROM tf_actor
                            WHERE keyword = 'IM'))
      THEN
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'RAD_RAD4'));
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'EYE_RAD4'));
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'RAD_RAD13'));
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'EYE_RAD13'));
    ELSEIF (sys.actor_id = (SELECT id
                            FROM tf_actor
                            WHERE keyword = 'TRO_CREATOR'))
      THEN
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'KSA_TRO_CREATOR'));
    ELSEIF (sys.actor_id = (SELECT id
                            FROM tf_actor
                            WHERE keyword = 'TRO_FULFILLER'))
      THEN
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'KSA_TRO_FULFILLER'));
    ELSEIF (sys.actor_id = (SELECT id
                            FROM tf_actor
                            WHERE keyword = 'TRO_FORWARDER'))
      THEN
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'KSA_TRO_FORWARDER'));
    ELSEIF (sys.actor_id = 0)
      THEN
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'EYE_RAD5'));
        INSERT INTO sys_conf_type_usages (system_configuration_id, listusages_id) VALUES (sys.id, (SELECT id
                                                                                                   FROM usage_metadata
                                                                                                   WHERE keyword =
                                                                                                         'RAD_RAD5'));
    END IF;

  END LOOP;
  RETURN;
END
$$;


ALTER FUNCTION public.sut_usages() OWNER TO gazelle;

--
-- Name: update_all(); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.update_all() RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  WHILE (SELECT count(id) > 0
         FROM cmn_message_instance
         WHERE hl7_message_id IS NOT NULL) LOOP
    PERFORM link_messages_to_transaction();
  END LOOP;
END
$$;


ALTER FUNCTION public.update_all() OWNER TO gazelle;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accession_number; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.accession_number (
    id integer NOT NULL
);


ALTER TABLE public.accession_number OWNER TO gazelle;

--
-- Name: affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.affinity_domain (
    id integer NOT NULL,
    keyword character varying(255),
    label_to_display character varying(255),
    profile character varying(255)
);


ALTER TABLE public.affinity_domain OWNER TO gazelle;

--
-- Name: affinity_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.affinity_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affinity_domain_id_seq OWNER TO gazelle;

--
-- Name: affinity_domain_transactions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.affinity_domain_transactions (
    affinity_domain_id integer NOT NULL,
    transaction_id integer NOT NULL
);


ALTER TABLE public.affinity_domain_transactions OWNER TO gazelle;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE public.app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_configuration (
    id integer NOT NULL,
    comment character varying(255),
    approved boolean NOT NULL,
    is_secured boolean,
    host_id integer
);


ALTER TABLE public.cfg_configuration OWNER TO gazelle;

--
-- Name: cfg_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_dicom_scp_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE public.cfg_dicom_scp_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_dicom_scp_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_dicom_scp_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_dicom_scu_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE public.cfg_dicom_scu_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_dicom_scu_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_dicom_scu_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_out integer,
    port_secure integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_v3_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_v3_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_v3_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_v3_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_v3_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_v3_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_v3_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_v3_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_host; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_host (
    id integer NOT NULL,
    alias character varying(255),
    comment character varying(255),
    hostname character varying(255) NOT NULL,
    ip character varying(255)
);


ALTER TABLE public.cfg_host OWNER TO gazelle;

--
-- Name: cfg_host_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_host_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_host_id_seq OWNER TO gazelle;

--
-- Name: cfg_sop_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_sop_class (
    id integer NOT NULL,
    keyword character varying(255) NOT NULL,
    name character varying(255)
);


ALTER TABLE public.cfg_sop_class OWNER TO gazelle;

--
-- Name: cfg_sop_class_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_sop_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_sop_class_id_seq OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_syslog_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_syslog_configuration OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_syslog_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_syslog_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_web_service_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    assigning_authority character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_web_service_configuration OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_web_service_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_web_service_configuration_id_seq OWNER TO gazelle;

--
-- Name: cmn_company_details; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_company_details (
    id integer NOT NULL,
    company_keyword character varying(255)
);


ALTER TABLE public.cmn_company_details OWNER TO gazelle;

--
-- Name: cmn_company_details_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_company_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_company_details_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE public.cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_home_id_seq OWNER TO gazelle;

--
-- Name: cmn_ip_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_ip_address (
    id integer NOT NULL,
    added_by character varying(255),
    added_on timestamp without time zone,
    value character varying(255),
    company_details_id integer
);


ALTER TABLE public.cmn_ip_address OWNER TO gazelle;

--
-- Name: cmn_ip_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_ip_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_ip_address_id_seq OWNER TO gazelle;

--
-- Name: cmn_message_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_message_instance (
    id integer NOT NULL,
    content bytea,
    issuer character varying(255),
    issuer_ip_address character varying(255),
    type character varying(255),
    validation_detailed_result bytea,
    validation_status character varying(255),
    issuing_actor integer,
    payload bytea
);


ALTER TABLE public.cmn_message_instance OWNER TO gazelle;

--
-- Name: cmn_message_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_message_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_message_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_message_instance_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_message_instance_metadata (
    id integer NOT NULL,
    label character varying(255),
    value character varying(255),
    message_instance_id integer
);


ALTER TABLE public.cmn_message_instance_metadata OWNER TO gazelle;

--
-- Name: cmn_message_instance_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_message_instance_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_message_instance_metadata_id_seq OWNER TO gazelle;

--
-- Name: cmn_receiver_console; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_receiver_console (
    id integer NOT NULL,
    code character varying(255),
    comment text,
    message_identifier character varying(255),
    message_type character varying(255),
    sut character varying(255),
    "timestamp" timestamp without time zone,
    domain_id integer,
    simulated_actor_id integer,
    transaction_id integer
);


ALTER TABLE public.cmn_receiver_console OWNER TO gazelle;

--
-- Name: cmn_receiver_console_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_receiver_console_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_receiver_console_id_seq OWNER TO gazelle;

--
-- Name: cmn_transaction_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_transaction_instance (
    id integer NOT NULL,
    company_keyword character varying(255),
    "timestamp" timestamp without time zone,
    is_visible boolean,
    domain_id integer,
    request_id integer,
    response_id integer,
    simulated_actor_id integer,
    transaction_id integer,
    standard character varying(64)
);


ALTER TABLE public.cmn_transaction_instance OWNER TO gazelle;

--
-- Name: cmn_transaction_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_transaction_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_transaction_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_validator_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_validator_usage (
    id integer NOT NULL,
    caller character varying(255),
    date date,
    status character varying(255),
    type character varying(255)
);


ALTER TABLE public.cmn_validator_usage OWNER TO gazelle;

--
-- Name: cmn_validator_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_validator_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_validator_usage_id_seq OWNER TO gazelle;

--
-- Name: cmn_value_set; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_value_set (
    id integer NOT NULL,
    value_set_name character varying(255),
    value_set_oid character varying(255),
    usage character varying(255),
    value_set_keyword character varying(255),
    accessible boolean,
    last_check timestamp without time zone
);


ALTER TABLE public.cmn_value_set OWNER TO gazelle;

--
-- Name: cmn_value_set_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_value_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_value_set_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_contextual_information (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    value character varying(255),
    path integer NOT NULL
);


ALTER TABLE public.gs_contextual_information OWNER TO gazelle;

--
-- Name: gs_contextual_information_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_contextual_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_contextual_information_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_contextual_information_instance (
    id integer NOT NULL,
    form character varying(255),
    value character varying(255),
    contextual_information_id integer NOT NULL,
    test_steps_instance_id integer NOT NULL
);


ALTER TABLE public.gs_contextual_information_instance OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_contextual_information_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_contextual_information_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_message (
    id integer NOT NULL,
    message_content bytea,
    message_type_id character varying(255),
    time_stamp timestamp without time zone,
    test_instance_participants_receiver_id integer,
    test_instance_participants_sender_id integer,
    transaction_id integer
);


ALTER TABLE public.gs_message OWNER TO gazelle;

--
-- Name: gs_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_message_id_seq OWNER TO gazelle;

--
-- Name: gs_system; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_system (
    id integer NOT NULL,
    institution_keyword character varying(255),
    keyword character varying(255),
    system_owner character varying(255)
);


ALTER TABLE public.gs_system OWNER TO gazelle;

--
-- Name: gs_system_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_system_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance (
    id integer NOT NULL,
    server_test_instance_id character varying(255) NOT NULL,
    test_instance_status_id integer
);


ALTER TABLE public.gs_test_instance OWNER TO gazelle;

--
-- Name: gs_test_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_oid (
    oid_configuration_id integer NOT NULL,
    test_instance_id integer NOT NULL
);


ALTER TABLE public.gs_test_instance_oid OWNER TO gazelle;

--
-- Name: gs_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_participants (
    id integer NOT NULL,
    server_aipo_id character varying(255) NOT NULL,
    server_test_instance_participants_id character varying(255) NOT NULL,
    aipo_id integer,
    system_id integer
);


ALTER TABLE public.gs_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_instance_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_participants_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_status (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE public.gs_test_instance_status OWNER TO gazelle;

--
-- Name: gs_test_instance_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_status_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_test_instance_participants (
    test_instance_id integer NOT NULL,
    test_instance_participants_id integer NOT NULL
);


ALTER TABLE public.gs_test_instance_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_steps_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_steps_instance (
    id integer NOT NULL,
    dicom_scp_config_id integer,
    dicom_scu_config_id integer,
    hl7v2_initiator_config_id integer,
    hl7v2_responder_config_id integer,
    hl7v3_initiator_config_id integer,
    hl7v3_responder_config_id integer,
    syslog_config_id integer,
    testinstance_id integer,
    web_service_config_id integer
);


ALTER TABLE public.gs_test_steps_instance OWNER TO gazelle;

--
-- Name: gs_test_steps_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_steps_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_steps_instance_id_seq OWNER TO gazelle;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO gazelle;

--
-- Name: hl7_charset; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.hl7_charset (
    id integer NOT NULL,
    display_name character varying(255) NOT NULL,
    hl7_code character varying(255),
    java_code character varying(255)
);


ALTER TABLE public.hl7_charset OWNER TO gazelle;

--
-- Name: hl7_charset_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hl7_charset_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hl7_charset_id_seq OWNER TO gazelle;

--
-- Name: hl7_simulator_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.hl7_simulator_responder_configuration (
    id integer NOT NULL,
    accepted_message_type character varying(255),
    accepted_trigger_event character varying(255),
    hl7_protocol integer,
    ip_address character varying(255),
    listening_port integer,
    message_encoding integer,
    name character varying(255) NOT NULL,
    receiving_application character varying(255) NOT NULL,
    receiving_facility character varying(255) NOT NULL,
    is_running boolean,
    server_bean_name character varying(255),
    url character varying(255),
    domain_id integer,
    simulated_actor_id integer NOT NULL,
    transaction_id integer
);


ALTER TABLE public.hl7_simulator_responder_configuration OWNER TO gazelle;

--
-- Name: hl7_simulator_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hl7_simulator_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hl7_simulator_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: hl7_validation_parameters_affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.hl7_validation_parameters_affinity_domain (
    validation_parameters_id integer NOT NULL,
    affinity_domain_id integer NOT NULL
);


ALTER TABLE public.hl7_validation_parameters_affinity_domain OWNER TO gazelle;

--
-- Name: om_appointment; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_appointment (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    filler_appointment_id character varying(255),
    filler_contact_person character varying(255),
    domain_id integer,
    simulated_actor_id integer,
    order_id integer
);


ALTER TABLE public.om_appointment OWNER TO gazelle;

--
-- Name: om_appointment_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_appointment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_appointment_id_seq OWNER TO gazelle;

--
-- Name: om_appointment_resource; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_appointment_resource (
    id integer NOT NULL,
    ais_id character varying(255),
    rgs_id character varying(255),
    start_date_time timestamp without time zone,
    status character varying(255),
    universal_service_identifier character varying(255),
    appointment_id integer
);


ALTER TABLE public.om_appointment_resource OWNER TO gazelle;

--
-- Name: om_appointment_resource_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_appointment_resource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_appointment_resource_id_seq OWNER TO gazelle;

--
-- Name: om_command_field_values; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_command_field_values (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    value_of_key character varying(255)
);


ALTER TABLE public.om_command_field_values OWNER TO gazelle;

--
-- Name: om_command_field_values_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_command_field_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_command_field_values_id_seq OWNER TO gazelle;

--
-- Name: om_container; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_container (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    cap_type character varying(255),
    carrier_identifier character varying(255),
    container_identifier character varying(255),
    location character varying(255),
    is_main_entity boolean,
    position_in_carrier character varying(255),
    position_in_tray character varying(255),
    primary_container_identifier character varying(255),
    tray_identifier character varying(255),
    work_order boolean,
    domain_id integer,
    simulated_actor_id integer,
    specimen_id integer
);


ALTER TABLE public.om_container OWNER TO gazelle;

--
-- Name: om_container_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_container_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_container_id_seq OWNER TO gazelle;

--
-- Name: om_dicom_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_dicom_message (
    id integer NOT NULL,
    dicom_commandfield character varying(255),
    file_command_set bytea,
    file_path character varying(255),
    from_host_port character varying(255),
    index character varying(255),
    index_int integer,
    is_request boolean,
    "timestamp" timestamp without time zone,
    to_host_port character varying(255),
    transfert_syntax text,
    connection_id integer,
    simulated_actor_id integer,
    simulated_transaction_id integer
);


ALTER TABLE public.om_dicom_message OWNER TO gazelle;

--
-- Name: om_dicom_message_om_dicom_validation_result; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_dicom_message_om_dicom_validation_result (
    om_dicom_message_id integer NOT NULL,
    validationresults_id integer NOT NULL
);


ALTER TABLE public.om_dicom_message_om_dicom_validation_result OWNER TO gazelle;

--
-- Name: om_dicom_request_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_dicom_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_dicom_request_id_seq OWNER TO gazelle;

--
-- Name: om_dicom_uid_values; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_dicom_uid_values (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    value_of_key character varying(255)
);


ALTER TABLE public.om_dicom_uid_values OWNER TO gazelle;

--
-- Name: om_dicom_uid_values_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_dicom_uid_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_dicom_uid_values_id_seq OWNER TO gazelle;

--
-- Name: om_dicom_validation_result; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_dicom_validation_result (
    id integer NOT NULL,
    validation_status character varying(255),
    dicom_message_id integer,
    oid character varying(255),
    permanent_link character varying(255),
    validation_date character varying(255),
    raw_result text
);


ALTER TABLE public.om_dicom_validation_result OWNER TO gazelle;

--
-- Name: om_dicom_validation_result_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_dicom_validation_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_dicom_validation_result_id_seq OWNER TO gazelle;

--
-- Name: om_encounter; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_encounter (
    id integer NOT NULL,
    assigned_patient_location character varying(255),
    creation_date timestamp without time zone,
    creator character varying(255),
    patient_class_code character varying(255),
    referring_doctor_code character varying(255),
    visit_number character varying(255),
    patient_id integer
);


ALTER TABLE public.om_encounter OWNER TO gazelle;

--
-- Name: om_encounter_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_encounter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_encounter_id_seq OWNER TO gazelle;

--
-- Name: om_lab_order; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_lab_order (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    call_back_phone_number character varying(255),
    danger_code character varying(255),
    entered_by character varying(255),
    entering_organization character varying(255),
    filler_order_number character varying(255),
    order_control_code character varying(255),
    order_result_status character varying(255),
    order_status character varying(255),
    ordering_facility_name character varying(255),
    ordering_provider character varying(255),
    placer_group_number character varying(255),
    placer_order_number character varying(255),
    quantity_timing character varying(255),
    reason_for_study character varying(255),
    relevant_clinical_info character varying(255),
    start_date_time timestamp without time zone,
    technician character varying(255),
    transaction_date timestamp without time zone,
    universal_service_id character varying(255),
    diagnostic_service_section_id character varying(255),
    expected_availability_date_time timestamp without time zone,
    is_main_entity boolean,
    result_copies_to character varying(255),
    is_work_order boolean,
    domain_id integer,
    simulated_actor_id integer,
    encounter_id integer,
    container_id integer
);


ALTER TABLE public.om_lab_order OWNER TO gazelle;

--
-- Name: om_lab_order_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_lab_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_lab_order_id_seq OWNER TO gazelle;

--
-- Name: om_lab_order_parent; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_lab_order_parent (
    om_lab_order_id integer NOT NULL,
    element character varying(255),
    parent_id integer NOT NULL
);


ALTER TABLE public.om_lab_order_parent OWNER TO gazelle;

--
-- Name: om_lab_order_specimen; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_lab_order_specimen (
    specimen_id integer NOT NULL,
    lab_order_id integer NOT NULL
);


ALTER TABLE public.om_lab_order_specimen OWNER TO gazelle;

--
-- Name: om_modality_code; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_modality_code (
    id integer NOT NULL,
    coding_scheme_designator character varying(255),
    modality_code character varying(255),
    protocol_code character varying(255),
    protocol_code_meaning character varying(255),
    requested_procedure character varying(255),
    sps_description character varying(255),
    universal_service_id_code character varying(255)
);


ALTER TABLE public.om_modality_code OWNER TO gazelle;

--
-- Name: om_modality_code_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_modality_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_modality_code_id_seq OWNER TO gazelle;

--
-- Name: om_note_and_comment; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_note_and_comment (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    comment text,
    set_id integer,
    source character varying(255),
    type character varying(255),
    domain_id integer,
    simulated_actor_id integer,
    observation_id integer
);


ALTER TABLE public.om_note_and_comment OWNER TO gazelle;

--
-- Name: om_note_and_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_note_and_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_note_and_comment_id_seq OWNER TO gazelle;

--
-- Name: om_number_generator; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_number_generator (
    id integer NOT NULL,
    namespace_id character varying(255),
    next_entity_identifier integer,
    universal_id character varying(255),
    universal_id_type character varying(255),
    actor_id integer
);


ALTER TABLE public.om_number_generator OWNER TO gazelle;

--
-- Name: om_number_generator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_number_generator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_number_generator_id_seq OWNER TO gazelle;

--
-- Name: om_observation; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_observation (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    access_checks character varying(255),
    analysis_date_time timestamp without time zone,
    equipment character varying(255),
    identifier character varying(255),
    observation_date_time timestamp without time zone,
    observation_method character varying(255),
    observation_type character varying(255),
    producer_id character varying(255),
    references_range character varying(255),
    responsible_observer character varying(255),
    result_status character varying(255),
    set_id integer,
    sub_id character varying(255),
    units character varying(255),
    value text,
    value_type character varying(255),
    domain_id integer,
    simulated_actor_id integer,
    order_id integer,
    specimen_id integer
);


ALTER TABLE public.om_observation OWNER TO gazelle;

--
-- Name: om_observation_abnormalflags; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_observation_abnormalflags (
    om_observation_id integer NOT NULL,
    abnormal_flags character varying(255),
    element character varying(255),
    abnormal_flag_id integer
);


ALTER TABLE public.om_observation_abnormalflags OWNER TO gazelle;

--
-- Name: om_observation_flag; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_observation_flag (
    abnormal_flag_id integer NOT NULL,
    element character varying(255)
);


ALTER TABLE public.om_observation_flag OWNER TO gazelle;

--
-- Name: om_observation_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_observation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_observation_id_seq OWNER TO gazelle;

--
-- Name: om_order; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_order (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    call_back_phone_number character varying(255),
    danger_code character varying(255),
    entered_by character varying(255),
    entering_organization character varying(255),
    filler_order_number character varying(255),
    order_control_code character varying(255),
    order_result_status character varying(255),
    order_status character varying(255),
    ordering_facility_name character varying(255),
    ordering_provider character varying(255),
    placer_group_number character varying(255),
    placer_order_number character varying(255),
    quantity_timing character varying(255),
    reason_for_study character varying(255),
    relevant_clinical_info character varying(255),
    start_date_time timestamp without time zone,
    technician character varying(255),
    transaction_date timestamp without time zone,
    universal_service_id character varying(255),
    accession_number character varying(255),
    specimen_source character varying(255),
    transport_arranged character varying(255),
    transportation_mode character varying(255),
    domain_id integer,
    simulated_actor_id integer,
    encounter_id integer
);


ALTER TABLE public.om_order OWNER TO gazelle;

--
-- Name: om_order_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_order_id_seq OWNER TO gazelle;

--
-- Name: om_order_parent_id; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_order_parent_id (
    om_parent_id integer NOT NULL,
    element character varying(255)
);


ALTER TABLE public.om_order_parent_id OWNER TO gazelle;

--
-- Name: om_patient; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_patient (
    id integer NOT NULL,
    character_set character varying(255),
    city character varying(255),
    country_code character varying(255),
    creation_date timestamp without time zone,
    date_of_birth timestamp without time zone,
    first_name character varying(255),
    gender_code character varying(255),
    last_name character varying(255),
    mother_maiden_name character varying(255),
    race_code character varying(255),
    religion_code character varying(255),
    state character varying(255),
    street character varying(255),
    zip_code character varying(255),
    contrast_allergies character varying(255),
    creator character varying(255),
    weight integer,
    alternate_first_name character varying(255),
    alternate_last_name character varying(255),
    alternate_mothers_maiden_name character varying(255),
    second_name character varying(255),
    third_name character varying(255),
    alternate_second_name character varying(255),
    alternate_third_name character varying(255),
    size integer
);


ALTER TABLE public.om_patient OWNER TO gazelle;

--
-- Name: om_patient_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_patient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_patient_id_seq OWNER TO gazelle;

--
-- Name: om_patient_identifier; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_patient_identifier (
    id integer NOT NULL,
    identifier character varying(255),
    identifier_type_code character varying(255),
    patient_id integer
);


ALTER TABLE public.om_patient_identifier OWNER TO gazelle;

--
-- Name: om_patient_identifier_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_patient_identifier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_patient_identifier_id_seq OWNER TO gazelle;

--
-- Name: om_protocol_item; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_protocol_item (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    coding_scheme character varying(255),
    contrast_agent_code character varying(255),
    protocol_code character varying(255),
    protocol_code_meaning character varying(255),
    domain_id integer,
    simulated_actor_id integer,
    scheduled_procedure_step_id integer
);


ALTER TABLE public.om_protocol_item OWNER TO gazelle;

--
-- Name: om_protocol_item_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_protocol_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_protocol_item_id_seq OWNER TO gazelle;

--
-- Name: om_proxy_connection; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_proxy_connection (
    id integer NOT NULL,
    uuid character varying(255)
);


ALTER TABLE public.om_proxy_connection OWNER TO gazelle;

--
-- Name: om_requested_procedure; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_requested_procedure (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    admitting_diagnostic_code character varying(255),
    code character varying(255),
    code_meaning character varying(255),
    coding_scheme character varying(255),
    description character varying(255),
    order_control_code character varying(255),
    order_status character varying(255),
    requested_procedure_id character varying(255),
    study_instance_uid character varying(255),
    domain_id integer,
    simulated_actor_id integer,
    order_id integer,
    reason_for_procedure character varying(255)
);


ALTER TABLE public.om_requested_procedure OWNER TO gazelle;

--
-- Name: om_requested_procedure_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_requested_procedure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_requested_procedure_id_seq OWNER TO gazelle;

--
-- Name: om_scheduled_procedure_step; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_scheduled_procedure_step (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    modality character varying(255),
    scheduled_procedure_id character varying(255),
    sps_description character varying(255),
    sps_start_date_time timestamp without time zone,
    domain_id integer,
    simulated_actor_id integer,
    requested_procedure_id integer
);


ALTER TABLE public.om_scheduled_procedure_step OWNER TO gazelle;

--
-- Name: om_scheduled_procedure_step_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_scheduled_procedure_step_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_scheduled_procedure_step_id_seq OWNER TO gazelle;

--
-- Name: om_scp_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_scp_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_scp_configuration_id_seq OWNER TO gazelle;

--
-- Name: om_scp_logger_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_scp_logger_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_scp_logger_id_seq OWNER TO gazelle;

--
-- Name: om_specimen; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_specimen (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    available boolean,
    collection_date_time timestamp without time zone,
    collection_method character varying(255),
    container_type character varying(255),
    filler_assigned_identifier character varying(255),
    grouped_specimen_count integer,
    is_main_entity boolean,
    placer_assigned_identifier character varying(255),
    received_date_time timestamp without time zone,
    reject_reason character varying(255),
    risk_code character varying(255),
    role character varying(255),
    source_site character varying(255),
    source_site_modifier character varying(255),
    type character varying(255),
    work_order boolean,
    domain_id integer,
    simulated_actor_id integer
);


ALTER TABLE public.om_specimen OWNER TO gazelle;

--
-- Name: om_specimen_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_specimen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_specimen_id_seq OWNER TO gazelle;

--
-- Name: om_tele_radiology_order; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_tele_radiology_order (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    call_back_phone_number character varying(255),
    danger_code character varying(255),
    entered_by character varying(255),
    entering_organization character varying(255),
    filler_order_number character varying(255),
    order_control_code character varying(255),
    order_result_status character varying(255),
    order_status character varying(255),
    ordering_facility_name character varying(255),
    ordering_provider character varying(255),
    placer_group_number character varying(255),
    placer_order_number character varying(255),
    quantity_timing character varying(255),
    reason_for_study character varying(255),
    relevant_clinical_info character varying(255),
    start_date_time timestamp without time zone,
    technician character varying(255),
    transaction_date timestamp without time zone,
    universal_service_id character varying(255),
    abort_reason character varying(255),
    comment character varying(255),
    imaging_study_source character varying(255),
    procedure_code character varying(255),
    procedure_description character varying(255),
    views character varying(255),
    domain_id integer,
    simulated_actor_id integer,
    encounter_id integer
);


ALTER TABLE public.om_tele_radiology_order OWNER TO gazelle;

--
-- Name: om_tele_radiology_order_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_tele_radiology_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_tele_radiology_order_id_seq OWNER TO gazelle;

--
-- Name: om_tele_radiology_order_relatedstudies; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_tele_radiology_order_relatedstudies (
    om_tele_radiology_order_id integer NOT NULL,
    element character varying(255),
    accession_identifier integer NOT NULL
);


ALTER TABLE public.om_tele_radiology_order_relatedstudies OWNER TO gazelle;

--
-- Name: om_tele_radiology_procedure; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_tele_radiology_procedure (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    accession_identifier character varying(255),
    modality character varying(255),
    protocol_code character varying(255),
    requested_procedure_id character varying(255),
    study_instance_uid character varying(255),
    domain_id integer,
    simulated_actor_id integer,
    tele_radiology_order_id integer
);


ALTER TABLE public.om_tele_radiology_procedure OWNER TO gazelle;

--
-- Name: om_tele_radiology_procedure_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_tele_radiology_procedure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_tele_radiology_procedure_id_seq OWNER TO gazelle;

--
-- Name: om_user_preferences; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_user_preferences (
    id integer NOT NULL,
    ae_title character varying(255),
    last_login timestamp without time zone,
    username character varying(255),
    view_only_my_objects boolean,
    domain_id integer,
    integration_profile_id integer,
    hl7v251_option boolean,
    system_configuration_id integer
);


ALTER TABLE public.om_user_preferences OWNER TO gazelle;

--
-- Name: om_user_preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_user_preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_user_preferences_id_seq OWNER TO gazelle;

--
-- Name: om_worklist_entry; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_worklist_entry (
    id integer NOT NULL,
    creator character varying(255),
    last_changed timestamp without time zone,
    is_deleted boolean,
    file_path character varying(255),
    station_ae_title character varying(255),
    domain_id integer,
    simulated_actor_id integer,
    scheduled_procedure_step_id integer
);


ALTER TABLE public.om_worklist_entry OWNER TO gazelle;

--
-- Name: om_worklist_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_worklist_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_worklist_entry_id_seq OWNER TO gazelle;

--
-- Name: om_worklist_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_worklist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_worklist_id_seq OWNER TO gazelle;

--
-- Name: om_wos_query_content; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_wos_query_content (
    id integer NOT NULL,
    carrier_id character varying(255),
    container_id character varying(255),
    location character varying(255),
    position_in_carrier character varying(255),
    position_in_tray character varying(255),
    primary_container_id character varying(255),
    query_type character varying(255),
    request character varying(255),
    "timestamp" timestamp without time zone,
    tray_id character varying(255)
);


ALTER TABLE public.om_wos_query_content OWNER TO gazelle;

--
-- Name: om_wos_query_content_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.om_wos_query_content_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.om_wos_query_content_id_seq OWNER TO gazelle;

--
-- Name: om_wos_query_orders; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.om_wos_query_orders (
    wos_query_content_id integer NOT NULL,
    wos_id integer NOT NULL
);


ALTER TABLE public.om_wos_query_orders OWNER TO gazelle;

--
-- Name: ort_validation_parameters_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.ort_validation_parameters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ort_validation_parameters_id_seq OWNER TO gazelle;

--
-- Name: package_name_for_profile_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.package_name_for_profile_oid (
    id integer NOT NULL,
    package_name character varying(255),
    profile_oid character varying(255)
);


ALTER TABLE public.package_name_for_profile_oid OWNER TO gazelle;

--
-- Name: package_name_for_profile_oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.package_name_for_profile_oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.package_name_for_profile_oid_id_seq OWNER TO gazelle;

--
-- Name: pat_patient; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pat_patient (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    alternate_first_name character varying(255),
    alternate_last_name character varying(255),
    alternate_mothers_maiden_name character varying(255),
    alternate_second_name character varying(255),
    alternate_third_name character varying(255),
    character_set character varying(255),
    country_code character varying(255),
    creation_date timestamp without time zone,
    date_of_birth timestamp without time zone,
    first_name character varying(255),
    gender_code character varying(255),
    last_name character varying(255),
    mother_maiden_name character varying(255),
    race_code character varying(255),
    religion_code character varying(255),
    second_name character varying(255),
    size integer,
    third_name character varying(255),
    weight integer,
    contrast_allergies character varying(255),
    creator character varying(255)
);


ALTER TABLE public.pat_patient OWNER TO gazelle;

--
-- Name: pat_patient_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pat_patient_address (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    address_line character varying(255),
    address_type integer,
    city character varying(255),
    country_code character varying(255),
    state character varying(255),
    street character varying(255),
    street_number character varying(255),
    zip_code character varying(255),
    is_main_address boolean,
    patient_id integer,
    insee_code VARCHAR(255)
);


ALTER TABLE public.pat_patient_address OWNER TO gazelle;

--
-- Name: pat_patient_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pat_patient_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pat_patient_address_id_seq OWNER TO gazelle;

--
-- Name: pat_patient_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pat_patient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pat_patient_id_seq OWNER TO gazelle;

--
-- Name: sys_conf_type_usages; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.sys_conf_type_usages (
    system_configuration_id integer NOT NULL,
    listusages_id integer NOT NULL
);


ALTER TABLE public.sys_conf_type_usages OWNER TO gazelle;

--
-- Name: system_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.system_configuration (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    is_available boolean,
    is_public boolean,
    name character varying(255),
    owner character varying(255),
    owner_company character varying(255),
    system_name character varying(255),
    url character varying(255),
    application character varying(255),
    facility character varying(255),
    hl7_protocol integer,
    ip_address character varying(255),
    message_encoding integer,
    port integer,
    private_ip boolean,
    ae_title character varying(255),
    host character varying(255),
    dicom_port integer,
    charset_id integer
);


ALTER TABLE public.system_configuration OWNER TO gazelle;

--
-- Name: system_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.system_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_configuration_id_seq OWNER TO gazelle;

--
-- Name: teleradiologyorder_relatedstudies; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.teleradiologyorder_relatedstudies (
    teleradiologyorder_id integer NOT NULL,
    relatedstudies character varying(255),
    accession_identifier integer NOT NULL
);


ALTER TABLE public.teleradiologyorder_relatedstudies OWNER TO gazelle;

--
-- Name: tf_actor; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128),
    can_act_as_responder boolean
);


ALTER TABLE public.tf_actor OWNER TO gazelle;

--
-- Name: tf_actor_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile (
    id integer NOT NULL,
    actor_id integer,
    integration_profile_id integer
);


ALTER TABLE public.tf_actor_integration_profile OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_option (
    id integer NOT NULL,
    actor_integration_profile_id integer NOT NULL,
    integration_profile_option_id integer
);


ALTER TABLE public.tf_actor_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_domain OWNER TO gazelle;

--
-- Name: tf_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_domain_integration_profiles; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_integration_profiles (
    domain_id integer NOT NULL,
    integration_profile_id integer NOT NULL
);


ALTER TABLE public.tf_domain_integration_profiles OWNER TO gazelle;

--
-- Name: tf_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile OWNER TO gazelle;

--
-- Name: tf_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_option (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_transaction OWNER TO gazelle;

--
-- Name: tf_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_id_seq OWNER TO gazelle;

--
-- Name: tm_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_oid (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    oid character varying(255) NOT NULL,
    system_id integer
);


ALTER TABLE public.tm_oid OWNER TO gazelle;

--
-- Name: tm_oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_oid_id_seq OWNER TO gazelle;

--
-- Name: tm_path; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_path (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    keyword character varying(255) NOT NULL,
    type character varying(255)
);


ALTER TABLE public.tm_path OWNER TO gazelle;

--
-- Name: tm_path_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_path_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_path_id_seq OWNER TO gazelle;

--
-- Name: uid_generator; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.uid_generator (
    id integer NOT NULL,
    last_index integer,
    root character varying(255),
    tag character varying(255),
    tag_keyword character varying(255)
);


ALTER TABLE public.uid_generator OWNER TO gazelle;

--
-- Name: uid_generator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.uid_generator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uid_generator_id_seq OWNER TO gazelle;

--
-- Name: usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usage_id_seq OWNER TO gazelle;

--
-- Name: usage_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usage_metadata (
    id integer NOT NULL,
    action character varying(255),
    keyword character varying(255),
    affinity_id integer,
    transaction_id integer
);


ALTER TABLE public.usage_metadata OWNER TO gazelle;

--
-- Name: user_account; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.user_account (
    id bigint NOT NULL,
    enabled boolean NOT NULL,
    password_hash character varying(255),
    username character varying(255) NOT NULL
);


ALTER TABLE public.user_account OWNER TO gazelle;

--
-- Name: user_account_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.user_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_account_id_seq OWNER TO gazelle;

--
-- Name: user_account_role; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.user_account_role (
    account_id bigint NOT NULL,
    member_of_role bigint NOT NULL
);


ALTER TABLE public.user_account_role OWNER TO gazelle;

--
-- Name: user_permission; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.user_permission (
    id bigint NOT NULL,
    action character varying(255),
    discriminator character varying(255),
    recipient character varying(255),
    target character varying(255)
);


ALTER TABLE public.user_permission OWNER TO gazelle;

--
-- Name: user_role; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.user_role (
    id bigint NOT NULL,
    conditional boolean NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_role OWNER TO gazelle;

--
-- Name: user_role_group; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.user_role_group (
    role_id bigint NOT NULL,
    member_of_role bigint NOT NULL
);


ALTER TABLE public.user_role_group OWNER TO gazelle;

--
-- Name: user_role_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_role_id_seq OWNER TO gazelle;

--
-- Name: validation_parameters; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.validation_parameters (
    id integer NOT NULL,
    java_package character varying(255),
    message_type character varying(255) NOT NULL,
    profile_oid character varying(255) NOT NULL,
    actor_id integer NOT NULL,
    domain_id integer,
    transaction_id integer NOT NULL,
    validator_type character varying(64)
);


ALTER TABLE public.validation_parameters OWNER TO gazelle;

--
-- Name: accession_number accession_number_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.accession_number
    ADD CONSTRAINT accession_number_pkey PRIMARY KEY (id);


--
-- Name: affinity_domain affinity_domain_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain
    ADD CONSTRAINT affinity_domain_keyword_key UNIQUE (keyword);


--
-- Name: affinity_domain affinity_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain
    ADD CONSTRAINT affinity_domain_pkey PRIMARY KEY (id);


--
-- Name: affinity_domain_transactions affinity_domain_transactions_affinity_domain_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT affinity_domain_transactions_affinity_domain_id_key UNIQUE (affinity_domain_id, transaction_id);


--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_configuration cfg_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_configuration
    ADD CONSTRAINT cfg_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scp_configuration cfg_dicom_scp_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT cfg_dicom_scp_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scu_configuration cfg_dicom_scu_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT cfg_dicom_scu_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_initiator_configuration cfg_hl7_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT cfg_hl7_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_responder_configuration cfg_hl7_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT cfg_hl7_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_initiator_configuration cfg_hl7_v3_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT cfg_hl7_v3_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_responder_configuration cfg_hl7_v3_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT cfg_hl7_v3_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_host cfg_host_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_host
    ADD CONSTRAINT cfg_host_pkey PRIMARY KEY (id);


--
-- Name: cfg_sop_class cfg_sop_class_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_sop_class
    ADD CONSTRAINT cfg_sop_class_pkey PRIMARY KEY (id);


--
-- Name: cfg_syslog_configuration cfg_syslog_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT cfg_syslog_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_web_service_configuration cfg_web_service_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT cfg_web_service_configuration_pkey PRIMARY KEY (id);


--
-- Name: cmn_company_details cmn_company_details_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_company_details
    ADD CONSTRAINT cmn_company_details_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_iso3_language_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_iso3_language_key UNIQUE (iso3_language);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: cmn_ip_address cmn_ip_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_ip_address
    ADD CONSTRAINT cmn_ip_address_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance_metadata cmn_message_instance_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance_metadata
    ADD CONSTRAINT cmn_message_instance_metadata_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance cmn_message_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance
    ADD CONSTRAINT cmn_message_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_receiver_console cmn_receiver_console_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT cmn_receiver_console_pkey PRIMARY KEY (id);


--
-- Name: cmn_transaction_instance cmn_transaction_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT cmn_transaction_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_validator_usage cmn_validator_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_validator_usage
    ADD CONSTRAINT cmn_validator_usage_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information_instance gs_contextual_information_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT gs_contextual_information_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information gs_contextual_information_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information
    ADD CONSTRAINT gs_contextual_information_pkey PRIMARY KEY (id);


--
-- Name: gs_message gs_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT gs_message_pkey PRIMARY KEY (id);


--
-- Name: gs_system gs_system_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_system
    ADD CONSTRAINT gs_system_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_server_aipo_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_server_aipo_id_key UNIQUE (server_aipo_id, server_test_instance_participants_id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_server_test_instance_particip_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_server_test_instance_particip_key UNIQUE (server_test_instance_participants_id);


--
-- Name: gs_test_instance gs_test_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT gs_test_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance gs_test_instance_server_test_instance_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT gs_test_instance_server_test_instance_id_key UNIQUE (server_test_instance_id);


--
-- Name: gs_test_instance_status gs_test_instance_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_status
    ADD CONSTRAINT gs_test_instance_status_pkey PRIMARY KEY (id);


--
-- Name: gs_test_steps_instance gs_test_steps_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT gs_test_steps_instance_pkey PRIMARY KEY (id);


--
-- Name: hl7_charset hl7_charset_display_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_charset
    ADD CONSTRAINT hl7_charset_display_name_key UNIQUE (display_name);


--
-- Name: hl7_charset hl7_charset_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_charset
    ADD CONSTRAINT hl7_charset_pkey PRIMARY KEY (id);


--
-- Name: hl7_simulator_responder_configuration hl7_simulator_responder_configuration_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT hl7_simulator_responder_configuration_name_key UNIQUE (name);


--
-- Name: hl7_simulator_responder_configuration hl7_simulator_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT hl7_simulator_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: hl7_validation_parameters_affinity_domain hl7_validation_parameters_affinity_validation_parameters_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_validation_parameters_affinity_domain
    ADD CONSTRAINT hl7_validation_parameters_affinity_validation_parameters_id_key UNIQUE (validation_parameters_id, affinity_domain_id);


--
-- Name: om_appointment om_appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_appointment
    ADD CONSTRAINT om_appointment_pkey PRIMARY KEY (id);


--
-- Name: om_appointment_resource om_appointment_resource_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_appointment_resource
    ADD CONSTRAINT om_appointment_resource_pkey PRIMARY KEY (id);


--
-- Name: om_command_field_values om_command_field_values_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_command_field_values
    ADD CONSTRAINT om_command_field_values_pkey PRIMARY KEY (id);


--
-- Name: om_container om_container_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_container
    ADD CONSTRAINT om_container_pkey PRIMARY KEY (id);


--
-- Name: om_dicom_message om_dicom_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_dicom_message
    ADD CONSTRAINT om_dicom_message_pkey PRIMARY KEY (id);


--
-- Name: om_dicom_uid_values om_dicom_uid_values_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_dicom_uid_values
    ADD CONSTRAINT om_dicom_uid_values_pkey PRIMARY KEY (id);


--
-- Name: om_dicom_validation_result om_dicom_validation_result_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_dicom_validation_result
    ADD CONSTRAINT om_dicom_validation_result_pkey PRIMARY KEY (id);


--
-- Name: om_encounter om_encounter_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_encounter
    ADD CONSTRAINT om_encounter_pkey PRIMARY KEY (id);


--
-- Name: om_lab_order_parent om_lab_order_parent_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order_parent
    ADD CONSTRAINT om_lab_order_parent_pkey PRIMARY KEY (om_lab_order_id, parent_id);


--
-- Name: om_lab_order om_lab_order_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order
    ADD CONSTRAINT om_lab_order_pkey PRIMARY KEY (id);


--
-- Name: om_lab_order_specimen om_lab_order_specimen_specimen_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order_specimen
    ADD CONSTRAINT om_lab_order_specimen_specimen_id_key UNIQUE (specimen_id, lab_order_id);


--
-- Name: om_modality_code om_modality_code_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_modality_code
    ADD CONSTRAINT om_modality_code_pkey PRIMARY KEY (id);


--
-- Name: om_note_and_comment om_note_and_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_note_and_comment
    ADD CONSTRAINT om_note_and_comment_pkey PRIMARY KEY (id);


--
-- Name: om_number_generator om_number_generator_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_number_generator
    ADD CONSTRAINT om_number_generator_pkey PRIMARY KEY (id);


--
-- Name: om_observation om_observation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_observation
    ADD CONSTRAINT om_observation_pkey PRIMARY KEY (id);


--
-- Name: om_order om_order_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_order
    ADD CONSTRAINT om_order_pkey PRIMARY KEY (id);


--
-- Name: om_patient_identifier om_patient_identifier_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_patient_identifier
    ADD CONSTRAINT om_patient_identifier_pkey PRIMARY KEY (id);


--
-- Name: om_patient om_patient_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_patient
    ADD CONSTRAINT om_patient_pkey PRIMARY KEY (id);


--
-- Name: om_protocol_item om_protocol_item_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_protocol_item
    ADD CONSTRAINT om_protocol_item_pkey PRIMARY KEY (id);


--
-- Name: om_proxy_connection om_proxy_connection_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_proxy_connection
    ADD CONSTRAINT om_proxy_connection_pkey PRIMARY KEY (id);


--
-- Name: om_requested_procedure om_requested_procedure_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_requested_procedure
    ADD CONSTRAINT om_requested_procedure_pkey PRIMARY KEY (id);


--
-- Name: om_scheduled_procedure_step om_scheduled_procedure_step_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_scheduled_procedure_step
    ADD CONSTRAINT om_scheduled_procedure_step_pkey PRIMARY KEY (id);


--
-- Name: om_specimen om_specimen_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_specimen
    ADD CONSTRAINT om_specimen_pkey PRIMARY KEY (id);


--
-- Name: om_tele_radiology_order om_tele_radiology_order_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_order
    ADD CONSTRAINT om_tele_radiology_order_pkey PRIMARY KEY (id);


--
-- Name: om_tele_radiology_order_relatedstudies om_tele_radiology_order_relatedstudies_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_order_relatedstudies
    ADD CONSTRAINT om_tele_radiology_order_relatedstudies_pkey PRIMARY KEY (om_tele_radiology_order_id, accession_identifier);


--
-- Name: om_tele_radiology_procedure om_tele_radiology_procedure_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_procedure
    ADD CONSTRAINT om_tele_radiology_procedure_pkey PRIMARY KEY (id);


--
-- Name: cmn_value_set om_value_set_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_value_set
    ADD CONSTRAINT om_value_set_pkey PRIMARY KEY (id);


--
-- Name: om_worklist_entry om_worklist_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_worklist_entry
    ADD CONSTRAINT om_worklist_entry_pkey PRIMARY KEY (id);


--
-- Name: om_wos_query_content om_wos_query_content_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_wos_query_content
    ADD CONSTRAINT om_wos_query_content_pkey PRIMARY KEY (id);


--
-- Name: om_wos_query_orders om_wos_query_orders_wos_query_content_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_wos_query_orders
    ADD CONSTRAINT om_wos_query_orders_wos_query_content_id_key UNIQUE (wos_query_content_id, wos_id);


--
-- Name: package_name_for_profile_oid package_name_for_profile_oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.package_name_for_profile_oid
    ADD CONSTRAINT package_name_for_profile_oid_pkey PRIMARY KEY (id);


--
-- Name: pat_patient_address pat_patient_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient_address
    ADD CONSTRAINT pat_patient_address_pkey PRIMARY KEY (id);


--
-- Name: pat_patient pat_patient_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient
    ADD CONSTRAINT pat_patient_pkey PRIMARY KEY (id);


--
-- Name: system_configuration system_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.system_configuration
    ADD CONSTRAINT system_configuration_pkey PRIMARY KEY (id);


--
-- Name: teleradiologyorder_relatedstudies teleradiologyorder_relatedstudies_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.teleradiologyorder_relatedstudies
    ADD CONSTRAINT teleradiologyorder_relatedstudies_pkey PRIMARY KEY (teleradiologyorder_id, accession_identifier);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_actor_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_actor_id_key UNIQUE (actor_id, integration_profile_id);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile_o_actor_integration_profile_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile_o_actor_integration_profile_id_key UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_actor tf_actor_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT tf_actor_keyword_key UNIQUE (keyword);


--
-- Name: tf_actor tf_actor_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT tf_actor_pkey PRIMARY KEY (id);


--
-- Name: tf_domain_integration_profiles tf_domain_integration_profiles_integration_profile_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT tf_domain_integration_profiles_integration_profile_id_key UNIQUE (integration_profile_id, domain_id);


--
-- Name: tf_domain tf_domain_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT tf_domain_keyword_key UNIQUE (keyword);


--
-- Name: tf_domain tf_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT tf_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_keyword_key UNIQUE (keyword);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_keyword_key UNIQUE (keyword);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction tf_transaction_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT tf_transaction_keyword_key UNIQUE (keyword);


--
-- Name: tf_transaction tf_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT tf_transaction_pkey PRIMARY KEY (id);


--
-- Name: tm_oid tm_oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid
    ADD CONSTRAINT tm_oid_pkey PRIMARY KEY (id);


--
-- Name: tm_path tm_path_keyword_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path
    ADD CONSTRAINT tm_path_keyword_key UNIQUE (keyword);


--
-- Name: tm_path tm_path_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path
    ADD CONSTRAINT tm_path_pkey PRIMARY KEY (id);


--
-- Name: uid_generator uid_generator_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.uid_generator
    ADD CONSTRAINT uid_generator_pkey PRIMARY KEY (id);


--
-- Name: app_configuration uk_20rnkdjn5jvlvmsb5f7io4b1o; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT uk_20rnkdjn5jvlvmsb5f7io4b1o UNIQUE (variable);


--
-- Name: system_configuration uk_2iwxlu65fuwpbhmeg96ebawhw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.system_configuration
    ADD CONSTRAINT uk_2iwxlu65fuwpbhmeg96ebawhw UNIQUE (name);


--
-- Name: tf_domain uk_436tct1jl8811q2xgd8bjth9q; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT uk_436tct1jl8811q2xgd8bjth9q UNIQUE (keyword);


--
-- Name: tf_transaction uk_6nt35dm69gbrrj0aegkkqalr2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT uk_6nt35dm69gbrrj0aegkkqalr2 UNIQUE (keyword);


--
-- Name: gs_test_instance_participants uk_7rrch4mgjwqsbug2p4fgu9nwm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT uk_7rrch4mgjwqsbug2p4fgu9nwm UNIQUE (server_aipo_id, server_test_instance_participants_id);


--
-- Name: tf_actor_integration_profile_option uk_8b4tb6bcp3eh7mtsucokgh7fx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT uk_8b4tb6bcp3eh7mtsucokgh7fx UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: tm_path uk_8ufv5mgjsoifbtbq28b64r87s; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path
    ADD CONSTRAINT uk_8ufv5mgjsoifbtbq28b64r87s UNIQUE (keyword);


--
-- Name: tf_actor_integration_profile uk_b6ejd87o8v27xinqw27nn1hss; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT uk_b6ejd87o8v27xinqw27nn1hss UNIQUE (actor_id, integration_profile_id);


--
-- Name: om_lab_order_specimen uk_bogdupn96vjoes7so2r9eris8; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order_specimen
    ADD CONSTRAINT uk_bogdupn96vjoes7so2r9eris8 UNIQUE (specimen_id, lab_order_id);


--
-- Name: hl7_charset uk_cr8csf9v305hy0glwmdrbagpr; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_charset
    ADD CONSTRAINT uk_cr8csf9v305hy0glwmdrbagpr UNIQUE (display_name);


--
-- Name: tf_actor uk_fpb6vpa9bnw6cjsb1pucuuivw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT uk_fpb6vpa9bnw6cjsb1pucuuivw UNIQUE (keyword);


--
-- Name: affinity_domain uk_ia6h2gkv0i2omgnnsnwyqwnqs; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain
    ADD CONSTRAINT uk_ia6h2gkv0i2omgnnsnwyqwnqs UNIQUE (keyword);


--
-- Name: om_wos_query_orders uk_irk0pjt3amvmpqfwv5qsm1eux; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_wos_query_orders
    ADD CONSTRAINT uk_irk0pjt3amvmpqfwv5qsm1eux UNIQUE (wos_query_content_id, wos_id);


--
-- Name: tf_domain_integration_profiles uk_iw9qgddyj9xsshmek3gr6u588; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT uk_iw9qgddyj9xsshmek3gr6u588 UNIQUE (integration_profile_id, domain_id);


--
-- Name: om_dicom_message_om_dicom_validation_result uk_j57j8c0rima7vwpa2hm12518w; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_dicom_message_om_dicom_validation_result
    ADD CONSTRAINT uk_j57j8c0rima7vwpa2hm12518w UNIQUE (validationresults_id);


--
-- Name: gs_test_instance_participants uk_l1ffr6swiqyeuvjixaptkpsk9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT uk_l1ffr6swiqyeuvjixaptkpsk9 UNIQUE (server_test_instance_participants_id);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: tf_integration_profile uk_ny4glgtyovt2w045nwct19arx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT uk_ny4glgtyovt2w045nwct19arx UNIQUE (keyword);


--
-- Name: hl7_simulator_responder_configuration uk_ov92e0w7cimojbr4oaco0x7dh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT uk_ov92e0w7cimojbr4oaco0x7dh UNIQUE (name);


--
-- Name: gs_test_instance uk_qprd2o4wqtcw4eify2dmrvprn; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT uk_qprd2o4wqtcw4eify2dmrvprn UNIQUE (server_test_instance_id);


--
-- Name: affinity_domain_transactions uk_r4g9ud7n9b2gx7tia9engpipe; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT uk_r4g9ud7n9b2gx7tia9engpipe UNIQUE (affinity_domain_id, transaction_id);


--
-- Name: cmn_company_details uk_r6osh086b3nqbuxpswcp1hcc2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_company_details
    ADD CONSTRAINT uk_r6osh086b3nqbuxpswcp1hcc2 UNIQUE (company_keyword);


--
-- Name: tf_integration_profile_option uk_thqc1vykug4qhn8jdij04d1bh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT uk_thqc1vykug4qhn8jdij04d1bh UNIQUE (keyword);


--
-- Name: usage_metadata usage_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT usage_metadata_pkey PRIMARY KEY (id);


--
-- Name: user_account user_account_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.user_account
    ADD CONSTRAINT user_account_pkey PRIMARY KEY (id);


--
-- Name: user_account_role user_account_role_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.user_account_role
    ADD CONSTRAINT user_account_role_pkey PRIMARY KEY (account_id, member_of_role);


--
-- Name: user_account user_account_username_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.user_account
    ADD CONSTRAINT user_account_username_key UNIQUE (username);


--
-- Name: user_permission user_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.user_permission
    ADD CONSTRAINT user_permission_pkey PRIMARY KEY (id);


--
-- Name: user_role_group user_role_group_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.user_role_group
    ADD CONSTRAINT user_role_group_pkey PRIMARY KEY (role_id, member_of_role);


--
-- Name: user_role user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (id);


--
-- Name: validation_parameters validation_parameters_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_parameters
    ADD CONSTRAINT validation_parameters_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance_issuing_actor_type_issuer_idx; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX cmn_message_instance_issuing_actor_type_issuer_idx ON public.cmn_message_instance USING btree (issuing_actor, type, issuer);


--
-- Name: cmn_transaction_instance_request_id_response_id_domain_id_t_idx; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX cmn_transaction_instance_request_id_response_id_domain_id_t_idx ON public.cmn_transaction_instance USING btree (request_id, response_id, domain_id, transaction_id, "timestamp", simulated_actor_id, is_visible);


--
-- Name: om_encounter_visit_number_patient_id_creation_date_creator_idx; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX om_encounter_visit_number_patient_id_creation_date_creator_idx ON public.om_encounter USING btree (visit_number, patient_id, creation_date, creator);


--
-- Name: om_lab_order_filler_order_number_placer_order_number_placer_idx; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX om_lab_order_filler_order_number_placer_order_number_placer_idx ON public.om_lab_order USING btree (filler_order_number, placer_order_number, placer_group_number, encounter_id, creator, last_changed, order_control_code, domain_id, simulated_actor_id, container_id);


--
-- Name: om_lab_order_specimen_lab_order_id_specimen_id_idx; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX om_lab_order_specimen_lab_order_id_specimen_id_idx ON public.om_lab_order_specimen USING btree (lab_order_id, specimen_id);


--
-- Name: om_observation_specimen_id_order_id_simulated_actor_id_doma_idx; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX om_observation_specimen_id_order_id_simulated_actor_id_doma_idx ON public.om_observation USING btree (specimen_id, order_id, simulated_actor_id, domain_id);


--
-- Name: om_order_filler_order_number_placer_order_number_placer_gro_idx; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX om_order_filler_order_number_placer_order_number_placer_gro_idx ON public.om_order USING btree (filler_order_number, placer_order_number, placer_group_number, encounter_id, creator, last_changed, order_control_code, domain_id, simulated_actor_id);


--
-- Name: om_patient_first_name_last_name_gender_code_creation_date_c_idx; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX om_patient_first_name_last_name_gender_code_creation_date_c_idx ON public.om_patient USING btree (first_name, last_name, gender_code, creation_date, creator);


--
-- Name: om_patient_identifier_patient_id_identifier_idx; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX om_patient_identifier_patient_id_identifier_idx ON public.om_patient_identifier USING btree (patient_id, identifier);


--
-- Name: om_requested_procedure_order_id_last_changed_creator_simula_idx; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX om_requested_procedure_order_id_last_changed_creator_simula_idx ON public.om_requested_procedure USING btree (order_id, last_changed, creator, simulated_actor_id, domain_id, study_instance_uid);


--
-- Name: om_scheduled_procedure_step_requested_procedure_id_creator__idx; Type: INDEX; Schema: public; Owner: gazelle
--

CREATE INDEX om_scheduled_procedure_step_requested_procedure_id_creator__idx ON public.om_scheduled_procedure_step USING btree (requested_procedure_id, creator, last_changed, scheduled_procedure_id, domain_id, simulated_actor_id);


--
-- Name: om_note_and_comment fk101c076b1b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_note_and_comment
    ADD CONSTRAINT fk101c076b1b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_note_and_comment fk101c076b64e2e06d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_note_and_comment
    ADD CONSTRAINT fk101c076b64e2e06d FOREIGN KEY (observation_id) REFERENCES public.om_observation(id);


--
-- Name: om_note_and_comment fk101c076b72ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_note_and_comment
    ADD CONSTRAINT fk101c076b72ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_requested_procedure fk123523611b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_requested_procedure
    ADD CONSTRAINT fk123523611b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_requested_procedure fk123523614f6f576d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_requested_procedure
    ADD CONSTRAINT fk123523614f6f576d FOREIGN KEY (order_id) REFERENCES public.om_order(id);


--
-- Name: om_requested_procedure fk1235236172ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_requested_procedure
    ADD CONSTRAINT fk1235236172ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_specimen fk18b2e0491b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_specimen
    ADD CONSTRAINT fk18b2e0491b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_specimen fk18b2e04972ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_specimen
    ADD CONSTRAINT fk18b2e04972ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: gs_test_instance_oid fk200bd51aadaef596; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_oid
    ADD CONSTRAINT fk200bd51aadaef596 FOREIGN KEY (test_instance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gs_test_instance_oid fk200bd51afb5a2c1c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_oid
    ADD CONSTRAINT fk200bd51afb5a2c1c FOREIGN KEY (oid_configuration_id) REFERENCES public.tm_oid(id);


--
-- Name: cfg_web_service_configuration fk23f4a6263927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT fk23f4a6263927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_web_service_configuration fk23f4a626511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT fk23f4a626511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: cfg_hl7_initiator_configuration fk2891f8ff3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk2891f8ff3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_initiator_configuration fk2891f8ff511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk2891f8ff511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: tf_domain_integration_profiles fk2c03ea431b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT fk2c03ea431b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tf_domain_integration_profiles fk2c03ea43866df480; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT fk2c03ea43866df480 FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: om_lab_order fk346938fb1b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order
    ADD CONSTRAINT fk346938fb1b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_lab_order fk346938fb22a0f50d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order
    ADD CONSTRAINT fk346938fb22a0f50d FOREIGN KEY (container_id) REFERENCES public.om_container(id);


--
-- Name: om_lab_order fk346938fb72ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order
    ADD CONSTRAINT fk346938fb72ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_lab_order fk346938fbdc4c5fcd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order
    ADD CONSTRAINT fk346938fbdc4c5fcd FOREIGN KEY (encounter_id) REFERENCES public.om_encounter(id);


--
-- Name: om_lab_order_parent fk3529cc6eda3aaf4d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order_parent
    ADD CONSTRAINT fk3529cc6eda3aaf4d FOREIGN KEY (om_lab_order_id) REFERENCES public.om_lab_order(id);


--
-- Name: om_order fk391799ad1b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_order
    ADD CONSTRAINT fk391799ad1b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_order fk391799ad72ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_order
    ADD CONSTRAINT fk391799ad72ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_order fk391799addc4c5fcd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_order
    ADD CONSTRAINT fk391799addc4c5fcd FOREIGN KEY (encounter_id) REFERENCES public.om_encounter(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk484bb2fe3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk484bb2fe3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk484bb2fe511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk484bb2fe511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: om_container fk4a2917601b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_container
    ADD CONSTRAINT fk4a2917601b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_container fk4a29176072ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_container
    ADD CONSTRAINT fk4a29176072ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_container fk4a2917609058ab67; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_container
    ADD CONSTRAINT fk4a2917609058ab67 FOREIGN KEY (specimen_id) REFERENCES public.om_specimen(id);


--
-- Name: gs_contextual_information fk4a7365f13b128c1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information
    ADD CONSTRAINT fk4a7365f13b128c1 FOREIGN KEY (path) REFERENCES public.tm_path(id);


--
-- Name: om_number_generator fk54dcb59e9d1084ab; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_number_generator
    ADD CONSTRAINT fk54dcb59e9d1084ab FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: gs_test_instance_participants fk5c650a50611bae11; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT fk5c650a50611bae11 FOREIGN KEY (system_id) REFERENCES public.gs_system(id);


--
-- Name: gs_test_instance_participants fk5c650a5083369963; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT fk5c650a5083369963 FOREIGN KEY (aipo_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: om_observation fk5f2b674b1b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_observation
    ADD CONSTRAINT fk5f2b674b1b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_observation fk5f2b674b2ed5c1a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_observation
    ADD CONSTRAINT fk5f2b674b2ed5c1a FOREIGN KEY (order_id) REFERENCES public.om_lab_order(id);


--
-- Name: om_observation fk5f2b674b72ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_observation
    ADD CONSTRAINT fk5f2b674b72ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_observation fk5f2b674b9058ab67; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_observation
    ADD CONSTRAINT fk5f2b674b9058ab67 FOREIGN KEY (specimen_id) REFERENCES public.om_specimen(id);


--
-- Name: validation_parameters fk61e8f9701b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_parameters
    ADD CONSTRAINT fk61e8f9701b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: validation_parameters fk61e8f9709d1084ab; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_parameters
    ADD CONSTRAINT fk61e8f9709d1084ab FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: validation_parameters fk61e8f970bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_parameters
    ADD CONSTRAINT fk61e8f970bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde4f3bdb32; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde4f3bdb32 FOREIGN KEY (sop_class_id) REFERENCES public.cfg_sop_class(id);


--
-- Name: cfg_dicom_scu_configuration fk62aeccde511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk62aeccde511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: om_tele_radiology_procedure fk7717f8b01b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_procedure
    ADD CONSTRAINT fk7717f8b01b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_tele_radiology_procedure fk7717f8b072ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_procedure
    ADD CONSTRAINT fk7717f8b072ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_tele_radiology_procedure fk7717f8b0cc357345; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_procedure
    ADD CONSTRAINT fk7717f8b0cc357345 FOREIGN KEY (tele_radiology_order_id) REFERENCES public.om_tele_radiology_order(id);


--
-- Name: tf_actor_integration_profile_option fk78115a4d698ea7bd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk78115a4d698ea7bd FOREIGN KEY (integration_profile_option_id) REFERENCES public.tf_integration_profile_option(id);


--
-- Name: tf_actor_integration_profile_option fk78115a4d72619921; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk78115a4d72619921 FOREIGN KEY (actor_integration_profile_id) REFERENCES public.tf_actor_integration_profile(id);


--
-- Name: usage_metadata fk7d18f40d7007d3ad; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT fk7d18f40d7007d3ad FOREIGN KEY (affinity_id) REFERENCES public.affinity_domain(id);


--
-- Name: usage_metadata fk7d18f40dbd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT fk7d18f40dbd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: cfg_configuration fk7d98485b9a8e05a9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_configuration
    ADD CONSTRAINT fk7d98485b9a8e05a9 FOREIGN KEY (host_id) REFERENCES public.cfg_host(id);


--
-- Name: om_appointment_resource fk84a4406f7464478d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_appointment_resource
    ADD CONSTRAINT fk84a4406f7464478d FOREIGN KEY (appointment_id) REFERENCES public.om_appointment(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk94b8666b3927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk94b8666b3927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk94b8666b511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk94b8666b511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: om_worklist_entry fk9830bbe31b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_worklist_entry
    ADD CONSTRAINT fk9830bbe31b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_worklist_entry fk9830bbe32d22976f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_worklist_entry
    ADD CONSTRAINT fk9830bbe32d22976f FOREIGN KEY (scheduled_procedure_step_id) REFERENCES public.om_scheduled_procedure_step(id);


--
-- Name: om_worklist_entry fk9830bbe372ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_worklist_entry
    ADD CONSTRAINT fk9830bbe372ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_note_and_comment fk_10cusmfua301595l5taeaoti; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_note_and_comment
    ADD CONSTRAINT fk_10cusmfua301595l5taeaoti FOREIGN KEY (observation_id) REFERENCES public.om_observation(id);


--
-- Name: teleradiologyorder_relatedstudies fk_11oq3rhbmwq199jildb1gmoqq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.teleradiologyorder_relatedstudies
    ADD CONSTRAINT fk_11oq3rhbmwq199jildb1gmoqq FOREIGN KEY (teleradiologyorder_id) REFERENCES public.om_tele_radiology_order(id);


--
-- Name: om_tele_radiology_order fk_1n2pcq29lry5y2f5d007c5wgy; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_order
    ADD CONSTRAINT fk_1n2pcq29lry5y2f5d007c5wgy FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cfg_dicom_scu_configuration fk_1uyssmhimn7bgvrpbosg76akr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk_1uyssmhimn7bgvrpbosg76akr FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: om_observation fk_24jt6y4fdjmo6gnbp9ara61qn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_observation
    ADD CONSTRAINT fk_24jt6y4fdjmo6gnbp9ara61qn FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: gs_test_instance_participants fk_2721m2mcive6uurmopwl26osu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT fk_2721m2mcive6uurmopwl26osu FOREIGN KEY (aipo_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: om_tele_radiology_procedure fk_2i34qahewbpbegnrocs4u6ifw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_procedure
    ADD CONSTRAINT fk_2i34qahewbpbegnrocs4u6ifw FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: gs_test_instance_test_instance_participants fk_2k4qd5vu7dd0i5qim72ixbkqw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_test_instance_participants
    ADD CONSTRAINT fk_2k4qd5vu7dd0i5qim72ixbkqw FOREIGN KEY (test_instance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: om_appointment fk_2keyxmpx2254h30mo3tv8l0fv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_appointment
    ADD CONSTRAINT fk_2keyxmpx2254h30mo3tv8l0fv FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_specimen fk_2lsbp7gxwxky0wa2bavef8nkv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_specimen
    ADD CONSTRAINT fk_2lsbp7gxwxky0wa2bavef8nkv FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_scheduled_procedure_step fk_2snxa672llqqscqajdhlik616; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_scheduled_procedure_step
    ADD CONSTRAINT fk_2snxa672llqqscqajdhlik616 FOREIGN KEY (requested_procedure_id) REFERENCES public.om_requested_procedure(id);


--
-- Name: gs_test_instance_test_instance_participants fk_2suwyu48vy0hed31ggom84fmu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_test_instance_participants
    ADD CONSTRAINT fk_2suwyu48vy0hed31ggom84fmu FOREIGN KEY (test_instance_participants_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: om_observation_flag fk_2ukh1vfcpy6amqo8roeysngan; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_observation_flag
    ADD CONSTRAINT fk_2ukh1vfcpy6amqo8roeysngan FOREIGN KEY (abnormal_flag_id) REFERENCES public.om_observation(id);


--
-- Name: om_order_parent_id fk_3aqpqfhpmhd63ey31toecc36q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_order_parent_id
    ADD CONSTRAINT fk_3aqpqfhpmhd63ey31toecc36q FOREIGN KEY (om_parent_id) REFERENCES public.om_lab_order(id);


--
-- Name: affinity_domain_transactions fk_3oy78tmux2c99fvrqkegtiq6v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT fk_3oy78tmux2c99fvrqkegtiq6v FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_test_instance fk_3u278s3asb3u74gte7oems8hr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT fk_3u278s3asb3u74gte7oems8hr FOREIGN KEY (test_instance_status_id) REFERENCES public.gs_test_instance_status(id);


--
-- Name: cfg_hl7_responder_configuration fk_4f82i9wrql9oc6mnvetcy7is9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT fk_4f82i9wrql9oc6mnvetcy7is9 FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: sys_conf_type_usages fk_4j6febpxhud8qe1uabp31nl19; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_conf_type_usages
    ADD CONSTRAINT fk_4j6febpxhud8qe1uabp31nl19 FOREIGN KEY (system_configuration_id) REFERENCES public.system_configuration(id);


--
-- Name: gs_message fk_4lkwhox4s91pq6sjfftcajcn4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fk_4lkwhox4s91pq6sjfftcajcn4 FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: om_patient_identifier fk_4oj3lc5rb77psibcax4r9j16o; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_patient_identifier
    ADD CONSTRAINT fk_4oj3lc5rb77psibcax4r9j16o FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk_57xerkmwir86jmw899uw3ykpf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk_57xerkmwir86jmw899uw3ykpf FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk_5fhnf9kr45rd5m1gd9riur2ev; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk_5fhnf9kr45rd5m1gd9riur2ev FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: tf_actor_integration_profile fk_5ilf7sfvvixf9f51lmphnwyoa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fk_5ilf7sfvvixf9f51lmphnwyoa FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: om_scheduled_procedure_step fk_5ljdlodk31nk8c34jtovv3cya; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_scheduled_procedure_step
    ADD CONSTRAINT fk_5ljdlodk31nk8c34jtovv3cya FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: hl7_simulator_responder_configuration fk_5s19daeg7pjo57niothsvnglc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT fk_5s19daeg7pjo57niothsvnglc FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tf_actor_integration_profile fk_5yubxkv7cw5mqpv5u0axd4f1x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fk_5yubxkv7cw5mqpv5u0axd4f1x FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_tele_radiology_procedure fk_60hjra9hk9unxol6aaar58i51; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_procedure
    ADD CONSTRAINT fk_60hjra9hk9unxol6aaar58i51 FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: gs_test_steps_instance fk_60pebu4cydl5h0hj12nkoisje; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_60pebu4cydl5h0hj12nkoisje FOREIGN KEY (syslog_config_id) REFERENCES public.cfg_syslog_configuration(id);


--
-- Name: gs_message fk_60qoisu4eph1wty4x7e55co98; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fk_60qoisu4eph1wty4x7e55co98 FOREIGN KEY (test_instance_participants_receiver_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: om_observation fk_6ho3cgrqm7h7wkmrn5wudmw9j; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_observation
    ADD CONSTRAINT fk_6ho3cgrqm7h7wkmrn5wudmw9j FOREIGN KEY (specimen_id) REFERENCES public.om_specimen(id);


--
-- Name: cmn_message_instance_metadata fk_6pxpnca029j7ewvr7dus8q3sf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance_metadata
    ADD CONSTRAINT fk_6pxpnca029j7ewvr7dus8q3sf FOREIGN KEY (message_instance_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: cmn_receiver_console fk_6s2mr59uvd93xmp2dpkepfdvp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT fk_6s2mr59uvd93xmp2dpkepfdvp FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_protocol_item fk_6s8fixk13d0mkv5d7mhh7d6df; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_protocol_item
    ADD CONSTRAINT fk_6s8fixk13d0mkv5d7mhh7d6df FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: cmn_message_instance fk_6vrp0236251naw1qr1sk7y58r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance
    ADD CONSTRAINT fk_6vrp0236251naw1qr1sk7y58r FOREIGN KEY (issuing_actor) REFERENCES public.tf_actor(id);


--
-- Name: om_specimen fk_7kl8vsat9gui04geca8ugg1mv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_specimen
    ADD CONSTRAINT fk_7kl8vsat9gui04geca8ugg1mv FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: hl7_simulator_responder_configuration fk_845qmauc0ndc9ppogm3wdye0x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT fk_845qmauc0ndc9ppogm3wdye0x FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cmn_ip_address fk_8b3pqht8rw1vyiu1o0dm1ewtt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_ip_address
    ADD CONSTRAINT fk_8b3pqht8rw1vyiu1o0dm1ewtt FOREIGN KEY (company_details_id) REFERENCES public.cmn_company_details(id);


--
-- Name: gs_contextual_information fk_8j7rd6ceveqykc8jl4gufcl75; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information
    ADD CONSTRAINT fk_8j7rd6ceveqykc8jl4gufcl75 FOREIGN KEY (path) REFERENCES public.tm_path(id);


--
-- Name: om_dicom_message fk_8tm2ppayhdppn31g860cds8lw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_dicom_message
    ADD CONSTRAINT fk_8tm2ppayhdppn31g860cds8lw FOREIGN KEY (connection_id) REFERENCES public.om_proxy_connection(id);


--
-- Name: om_dicom_message_om_dicom_validation_result fk_91gthgphvc6td9o53dx07les1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_dicom_message_om_dicom_validation_result
    ADD CONSTRAINT fk_91gthgphvc6td9o53dx07les1 FOREIGN KEY (om_dicom_message_id) REFERENCES public.om_dicom_message(id);


--
-- Name: om_worklist_entry fk_9a3pdcbokk0n8l2iofas0t7ef; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_worklist_entry
    ADD CONSTRAINT fk_9a3pdcbokk0n8l2iofas0t7ef FOREIGN KEY (scheduled_procedure_step_id) REFERENCES public.om_scheduled_procedure_step(id);


--
-- Name: om_protocol_item fk_9m7ajknqnpooo27clnm36s5d0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_protocol_item
    ADD CONSTRAINT fk_9m7ajknqnpooo27clnm36s5d0 FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cfg_syslog_configuration fk_9n8k78nx4ygpn1aiv61gdhx8n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT fk_9n8k78nx4ygpn1aiv61gdhx8n FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: om_observation fk_9s2o2l6qi7jl1lq53nv396gtj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_observation
    ADD CONSTRAINT fk_9s2o2l6qi7jl1lq53nv396gtj FOREIGN KEY (order_id) REFERENCES public.om_lab_order(id);


--
-- Name: pat_patient_address fk_9unnhbo9qfbfu2c7hfexts89g; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pat_patient_address
    ADD CONSTRAINT fk_9unnhbo9qfbfu2c7hfexts89g FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


--
-- Name: cfg_hl7_initiator_configuration fk_9uygh1fl3hk66l9fhpnov62ov; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk_9uygh1fl3hk66l9fhpnov62ov FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: om_number_generator fk_a0nph399vvg3g398bpoi5cmml; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_number_generator
    ADD CONSTRAINT fk_a0nph399vvg3g398bpoi5cmml FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cfg_syslog_configuration fk_a5s6a6cfwata8c4iae37f30md; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT fk_a5s6a6cfwata8c4iae37f30md FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: om_appointment fk_ag9nollbfeve4d0cag0cgfthf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_appointment
    ADD CONSTRAINT fk_ag9nollbfeve4d0cag0cgfthf FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: cfg_dicom_scu_configuration fk_atmtmfff6dh3cfmgymfvuv32x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk_atmtmfff6dh3cfmgymfvuv32x FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: om_requested_procedure fk_aygt0icryyrkg6uewyp78q6l3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_requested_procedure
    ADD CONSTRAINT fk_aygt0icryyrkg6uewyp78q6l3 FOREIGN KEY (order_id) REFERENCES public.om_order(id);


--
-- Name: om_wos_query_orders fk_b3jksr50cm4pr1xaqc0bjlu58; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_wos_query_orders
    ADD CONSTRAINT fk_b3jksr50cm4pr1xaqc0bjlu58 FOREIGN KEY (wos_query_content_id) REFERENCES public.om_wos_query_content(id);


--
-- Name: cfg_dicom_scp_configuration fk_bb0i01umf0nkm6s0ntwf16jve; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fk_bb0i01umf0nkm6s0ntwf16jve FOREIGN KEY (sop_class_id) REFERENCES public.cfg_sop_class(id);


--
-- Name: om_worklist_entry fk_bh5rmj6ewcjpm6e9d58oo4b04; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_worklist_entry
    ADD CONSTRAINT fk_bh5rmj6ewcjpm6e9d58oo4b04 FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: gs_test_steps_instance fk_bq3udwuexl1gy795hpwru5dqe; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_bq3udwuexl1gy795hpwru5dqe FOREIGN KEY (dicom_scu_config_id) REFERENCES public.cfg_dicom_scu_configuration(id);


--
-- Name: om_appointment_resource fk_bw2k1mwgt77fnl10c2e14o54m; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_appointment_resource
    ADD CONSTRAINT fk_bw2k1mwgt77fnl10c2e14o54m FOREIGN KEY (appointment_id) REFERENCES public.om_appointment(id);


--
-- Name: cfg_web_service_configuration fk_cam5r7r5pfmnofleffgk0e32f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT fk_cam5r7r5pfmnofleffgk0e32f FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: cfg_hl7_responder_configuration fk_cnmpmi83qdgkf29ca5vp0nlk0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT fk_cnmpmi83qdgkf29ca5vp0nlk0 FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: system_configuration fk_d64t2lopq2p50cq65m2g4p4bw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.system_configuration
    ADD CONSTRAINT fk_d64t2lopq2p50cq65m2g4p4bw FOREIGN KEY (charset_id) REFERENCES public.hl7_charset(id);


--
-- Name: gs_test_steps_instance fk_d6l3ub5y7jlicc47udu6pc8gf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_d6l3ub5y7jlicc47udu6pc8gf FOREIGN KEY (hl7v2_responder_config_id) REFERENCES public.cfg_hl7_responder_configuration(id);


--
-- Name: gs_test_steps_instance fk_d7nna7jtx4je8ruwukta41upc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_d7nna7jtx4je8ruwukta41upc FOREIGN KEY (dicom_scp_config_id) REFERENCES public.cfg_dicom_scp_configuration(id);


--
-- Name: om_requested_procedure fk_dc877iw8dd00den0s4thv9nv7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_requested_procedure
    ADD CONSTRAINT fk_dc877iw8dd00den0s4thv9nv7 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tf_actor_integration_profile_option fk_dd4cllr6xpdtoxp5wnjv4i2jn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_dd4cllr6xpdtoxp5wnjv4i2jn FOREIGN KEY (actor_integration_profile_id) REFERENCES public.tf_actor_integration_profile(id);


--
-- Name: om_dicom_message fk_dibn3llg12dotufd4yu8u6cd7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_dicom_message
    ADD CONSTRAINT fk_dibn3llg12dotufd4yu8u6cd7 FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_domain_integration_profiles fk_ditnlab3yy7ipby4do2dl4k9v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT fk_ditnlab3yy7ipby4do2dl4k9v FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: cmn_receiver_console fk_dvjns4jx634sntdq34baohlny; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT fk_dvjns4jx634sntdq34baohlny FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: om_lab_order_specimen fk_e3wox7ldjlael8nos8h341rfj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order_specimen
    ADD CONSTRAINT fk_e3wox7ldjlael8nos8h341rfj FOREIGN KEY (specimen_id) REFERENCES public.om_specimen(id);


--
-- Name: gs_test_steps_instance fk_e47nbep6bi6div7m4h09j40yx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_e47nbep6bi6div7m4h09j40yx FOREIGN KEY (hl7v3_responder_config_id) REFERENCES public.cfg_hl7_v3_responder_configuration(id);


--
-- Name: om_protocol_item fk_e53ih7k4ncstv01590js5bnh1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_protocol_item
    ADD CONSTRAINT fk_e53ih7k4ncstv01590js5bnh1 FOREIGN KEY (scheduled_procedure_step_id) REFERENCES public.om_scheduled_procedure_step(id);


--
-- Name: om_encounter fk_e57flwppqy91426ponmcs8iib; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_encounter
    ADD CONSTRAINT fk_e57flwppqy91426ponmcs8iib FOREIGN KEY (patient_id) REFERENCES public.pat_patient(id);


--
-- Name: om_tele_radiology_procedure fk_e9kbgpkbyaaafi2j6wr1uawsf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_procedure
    ADD CONSTRAINT fk_e9kbgpkbyaaafi2j6wr1uawsf FOREIGN KEY (tele_radiology_order_id) REFERENCES public.om_tele_radiology_order(id);


--
-- Name: gs_message fk_f0ffk1767j7rom46uil2fojou; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fk_f0ffk1767j7rom46uil2fojou FOREIGN KEY (test_instance_participants_sender_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: om_wos_query_orders fk_fcn0t1m1m0aouy2g981awy1aq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_wos_query_orders
    ADD CONSTRAINT fk_fcn0t1m1m0aouy2g981awy1aq FOREIGN KEY (wos_id) REFERENCES public.om_lab_order(id);


--
-- Name: gs_test_instance_participants fk_g5b1negarlf7einpqd9dyxhy5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT fk_g5b1negarlf7einpqd9dyxhy5 FOREIGN KEY (system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_web_service_configuration fk_g9fr1fk5hwvt5qd0b151x68b9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT fk_g9fr1fk5hwvt5qd0b151x68b9 FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: gs_test_steps_instance fk_gci5txhtlwlkonbqjdfeue6k9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_gci5txhtlwlkonbqjdfeue6k9 FOREIGN KEY (web_service_config_id) REFERENCES public.cfg_web_service_configuration(id);


--
-- Name: cfg_dicom_scp_configuration fk_gmhl29xoaas6axade58e6h18y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fk_gmhl29xoaas6axade58e6h18y FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: usage_metadata fk_h6b2lqp7q4crn70h4noft57dm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT fk_h6b2lqp7q4crn70h4noft57dm FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tm_oid fk_hcooq91f0xb0dt3lw69f4mhrp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid
    ADD CONSTRAINT fk_hcooq91f0xb0dt3lw69f4mhrp FOREIGN KEY (system_id) REFERENCES public.gs_system(id);


--
-- Name: gs_test_instance_oid fk_he8jcif34crxu9bx6bjpf70ro; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_oid
    ADD CONSTRAINT fk_he8jcif34crxu9bx6bjpf70ro FOREIGN KEY (test_instance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gs_test_steps_instance fk_hhvsfxik1plfet5oenwrhmmsq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_hhvsfxik1plfet5oenwrhmmsq FOREIGN KEY (testinstance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: om_order fk_i0cagct2w93c4bksuqegd76ue; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_order
    ADD CONSTRAINT fk_i0cagct2w93c4bksuqegd76ue FOREIGN KEY (encounter_id) REFERENCES public.om_encounter(id);


--
-- Name: om_container fk_i2ofumets325n2n660hlcutgq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_container
    ADD CONSTRAINT fk_i2ofumets325n2n660hlcutgq FOREIGN KEY (specimen_id) REFERENCES public.om_specimen(id);


--
-- Name: tf_actor_integration_profile_option fk_ichumu56164vfu1j44qb0uos3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_ichumu56164vfu1j44qb0uos3 FOREIGN KEY (integration_profile_option_id) REFERENCES public.tf_integration_profile_option(id);


--
-- Name: om_tele_radiology_order fk_if1oq8xl02nkgv09d6eivv8vh; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_order
    ADD CONSTRAINT fk_if1oq8xl02nkgv09d6eivv8vh FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: gs_test_steps_instance fk_ijvev88dlp0g3wyawbewr0k98; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_ijvev88dlp0g3wyawbewr0k98 FOREIGN KEY (hl7v2_initiator_config_id) REFERENCES public.cfg_hl7_initiator_configuration(id);


--
-- Name: cmn_transaction_instance fk_iuc2f38isetr9m12flpqj049i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fk_iuc2f38isetr9m12flpqj049i FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tf_domain_integration_profiles fk_j3fpkysr19ckcu98bb7hbj8nu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT fk_j3fpkysr19ckcu98bb7hbj8nu FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: validation_parameters fk_j47d842lktk2q8ppb5vw49285; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_parameters
    ADD CONSTRAINT fk_j47d842lktk2q8ppb5vw49285 FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_test_instance_oid fk_j4ksfu8dbved4pd1jgcrtagqf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_oid
    ADD CONSTRAINT fk_j4ksfu8dbved4pd1jgcrtagqf FOREIGN KEY (oid_configuration_id) REFERENCES public.tm_oid(id);


--
-- Name: om_dicom_message_om_dicom_validation_result fk_j57j8c0rima7vwpa2hm12518w; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_dicom_message_om_dicom_validation_result
    ADD CONSTRAINT fk_j57j8c0rima7vwpa2hm12518w FOREIGN KEY (validationresults_id) REFERENCES public.om_dicom_validation_result(id);


--
-- Name: om_lab_order fk_j7hk25rd11ngeter7tjadimxw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order
    ADD CONSTRAINT fk_j7hk25rd11ngeter7tjadimxw FOREIGN KEY (encounter_id) REFERENCES public.om_encounter(id);


--
-- Name: om_appointment fk_j7nx12j2c8r6uyk3crtnb4u5p; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_appointment
    ADD CONSTRAINT fk_j7nx12j2c8r6uyk3crtnb4u5p FOREIGN KEY (order_id) REFERENCES public.om_order(id);


--
-- Name: om_lab_order fk_jpykgom9ohnay03evh3o8ao5g; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order
    ADD CONSTRAINT fk_jpykgom9ohnay03evh3o8ao5g FOREIGN KEY (container_id) REFERENCES public.om_container(id);


--
-- Name: om_container fk_k59bahc4em3tjdrr4ell36uqp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_container
    ADD CONSTRAINT fk_k59bahc4em3tjdrr4ell36uqp FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_dicom_validation_result fk_k636rfd2mttw5v2x1edpadu4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_dicom_validation_result
    ADD CONSTRAINT fk_k636rfd2mttw5v2x1edpadu4 FOREIGN KEY (dicom_message_id) REFERENCES public.om_dicom_message(id);


--
-- Name: om_dicom_message fk_k95gkem5ja15pric0nmigr4iu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_dicom_message
    ADD CONSTRAINT fk_k95gkem5ja15pric0nmigr4iu FOREIGN KEY (simulated_transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: om_worklist_entry fk_kemqk2tncer31w1a7oelkjgdy; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_worklist_entry
    ADD CONSTRAINT fk_kemqk2tncer31w1a7oelkjgdy FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: gs_test_steps_instance fk_knidtr35pirurpxk347rf9l6d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_knidtr35pirurpxk347rf9l6d FOREIGN KEY (hl7v3_initiator_config_id) REFERENCES public.cfg_hl7_v3_initiator_configuration(id);


--
-- Name: cmn_transaction_instance fk_kp300ev0h6s2ocpy5j8djco4v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fk_kp300ev0h6s2ocpy5j8djco4v FOREIGN KEY (request_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: om_note_and_comment fk_la09y3953tuqxt64d5yubb1if; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_note_and_comment
    ADD CONSTRAINT fk_la09y3953tuqxt64d5yubb1if FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cmn_transaction_instance fk_leqiasjr73fuy7ffhqmrhhsaa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fk_leqiasjr73fuy7ffhqmrhhsaa FOREIGN KEY (response_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: cfg_configuration fk_lpgg35tul90orrn8vvvtyb80r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_configuration
    ADD CONSTRAINT fk_lpgg35tul90orrn8vvvtyb80r FOREIGN KEY (host_id) REFERENCES public.cfg_host(id);


--
-- Name: validation_parameters fk_lx4r7f3abjtkl3odn5jnahrc3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_parameters
    ADD CONSTRAINT fk_lx4r7f3abjtkl3odn5jnahrc3 FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_lab_order_specimen fk_m1olp8i13dchsbsahc88dt5wt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order_specimen
    ADD CONSTRAINT fk_m1olp8i13dchsbsahc88dt5wt FOREIGN KEY (lab_order_id) REFERENCES public.om_lab_order(id);


--
-- Name: sys_conf_type_usages fk_mvt7iq3yl2ublo7ab7rpg98s; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_conf_type_usages
    ADD CONSTRAINT fk_mvt7iq3yl2ublo7ab7rpg98s FOREIGN KEY (listusages_id) REFERENCES public.usage_metadata(id);


--
-- Name: om_container fk_n526pdcwgtexyuadc2fatj98t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_container
    ADD CONSTRAINT fk_n526pdcwgtexyuadc2fatj98t FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: validation_parameters fk_nklubuc74w1d4mans3vnlvhcy; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_parameters
    ADD CONSTRAINT fk_nklubuc74w1d4mans3vnlvhcy FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_order fk_o6rgrk1keits85o3k761psmd5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_order
    ADD CONSTRAINT fk_o6rgrk1keits85o3k761psmd5 FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cfg_dicom_scp_configuration fk_o8cpt1ws21yuupo411fqdvf0t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fk_o8cpt1ws21yuupo411fqdvf0t FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: om_observation fk_obulmlnlilpjts2d93aputnyb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_observation
    ADD CONSTRAINT fk_obulmlnlilpjts2d93aputnyb FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: affinity_domain_transactions fk_oonw5dnptgu8fqtebob7173p5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT fk_oonw5dnptgu8fqtebob7173p5 FOREIGN KEY (affinity_domain_id) REFERENCES public.affinity_domain(id);


--
-- Name: cfg_dicom_scu_configuration fk_p47iuej292p3f2sawsexrx4m2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk_p47iuej292p3f2sawsexrx4m2 FOREIGN KEY (sop_class_id) REFERENCES public.cfg_sop_class(id);


--
-- Name: cfg_hl7_initiator_configuration fk_p641ssuuyy3ag6tg9uunvy1pf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk_p641ssuuyy3ag6tg9uunvy1pf FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: gs_contextual_information_instance fk_pa62xsmkceyisp0dclhsy36tl; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT fk_pa62xsmkceyisp0dclhsy36tl FOREIGN KEY (test_steps_instance_id) REFERENCES public.gs_test_steps_instance(id);


--
-- Name: om_requested_procedure fk_pcsf59w89ql348h50f5d9x964; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_requested_procedure
    ADD CONSTRAINT fk_pcsf59w89ql348h50f5d9x964 FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk_phc7kx0qg1jjfrs9b13uaclta; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk_phc7kx0qg1jjfrs9b13uaclta FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: usage_metadata fk_pl8yqxsqsua7w6nk02nmibrym; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT fk_pl8yqxsqsua7w6nk02nmibrym FOREIGN KEY (affinity_id) REFERENCES public.affinity_domain(id);


--
-- Name: om_lab_order fk_po6kmw98kh9o1v2csnljivni5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order
    ADD CONSTRAINT fk_po6kmw98kh9o1v2csnljivni5 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_lab_order fk_pq2aacv8wx6h6u3gjso5e2jby; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order
    ADD CONSTRAINT fk_pq2aacv8wx6h6u3gjso5e2jby FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk_pwniunln1hw1lb8tb8jp3cxd9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk_pwniunln1hw1lb8tb8jp3cxd9 FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: cmn_transaction_instance fk_qosa869w00k0f6cp3u3i4i6y8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fk_qosa869w00k0f6cp3u3i4i6y8 FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_scheduled_procedure_step fk_qt1m7yst8a5gb1iv6u5cbwrrx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_scheduled_procedure_step
    ADD CONSTRAINT fk_qt1m7yst8a5gb1iv6u5cbwrrx FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: gs_contextual_information_instance fk_r37ybyowksux2ytociytsbeij; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT fk_r37ybyowksux2ytociytsbeij FOREIGN KEY (contextual_information_id) REFERENCES public.gs_contextual_information(id);


--
-- Name: cmn_transaction_instance fk_s1syfhsdftpjsr8wk5pvk6idr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fk_s1syfhsdftpjsr8wk5pvk6idr FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: hl7_simulator_responder_configuration fk_s5auyaoq2re01kyo65rwpjktn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT fk_s5auyaoq2re01kyo65rwpjktn FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: om_tele_radiology_order fk_spc88qludd3kh1kil4xs0vfcw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_order
    ADD CONSTRAINT fk_spc88qludd3kh1kil4xs0vfcw FOREIGN KEY (encounter_id) REFERENCES public.om_encounter(id);


--
-- Name: om_order fk_tcjs0n4ifvymotwwg65frerwv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_order
    ADD CONSTRAINT fk_tcjs0n4ifvymotwwg65frerwv FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_note_and_comment fk_th8ntgu8jvl57p9x081w8f55b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_note_and_comment
    ADD CONSTRAINT fk_th8ntgu8jvl57p9x081w8f55b FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: cmn_receiver_console fk_tnagd28ej30pkampt9gjs28d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT fk_tnagd28ej30pkampt9gjs28d FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cfg_dicom_scp_configuration fka165b9993927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b9993927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_dicom_scp_configuration fka165b9994f3bdb32; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b9994f3bdb32 FOREIGN KEY (sop_class_id) REFERENCES public.cfg_sop_class(id);


--
-- Name: cfg_dicom_scp_configuration fka165b999511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fka165b999511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: cfg_syslog_configuration fka36479093927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT fka36479093927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_syslog_configuration fka3647909511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT fka3647909511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: om_observation_abnormalflags fka4f3192bc21208ee; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_observation_abnormalflags
    ADD CONSTRAINT fka4f3192bc21208ee FOREIGN KEY (om_observation_id) REFERENCES public.om_observation(id);


--
-- Name: gs_message fka9c507b465385ec7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fka9c507b465385ec7 FOREIGN KEY (test_instance_participants_receiver_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: gs_message fka9c507b4bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fka9c507b4bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_message fka9c507b4e0c3e041; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fka9c507b4e0c3e041 FOREIGN KEY (test_instance_participants_sender_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: user_role_group fkbbc6d66a8305e6d6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.user_role_group
    ADD CONSTRAINT fkbbc6d66a8305e6d6 FOREIGN KEY (member_of_role) REFERENCES public.user_role(id);


--
-- Name: user_role_group fkbbc6d66ab2724b21; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.user_role_group
    ADD CONSTRAINT fkbbc6d66ab2724b21 FOREIGN KEY (role_id) REFERENCES public.user_role(id);


--
-- Name: gs_test_instance_test_instance_participants fkbc3b31cd63e3d0fb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_test_instance_participants
    ADD CONSTRAINT fkbc3b31cd63e3d0fb FOREIGN KEY (test_instance_participants_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: gs_test_instance_test_instance_participants fkbc3b31cdadaef596; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_test_instance_participants
    ADD CONSTRAINT fkbc3b31cdadaef596 FOREIGN KEY (test_instance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: om_protocol_item fkc2447b91b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_protocol_item
    ADD CONSTRAINT fkc2447b91b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_protocol_item fkc2447b92d22976f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_protocol_item
    ADD CONSTRAINT fkc2447b92d22976f FOREIGN KEY (scheduled_procedure_step_id) REFERENCES public.om_scheduled_procedure_step(id);


--
-- Name: om_protocol_item fkc2447b972ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_protocol_item
    ADD CONSTRAINT fkc2447b972ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: hl7_simulator_responder_configuration fkc5ef82b272ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT fkc5ef82b272ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: hl7_simulator_responder_configuration fkc5ef82b2bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_simulator_responder_configuration
    ADD CONSTRAINT fkc5ef82b2bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_test_instance fkc783578f43e9dc7b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT fkc783578f43e9dc7b FOREIGN KEY (test_instance_status_id) REFERENCES public.gs_test_instance_status(id);


--
-- Name: tm_oid fkcc1f0704611bae11; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid
    ADD CONSTRAINT fkcc1f0704611bae11 FOREIGN KEY (system_id) REFERENCES public.gs_system(id);


--
-- Name: om_lab_order_specimen fkccbbe5cc36095a8c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order_specimen
    ADD CONSTRAINT fkccbbe5cc36095a8c FOREIGN KEY (lab_order_id) REFERENCES public.om_lab_order(id);


--
-- Name: om_lab_order_specimen fkccbbe5cc9058ab67; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_lab_order_specimen
    ADD CONSTRAINT fkccbbe5cc9058ab67 FOREIGN KEY (specimen_id) REFERENCES public.om_specimen(id);


--
-- Name: tf_actor_integration_profile fkd5a41967866df480; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fkd5a41967866df480 FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tf_actor_integration_profile fkd5a419679d1084ab; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fkd5a419679d1084ab FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cmn_transaction_instance fkd74918f11b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f11b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: cmn_transaction_instance fkd74918f11bc07e58; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f11bc07e58 FOREIGN KEY (response_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: cmn_transaction_instance fkd74918f172ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f172ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: cmn_transaction_instance fkd74918f1afd7554a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f1afd7554a FOREIGN KEY (request_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: cmn_transaction_instance fkd74918f1bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fkd74918f1bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: sys_conf_type_usages fkd97ede2e5e9a5b69; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_conf_type_usages
    ADD CONSTRAINT fkd97ede2e5e9a5b69 FOREIGN KEY (system_configuration_id) REFERENCES public.system_configuration(id);


--
-- Name: sys_conf_type_usages fkd97ede2edd54bc19; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_conf_type_usages
    ADD CONSTRAINT fkd97ede2edd54bc19 FOREIGN KEY (listusages_id) REFERENCES public.usage_metadata(id);


--
-- Name: cfg_hl7_responder_configuration fkdc2545923927e7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT fkdc2545923927e7e FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_responder_configuration fkdc254592511b8deb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT fkdc254592511b8deb FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: om_scheduled_procedure_step fkddfee1cb1b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_scheduled_procedure_step
    ADD CONSTRAINT fkddfee1cb1b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_scheduled_procedure_step fkddfee1cb521cb8aa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_scheduled_procedure_step
    ADD CONSTRAINT fkddfee1cb521cb8aa FOREIGN KEY (requested_procedure_id) REFERENCES public.om_requested_procedure(id);


--
-- Name: om_scheduled_procedure_step fkddfee1cb72ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_scheduled_procedure_step
    ADD CONSTRAINT fkddfee1cb72ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: gs_contextual_information_instance fke02d7f235a577540; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT fke02d7f235a577540 FOREIGN KEY (contextual_information_id) REFERENCES public.gs_contextual_information(id);


--
-- Name: gs_contextual_information_instance fke02d7f2383812bd3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT fke02d7f2383812bd3 FOREIGN KEY (test_steps_instance_id) REFERENCES public.gs_test_steps_instance(id);


--
-- Name: affinity_domain_transactions fke0a98f5972a3043a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT fke0a98f5972a3043a FOREIGN KEY (affinity_domain_id) REFERENCES public.affinity_domain(id);


--
-- Name: affinity_domain_transactions fke0a98f59bd491f4b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT fke0a98f59bd491f4b FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_test_steps_instance fke1c7ae27146029f1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27146029f1 FOREIGN KEY (testinstance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gs_test_steps_instance fke1c7ae2724c9d386; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2724c9d386 FOREIGN KEY (hl7v3_responder_config_id) REFERENCES public.cfg_hl7_v3_responder_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae273202260; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae273202260 FOREIGN KEY (syslog_config_id) REFERENCES public.cfg_syslog_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae2739f74269; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2739f74269 FOREIGN KEY (web_service_config_id) REFERENCES public.cfg_web_service_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27462961a4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27462961a4 FOREIGN KEY (hl7v2_initiator_config_id) REFERENCES public.cfg_hl7_initiator_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae2753d7b3a7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae2753d7b3a7 FOREIGN KEY (dicom_scp_config_id) REFERENCES public.cfg_dicom_scp_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae279cf3f066; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae279cf3f066 FOREIGN KEY (hl7v3_initiator_config_id) REFERENCES public.cfg_hl7_v3_initiator_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27a5a42187; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27a5a42187 FOREIGN KEY (dicom_scu_config_id) REFERENCES public.cfg_dicom_scu_configuration(id);


--
-- Name: gs_test_steps_instance fke1c7ae27cdff44c4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fke1c7ae27cdff44c4 FOREIGN KEY (hl7v2_responder_config_id) REFERENCES public.cfg_hl7_responder_configuration(id);


--
-- Name: om_wos_query_orders fkf05993a16606191b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_wos_query_orders
    ADD CONSTRAINT fkf05993a16606191b FOREIGN KEY (wos_query_content_id) REFERENCES public.om_wos_query_content(id);


--
-- Name: om_wos_query_orders fkf05993a18abdf7ad; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_wos_query_orders
    ADD CONSTRAINT fkf05993a18abdf7ad FOREIGN KEY (wos_id) REFERENCES public.om_lab_order(id);


--
-- Name: om_tele_radiology_order fkf6e873ab1b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_order
    ADD CONSTRAINT fkf6e873ab1b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_tele_radiology_order fkf6e873ab72ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_order
    ADD CONSTRAINT fkf6e873ab72ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: om_tele_radiology_order fkf6e873abdc4c5fcd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_order
    ADD CONSTRAINT fkf6e873abdc4c5fcd FOREIGN KEY (encounter_id) REFERENCES public.om_encounter(id);


--
-- Name: om_tele_radiology_order_relatedstudies fkf721649066954be4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_tele_radiology_order_relatedstudies
    ADD CONSTRAINT fkf721649066954be4 FOREIGN KEY (om_tele_radiology_order_id) REFERENCES public.om_tele_radiology_order(id);


--
-- Name: hl7_validation_parameters_affinity_domain fkfaa54f6016d7df47; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_validation_parameters_affinity_domain
    ADD CONSTRAINT fkfaa54f6016d7df47 FOREIGN KEY (affinity_domain_id) REFERENCES public.affinity_domain(id);


--
-- Name: hl7_validation_parameters_affinity_domain fkfaa54f6072a3043a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_validation_parameters_affinity_domain
    ADD CONSTRAINT fkfaa54f6072a3043a FOREIGN KEY (affinity_domain_id) REFERENCES public.affinity_domain(id);


--
-- Name: hl7_validation_parameters_affinity_domain fkfaa54f60db74cba9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.hl7_validation_parameters_affinity_domain
    ADD CONSTRAINT fkfaa54f60db74cba9 FOREIGN KEY (validation_parameters_id) REFERENCES public.validation_parameters(id);


--
-- Name: user_account_role fkfe2a433c8305e6d6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.user_account_role
    ADD CONSTRAINT fkfe2a433c8305e6d6 FOREIGN KEY (member_of_role) REFERENCES public.user_role(id);


--
-- Name: user_account_role fkfe2a433cce100c13; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.user_account_role
    ADD CONSTRAINT fkfe2a433cce100c13 FOREIGN KEY (account_id) REFERENCES public.user_account(id);


--
-- Name: cmn_message_instance fkfeccef683360a4d2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance
    ADD CONSTRAINT fkfeccef683360a4d2 FOREIGN KEY (issuing_actor) REFERENCES public.tf_actor(id);


--
-- Name: om_appointment fkfff5eabe1b781a49; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_appointment
    ADD CONSTRAINT fkfff5eabe1b781a49 FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: om_appointment fkfff5eabe4f6f576d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_appointment
    ADD CONSTRAINT fkfff5eabe4f6f576d FOREIGN KEY (order_id) REFERENCES public.om_order(id);


--
-- Name: om_appointment fkfff5eabe72ad8b4a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.om_appointment
    ADD CONSTRAINT fkfff5eabe72ad8b4a FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- PostgreSQL database dump complete
--

