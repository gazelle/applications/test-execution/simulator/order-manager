-- schema modified by HL7Common module
ALTER TABLE sys_config ALTER COLUMN port DROP NOT NULL ;
ALTER TABLE sys_config ALTER COLUMN ip_address DROP NOT NULL ;
UPDATE sys_config SET hl7_protocol = 1;
UPDATE sys_config SET message_encoding = 1;
UPDATE hl7_message SET message_encoding = 1;
ALTER TABLE hl7_simulator_responder_configuration ALTER COLUMN ip_address DROP NOT NULL;
ALTER TABLE hl7_simulator_responder_configuration ALTER COLUMN listening_port DROP NOT NULL;
UPDATE hl7_simulator_responder_configuration SET message_encoding = 1;
UPDATE hl7_simulator_responder_configuration SET hl7_protocol = 1;

INSERT INTO app_configuration (id, value, variable) VALUES (57, 'TRO Forwarder', 'local_forwarder_name');
INSERT INTO app_configuration (id, value, variable) VALUES (58, 'true', 'SeHE_mode_enabled');
SELECT pg_catalog.setval('app_configuration_id_seq', 58, true);

INSERT INTO tf_domain (id, description, keyword, name) VALUES (5, 'Tele-radiology Orders', 'TRO', 'Tele-radiology Orders');
SELECT pg_catalog.setval('tf_domain_id_seq', 5, true);

INSERT INTO affinity_domain (id, keyword, label_to_display, profile) VALUES (6, 'SeHE', 'Saudi eHealth Exchange', 'All');
SELECT pg_catalog.setval('affinity_domain_id_seq', 6, true);

INSERT INTO tf_transaction(id, description, keyword, name) VALUES (18, 'Unknown transaction', 'UNKNOWN', 'UNKNOWN');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (19, 'Manage Order', 'KSA-TRO', 'Manage Order');
SELECT pg_catalog.setval('tf_transaction_id_seq', 19, true);

INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (10, 'Unknown actor', 'UNKNOWN', 'UNKNOWN', false);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (11, 'Tele-Radiology Order Forwarder', 'TRO_FORWARDER', 'Tele-Radiology Order Forwarder', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (12, 'Tele-Radiology Order Creator', 'TRO_CREATOR', 'Tele-Radiology Order Creator', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (13, 'Tele-Radiology Order Filfiller', 'TRO_FULFILLER', 'Tele-Radiology Order Fulfiller', true);
SELECT pg_catalog.setval('tf_actor_id_seq', 13, true);

INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (67, 'OMG^O19^OMG_O19 (NW)', '1.3.6.1.4.12559.11.1.1.205', 1, 3);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (68, 'OMG^O19^OMG_O19 (CA)', '1.3.6.1.4.12559.11.1.1.206', 1, 3);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (69, 'ORG^O20^ORG_O20', '1.3.6.1.4.12559.11.1.1.207', 1, 6);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (70, 'OMG^O19^OMG_O19 (SN)', '1.3.6.1.4.12559.11.1.1.201', 2, 6);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (71, 'OMG^O19^OMG_O19 (SC)', '1.3.6.1.4.12559.11.1.1.202', 2, 6);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (72, 'OMG^O19^OMG_O19 (OC)', '1.3.6.1.4.12559.11.1.1.203', 2, 6);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (73, 'ACK^O19^ACK', '1.3.6.1.4.12559.11.1.1.204', 2, 3);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (74, 'ACK^O19^ACK', '1.3.6.1.4.12559.11.1.1.208', 1, 6);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (75, 'OMI^O23^OMI_O23', '1.3.6.1.4.12559.11.1.1.209', 2, 14);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (76, 'OMI^O23^OMI_O23', '1.3.6.1.4.12559.11.1.1.210', 2, 15);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (77, 'ACK^O23^ACK', '1.3.6.1.4.12559.11.1.1.211', 7, 14);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (78, 'ACK^O23^ACK', '1.3.6.1.4.12559.11.1.1.212', 7, 15);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (79, 'OMG^O19^OMG_O19 (DC)', '1.3.6.1.4.12559.11.1.1.206', 1, 3);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (80, 'OMI^O23^OMI_O23 (NW)', '1.3.6.1.4.1.12559.11.13.8.1.1', 12, 19);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (81, 'OMI^O23^OMI_O23 (OC)', '1.3.6.1.4.1.12559.11.13.8.1.2', 12, 19);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (82, 'OMI^O23^OMI_O23 (SC)', '1.3.6.1.4.1.12559.11.13.8.1.3', 13, 19);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (83, 'ACK^O23^ACK', '1.3.6.1.4.1.12559.11.13.8.1.4', 12, 19);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (84, 'ACK^O23^ACK', '1.3.6.1.4.1.12559.11.13.8.1.5', 13, 19);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (85, 'ACK^O23^ACK', '1.3.6.1.4.1.12559.11.13.8.1.5', 11, 19);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (86, 'OMI^O23^OMI_O23 (NW)', '1.3.6.1.4.1.12559.11.13.8.1.1', 11, 19);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (87, 'OMI^O23^OMI_O23 (OC)', '1.3.6.1.4.1.12559.11.13.8.1.2', 11, 19);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id) VALUES (88, 'OMI^O23^OMI_O23 (SC)', '1.3.6.1.4.1.12559.11.13.8.1.3', 11, 19);

SELECT pg_catalog.setval('ort_validation_parameters_id_seq', 88, true);

INSERT INTO hl7_validation_parameters_affinity_domain VALUES (67, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (68, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (69, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (70, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (71, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (72, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (73, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (74, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (75, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (76, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (77, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (78, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (79, 1);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (80, 6);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (81, 6);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (82, 6);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (83, 6);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (84, 6);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (85, 6);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (86, 6);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (87, 6);
INSERT INTO hl7_validation_parameters_affinity_domain VALUES (88, 6);

INSERT INTO om_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (31,'Ordering codes - Universal Service Id (TRO)','1.3.6.1.4.1.21367.101.118','OBR-4','troUniversalServiceId');
INSERT INTO om_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (32,'Order Priority (TRO)','2.16.840.1.113883.3.3731.1.204.10.1','TQ1-9','troPriority');
INSERT INTO om_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (33,'Modality (TRO)','2.16.840.1.113883.3.3731.1.204.13.1','IPC-5','troModality');
INSERT INTO om_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (34,'Procedures (TRO)','2.16.840.1.113883.3.3731.1.204.15.1','OBR-44','troProcedure');
SELECT pg_catalog.setval('om_value_set_id_seq', 34, true);


