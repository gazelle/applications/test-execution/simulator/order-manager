ALTER TABLE om_dicom_validation_result DROP COLUMN raw_result;
ALTER TABLE om_dicom_validation_result DROP COLUMN tool;
ALTER TABLE om_dicom_validation_result DROP COLUMN tool_version;
ALTER TABLE om_dicom_validation_result DROP COLUMN validation_date;

INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'evs_client_url', 'https://gazelle.ihe.net/EVSClient');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'tool_instance_oid', 'oid for this tool');
DELETE FROM app_configuration WHERE variable LIKE 'dccheck%';
INSERT INTO cmn_value_set (id, value_set_keyword, value_set_name, value_set_oid, usage) VALUES (nextval('cmn_value_set_id_seq'), 'ADMITTING_DIAG', 'Admitting Diagnosis', '1.3.6.1.4.1.213376.101.210', '0008,1084');
UPDATE om_patient SET weight = 75, size = 180 where gender_code = 'M';
UPDATE om_patient SET weight = 65, size = 165 where gender_code != 'M';