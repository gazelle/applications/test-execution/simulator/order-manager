ALTER TABLE  cmn_value_set ADD COLUMN  accessible boolean;
ALTER TABLE cmn_value_set ADD COLUMN last_check timestamp without time zone;
update cmn_value_set set accessible = FALSE ;

ALTER TABLE om_dicom_validation_result DROP COLUMN raw_result_text;
ALTER TABLE om_dicom_validation_result DROP COLUMN tool;
ALTER TABLE om_dicom_validation_result DROP COLUMN tool_version;

ALTER TABLE om_observation ALTER COLUMN "value" TYPE text;

ALTER TABLE om_requested_procedure ADD COLUMN reason_for_procedure character varying(255);

--
-- Name: pat_patient; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--

CREATE TABLE pat_patient (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    alternate_first_name character varying(255),
    alternate_last_name character varying(255),
    alternate_mothers_maiden_name character varying(255),
    alternate_second_name character varying(255),
    alternate_third_name character varying(255),
    character_set character varying(255),
    country_code character varying(255),
    creation_date timestamp without time zone,
    date_of_birth timestamp without time zone,
    first_name character varying(255),
    gender_code character varying(255),
    last_name character varying(255),
    mother_maiden_name character varying(255),
    race_code character varying(255),
    religion_code character varying(255),
    second_name character varying(255),
    size integer,
    third_name character varying(255),
    weight integer,
    contrast_allergies character varying(255),
    creator character varying(255)
);


ALTER TABLE public.pat_patient OWNER TO gazelle;

--
-- Name: pat_patient_address; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--

CREATE TABLE pat_patient_address (
    id integer NOT NULL,
    address_line character varying(255),
    address_type integer,
    city character varying(255),
    country_code character varying(255),
    is_main_address boolean,
    state character varying(255),
    street character varying(255),
    street_number character varying(255),
    zip_code character varying(255),
    patient_id integer,
    dtype character varying(31)
);


ALTER TABLE public.pat_patient_address OWNER TO gazelle;

--
-- Name: pat_patient_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE pat_patient_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pat_patient_address_id_seq OWNER TO gazelle;

--
-- Name: pat_patient_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE pat_patient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pat_patient_id_seq OWNER TO gazelle;

--
-- Name: pat_patient_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY pat_patient_address
    ADD CONSTRAINT pat_patient_address_pkey PRIMARY KEY (id);


--
-- Name: pat_patient_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY pat_patient
    ADD CONSTRAINT pat_patient_pkey PRIMARY KEY (id);

--
-- Name: fk_4oj3lc5rb77psibcax4r9j16o; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY om_patient_identifier
    ADD CONSTRAINT fk_4oj3lc5rb77psibcax4r9j16o FOREIGN KEY (patient_id) REFERENCES pat_patient(id);

--
-- Name: fk_9unnhbo9qfbfu2c7hfexts89g; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY pat_patient_address
    ADD CONSTRAINT fk_9unnhbo9qfbfu2c7hfexts89g FOREIGN KEY (patient_id) REFERENCES pat_patient(id);

--
-- Name: fk_e57flwppqy91426ponmcs8iib; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY om_encounter
    ADD CONSTRAINT fk_e57flwppqy91426ponmcs8iib FOREIGN KEY (patient_id) REFERENCES pat_patient(id);
