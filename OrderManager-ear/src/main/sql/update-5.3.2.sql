-- (not used for by OM yet)
ALTER TABLE pat_patient_address ADD COLUMN insee_code VARCHAR(255);

-- HLSEVENCMN-90 (not used for OM)
ALTER TABLE cmn_message_instance ADD COLUMN payload bytea;

-- HLSEVENCMN-91
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v2_length_fault_level', 'WARNING');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v2_structure_fault_level', 'ERROR');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v2_datatype_fault_level', 'ERROR');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v2_value_fault_level', 'WARNING');
