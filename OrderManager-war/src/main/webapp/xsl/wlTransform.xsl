<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xd"
                version="1.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b>
                Nov 28, 2011
            </xd:p>
            <xd:p>
                <xd:b>Author:</xd:b>
                aberge
            </xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="element">
        <xsl:param name="depth"/>
        <tr>
            <xsl:if test="$depth = 0">
                <td>
                    <xsl:value-of select="@name"/>
                </td>
                <td>
                    <xsl:value-of select="@tag"/>
                </td>
            </xsl:if>
            <xsl:if test="$depth = 1">
                <td>
                    &gt;
                    <xsl:value-of select="@name"/>
                </td><td>
                <xsl:value-of select="@tag" />
            </td>
            </xsl:if>
            <xsl:if test="$depth = 2">
                <td>&gt;&gt;
                    <xsl:value-of select="@name"/>
                </td><td>
                <xsl:value-of select="@tag" />
            </td>
            </xsl:if>
            <td>
                <xsl:value-of select="@vr"/>
            </td>
            <td>
                <xsl:value-of select="text()"/>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="sequence">
        <xsl:param name="depth"/>
        <tr>
            <td colspan="3">
                <b>
                    <xsl:if test="$depth = 0">
                        <xsl:value-of select="@name"/>
                        (
                        <xsl:value-of select="@tag"/>
                        )
                    </xsl:if>
                    <xsl:if test="$depth = 1">
                        &gt;
                        <xsl:value-of select="@name"/>
                        (
                        <xsl:value-of select="@tag"/>
                        )
                    </xsl:if>
                </b>
            </td>
        </tr>
        <xsl:apply-templates select="item/element">
            <xsl:with-param name="depth" select="$depth + 1"/>
        </xsl:apply-templates>
        <xsl:apply-templates select="item/sequence">
            <xsl:with-param name="depth" select="$depth + 1"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="/file-format/meta-header">
        <div class="panel panel-default">
            <div class="panel-heading">Meta Header</div>
            <div class="panel-body">
                <xsl:if test="@name">
                    <p>
                        <b>Transfer syntax</b>
                        :
                        <xsl:value-of select="@name"/>
                        (
                        <xsl:value-of select="@xfer"/>
                        )
                        <br/>
                    </p>
                </xsl:if>
                <xsl:if test="count(element) &gt; 0">
                    <table class="rf-dt">
                        <thead>
                            <tr>
                                <th>DICOM description</th>
                                <th>Tag</th>
                                <th>Value representation</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:apply-templates select="element">
                                <xsl:with-param name="depth" select="0"/>
                            </xsl:apply-templates>
                        </tbody>
                    </table>
                </xsl:if>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="/file-format/data-set">
        <div class="panel panel-default">
            <div class="panel-heading">Data Set</div>
            <div class="panel-body">
                <p>
                    <b>Transfer syntax</b>
                    :
                    <xsl:value-of select="@name"/>
                    (
                    <xsl:value-of select="@xfer"/>
                    )
                    <br/>
                </p>
                <xsl:if test="count(element) &gt;0">
                    <table class="rf-dt">
                        <thead>
                            <tr>
                                <th>DICOM description</th>
                                <th>Tag</th>
                                <th>Value representation</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:apply-templates select="element">
                                <xsl:with-param name="depth" select="0"/>
                            </xsl:apply-templates>
                            <xsl:apply-templates select="sequence">
                                <xsl:with-param name="depth" select="0"/>
                            </xsl:apply-templates>
                        </tbody>
                    </table>
                </xsl:if>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>