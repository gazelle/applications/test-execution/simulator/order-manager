<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xd"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	version="1.0">
	<xd:doc scope="stylesheet">
		<xd:desc>
			<xd:p>
				<xd:b>Created on:</xd:b>
				Nov 28, 2011
			</xd:p>
			<xd:p>
				<xd:b>Author:</xd:b>
				aberge
			</xd:p>
			<xd:p></xd:p>
		</xd:desc>
	</xd:doc>
	
	<xsl:param name="requiredTags" select="fn:tokenize('0040,0001 0040,0002 0040,0003 0040,0100','\s+')"/>
	
	<xsl:template match="element">
		<xsl:param name="depth" />
		<tr onmouseover="this.style.backgroundColor='#94C2E6';"
			onmouseout="this.style.backgroundColor='white';">
			<xsl:if test="$depth = 0">
				<td>
					<xsl:value-of select="@name" />
					<br />
					(
					<xsl:value-of select="@tag" />
					)
				</td>
			</xsl:if>
			<xsl:if test="$depth = 1">
				<td>
					&gt;
					<xsl:value-of select="@name" />
					<br />
					(
					<xsl:value-of select="@tag" />
					)
				</td>
			</xsl:if>
			<xsl:if test="$depth = 2">
				<td>&gt;&gt;
					<xsl:value-of select="@name" />
					<br />
					(
					<xsl:value-of select="@tag" />
					)
				</td>
			</xsl:if>

			<td>
				<xsl:value-of select="@vr" />
			</td>
			<td>
				<xsl:value-of select="text()" />
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="sequence">
		<xsl:param name="depth" />
		<tr onmouseover="this.style.backgroundColor='#94C2E6';"
			onmouseout="this.style.backgroundColor='white';">
			<td colspan="3">
				<b>
					<xsl:if test="$depth = 0">
						<xsl:value-of select="@name" />
						(
						<xsl:value-of select="@tag" />
						)
					</xsl:if>
					<xsl:if test="$depth = 1">
						&gt;
						<xsl:value-of select="@name" />
						(
						<xsl:value-of select="@tag" />
						)
					</xsl:if>
					<xsl:if test="fn:index-of($requiredTags, @tag)">
						<i>required</i>
					</xsl:if>
				</b>
			</td>
		</tr>
		<xsl:apply-templates select="item/element">
			<xsl:with-param name="depth" select="$depth + 1" />
		</xsl:apply-templates>
		<xsl:apply-templates select="item/sequence">
			<xsl:with-param name="depth" select="$depth + 1" />
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="/file-format/meta-header">
		<div class="rich-panel">
			<div class="rich-panel-header">Meta Header</div>
			<div class="rich-panel-body">
				<xsl:if test="@name">
					<p>
						<b>Transfer syntax</b>
						:
						<xsl:value-of select="@name" />
						(
						<xsl:value-of select="@xfer" />
						)
						<br />
					</p>
				</xsl:if>
				<xsl:if test="count(element) &gt; 0">
					<table class="dicom-table">
						<thead>
							<tr>
								<th>DICOM description</th>
								<th>Value representation</th>
								<th>Value</th>
							</tr>
						</thead>
						<tbody>
							<xsl:apply-templates select="element">
								<xsl:with-param name="depth" select="0" />
							</xsl:apply-templates>
						</tbody>
					</table>
				</xsl:if>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="/file-format/data-set">
		<div class="rich-panel">
			<div class="rich-panel-header">Data Set</div>
			<div class="rich-panel-body">
				<p>
					<b>Transfer syntax</b>
					:
					<xsl:value-of select="@name" />
					(
					<xsl:value-of select="@xfer" />
					)
					<br />
				</p>
				<xsl:if test="count(element) &gt;0">
					<table class="dicom-table">
						<thead>
							<tr>
								<th>DICOM description</th>
								<th>Value representation</th>
								<th>Value</th>
							</tr>
						</thead>
						<tbody>
							<xsl:apply-templates select="element">
								<xsl:with-param name="depth" select="0" />
							</xsl:apply-templates>
							<xsl:apply-templates select="sequence">
								<xsl:with-param name="depth" select="0" />
							</xsl:apply-templates>
						</tbody>
					</table>
				</xsl:if>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>