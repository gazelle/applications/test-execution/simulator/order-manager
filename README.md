# Order Manager

<!-- TOC -->
* [Order Manager](#order-manager)
  * [Build and tests](#build-and-tests)
  * [Installation manual](#installation-manual)
    * [Configuration for sso-client-v7](#configuration-for-sso-client-v7)
  * [User manual](#user-manual)
  * [Release note](#release-note)
<!-- TOC -->

## Build and tests

The project must be build using JDK-8 and run on a JDK-7 environment.

```shell
mvn clean install
```

## Installation manual

The Order Manager installation manual is available here:
https://gazelle.ihe.net/gazelle-documentation/Order-Manager/installation.html

### Configuration for sso-client-v7

As Order Manager depends on sso-client-v7, it needs to be configured properly to work. You can follow the README of sso-client-v7
available here: [sso-client-v7](https://gitlab.inria.fr/gazelle/library/sso-client-v7)

## User manual

The Order Manager user manual is available here:
https://gazelle.ihe.net/gazelle-documentation/Order-Manager/user.html

## Release note

The Order Manager release note is available here:
https://gazelle.ihe.net/gazelle-documentation/Order-Manager/release-note.html