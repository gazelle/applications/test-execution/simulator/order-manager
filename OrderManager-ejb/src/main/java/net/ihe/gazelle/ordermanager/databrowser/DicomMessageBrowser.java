package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ordermanager.model.DicomMessage;
import net.ihe.gazelle.ordermanager.model.DicomMessageQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.util.Map;

@Name("dicomMessageBrowser")
@Scope(ScopeType.PAGE)
public class DicomMessageBrowser extends DataBrowser<DicomMessage> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2288603037403217471L;

	@Override
	public void modifyQuery(HQLQueryBuilder<DicomMessage> queryBuilder, Map<String, Object> map) {

	}

    @Override
    public Filter<DicomMessage> getFilter() {
        if (filter == null){
            filter = new Filter<DicomMessage>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return filter;
    }

    @Override
	protected HQLCriterionsForFilter<DicomMessage> getHQLCriterionsForFilter() {
		DicomMessageQuery query = new DicomMessageQuery();
		HQLCriterionsForFilter<DicomMessage> criterions = query.getHQLCriterionsForFilter();
		criterions.addPath("lastChanged", query.timestamp());
		criterions.addPath("from", query.from());
		criterions.addPath("to", query.to());
		criterions.addPath("command", query.commandField());
		criterions.addPath("simulatedActor", query.simulatedActor());
		return criterions;
	}

	@Override
	public String getPermanentLink(Integer objectId) {
		return "/browsing/dicomMessage.seam?id=" + objectId;
	}

    @Override
    public FilterDataModel<DicomMessage> getDataModel() {
        return new FilterDataModel<DicomMessage>(getFilter()) {
            @Override
            protected Object getId(DicomMessage dicomMessage) {
                return dicomMessage.getId();
            }
        };
    }

}
