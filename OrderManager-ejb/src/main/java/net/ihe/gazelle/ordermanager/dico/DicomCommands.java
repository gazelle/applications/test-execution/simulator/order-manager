/* Copyright (c) 2001-2005, David A. Clunie DBA Pixelmed Publishing. All rights reserved. */

package net.ihe.gazelle.ordermanager.dico;

/**
 * <p>DicomCommands class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class DicomCommands {
	/***/
	
	private DicomCommands(){
		
	}

	/** Constant <code>NOCOMMAND=0x0000</code> */
	public static final int NOCOMMAND = 0x0000; // used as a flag

	/** Constant <code>C_STORE_RQ=0x0001</code> */
	public static final int C_STORE_RQ = 0x0001;
	/** Constant <code>C_STORE_RSP=0x8001</code> */
	public static final int C_STORE_RSP = 0x8001;
	/** Constant <code>C_GET_RQ=0x0010</code> */
	public static final int C_GET_RQ = 0x0010;
	/** Constant <code>C_GET_RSP=0x8010</code> */
	public static final int C_GET_RSP = 0x8010;
	/** Constant <code>C_FIND_RQ=0x0020</code> */
	public static final int C_FIND_RQ = 0x0020;
	/** Constant <code>C_FIND_RSP=0x8020</code> */
	public static final int C_FIND_RSP = 0x8020;
	/** Constant <code>C_MOVE_RQ=0x0021</code> */
	public static final int C_MOVE_RQ = 0x0021;
	/** Constant <code>C_MOVE_RSP=0x8021</code> */
	public static final int C_MOVE_RSP = 0x8021;
	/** Constant <code>C_ECHO_RQ=0x0030</code> */
	public static final int C_ECHO_RQ = 0x0030;
	/** Constant <code>C_ECHO_RSP=0x8030</code> */
	public static final int C_ECHO_RSP = 0x8030;
	/** Constant <code>N_EVENT_REPORT_RQ=0x0100</code> */
	public static final int N_EVENT_REPORT_RQ = 0x0100;
	/** Constant <code>N_EVENT_REPORT_RSP=0x8100</code> */
	public static final int N_EVENT_REPORT_RSP = 0x8100;
	/** Constant <code>N_GET_RQ=0x0110</code> */
	public static final int N_GET_RQ = 0x0110;
	/** Constant <code>N_GET_RSP=0x8110</code> */
	public static final int N_GET_RSP = 0x8110;
	/** Constant <code>N_SET_RQ=0x0120</code> */
	public static final int N_SET_RQ = 0x0120;
	/** Constant <code>N_SET_RSP=0x8120</code> */
	public static final int N_SET_RSP = 0x8120;
	/** Constant <code>N_ACTION_RQ=0x0130</code> */
	public static final int N_ACTION_RQ = 0x0130;
	/** Constant <code>N_ACTION_RSP=0x8130</code> */
	public static final int N_ACTION_RSP = 0x8130;
	/** Constant <code>N_CREATE_RQ=0x0140</code> */
	public static final int N_CREATE_RQ = 0x0140;
	/** Constant <code>N_CREATE_RSP=0x8140</code> */
	public static final int N_CREATE_RSP = 0x8140;
	/** Constant <code>N_DELETE_RQ=0x0150</code> */
	public static final int N_DELETE_RQ = 0x0150;
	/** Constant <code>N_DELETE_RSP=0x8150</code> */
	public static final int N_DELETE_RSP = 0x8150;
	/** Constant <code>C_CANCEL_RQ=0x0FFF</code> */
	public static final int C_CANCEL_RQ = 0x0FFF;

	/**
	 * <p>toString.</p>
	 *
	 * @param command a int.
	 * @return a {@link java.lang.String} object.
	 */
	public static final String toString(int command) {
		String s;
		switch (command) {
		case C_STORE_RQ:
			s = "C-STORE-RQ";
			break;
		case C_STORE_RSP:
			s = "C-STORE-RSP";
			break;
		case C_GET_RQ:
			s = "C-GET-RQ";
			break;
		case C_GET_RSP:
			s = "C-GET-RSP";
			break;
		case C_FIND_RQ:
			s = "C-FIND-RQ";
			break;
		case C_FIND_RSP:
			s = "C-FIND-RSP";
			break;
		case C_MOVE_RQ:
			s = "C-MOVE-RQ";
			break;
		case C_MOVE_RSP:
			s = "C-MOVE-RSP";
			break;
		case C_ECHO_RQ:
			s = "C-ECHO-RQ";
			break;
		case C_ECHO_RSP:
			s = "C-ECHO-RSP";
			break;
		case N_EVENT_REPORT_RQ:
			s = "N-EVENT-REPORT-RQ";
			break;
		case N_EVENT_REPORT_RSP:
			s = "N-EVENT-REPORT-RSP";
			break;
		case N_GET_RQ:
			s = "N-GET-RQ";
			break;
		case N_GET_RSP:
			s = "N-GET-RSP";
			break;
		case N_SET_RQ:
			s = "N-SET-RQ";
			break;
		case N_SET_RSP:
			s = "N-SET-RSP";
			break;
		case N_ACTION_RQ:
			s = "N-ACTION-RQ";
			break;
		case N_ACTION_RSP:
			s = "N-ACTION-RSP";
			break;
		case N_CREATE_RQ:
			s = "N-CREATE-RQ";
			break;
		case N_CREATE_RSP:
			s = "N-CREATE-RSP";
			break;
		case N_DELETE_RQ:
			s = "N-DELETE-RQ";
			break;
		case N_DELETE_RSP:
			s = "N-DELETE-RSP";
			break;
		case C_CANCEL_RQ:
			s = "C-CANCEL-RQ";
			break;
		default:
			s = "--UNKNOWN--";
			break;
		}
		return s;
	}
}
