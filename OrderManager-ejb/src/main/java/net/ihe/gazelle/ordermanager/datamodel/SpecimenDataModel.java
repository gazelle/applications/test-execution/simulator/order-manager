package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.Component;
import org.jboss.seam.security.Identity;

import java.util.Date;

/**
 * Class description : <b>SpecimenDataModel</b>
 * 
 * This class is a data model for properly and quickly displaying the specimen objects in the GUI
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, December 6th
 * 
 */

public class SpecimenDataModel extends FilterDataModel<Specimen> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3513436566895764509L;
	private String creator;
	private Date startDate;
	private Date endDate;
	private String patientId;
	private Boolean isMainEntity;
	private boolean isPartOfWorkOrder;
	private LabOrder relatedLabOrder;
	private Actor simulatedActor;
	// this field is used to indicate if we look for SPECIMEN-CENTRIC (false) or "CONTAINER-CENTRIC" (true)
	private Boolean searchForSingleContainers;

	public SpecimenDataModel() {
		super(new SpecimenFilter());
	}

	public SpecimenDataModel(LabOrder relatedOrder) {
		super(new SpecimenFilter());
		this.relatedLabOrder = relatedOrder;
		this.creator = null;
		this.startDate = null;
		this.endDate = null;
		this.patientId = null;
		this.isMainEntity = false;
		if (relatedOrder != null) {
			this.isPartOfWorkOrder = relatedOrder.isWorkOrder();
			this.simulatedActor = relatedOrder.getSimulatedActor();
		}
		searchForSingleContainers = null;
	}

	public SpecimenDataModel(Date startDate, Date endDate, String patientId, Boolean isMainEntity,
			boolean isPartOfWorkOrder, Actor simulatedActor, Boolean searchForSingleContainers) {
		super(new SpecimenFilter());
		this.creator = null;
		if (Identity.instance().isLoggedIn()) {
			UserPreferences preferences = (UserPreferences) Component.getInstance("userPreferences");
			if (preferences.isViewOnlyMyObjects()) {
				this.creator = preferences.getUsername();
			}
		}
		if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        }
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        }
		this.patientId = patientId;
		this.isMainEntity = isMainEntity;
		this.isPartOfWorkOrder = isPartOfWorkOrder;
		this.relatedLabOrder = null;
		this.simulatedActor = simulatedActor;
		this.searchForSingleContainers = searchForSingleContainers;
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<Specimen> hqlBuilder) {
		hqlBuilder.addEq("workOrder", isPartOfWorkOrder);
		if (searchForSingleContainers != null) {
			// retrieve the specimens for which the container are main entities (that means that orders are related to a single container of a single specimen)
			// or not (that means that orders are related to a single specimen)
			hqlBuilder.addEq("containers.mainEntity", searchForSingleContainers);
		}
		if (relatedLabOrder != null) {
			hqlBuilder.addEq("orders.id", relatedLabOrder.getId());
		}
		if ((creator != null) && !creator.isEmpty()) {
			hqlBuilder.addEq("creator", creator);
		}
		if (startDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.gt("lastChanged", startDate));
		}
		if (endDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.lt("lastChanged", endDate));
		}
		if ((patientId != null) && !patientId.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("patient.patientIdentifiers.identifier", patientId,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (isMainEntity != null) {
			hqlBuilder.addEq("mainEntity", isMainEntity);
		}
		if (simulatedActor != null) {
			hqlBuilder.addEq("simulatedActor", simulatedActor);
		}
	}

@Override
        protected Object getId(Specimen t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
