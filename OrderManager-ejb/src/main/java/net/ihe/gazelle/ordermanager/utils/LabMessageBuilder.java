package net.ihe.gazelle.ordermanager.utils;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.group.OML_O21_SPECIMEN;
import ca.uhn.hl7v2.model.v25.group.OML_O33_ORDER;
import ca.uhn.hl7v2.model.v25.group.OML_O35_ORDER;
import ca.uhn.hl7v2.model.v25.group.OML_O35_SPECIMEN_CONTAINER;
import ca.uhn.hl7v2.model.v25.message.OML_O21;
import ca.uhn.hl7v2.model.v25.message.OML_O33;
import ca.uhn.hl7v2.model.v25.message.OML_O35;
import ca.uhn.hl7v2.model.v25.message.ORU_R01;
import ca.uhn.hl7v2.model.v25.message.OUL_R22;
import ca.uhn.hl7v2.model.v25.message.OUL_R23;
import ca.uhn.hl7v2.model.v25.segment.MSH;
import ca.uhn.hl7v2.model.v25.segment.OBR;
import ca.uhn.hl7v2.model.v25.segment.OBX;
import ca.uhn.hl7v2.model.v25.segment.ORC;
import ca.uhn.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.model.v25.segment.PV1;
import ca.uhn.hl7v2.model.v25.segment.SAC;
import ca.uhn.hl7v2.model.v25.segment.SPM;
import ca.uhn.hl7v2.model.v25.segment.TQ1;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.group.RSP_K11_ORDER;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message.RSP_K11;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.segment.QPD;
import net.ihe.gazelle.ordermanager.hl7.initiators.AbstractResultManager;
import net.ihe.gazelle.ordermanager.hl7.initiators.LabOrderSender;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Observation;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.WOSQueryContent;
import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import org.jboss.seam.contexts.Lifecycle;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Class description : <b>MessageGenerator</b>
 * <p/>
 * This class contains the methods to create the HL7 messages used by the OrderManager simulator
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 * @version 1.0 - 2011, November 17th
 *
 *
 */
public class LabMessageBuilder extends SegmentBuilder {

    protected static final String ANALYZER_MGR = "ANALYZER_MGR";
    protected static final String ANALYZER = "ANALYZER";
    private static final String OF_KEYWORD = "OF";
    private static final String AM = "AM";
    private static final String LB = "LB";
    private static Log log = Logging.getLog(LabMessageBuilder.class);

    private String transactionKeyword;
    private String sendingActorKeyword;
    private LabOrder order;
    private Specimen specimen;
    private HL7V2ResponderSUTConfiguration sut;
    private String sendingApplication;
    private String sendingFacility;
    private SimpleDateFormat sdf;
    private String messageStructure;
    private Encounter encounter;
    private List<LAWOptions> options;
    private EntityManager entityManager;

    /**
     * Constructor
     *
     * @param inTransactionKeyword  is required to determine the type of message to send
     * @param inSendingActorKeyword is required to determine the type of message to send
     * @param encounter             TODO
     * @param inOrder               , the order for which a message needs to be create
     * @param sut                   , the SUT to which sends the message
     * @param sendingApplication    , sending application of the simulator
     * @param sendingFacility
     * @param options
     */
    public LabMessageBuilder(String inTransactionKeyword, String inSendingActorKeyword, Encounter encounter,
                             LabOrder inOrder, Specimen inSpecimen, HL7V2ResponderSUTConfiguration sut, String sendingApplication,
                             String sendingFacility, String messageStructure, List<LAWOptions> options) {
        this.transactionKeyword = inTransactionKeyword;
        this.sendingApplication = sendingApplication;
        this.sendingFacility = sendingFacility;
        this.sut = sut;
        this.sendingActorKeyword = inSendingActorKeyword;
        this.order = inOrder;
        this.specimen = inSpecimen;
        this.sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        this.messageStructure = messageStructure;
        this.encounter = encounter;
        if (options != null) {
            this.options = options;
        }
        this.entityManager = EntityManagerService.provideEntityManager();
    }

    public LabMessageBuilder(String inTransactionKeyword, String inSendingActorKeyword, Encounter encounter, String sendingApplication,
                             String sendingFacility, String messageStructure, EntityManager entityManager) {
        this.transactionKeyword = inTransactionKeyword;
        this.sendingApplication = sendingApplication;
        this.sendingFacility = sendingFacility;
        this.sut = null;
        this.sendingActorKeyword = inSendingActorKeyword;
        this.order = null;
        this.specimen = null;
        this.sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        this.messageStructure = messageStructure;
        this.encounter = encounter;
        this.options = null;
        this.entityManager = entityManager;
    }

    /**
     * Thanks to the transaction and actor, creates the appropriates message
     *
     * @return the String version of the created message
     * @throws HL7Exception
     */
    public String generateMessage() throws HL7Exception {
        Message message = null;

        if (transactionKeyword.equals("PAT-1")) {
            message = buildPAT1Message();
        } else if (transactionKeyword.equals("PAT-2")) {
            message = buildPAT2Message();
        } else if (transactionKeyword.equals("LAB-1")) {
            message = buildLAB1Message();
        } else if (transactionKeyword.equals("LAB-2")) {
            message = buildLAB2Message();
        } else if (transactionKeyword.equals("LAB-3")) {
            message = buildLAB3Message();
        } else if (transactionKeyword.equals("LAB-4")) {
            message = buildLAB4Message();
        } else if (transactionKeyword.equals("LAB-5")) {
            message = buildLAB5Message();
        } else if (transactionKeyword.equals("LAB-28")) {
            message = buildLAB28Message();
        } else if (transactionKeyword.equals("LAB-29")) {
            message = buildLAB5Message();

            ca.uhn.hl7v2.model.v25.message.OUL_R22 messageOUL = (OUL_R22) message;
            messageOUL.getMSH().getVersionID().getVersionID().setValue("2.5.1");
            messageOUL.getMSH().getAcceptAcknowledgmentType().setValue("NE");
            messageOUL.getMSH().getApplicationAcknowledgmentType().setValue("AL");
            messageOUL.getMSH().getMessageProfileIdentifier(0).parse("LAB-29^IHE");
            message = messageOUL;
        } else if (transactionKeyword.equals("LAB-61")) {
            message = buildLAB61Message();
        } else if (transactionKeyword.equals("LAB-63")) {
            message = buildLAB63Message();
        } else {
            message = null;
        }
        // message to string and returns
        if (message == null) {
            return null;
        } else {
            return PipeParser.getInstanceWithNoValidation().encode(message);
        }
    }

    private Message buildLAB63Message() throws HL7Exception {
        return buildO33();
    }

    private Message buildLAB61Message() throws HL7Exception {
        return buildO33();
    }

    private Message buildLAB5Message() throws HL7Exception {
        if (messageStructure.equals(AbstractResultManager.SPECIMENCENTRIC)) {
            messageStructure = "OUL^R22^OUL_R22";
            return buildR22();
        } else {
            messageStructure = "OUL^R23^OUL_R23";
            return buildR23();
        }
    }

    private Message buildLAB4Message() throws HL7Exception {
        if (messageStructure.equals(LabOrderSender.BATTERY_CENTRIC)) {
            return buildO21();
        } else if (messageStructure.equals(LabOrderSender.SPECIMEN_CENTRIC)) {
            return buildO33();
        } else {
            return buildO35();
        }
    }

    private Message buildLAB3Message() throws HL7Exception {
        if (messageStructure.equals(AbstractResultManager.BATTERYCENTRIC)) {
            messageStructure = "ORU^R01^ORU_R01";
            return buildR01();
        } else {
            messageStructure = "OUL^R22^OUL_R22";
            return buildR22();
        }
    }

    private Message buildLAB2Message() throws HL7Exception {
        if (messageStructure.equals(LabOrderSender.BATTERY_CENTRIC)) {
            return buildO21();
        } else if (messageStructure.equals(LabOrderSender.SPECIMEN_CENTRIC)) {
            return buildO33();
        } else {
            return buildO35();
        }
    }

    private Message buildLAB1Message() throws HL7Exception {
        if (messageStructure.equals(LabOrderSender.BATTERY_CENTRIC)) {
            return buildO21();
        } else if (messageStructure.equals(LabOrderSender.SPECIMEN_CENTRIC)) {
            return buildO33();
        } else {
            return buildO35();
        }
    }

    private Message buildLAB28Message() throws HL7Exception {
        OML_O33 omlO33 = (OML_O33) buildO33();
        omlO33.getMSH().getVersionID().parse("2.5.1");
        omlO33.getMSH().getAcceptAcknowledgmentType().setValue("NE");
        omlO33.getMSH().getApplicationAcknowledgmentType().setValue("AL");
        omlO33.getMSH().getMessageProfileIdentifier(0).parse("LAB-28^IHE");
        return omlO33;
    }

    /**
     * builds the ORU^R01^ORU_R01 message according the specifications from TF-LAB vol.2 (6.5.2) Rev 3.0 Final Text 2011-05-19
     *
     * @return
     * @throws HL7Exception
     */
    private Message buildR01() throws HL7Exception {
        ORU_R01 oruMessage = new ORU_R01();
        populateMSHv25(oruMessage.getMSH(), "ORU^R01^ORU_R01");
        // PID/PV1
        if (encounter != null) {
            if (encounter.getPatient() != null) {
                populatePIDv25(oruMessage.getPATIENT_RESULT().getPATIENT().getPID());
            }
            populatePV1v25(oruMessage.getPATIENT_RESULT().getPATIENT().getVISIT().getPV1());
        }
        // ORDER_OBSERVATION begin
        populateORCv25(oruMessage.getPATIENT_RESULT().getORDER_OBSERVATION().getORC(), order, true, true);
        oruMessage.getPATIENT_RESULT().getORDER_OBSERVATION().getORC().getOrderControl().setValue("SC");
        populateOBRv25(oruMessage.getPATIENT_RESULT().getORDER_OBSERVATION().getOBR(), order, true, true);
        populateTQ1v25(oruMessage.getPATIENT_RESULT().getORDER_OBSERVATION().getTIMING_QTY().getTQ1(), order);
        // OBSERVATION begin
        if (order.getObservations() != null) {
            for (Observation observation : order.getObservations()) {
                if ((observation.getIncludeInMessage() != null) && observation.getIncludeInMessage()) {
                    // SetID starts from 1 whereas repetition number starts from 0
                    populateOBXv25(oruMessage.getPATIENT_RESULT().getORDER_OBSERVATION()
                            .getOBSERVATION(observation.getSetId() - 1).getOBX(), observation);
                }
            }
        }
        // reload object to avoid LazyInitializationException exception
        order = (entityManager).find(LabOrder.class, order.getId());
        // SPECIMEN begin
        if (order.getSpecimens() != null) {
            int specimenIndex = 0;
            Date observationDateToCopy = null;
            for (Specimen specimen : order.getSpecimens()) {
                observationDateToCopy = specimen.getCollectionDateTime();
                populateSPMv25(
                        oruMessage.getPATIENT_RESULT().getORDER_OBSERVATION().getSPECIMEN(specimenIndex).getSPM(),
                        specimen, specimenIndex);
                if (specimen.getObservations() != null) {
                    for (Observation observation : specimen.getObservations()) {
                        populateOBXv25(oruMessage.getPATIENT_RESULT().getORDER_OBSERVATION().getSPECIMEN(specimenIndex)
                                .getOBX(observation.getSetId() - 1), observation);
                    }
                }
            }
            if (order.getSpecimens().size() == 1) {
                oruMessage.getPATIENT_RESULT().getORDER_OBSERVATION().getOBR().getObservationDateTime().getTime().setValue(sdf.format(observationDateToCopy));
            } else {
                oruMessage.getPATIENT_RESULT().getORDER_OBSERVATION().getOBR().getObservationDateTime().getTime().setValue("\"\"");
            }
        }
        // SPECIMEN end
        // ORDER_OBSERVATION end
        return oruMessage;
    }

    /**
     * builds the OUL^R22^UL_R22 message according the specifications from TF-LAB vol.2 (6.5.1) Rev 3.0 Final Text 2011-05-19
     *
     * @return
     * @throws HL7Exception
     */
    private Message buildR22() throws HL7Exception {
        OUL_R22 oulMessage = new OUL_R22();
        populateMSHv25(oulMessage.getMSH(), "OUL^R22^OUL_R22");
        // PID/PV1
        if (encounter != null && (!sendingActorKeyword.equals(ANALYZER) || options == null || options
                .contains(LAWOptions.LAW_PAT_DEM))) {
            if (encounter.getPatient() != null) {
                populatePIDv25(oulMessage.getPATIENT().getPID());
            }
            populatePV1v25(oulMessage.getVISIT().getPV1());
        }
        // SPECIMEN begin
        populateSPMv25(oulMessage.getSPECIMEN().getSPM(), specimen, 1);
        if (specimen.getObservations() != null) {
            for (Observation observation : specimen.getObservations()) {
                populateOBXv25(oulMessage.getSPECIMEN().getOBX(observation.getSetId() - 1), observation);
            }
        }
        if (specimen.getContainers() != null) {
            int containerIndex = 0;
            for (Container container : specimen.getContainers()) {
                populateSACv25(oulMessage.getSPECIMEN().getCONTAINER(containerIndex).getSAC(), container);
                containerIndex++;
            }
        }
        if (specimen.getOrders() != null) {
            int orderIndex = 0;
            for (LabOrder currentOrder : specimen.getOrders()) {
                // reload object to avoid LazyInitializationException exception
                currentOrder = (entityManager)
                        .find(LabOrder.class, currentOrder.getId());
                // ORDER begin
                populateOBRv25(oulMessage.getSPECIMEN().getORDER(orderIndex).getOBR(), currentOrder, true, true);
                if (transactionKeyword.equals("LAB-3")) {
                    oulMessage.getSPECIMEN().getORDER(orderIndex).getOBR().getObservationDateTime().getTime().setValue("\"\"");
                }
                populateORCv25(oulMessage.getSPECIMEN().getORDER(orderIndex).getORC(), currentOrder,
                        !sendingActorKeyword.equals(ANALYZER), !sendingActorKeyword.equals(ANALYZER));
                oulMessage.getSPECIMEN().getORDER(orderIndex).getORC().getOrderControl()
                        .setValue(currentOrder.getOrderControlCode());
                if (!sendingActorKeyword.equals(ANALYZER) || (options != null && options
                        .contains(LAWOptions.LAW_AWOS_PRIORITY))) {
                    populateTQ1v25(oulMessage.getSPECIMEN().getORDER(orderIndex).getTIMING_QTY().getTQ1(),
                            currentOrder);
                }
                if ((currentOrder.getObservations() != null) && !currentOrder.getObservations().isEmpty()) {
                    if (currentOrder.getObservations() != null && (!sendingActorKeyword.equals(ANALYZER) || currentOrder
                            .getOrderStatus().equals("CM") || currentOrder.getOrderStatus().equals("IP"))) {
                        for (Observation observation : currentOrder.getObservations()) {
                            if ((observation.getIncludeInMessage() != null) && observation.getIncludeInMessage()) {
                                populateOBXv25(oulMessage.getSPECIMEN().getORDER(orderIndex)
                                        .getRESULT(observation.getSetId() - 1).getOBX(), observation);
                            }
                        }
                    }
                    // ORDER end
                    orderIndex++;
                } else {
                    log.warn("There's no observation to report");
                }
            }
        }
        // SPECIMEN end
        return oulMessage;
    }

    /**
     * builds the OUL^R23^UL_R23 message according the specifications from TF-LAB vol.2 (8.5.1.2) Rev 3.0 Final Text 2011-05-19
     *
     * @return
     */
    private Message buildR23() throws HL7Exception {
        OUL_R23 oulMessage = new OUL_R23();
        populateMSHv25(oulMessage.getMSH(), "OUL^R23^OUL_R23");
        // PID/PV1
        if (encounter != null) {
            if (encounter.getPatient() != null) {
                populatePIDv25(oulMessage.getPATIENT().getPID());
            }
            populatePV1v25(oulMessage.getVISIT().getPV1());
        }
        // SPECIMEN begin
        populateSPMv25(oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI().getSPM(), specimen, 1);
        if (specimen.getObservations() != null) {
            for (Observation observation : specimen.getObservations()) {
                populateOBXv25(
                        oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI().getOBX(observation.getSetId() - 1),
                        observation);
            }
        }
        if (specimen.getContainers() != null) {
            int sacIndex = 0;
            for (Container container : specimen.getContainers()) {
                // CONTAINER begin
                populateSACv25(
                        oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI().getCONTAINER(sacIndex).getSAC(),
                        container);
                if (container.getOrders() != null) {
                    int orderIndex = 0;
                    for (LabOrder currentOrder : container.getOrders()) {
                        currentOrder = (entityManager)
                                .find(LabOrder.class, currentOrder.getId());
                        // ORDER begin
                        populateOBRv25(oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI().getCONTAINER(sacIndex)
                                .getORDER(orderIndex).getOBR(), currentOrder, true, true);
                        populateORCv25(oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI().getCONTAINER(sacIndex)
                                .getORDER(orderIndex).getORC(), currentOrder, true, true);
                        oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI().getCONTAINER(sacIndex)
                                .getORDER(orderIndex).getORC().getOrderControl().setValue("SC");
                        if (currentOrder.getObservations() != null) {
                            for (Observation observation : currentOrder.getObservations()) {
                                if ((observation.getIncludeInMessage() != null) && observation.getIncludeInMessage()) {
                                    populateOBXv25(oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI()
                                            .getCONTAINER(sacIndex).getORDER(orderIndex)
                                            .getRESULT(observation.getSetId() - 1).getOBX(), observation);
                                }
                            }
                        }
                        // ORDER end
                        orderIndex++;
                    }
                }
                // CONTAINER end
            }
        }
        // SPECIMEN end
        return oulMessage;
    }

    /**
     * builds the OML^O35^OML_O35 message according the specifications from TF-LAB vol.2 (4.5.7) Rev 3.0 Final Text 2011-05-19
     *
     * @return
     * @throws HL7Exception
     */
    private Message buildO35() throws HL7Exception {
        OML_O35 omlMessage = new OML_O35();
        // MSH
        populateMSHv25(omlMessage.getMSH(), messageStructure);
        // PATIENT begin
        populatePIDv25(omlMessage.getPATIENT().getPID());
        populatePV1v25(omlMessage.getPATIENT().getPATIENT_VISIT().getPV1());
        // PATIENT end
        if ((order != null) && (order.getContainer() != null)) {
            specimen = order.getContainer().getSpecimen();
        }
        if (specimen != null) {
            // SPECIMEN begin
            populateSPMv25(omlMessage.getSPECIMEN(0).getSPM(), specimen, 1);
            if ((order != null) && (order.getContainer() != null)) {
                // CONTAINER begin
                OML_O35_SPECIMEN_CONTAINER containerGroup = omlMessage.getSPECIMEN(0).getSPECIMEN_CONTAINER(0);
                populateSACv25(containerGroup.getSAC(), order.getContainer());
                // ORDER begin
                OML_O35_ORDER orderGroup = containerGroup.getORDER(0);
                populateORCv25(orderGroup.getORC(), order, true, true);
                populateTQ1v25(orderGroup.getTIMING(0).getTQ1(), order);
                // OBSERVATION REQUEST begin
                populateOBRv25(orderGroup.getOBSERVATION_REQUEST().getOBR(), order, true, true);
                // OBSERVATION REQUEST end
                // ORDER end
                // CONTAINER end
            } else if ((specimen.getContainers() != null) && !specimen.getContainers().isEmpty()) {
                int containerIndex = 0;
                for (Container container : specimen.getContainers()) {
                    // CONTAINER begin
                    OML_O35_SPECIMEN_CONTAINER containerGroup = omlMessage.getSPECIMEN(0)
                            .getSPECIMEN_CONTAINER(containerIndex);
                    populateSACv25(containerGroup.getSAC(), container);
                    if ((container.getOrders() != null) && !container.getOrders().isEmpty()) {
                        int orderIndex = 0;
                        for (LabOrder order : container.getOrders()) {
                            // ORDER begin
                            OML_O35_ORDER orderGroup = containerGroup.getORDER(orderIndex);
                            populateORCv25(orderGroup.getORC(), order, true, true);
                            populateTQ1v25(orderGroup.getTIMING(0).getTQ1(), order);
                            // OBSERVATION REQUEST begin
                            populateOBRv25(orderGroup.getOBSERVATION_REQUEST().getOBR(), order, true, true);
                            // OBSERVATION REQUEST end
                            // ORDER end
                            orderIndex++;
                        }
                    }
                    // CONTAINER end
                    containerIndex++;
                }
            }
            // SPECIMEN end
        } else if (order != null){
            int specimenIndex = 0;
            for (Specimen currentSpecimen : order.getSpecimens()) {
                // SPECIMEN begin
                populateSPMv25(omlMessage.getSPECIMEN(specimenIndex).getSPM(), currentSpecimen, 1);
                int containerIndex = 0;
                for (Container currentContainer : currentSpecimen.getContainers()) {
                    // CONTAINER begin
                    OML_O35_SPECIMEN_CONTAINER containerGroup = omlMessage.getSPECIMEN(specimenIndex)
                            .getSPECIMEN_CONTAINER(containerIndex);
                    populateSACv25(containerGroup.getSAC(), currentContainer);
                    // ORDER begin
                    OML_O35_ORDER orderGroup = containerGroup.getORDER(0);
                    populateORCv25(orderGroup.getORC(), order, true, true);
                    populateTQ1v25(orderGroup.getTIMING(0).getTQ1(), order);
                    // OBSERVATION REQUEST begin
                    populateOBRv25(orderGroup.getOBSERVATION_REQUEST().getOBR(), order, true, true);
                    // OBSERVATION REQUEST end
                    // ORDER end
                    // CONTAINER end
                    containerIndex++;
                }
                // SPECIMEN end
                specimenIndex++;
            }
        }
        return omlMessage;
    }

    /**
     * builds the OML^O33^OML_O33 message according the specifications from TF-LAB vol.2 (4.5.5) Rev 3.0 Final Text 2011-05-19
     *
     * @return
     * @throws HL7Exception
     */
    private Message buildO33() throws HL7Exception {
        OML_O33 omlMessage = new OML_O33();
        // MSH
        populateMSHv25(omlMessage.getMSH(), messageStructure);
        // PATIENT begin
        if (!sendingActorKeyword.equals(ANALYZER_MGR) || (options != null && options
                .contains(LAWOptions.LAW_PAT_DEM))) {
            populatePIDv25(omlMessage.getPATIENT().getPID());
            populatePV1v25(omlMessage.getPATIENT().getPATIENT_VISIT().getPV1());
        }
        // PATIENT end
        // if specimen is null, that means that we are cancelling or updating an order, we should
        // retrieve the specimens starting from the order
        if (specimen != null) {
            // SPECIMEN begin
            populateSPMv25(omlMessage.getSPECIMEN(0).getSPM(), specimen, 1);
            if ((specimen.getContainers() != null) && !specimen.getContainers().isEmpty()) {
                for (int index = 0; index < specimen.getContainers().size(); index++) {
                    populateSACv25(omlMessage.getSPECIMEN(0).getSAC(index), specimen.getContainers().get(index));
                }
            }
            if ((specimen.getOrders() != null) && !specimen.getOrders().isEmpty()) {
                int index = 0; // index is used to retrieve the appropriate repetition of the ORDER group
                for (LabOrder order : specimen.getOrders()) {
                    // ORDER begin
                    OML_O33_ORDER orderGroup = omlMessage.getSPECIMEN(0).getORDER(index);
                    // ORC-2 SHALL NOT be populated in OML_O33 messages issued by the Analyzer Mgr
                    populateORCv25(orderGroup.getORC(), order, !sendingActorKeyword.equals(ANALYZER_MGR), true);
                    if (!sendingActorKeyword.equals(ANALYZER_MGR) || (options != null && options
                            .contains(LAWOptions.LAW_AWOS_PRIORITY))) {
                        populateTQ1v25(orderGroup.getTIMING(0).getTQ1(), order);
                    }
                    // OBSERVATION REQUEST begin
                    populateOBRv25(orderGroup.getOBSERVATION_REQUEST().getOBR(), order, true,
                            !sendingActorKeyword.equals(ANALYZER_MGR));
                    // OBSERVATION REQUEST end
                    // ORDER end
                    index++;
                }
            }
            // SPECIMEN end
        } else {
            // Order has been received in a OML_O33 message
            if ((order.getSpecimens() != null) && !order.getSpecimens().isEmpty()) {
                int specimenIndex = 0;
                for (Specimen currentSpecimen : order.getSpecimens()) {
                    // SPECIMEN begin
                    populateSPMv25(omlMessage.getSPECIMEN(specimenIndex).getSPM(), currentSpecimen, 1);
                    if ((currentSpecimen.getContainers() != null) && !currentSpecimen.getContainers().isEmpty()) {
                        for (int containerIndex = 0;
                             containerIndex < currentSpecimen.getContainers().size(); containerIndex++) {
                            populateSACv25(omlMessage.getSPECIMEN(specimenIndex).getSAC(containerIndex),
                                    currentSpecimen.getContainers().get(containerIndex));
                        }
                    } else {
                        log.info("no container defined for specimen");
                    }
                    // ORDER begin
                    OML_O33_ORDER orderGroup = omlMessage.getSPECIMEN(specimenIndex).getORDER(0);
                    // ORC-2 SHALL NOT be populated in OML_O33 messages issued by the Analyzer Mgr
                    populateORCv25(orderGroup.getORC(), order, !sendingActorKeyword.equals(ANALYZER_MGR), true);
                    populateTQ1v25(orderGroup.getTIMING(0).getTQ1(), order);
                    // OBSERVATION REQUEST begin
                    populateOBRv25(orderGroup.getOBSERVATION_REQUEST().getOBR(), order, true,
                            !sendingActorKeyword.equals(ANALYZER_MGR));
                    // OBSERVATION REQUEST end
                    // ORDER end
                    // SPECIMEN end
                    specimenIndex++;
                }
            }
            // Order has been received in a OML_O35 message
            else if (order.getContainer() != null) {
                specimen = order.getContainer().getSpecimen();
                // SPECIMEN begin
                populateSPMv25(omlMessage.getSPECIMEN(0).getSPM(), specimen, 1);
                populateSACv25(omlMessage.getSPECIMEN(0).getSAC(0), order.getContainer());
                // ORDER begin
                OML_O33_ORDER orderGroup = omlMessage.getSPECIMEN(0).getORDER(0);
                populateORCv25(orderGroup.getORC(), order, !sendingActorKeyword.equals(ANALYZER_MGR), true);
                populateTQ1v25(orderGroup.getTIMING(0).getTQ1(), order);
                // OBSERVATION REQUEST begin
                populateOBRv25(orderGroup.getOBSERVATION_REQUEST().getOBR(), order, true,
                        !sendingActorKeyword.equals(ANALYZER_MGR));
                // OBSERATION REQUEST end
                // ORDER end
                // SPECIMEN end
            }
        }
        return omlMessage;
    }

    /**
     * builds the OML^O21^OML_O21 message according the specifications from TF-LAB vol.2 (4.5.3) Rev 3.0 Final Text 2011-05-19
     *
     * @return
     * @throws HL7Exception
     */
    private Message buildO21() throws HL7Exception {
        OML_O21 omlMessage = new OML_O21();
        // MSH
        populateMSHv25(omlMessage.getMSH(), messageStructure);
        // PATIENT begin
        populatePIDv25(omlMessage.getPATIENT().getPID());
        populatePV1v25(omlMessage.getPATIENT().getPATIENT_VISIT().getPV1());
        // PATIENT end
        // ORDER begin
        populateORCv25(omlMessage.getORDER(0).getORC(), order, order.getPlacerOrderNumber() != null,
                order.getFillerOrderNumber() != null);
        populateTQ1v25(omlMessage.getORDER(0).getTIMING(0).getTQ1(), order);
        // OBSERVATION REQUEST begin
        populateOBRv25(omlMessage.getORDER(0).getOBSERVATION_REQUEST().getOBR(), order,
                order.getPlacerOrderNumber() != null, order.getFillerOrderNumber() != null);
        if ((order.getSpecimens() != null) && !order.getSpecimens().isEmpty()) {
            for (int index = 0; index < order.getSpecimens().size(); index++) {
                // SPECIMEN begin
                Specimen currentSpecimen = order.getSpecimens().get(index);
                OML_O21_SPECIMEN specimenGroup = omlMessage.getORDER(0).getOBSERVATION_REQUEST().getSPECIMEN(index);
                populateSPMv25(specimenGroup.getSPM(), currentSpecimen, index + 1);
                if ((currentSpecimen.getContainers() != null) && !currentSpecimen.getContainers().isEmpty()) {
                    for (int cIndex = 0; cIndex < currentSpecimen.getContainers().size(); cIndex++) {
                        populateSACv25(specimenGroup.getCONTAINER(cIndex).getSAC(),
                                currentSpecimen.getContainers().get(cIndex));
                    }
                }
                // SPECIMEN end
            }
        }
        // OBSERVATION REQUEST end
        // ORDER end
        return omlMessage;
    }

    private Message buildPAT2Message() {
        // TODO Auto-generated method stub
        return null;
    }

    private Message buildPAT1Message() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * fills the MSH segment of the HL7v2.5 messages
     *
     * @param mshSegment
     * @param messageType
     * @throws HL7Exception
     */
    private void populateMSHv25(MSH mshSegment, String messageType) throws HL7Exception {
        mshSegment.getFieldSeparator().setValue("|");
        mshSegment.getEncodingCharacters().setValue("^~\\&");
        mshSegment.getSendingApplication().getNamespaceID().setValue(sendingApplication);
        mshSegment.getSendingFacility().getNamespaceID().setValue(sendingFacility);
        mshSegment.getReceivingApplication().getNamespaceID().setValue(sut.getApplication());
        mshSegment.getReceivingFacility().getNamespaceID().setValue(sut.getFacility());
        mshSegment.getDateTimeOfMessage().getTime().setValue(sdf.format(new Date()));
        mshSegment.getMessageType().parse(messageType);
        mshSegment.getProcessingID().getProcessingID().setValue("P");
        mshSegment.getMessageControlID().setValue(sdf.format(new Date()));
        mshSegment.getVersionID().getVersionID().setValue("2.5");
        mshSegment.getMessageProfileIdentifier(0).getEi1_EntityIdentifier().setValue(transactionKeyword);
        mshSegment.getMessageProfileIdentifier(0).getEi2_NamespaceID().setValue("IHE");
        if (sut.getCharset() != null) {
            mshSegment.getCharacterSet(0).setValue(sut.getCharset().getHl7Code());
        }
    }

    /**
     * fills the PID segment of the HL7v2.5 messages
     *
     * @param pidSegment
     * @throws HL7Exception
     */
    private void populatePIDv25(PID pidSegment) throws HL7Exception {
        if (encounter != null) {
            Patient patient = encounter.getPatient();
            List<PatientIdentifier> identifiers = PatientIdentifier.getIdentifiersForPatient(patient);

            if ((identifiers != null) && (identifiers.size() > 0)) {
                int index = 0;
                for (PatientIdentifier pid : identifiers) {
                    pidSegment.getPatientIdentifierList(index).parse(pid.getIdentifier());
                    pidSegment.getPatientIdentifierList(index).getIdentifierTypeCode()
                            .setValue(pid.getIdentifierTypeCode());
                    index++;
                }
            }
            pidSegment.getPatientName(0).getFamilyName().getSurname().setValue(patient.getLastName());
            pidSegment.getPatientName(0).getGivenName().setValue(patient.getFirstName());
            pidSegment.getPatientName(0).getNameTypeCode().setValue("L");
            pidSegment.getMotherSMaidenName(0).getFamilyName().getSurname().setValue(patient.getMotherMaidenName());
            if (patient.getMotherMaidenName() != null) {
                pidSegment.getMotherSMaidenName(0).getNameTypeCode().setValue("L");
            }
            if (patient.getDateOfBirth() != null) {
                pidSegment.getDateTimeOfBirth().getTime().setValue(sdf.format(patient.getDateOfBirth()));
            }
            pidSegment.getAdministrativeSex().setValue(patient.getGenderCode());
            if (patient.getRaceCode() != null) {
                pidSegment.getRace(0).getIdentifier()
                        .parse(ValueSetProvider.getInstance().buildCEElementFromCode(patient.getRaceCode(), "race"));
            }
            pidSegment.getPatientAddress(0).getStreetAddress().getStreetOrMailingAddress()
                    .setValue(patient.getStreet());
            pidSegment.getPatientAddress(0).getCity().setValue(patient.getCity());
            pidSegment.getPatientAddress(0).getStateOrProvince().setValue(patient.getState());
            pidSegment.getPatientAddress(0).getZipOrPostalCode().setValue(patient.getZipCode());
            pidSegment.getPatientAddress(0).getCountry().setValue(patient.getCountryCode());
            if (patient.getReligionCode() != null) {
                pidSegment.getReligion().parse(ValueSetProvider.getInstance()
                        .buildCEElementFromCode(patient.getReligionCode(), "religion"));
            }
            pidSegment.getIdentityUnknownIndicator().setValue("N");
        } else {
            log.debug("No encounter provided");
        }
    }

    /**
     * fills the PV1 segment of the HL7v2.5 messages
     *
     * @param pv1Segment
     * @throws HL7Exception
     */
    private void populatePV1v25(PV1 pv1Segment) throws HL7Exception {
        if (encounter != null) {
            pv1Segment.getVisitNumber().parse(encounter.getVisitNumber());
            pv1Segment.getVisitIndicator().setValue("V");
            pv1Segment.getPatientClass().setValue(encounter.getPatientClassCode());
            if ((encounter.getReferringDoctorCode() != null) && !encounter.getReferringDoctorCode().isEmpty()) {
                String doctorName = encounter.getReferringDoctorCode().concat("^")
                        .concat(encounter.getReferringDoctor());
                pv1Segment.getReferringDoctor(0).parse(doctorName);
            }
        } else {
            log.debug("No encounter provided");
        }
    }

    /**
     * fills the ORC segment of the HL7v2.5 messages
     *
     * @param orcSegment
     * @param valuePlacerNumber indicates whether the placer Order Number field should be set or not
     * @param valueFillerNumber indicates whether the filler Order Number field should be set or not
     * @throws HL7Exception
     */
    private void populateORCv25(ORC orcSegment, LabOrder currentOrder, boolean valuePlacerNumber,
                                boolean valueFillerNumber) throws HL7Exception {
        if (currentOrder.getOrderControlCode() != null && currentOrder.getOrderControlCode().equals("UA")) { // that means that we are sending again a
            // WOS order which was not accepted the first time
            orcSegment.getOrderControl().setValue("NW");
        } else {
            orcSegment.getOrderControl().setValue(currentOrder.getOrderControlCode());
        }
        if (valuePlacerNumber) {
            orcSegment.getPlacerOrderNumber().parse(currentOrder.getPlacerOrderNumber());
        }
        // ORC-3
        if (valueFillerNumber) {
            orcSegment.getFillerOrderNumber().parse(currentOrder.getFillerOrderNumber());
        }
        if (currentOrder.getPlacerGroupNumber() != null) {
            orcSegment.getPlacerGroupNumber().parse(currentOrder.getPlacerGroupNumber());
        }
        if (sendingActorKeyword.equals(OF_KEYWORD) || sendingActorKeyword.equals(ANALYZER)) {
            orcSegment.getOrderStatus().setValue(currentOrder.getOrderStatus());
            if (currentOrder.getExpectedAvailabilityDateTime() != null) {
                orcSegment.getFillerSExpectedAvailabilityDateTime().getTime()
                        .setValue(sdf.format(currentOrder.getExpectedAvailabilityDateTime()));
            }
        }
        if (!sendingActorKeyword.equals(ANALYZER) && currentOrder.getTransactionDate() != null) {
            orcSegment.getDateTimeOfTransaction().getTime().setValue(sdf.format(currentOrder.getTransactionDate()));
        }
        String xcnName;
        if ((currentOrder.getEnteredBy() != null) && !currentOrder.getEnteredBy().isEmpty()) {
            xcnName = ValueSet.getDisplayNameForCode("doctor", currentOrder.getEnteredBy());
            if ((xcnName != null) && !xcnName.equals(currentOrder.getEnteredBy())) {
                orcSegment.getEnteredBy(0).parse(currentOrder.getEnteredBy().concat("^").concat(xcnName));
            } else {
                orcSegment.getEnteredBy(0).getIDNumber().setValue(currentOrder.getEnteredBy());
            }
        }
        if ((currentOrder.getEnteringOrganization() != null) && !currentOrder.getEnteringOrganization().isEmpty()) {
            orcSegment.getEnteringOrganization().parse(ValueSetProvider.getInstance()
                    .buildCEElementFromCode(currentOrder.getEnteringOrganization(), "enteringOrganization"));
        }
        if ((currentOrder.getOrderingProvider() != null) && !currentOrder.getOrderingProvider().isEmpty()) {
            xcnName = ValueSet.getDisplayNameForCode("doctor", currentOrder.getOrderingProvider());
            if ((xcnName != null) && !xcnName.equals(currentOrder.getOrderingProvider())) {
                orcSegment.getOrderingProvider(0).parse(currentOrder.getOrderingProvider().concat("^").concat(xcnName));
            } else {
                orcSegment.getOrderingProvider(0).getIDNumber().setValue(currentOrder.getOrderingProvider());
            }
        }
        if ((encounter != null) && (encounter.getPatientClassCode() != null)) {
            if (encounter.getPatientClassCode().equals("I")) {
                orcSegment.getOrderType().getIdentifier().setValue("I");
            } else if (encounter.getPatientClassCode().equals("O")) {
                orcSegment.getOrderType().getIdentifier().setValue("O");
            }
        }
        if (!sendingActorKeyword.equals(ANALYZER_MGR) && (options == null || options.contains(LAWOptions.LAW_REFLEX))
                && (currentOrder.getParent() != null && !currentOrder.getParent().isEmpty())) {
            orcSegment.getParentOrder().parse(currentOrder.getParent().get(0));
        }
    }

    /**
     * @param obxSegment
     * @param observation
     * @throws HL7Exception
     */
    private void populateOBXv25(OBX obxSegment, Observation observation) throws HL7Exception {
        observation = entityManager.find(Observation.class, observation.getId());
        obxSegment.getSetIDOBX().setValue(observation.getSetId().toString());
        if ((observation.getValueType() != null) && !observation.getValueType().isEmpty()) {
            obxSegment.getValueType().setValue(observation.getValueType());
            obxSegment.getObservationValue(0).parse(observation.getValue());
        }

        if (observation.getIdentifier() != null) {
            // valueSetId shall be changed to observationIdentifier as soon as values are available
            Concept identifier = null;
            if (observation.getOrder() != null) {
                identifier = ValueSet.getConceptForCode("orderObservationIdentifier", observation.getIdentifier());
            } else {
                identifier = ValueSet.getConceptForCode("specimenObservationIdentifier", observation.getIdentifier());
            }
            obxSegment.getObservationIdentifier().getIdentifier().setValue(observation.getIdentifier());
            if (identifier != null) {
                obxSegment.getObservationIdentifier().getText().setValue(identifier.getDisplayName());
                obxSegment.getObservationIdentifier().getNameOfCodingSystem().setValue(identifier.getCodeSystem());
            }
        }
        obxSegment.getObservationSubID().setValue(observation.getSubID());
        obxSegment.getUnits()
                .parse(ValueSetProvider.getInstance().buildCEElementFromCode(observation.getUnits(), "units"));
        obxSegment.getReferencesRange().setValue(observation.getReferencesRange());
        if ((observation.getAbnormalFlags() != null) && !observation.getAbnormalFlags().isEmpty()) {
            int index = 0;
            for (String flag : observation.getAbnormalFlags()) {
                obxSegment.getAbnormalFlags(index)
                        .parse(ValueSetProvider.getInstance().buildCEElementFromCode(flag, "abnormalFlag"));
                index++;
            }
        }
        obxSegment.getObservationResultStatus().setValue(observation.getResultStatus());
        obxSegment.getUserDefinedAccessChecks().setValue(observation.getAccessChecks());
        if (!sendingActorKeyword.equals(ANALYZER) && (observation.getObservationDateTime() != null)) {
            obxSegment.getDateTimeOfTheObservation().getTime()
                    .setValue(sdf.format(observation.getObservationDateTime()));
        }
        if (observation.getProducerId() != null) {
            obxSegment.getProducerSID().parse(observation.getProducerId());
        }
        String xcnName = ValueSet.getDisplayNameForCode("doctor", observation.getResponsibleObserver());
        if ((xcnName != null) && !xcnName.equals(observation.getResponsibleObserver())) {
            obxSegment.getResponsibleObserver(0)
                    .parse(observation.getResponsibleObserver().concat("^").concat(xcnName));
        } else {
            obxSegment.getResponsibleObserver(0).parse(observation.getResponsibleObserver());
        }
        if (observation.getObservationMethod() != null) {
            obxSegment.getObservationMethod(0).parse(observation.getObservationMethod());
        }
        if ((observation.getEquipment() != null) && !observation.getEquipment().isEmpty()) {
            String[] equipments = observation.getEquipment().split("~");
            int index = 0;
            for (String equipment : equipments) {
                obxSegment.getEquipmentInstanceIdentifier(index).parse(equipment);
                index++;
            }
        }
        if (observation.getAnalysisDateTime() != null) {
            obxSegment.getDateTimeOfTheAnalysis().getTime().setValue(sdf.format(observation.getAnalysisDateTime()));
        }
        if (observation.getObservationType() != null) {
            obxSegment.getField(29, 0).parse(observation.getObservationType());
        }
    }

    /**
     * @param tq1Segment
     * @param currentOrder
     * @throws HL7Exception
     */
    private void populateTQ1v25(TQ1 tq1Segment, LabOrder currentOrder) throws HL7Exception {
        if (currentOrder.getStartDateTime() != null) {
            tq1Segment.getStartDateTime().getTime().setValue(sdf.format(currentOrder.getStartDateTime()));
        }
        tq1Segment.getPriority(0).parse(ValueSetProvider.getInstance()
                .buildCEElementFromCode(currentOrder.getQuantityTiming(), "quantityTiming"));
    }

    /**
     * fills the OBR segment of the HL7v2.5 messages
     *
     * @param obrSegment
     * @throws HL7Exception
     */
    private void populateOBRv25(OBR obrSegment, LabOrder currentOrder, boolean valuePlacerNumber,
                                boolean valueFillerNumber) throws HL7Exception {

        if (valuePlacerNumber) {
            obrSegment.getPlacerOrderNumber().parse(currentOrder.getPlacerOrderNumber());
        }
        if (valueFillerNumber) {
            obrSegment.getFillerOrderNumber().parse(currentOrder.getFillerOrderNumber());
        }
        obrSegment.getUniversalServiceIdentifier().parse(ValueSetProvider.getInstance()
                .buildCEElementFromCode(currentOrder.getUniversalServiceId(), "labUniversalServiceId"));
        String xcnName;
        if (!sendingActorKeyword.equals(ANALYZER) && currentOrder.getOrderingProvider() != null && !currentOrder
                .getOrderingProvider().isEmpty()) {
            xcnName = ValueSet.getDisplayNameForCode("doctor", currentOrder.getOrderingProvider());
            if ((xcnName != null) && !xcnName.equals(currentOrder.getOrderingProvider())) {
                obrSegment.getOrderingProvider(0).parse(currentOrder.getOrderingProvider().concat("^").concat(xcnName));
            } else {
                obrSegment.getOrderingProvider(0).getIDNumber().setValue(currentOrder.getOrderingProvider());
            }
        }
        if (currentOrder.getCallBackPhoneNumber() != null) {
            obrSegment.getOrderCallbackPhoneNumber(0).getTelephoneNumber()
                    .setValue(currentOrder.getCallBackPhoneNumber());
        }
        if (getActorKeywordForDiagService().contains(sendingActorKeyword)) {
            obrSegment.getDiagnosticServSectID().setValue(currentOrder.getDiagnosticServiceSectionID());
            // this field is only sent by OF in LAB-1 and by AM in LAB-5 and by ANALYZER and LB
            if (currentOrder.getOrderResultStatus() != null) {
                obrSegment.getResultStatus().setValue(currentOrder.getOrderResultStatus());
            }
        }
        if ((currentOrder.getResultCopiesTo() != null) && !currentOrder.getResultCopiesTo().isEmpty()) {
            xcnName = ValueSet.getDisplayNameForCode("doctor", currentOrder.getResultCopiesTo());
            if ((xcnName != null) && !xcnName.equals(currentOrder.getOrderingProvider())) {
                obrSegment.getResultCopiesTo(0).parse(currentOrder.getOrderingProvider().concat("^").concat(xcnName));
            } else {
                obrSegment.getResultCopiesTo(0).getIDNumber().setValue(currentOrder.getOrderingProvider());
            }
        }
    }

    private static List<String> getActorKeywordForDiagService(){
        return Arrays.asList(OF_KEYWORD, AM, ANALYZER, ANALYZER_MGR, LB);
    }

    /**
     * fills the SPM segment of the HL7v2.5 messages
     *
     * @param spmSegment
     * @throws HL7Exception
     */
    private void populateSPMv25(SPM spmSegment, Specimen currentSpecimen, int index) throws HL7Exception {
        spmSegment.getSetIDSPM().setValue(Integer.toString(index));
        String eip = "";
        if (!(sendingActorKeyword.equals(ANALYZER) || sendingActorKeyword.equals(ANALYZER_MGR)) || (options != null && (
                options.contains(LAWOptions.LAW_SPECIMEN) || options.contains(LAWOptions.LAW_POOL_NOAN)))) {
            if (currentSpecimen.getPlacerAssignedIdentifier() != null) {
                eip = currentSpecimen.getPlacerAssignedIdentifier();
            }
            if (currentSpecimen.getFillerAssignedIdentifier() != null) {
                eip = eip.concat("^").concat(currentSpecimen.getFillerAssignedIdentifier());
            }
        }
        spmSegment.getSpecimenID().parse(eip);
        if (currentSpecimen.getType() != null) {
            spmSegment.getSpecimenType().parse(ValueSetProvider.getInstance()
                    .buildCEElementFromCode(currentSpecimen.getType(), "specimenType"));
        }
        if (!sendingActorKeyword.equals(ANALYZER) && (!sendingActorKeyword.equals(ANALYZER_MGR) || (options != null && (
                options.contains(LAWOptions.LAW_SPECIMEN) || options.contains(LAWOptions.LAW_POOL_NOAN))))) {
            if (currentSpecimen.getCollectionMethod() != null) {
                spmSegment.getSpecimenCollectionMethod().parse(ValueSetProvider.getInstance()
                        .buildCEElementFromCode(currentSpecimen.getCollectionMethod(), "collectionMethod"));
            }
            if (currentSpecimen.getSourceSite() != null) {
                spmSegment.getSpecimenSourceSite().parse(currentSpecimen.getSourceSite());
            }
            if ((currentSpecimen.getSourceSiteModifier() != null) && !currentSpecimen.getSourceSiteModifier()
                    .equals("null")) {
                spmSegment.getSpecimenSourceSiteModifier(0).getIdentifier()
                        .setValue(currentSpecimen.getSourceSiteModifier());
            }
            if (currentSpecimen.getRiskCode() != null) {
                spmSegment.getSpecimenRiskCode(0).parse(ValueSetProvider.getInstance()
                        .buildCEElementFromCode(currentSpecimen.getRiskCode(), "riskCode"));
            }
            if (currentSpecimen.getCollectionDateTime() != null) {
                spmSegment.getSpecimenCollectionDateTime().getRangeStartDateTime().getTime()
                        .setValue(sdf.format(currentSpecimen.getCollectionDateTime()));
            }
            if (currentSpecimen.getReceivedDateTime() != null) {
                spmSegment.getSpecimenReceivedDateTime().getTime()
                        .setValue(sdf.format(currentSpecimen.getReceivedDateTime()));
            }
        }
        if (currentSpecimen.getRole() != null) {
            if (sendingActorKeyword.equals(ANALYZER_MGR) || sendingActorKeyword.equals(ANALYZER)) {
                LAWSpecimenRole role = LAWSpecimenRole.valueOf(currentSpecimen.getRole());
                if (role != null) {
                    spmSegment.getSpecimenRole(0).getIdentifier().setValue(role.getCode());
                    spmSegment.getSpecimenRole(0).getText().setValue(role.getLabel());
                    spmSegment.getSpecimenRole(0).getNameOfCodingSystem().setValue(role.getCodingSystem());
                }
            } else {
                spmSegment.getSpecimenRole(0).parse(ValueSetProvider.getInstance()
                        .buildCEElementFromCode(currentSpecimen.getRole(), "specimenRole"));
            }
        }
        if (currentSpecimen.isAvailable()) {
            spmSegment.getSpecimenAvailability().setValue("Y");
        } else {
            spmSegment.getSpecimenAvailability().setValue("N");
        }
        if (currentSpecimen.getContainers() != null) {
            spmSegment.getNumberOfSpecimenContainers()
                    .setValue(Integer.toString(currentSpecimen.getContainers().size()));
        } else {
            spmSegment.getNumberOfSpecimenContainers().setValue("0");
        }
        if (options != null && options.contains(LAWOptions.LAW_POOL_NOAN)
                && currentSpecimen.getGroupedSpecimenCount() != null) {
            spmSegment.getGroupedSpecimenCount().setValue(currentSpecimen.getGroupedSpecimenCount().toString());
        }
    }

    /**
     * fills the SAC segment of the HL7v2.5 messages
     *
     * @param sacSegment
     * @throws HL7Exception
     */
    private void populateSACv25(SAC sacSegment, Container container) throws HL7Exception {
        if ((container.getPrimaryContainerIdentifier() != null) && !container.getPrimaryContainerIdentifier()
                .isEmpty()) {
            sacSegment.getSac4_PrimaryParentContainerIdentifier().parse(container.getPrimaryContainerIdentifier());
        }
        if ((container.getContainerIdentifier() != null) && !container.getContainerIdentifier().isEmpty()) {
            sacSegment.getSac3_ContainerIdentifier().parse(container.getContainerIdentifier());
        }
        if ((container.getCarrierIdentifier() != null) && !container.getCarrierIdentifier().isEmpty()) {
            sacSegment.getSac10_CarrierIdentifier().parse(container.getCarrierIdentifier());
        }
        if ((container.getPositionInCarrier() != null) && !container.getPositionInCarrier().isEmpty()) {
            sacSegment.getSac11_PositionInCarrier().parse(container.getPositionInCarrier());
        }
        if ((container.getTrayIdentifier() != null) && !container.getTrayIdentifier().isEmpty()) {
            sacSegment.getSac13_TrayIdentifier().parse(container.getTrayIdentifier());
        }
        if ((container.getPositionInTray() != null) && !container.getPositionInTray().isEmpty()) {
            sacSegment.getSac14_PositionInTray().parse(container.getPositionInTray());
        }
        if ((container.getLocation() != null) && !container.getLocation().isEmpty()) {
            sacSegment.getSac15_Location(0).parse(container.getLocation());
        }
        if (container.getCapType() != null && !container.getCapType().isEmpty()) {
            sacSegment.getCapType().parse(container.getCapType());
        }
    }

    public static Message buildNegativeQueryResponse(HL7V2ResponderSUTConfiguration sut, WOSQueryContent queryContent)
            throws HL7Exception {
        OML_O33 omlMessage = new OML_O33();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        fillMSHSegment(omlMessage.getMSH(), sut.getApplication(), sut.getFacility(), "OM_LAB_ANALYZER_MGR", "IHE",
                "OML", "O33", "OML_O33", "2.5.1", sdf, sut.getCharset().getHl7Code());
        omlMessage.getMSH().getAcceptAcknowledgmentType().setValue("ER");
        omlMessage.getMSH().getApplicationAcknowledgmentType().setValue("AL");
        omlMessage.getMSH().getMessageProfileIdentifier(0).getEi1_EntityIdentifier().setValue("LAB-28");
        omlMessage.getMSH().getMessageProfileIdentifier(0).getEi2_NamespaceID().setValue("IHE");
        // SPM
        omlMessage.getSPECIMEN(0).getSPM().getSpm1_SetIDSPM().setValue("1");
        omlMessage.getSPECIMEN(0).getSPM().getSpm4_SpecimenType().getCwe1_Identifier().setValue("\"\"");
        omlMessage.getSPECIMEN(0).getSPM().getSpm11_SpecimenRole(0).getCwe1_Identifier().setValue("U");
        // SAC
        if (queryContent.getQueryType().equals("WOS_ALL")) {
            omlMessage.getSPECIMEN(0).getSAC(0).getSac3_ContainerIdentifier().getEi1_EntityIdentifier()
                    .setValue("\"\"");
        } else {
            if ((queryContent.getContainerId() != null) && !queryContent.getContainerId().isEmpty()) {
                omlMessage.getSPECIMEN(0).getSAC(0).getSac3_ContainerIdentifier().parse(queryContent.getContainerId());
            }
            if ((queryContent.getCarrierId() != null) && !queryContent.getCarrierId().isEmpty()) {
                omlMessage.getSPECIMEN(0).getSAC(0).getSac10_CarrierIdentifier().parse(queryContent.getCarrierId());
            }
            if ((queryContent.getPositionInCarrier() != null) && !queryContent.getPositionInCarrier().isEmpty()) {
                omlMessage.getSPECIMEN(0).getSAC(0).getSac11_PositionInCarrier()
                        .parse(queryContent.getPositionInCarrier());
            }
            if ((queryContent.getTrayId() != null) && !queryContent.getTrayId().isEmpty()) {
                omlMessage.getSPECIMEN(0).getSAC(0).getSac13_TrayIdentifier().parse(queryContent.getTrayId());
            }
            if ((queryContent.getPositionInTray() != null) && !queryContent.getPositionInTray().isEmpty()) {
                omlMessage.getSPECIMEN(0).getSAC(0).getSac14_PositionInTray().parse(queryContent.getPositionInTray());
            }
            if ((queryContent.getLocation() != null) && !queryContent.getLocation().isEmpty()) {
                omlMessage.getSPECIMEN(0).getSAC(0).getSac15_Location(0).parse(queryContent.getLocation());
            }
        }
        omlMessage.getSPECIMEN(0).getORDER(0).getORC().getOrc1_OrderControl().setValue("DC");
        omlMessage.getSPECIMEN(0).getORDER(0).getORC().getOrc9_DateTimeOfTransaction().getTs1_Time()
                .setValue(sdf.format(new Date()));
        return omlMessage;
    }

    /**
     * GETTERS and SETTERS
     */
    public String getSendingActorKeyword() {
        return sendingActorKeyword;
    }

    public void setSendingActorKeyword(String sendingActorKeyword) {
        this.sendingActorKeyword = sendingActorKeyword;
    }

    /**
     * @param transactionKeyword the transactionKeyword to set
     */
    public void setTransactionKeyword(String transactionKeyword) {
        this.transactionKeyword = transactionKeyword;
    }

    /**
     * @return the transactionKeyword
     */
    public String getTransactionKeyword() {
        return transactionKeyword;
    }

    public HL7V2ResponderSUTConfiguration getSut() {
        return sut;
    }

    public void setSut(HL7V2ResponderSUTConfiguration sut) {
        this.sut = sut;
    }

    public String getSendingApplication() {
        return sendingApplication;
    }

    public void setSendingApplication(String sendingApplication) {
        this.sendingApplication = sendingApplication;
    }

    public String getSendingFacility() {
        return sendingFacility;
    }

    public void setSendingFacility(String sendingFacility) {
        this.sendingFacility = sendingFacility;
    }

    public LabOrder getOrder() {
        return order;
    }

    public void setOrder(LabOrder order) {
        this.order = order;
    }

    public Specimen getSpecimen() {
        return specimen;
    }

    public void setSpecimen(Specimen specimen) {
        this.specimen = specimen;
    }

    public void setMessageStructure(String messageStructure) {
        this.messageStructure = messageStructure;
    }

    public String getMessageStructure() {
        return messageStructure;
    }

    public void setEncounter(Encounter encounter) {
        this.encounter = encounter;
    }

    public Encounter getEncounter() {
        return encounter;
    }

    public RSP_K11 buildLAB62Response(List<Specimen> specimens, QPD qpdSegment,
                                      net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.segment.MSH queryHeader) throws HL7Exception {
        Lifecycle.beginCall();
        RSP_K11 rsp = new RSP_K11();
        // use an intermediate OML message to be able to use already defined methods
        OML_O33 oml = new OML_O33();
        // fill MSH
        String receivingApp = queryHeader.getSendingApplication().getNamespaceID().getValue();
        String receivingFac = queryHeader.getSendingFacility().getNamespaceID().getValue();
        String charset = queryHeader.getCharacterSet().getValue();
        String messageControlId = queryHeader.getMessageControlID().getValue();
        fillMSHSegment(oml.getMSH(), receivingApp, receivingFac, sendingApplication, sendingFacility, "RSP", "SLI",
                "RSP_K11", "2.5", new SimpleDateFormat("yyyyMMddHHmmss"), charset);
        String msh = oml.getMSH().encode();
        rsp.getMSH().getFieldSeparator().setValue("|");
        rsp.getMSH().getEncodingCharacters().setValue("^~\\&");
        rsp.getMSH().parse(msh);
        // fill MSA
        rsp.getMSA().getAcknowledgmentCode().setValue("AA");
        rsp.getMSA().getMessageControlID().setValue(messageControlId);
        // copy QPD from query
        String qpd = qpdSegment.encode();
        rsp.getQPD().parse(qpd);
        // fill QAK
        rsp.getQAK().getQueryTag().setValue(qpdSegment.getQueryTag().getValue());
        rsp.getQAK().getMessageQueryName().parse(qpdSegment.getMessageQueryName().encode());
        if (specimens == null || specimens.isEmpty()) {
            rsp.getQAK().getQueryResponseStatus().setValue("NF");
        } else {
            rsp.getQAK().getQueryResponseStatus().setValue("OK");
            // PID
            populatePIDv25(oml.getPATIENT().getPID());
            String pid = oml.getPATIENT().getPID().encode();
            rsp.getPATIENT().getPID().parse(pid);
            // PV1
            populatePV1v25(oml.getPATIENT().getPATIENT_VISIT().getPV1());
            String pv1 = oml.getPATIENT().getPATIENT_VISIT().getPV1().encode();
            rsp.getPATIENT().getPATIENT_VISIT().getPV1().parse(pv1);
            int spmIndex = 0;
            for (Specimen specimen : specimens) {
                fillSPECIMENInRSPK11(oml, rsp, specimen, spmIndex);
                spmIndex++;
            }
        }
        Lifecycle.endCall();
        return rsp;
    }

    private void fillSPECIMENInRSPK11(OML_O33 oml, RSP_K11 rsp, Specimen specimen, int spmIndex) throws HL7Exception {
        SPM spm = oml.getSPECIMEN(spmIndex).getSPM();
        populateSPMv25(spm, specimen, spmIndex);
        rsp.getPATIENT().getSPECIMEN(spmIndex).getSPM().parse(spm.encode());
        if (specimen.getContainers() != null) {
            int sacIndex = 0;
            for (Container container : specimen.getContainers()) {
                fillSACInRSPK11(oml, rsp, container, spmIndex, sacIndex);
                sacIndex++;
            }
        }
        if (specimen.getOrders() != null) {
            int orcIndex = 0;
            for (LabOrder order : specimen.getOrders()) {
                fillORDERInRSPK11(oml, rsp, order, spmIndex, orcIndex);
                orcIndex++;
            }
        }
    }

    private void fillSACInRSPK11(OML_O33 oml, RSP_K11 rsp, Container container, int spmIndex, int sacIndex) throws HL7Exception{
        SAC sac = oml.getSPECIMEN(spmIndex).getSAC(sacIndex);
        populateSACv25(sac, container);
        rsp.getPATIENT().getSPECIMEN(spmIndex).getSAC(sacIndex).parse(sac.encode());
    }

    private void fillORDERInRSPK11(OML_O33 oml, RSP_K11 rsp, LabOrder order, int spmIndex, int orcIndex) throws HL7Exception{
        OML_O33_ORDER orderGroup = oml.getSPECIMEN(spmIndex).getORDER(orcIndex);
        RSP_K11_ORDER k11group = rsp.getPATIENT().getSPECIMEN(spmIndex).getORDER(orcIndex);
        ORC orc = orderGroup.getORC();
        populateORCv25(orc, order, true, true);
        k11group.getORC().parse(orc.encode());
        OBR obr = orderGroup.getOBSERVATION_REQUEST().getOBR();
        populateOBRv25(obr, order, true, true);
        k11group.getOBSERVATION_REQUEST().getOBR().parse(obr.encode());
        TQ1 tq1 = orderGroup.getTIMING().getTQ1();
        populateTQ1v25(tq1, order);
        k11group.getTIMING().getTQ1().parse(tq1.encode());
    }
}
