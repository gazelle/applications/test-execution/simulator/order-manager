package net.ihe.gazelle.ordermanager.hl7.initiators;

import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import net.ihe.gazelle.ordermanager.utils.LabMessageBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("workOrderSender")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 30000)
public class WorkOrderSender implements Serializable, UserAttributeCommon {

	
	private static Logger log = LoggerFactory.getLogger(WorkOrderSender.class);
	private static final long serialVersionUID = 1L;

	private LabOrder selectedWorkOrder;
	private Specimen selectedSpecimen;
	private String messageStructure;
	private Integer selectedContainerId;
	private List<TransactionInstance> hl7Messages;
	private String username;
    private List<HL7V2ResponderSUTConfiguration> availableSuts;

	public static final String BATTERY_CENTRIC = "OML^O21^OML_O21";
	public static final String SPECIMEN_CENTRIC = "OML^O33^OML_O33";
	public static final String CONTAINER_CENTRIC = "OML^O35^OML_O35";
	private static final String APPLICATION = "OM_LAB_OF";
	private static final String FACILITY = "IHE";
	private static final String SIMU_ACTOR = "OF";
	private static final String TRANSACTION = "LAB-4";
	private static final String SUT_ACTOR = "AM";

	@In(value="gumUserService")
	private UserService userService;

    @Create
	public void initializePage() {
		log.info("initialize page");
		UserPreferences preferences = (UserPreferences) Component.getInstance("userPreferences");
		username = preferences.getUsername();
		hl7Messages = null;
		Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		selectedSpecimen = null;
		selectedWorkOrder = null;
		if ((urlParams != null) && !urlParams.isEmpty()) {
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			if (urlParams.containsKey("orderId")) {
				try {
					Integer orderId = Integer.valueOf(urlParams.get("orderId"));
					LabOrder order = entityManager.find(LabOrder.class, orderId);
					if (order != null) {
						convertOrderToWorkOrder(order);
						messageStructure = BATTERY_CENTRIC;
					} else {
						log.error("No lab order found for the given identifier");
						FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No lab order found for the given identifier");
					}
				} catch (NumberFormatException e) {
					log.error("Number Format Exception: " + urlParams.containsKey("orderId")
							+ " is not a good identifier, a positive integer is required");
					FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "Number Format Exception: " + urlParams.containsKey("orderId")
									+ " is not a good identifier, a positive integer is required");
				}
			} else if (urlParams.containsKey("specimenId")) {
				try {
					Integer specimenId = Integer.valueOf(urlParams.get("specimenId"));
					Specimen specimen = entityManager.find(Specimen.class, specimenId);
					if (specimen != null) {
						if ((specimen.getOrders() != null) && !specimen.getOrders().isEmpty()) {
							messageStructure = SPECIMEN_CENTRIC;
						} else {
							messageStructure = CONTAINER_CENTRIC;
						}
						convertSpecimenToWorkOrder(specimen);
					} else {
						log.error("No specimen found for the given identifier");
						FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No specimen found for the given identifier");
					}
				} catch (NumberFormatException e) {
					log.error("Number Format Exception: " + urlParams.containsKey("specimenId")
							+ " is not a good identifier, a positive integer is required");
					FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "Number Format Exception: " + urlParams.containsKey("specimenId")
									+ " is not a good identifier, a positive integer is required");
				}
			} else {
				log.error("This page requires either an orderId param or a specimenId param");
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "This page requires either an orderId param or a specimenId param");
			}
		} else {
			log.error("This page requires either an orderId param or a specimenId param");
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "This page requires either an orderId param or a specimenId param");
		}
	}

	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}

	private void convertSpecimenToWorkOrder(Specimen specimen) {
		selectedSpecimen = new Specimen(true, specimen);
		if ((specimen.getOrders() != null) && !specimen.getOrders().isEmpty()) {
			List<LabOrder> orders = new ArrayList<LabOrder>();
			for (LabOrder order : specimen.getOrders()) {
				LabOrder workOrder = new LabOrder(order, false);
				workOrder.setCreator(null);
				workOrder.setSpecimens(new ArrayList<Specimen>());
				workOrder.getSpecimens().add(selectedSpecimen);
				orders.add(workOrder);
			}
			selectedSpecimen.setOrders(orders);
		}
		if (specimen.getContainers() != null) {
			List<Container> containers = new ArrayList<Container>();
			for (Container sac : specimen.getContainers()) {
				Container container;
				if ((sac.getOrders() != null) && !sac.getOrders().isEmpty()) {
					container = new Container(true, sac);
					container.setSpecimen(selectedSpecimen);
					List<LabOrder> orders = new ArrayList<LabOrder>();
					for (LabOrder order : sac.getOrders()) {
						LabOrder workOrder = new LabOrder(order, false);
						workOrder.setCreator(username);
						workOrder.setContainer(container);
						orders.add(workOrder);
					}
					container.setOrders(orders);
				} else {
					container = new Container(false, sac);
					container.setOrders(null);
				}
				container.setCreator(username);
				containers.add(container);
			}
			selectedSpecimen.setContainers(containers);
		}
	}

	private void convertOrderToWorkOrder(LabOrder order) {
		selectedWorkOrder = new LabOrder(order, true);
		selectedWorkOrder.setCreator(username);
		if (order.getSpecimens() != null) {
			List<Specimen> workOrderSpecimens = new ArrayList<Specimen>();
			for (Specimen spm : order.getSpecimens()) {
				Specimen newSpecimen = new Specimen(false, spm);
				newSpecimen.setCreator(username);
				newSpecimen.setOrders(new ArrayList<LabOrder>());
				newSpecimen.getOrders().add(selectedWorkOrder);
				if (spm.getContainers() != null) {
					List<Container> containers = new ArrayList<Container>();
					for (Container sac : spm.getContainers()) {
						Container container = new Container(false, sac);
						container.setCreator(username);
						container.setSpecimen(newSpecimen);
						containers.add(container);
					}
					newSpecimen.setContainers(containers);
				}
				workOrderSpecimens.add(newSpecimen);
			}
			selectedWorkOrder.setSpecimens(workOrderSpecimens);
		}
	}

	/**
	 * BATTERY-CENTRIC case only: removes the given specimen from the list of specimens related to the current workOrder
	 * 
	 * @param specimen
	 *            : specimen to remove
	 */
	public void removeSpecimen(Specimen specimen) {
		selectedWorkOrder.getSpecimens().remove(specimen);
	}

	/**
	 * SPECIMEN-CENTRIC/CONTAINER-CENTRIC cases: removes the given container from the list of containers related to the current specimen
	 * 
	 * @param container
	 */
	public void removeContainer(Container container) {
		selectedSpecimen.getContainers().remove(container);
	}

	/**
	 * SPECIMEN-CENTRIC case only: removes the given order from the list of orders related to the current specimen
	 * 
	 * @param workOrder
	 */
	public void removeOrderFromSpecimen(LabOrder workOrder) {
		selectedSpecimen.getOrders().remove(workOrder);
	}

	/**
	 * CONTAINER-CENTRIC case only: removes the given order from the list of orders related to the given container
	 * 
	 * @param workOrder
	 * @param container
	 */
	public void removeOrderFromContainer(LabOrder workOrder, Container container) {
		int currentContainerIndex = selectedSpecimen.getContainers().indexOf(container);
		container = selectedSpecimen.getContainers().get(currentContainerIndex);
		container.getOrders().remove(workOrder);
		selectedSpecimen.getContainers().set(currentContainerIndex, container);
	}

	public void send() {
		HL7V2ResponderSUTConfiguration sut = (HL7V2ResponderSUTConfiguration) Component.getInstance("selectedSUT");
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		Encounter encounter = null;
		if (sut == null) {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No SUT selected ! please select the system to test and retry");
			return;
		}
		if (messageStructure.equals(BATTERY_CENTRIC)) {
			selectedWorkOrder = selectedWorkOrder.save(entityManager);
			encounter = selectedWorkOrder.getEncounter();
		} else {
			selectedSpecimen = selectedSpecimen.save(entityManager);
			if (messageStructure.equals(SPECIMEN_CENTRIC) && !selectedSpecimen.getOrders().isEmpty()) {
				encounter = selectedSpecimen.getOrders().get(0).getEncounter();
			} else if (messageStructure.equals(CONTAINER_CENTRIC) && !selectedSpecimen.getContainers().isEmpty()
					&& !selectedSpecimen.getContainers().get(0).getOrders().isEmpty()) {
				encounter = selectedSpecimen.getContainers().get(0).getOrders().get(0).getEncounter();
			} else {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Structure is not well-formed");
				return;
			}
		}
		LabMessageBuilder generator = new LabMessageBuilder(TRANSACTION, SIMU_ACTOR, encounter, selectedWorkOrder,
				selectedSpecimen, sut, APPLICATION, FACILITY, messageStructure, null);
		try {
			String messageToSend = generator.generateMessage();
			if (messageToSend != null) {
				Initiator hl7Initiator = new Initiator(sut, FACILITY, APPLICATION,
						Actor.findActorWithKeyword(SIMU_ACTOR), Transaction.GetTransactionByKeyword(TRANSACTION),
						messageToSend, messageStructure, "LAB", Actor.findActorWithKeyword(SUT_ACTOR));

				TransactionInstance hl7Message = hl7Initiator.sendMessageAndGetTheHL7Message();
				hl7Messages = new ArrayList<TransactionInstance>();
                hl7Messages.add(hl7Message);
			} else {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The message has not been built !");
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			FacesMessages.instance().add(e.getMessage());
		}
	}

	/**
	 * Go back to order/specimen browser
	 */
	public String performAnotherTest() {
		return "/browsing/dataBrowser.seam";
	}

	public LabOrder getSelectedWorkOrder() {
		return selectedWorkOrder;
	}

	public void setSelectedWorkOrder(LabOrder selectedWorkOrder) {
		this.selectedWorkOrder = selectedWorkOrder;
	}

	public Specimen getSelectedSpecimen() {
		return selectedSpecimen;
	}

	public void setSelectedSpecimen(Specimen selectedSpecimen) {
		this.selectedSpecimen = selectedSpecimen;
	}

	public String getMessageStructure() {
		return messageStructure;
	}

	public void setMessageStructure(String messageStructure) {
		this.messageStructure = messageStructure;
	}

	public Integer getSelectedContainerId() {
		return selectedContainerId;
	}

	public void setSelectedContainerId(Integer selectedContainerId) {
		this.selectedContainerId = selectedContainerId;
	}

	public List<TransactionInstance> getHl7Messages() {
		return hl7Messages;
	}

	public void setHl7Messages(List<TransactionInstance> hl7Messages) {
		this.hl7Messages = hl7Messages;
	}

	public String getBATTERY_CENTRIC() {
		return BATTERY_CENTRIC;
	}

	public String getSPECIMEN_CENTRIC() {
		return SPECIMEN_CENTRIC;
	}

	public String getCONTAINER_CENTRIC() {
		return CONTAINER_CENTRIC;
	}

    public List<HL7V2ResponderSUTConfiguration> getAvailableSuts() {
        if (availableSuts == null){
            availableSuts = HL7V2ResponderSUTConfiguration.getAllConfigurationsForSelection(TRANSACTION, HL7Protocol.MLLP);
        }
        return availableSuts;
    }
}
