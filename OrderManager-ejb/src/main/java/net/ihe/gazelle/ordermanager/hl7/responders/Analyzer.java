package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.Type;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message.ORL_O42;
import net.ihe.gazelle.oml.hl7v2.model.v25.group.OML_O33_ORDER;
import net.ihe.gazelle.oml.hl7v2.model.v25.message.OML_O33;
import net.ihe.gazelle.ordermanager.dico.HL7Constant;
import net.ihe.gazelle.ordermanager.dico.HL7FieldPosition;
import net.ihe.gazelle.ordermanager.dico.SupportedTriggerEvents;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.ordermanager.utils.ORLMessageBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Class description : <b>Analyzer</b>
 * <p/>
 * This class is the handler for messages received by the Automation Manager in the context of the LAB-4 transaction
 *
 * @author Nicolas Lefebvre - INRIA Rennes/Bretagne Atlantique
 * @version 1.0 - 2012, February 20th
 */
public class Analyzer extends OrderReceiver {

    /** Constant <code>ANALYZER="ANALYZER"</code> */
    protected static final String ANALYZER = "ANALYZER";
    /** Constant <code>ANALYZER_MGR="ANALYZER_MGR"</code> */
    public static final String ANALYZER_MGR = "ANALYZER_MGR";
    /** Constant <code>NO_REP=0</code> */
    private static final int NO_REP = 0;
    /** Constant <code>SAC_COUNT=1</code> */
    private static final int SAC_COUNT = 1;
    private static final Logger LOG = LoggerFactory.getLogger(Analyzer.class);
    private static final Integer PIDMINLENGTH = 5;

    /** {@inheritDoc} */
    @Override
    public IHEDefaultHandler newInstance() {
        return new Analyzer();
    }


    /** {@inheritDoc} */
    @Override
    public Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception {
        Message response;
        orderControlCode = null;
        try {
            // check if we must accept the message
            response = Receiver.generateNack(incomingMessage, serverApplication, serverFacility, Arrays.asList("OML"),
                    Arrays.asList(SupportedTriggerEvents.O33), hl7Charset);
            simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-28", entityManager);
            // if no NACK is generated, process the message
            if (response == null) {
                if (messageType == null) {
                    response = Receiver
                            .buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, "ACK",
                                    AcknowledgmentCode.AA, "MSH-9 is required but missing", null, Receiver.REQUIRED_FIELD_MISSING,
                                    Receiver.SEVERITY_ERROR, "ACK", hl7Charset, "2.5.1");
                    LOG.error("Message type is empty");
                } else {
                    // This part is specific to the LAW Profile. In order to reuse the methods used by the others
                    // simulators to decode the HL7 message, it is necessary to have an HL7 v2.5 message.
                    PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();

                    String messageString = pipeParser.encode(incomingMessage);
                    messageString = messageString.replace("|2.5.1|", "|2.5|");

                    response = processOMLMessage(pipeParser.parse(messageString), HL7Constant.NW, HL7Constant.CA, null, HL7Constant.DC);
                }
            }

            String responseType = null;
            String messageType;
            boolean negativeQueryAck = false;
            if (response != null) {
                if (response instanceof ORL_O42) {
                    ORL_O42 orl = (ORL_O42) response;
                    for (int i = 0; i < orl.getRESPONSE().getSPECIMENReps(); i++) {
                        for (int j = 0; j < orl.getRESPONSE().getSPECIMEN(i).getORDERReps(); j++) {
                            if (orl.getRESPONSE().getSPECIMEN(i).getORDER(j).getORC().getOrderControl().encode()
                                    .equals(HL7Constant.OK)) {
                                orl.getRESPONSE().getSPECIMEN(i).getORDER(j).getORC().getOrderStatus().parse(HL7Constant.SC);
                            } else if (orl.getRESPONSE().getSPECIMEN(i).getORDER(j).getORC().getOrderControl().encode()
                                    .equals(HL7Constant.UA) || orl.getRESPONSE().getSPECIMEN(i).getORDER(j).getORC().getOrderControl().encode()
                                    .equals(HL7Constant.CR)) {
                                orl.getRESPONSE().getSPECIMEN(i).getORDER(j).getORC().getOrderStatus().parse(HL7Constant.CA);
                            } else if (orl.getRESPONSE().getSPECIMEN(i).getORDER(j).getORC().getOrderControl().encode()
                                    .equals(HL7Constant.UC)) {
                                orl.getRESPONSE().getSPECIMEN(i).getORDER(j).getORC().getOrderStatus().parse(HL7Constant.IP);
                            }
                        }
                    }
                    ca.uhn.hl7v2.model.v251.message.OML_O33 omlMessage = (ca.uhn.hl7v2.model.v251.message.OML_O33) incomingMessage;
                    int index = HL7FieldPosition.FIRST_REP;
                    for (Type type : omlMessage.getMSH().getMessageProfileIdentifier()) {
                        orl.getMSH().getMessageProfileIdentifier(index).parse(type.encode());
                        index++;
                    }
                    response = orl;
                    // check received control code (are we in the case of a Negative Query Acknowledgement)
                    negativeQueryAck = omlMessage.getSPECIMEN().getORDER().getORC().getOrc1_OrderControl().getValue()
                            .equals(HL7Constant.DC);
                }
                responseType = MessageDecoder.buildMessageType(response);
            }

            messageType = MessageDecoder.buildMessageType(incomingMessage);
            if (negativeQueryAck) {
                messageType = messageType.concat(" (" + HL7Constant.DC + ")");
                setIncomingMessageType(messageType);
            }

            setResponseMessageType(responseType);
            setSutActor(Actor.findActorWithKeyword(ANALYZER_MGR, entityManager));
        } catch (HL7Exception e) {
            throw new HL7Exception(e);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new ReceivingApplicationException(e);
        }
        return response;
    }

    /** {@inheritDoc} */
    @Override
    protected Message parseOMLO33(Message inMessage, String newControlCode, String cancelControlCode,
            String updateControlCode, String discontinueRequestControlCode, ORLMessageBuilder orlBuilder, Domain domain,
            String creator, Encounter selectedEncounter) {
        PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();
        // cast message using the class generated using the IHE HL7 message profile
        OML_O33 o33Message;
        try {
            o33Message = (OML_O33) pipeParser
                    .parseForSpecificPackage(inMessage.encode(), "net.ihe.gazelle.oml.hl7v2.model.v25.message");
        } catch (HL7Exception e) {
            return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
                    "Cannot properly encode the message for parsing", null);
        }
        // prepare response
        // Sometimes, the PATIENT group is empty for the LAB-28 transaction
        ORL_O42 response = null;
        try {
            if (o33Message.getPATIENT().getPID().encode().length() > PIDMINLENGTH) {
                response = orlBuilder.createNewORL_O42(AcknowledgmentCode.AA, o33Message.getPATIENT().getPID());
            } else {
                response = orlBuilder.createNewORL_O42(AcknowledgmentCode.AA, null);
            }
            if (o33Message.getSPECIMEN(HL7FieldPosition.FIRST_REP).getORDER(HL7FieldPosition.FIRST_REP).getORC().getOrderControl().getValue().equals(HL7Constant.DC)) {
                // Negative Query Response
                return response;
            }
        } catch (HL7Exception e) {
            LOG.error(e.getMessage(), e);
        }
        int nbOfSpecimens = o33Message.getSPECIMENReps();
        if (nbOfSpecimens > NO_REP) {
            for (int spmIndex = 0; spmIndex < nbOfSpecimens; spmIndex++) {
                Container selectedContainer;
                Specimen specimen = MessageDecoder
                        .extractSpecimenFromSegment(o33Message.getSPECIMEN(spmIndex).getSPM(), creator, simulatedActor,
                                domain);
                // we do not try to match the received specimen with one from the database since they might not be uniquely identified (see LAW specifications)
                if (o33Message.getSPECIMEN(spmIndex).getSACReps() != SAC_COUNT) {
                    return orlBuilder
                            .createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
                                    "Exactly one SAC segment is expected", null);
                } else {
                    Container messageContainer = MessageDecoder
                            .extractContainerFromSAC(o33Message.getSPECIMEN(spmIndex).getSAC(), simulatedActor,
                                    specimen, creator, domain);
                    messageContainer.setMainEntity(false);
                    selectedContainer = Container.getContainerBasedOnDescription(messageContainer, entityManager);
                    if (selectedContainer == null) {
                        specimen = specimen.save(entityManager);
                        selectedContainer = messageContainer;
                        selectedContainer.setSpecimen(specimen);
                    }
                }
                // extract orders
                int nbOfOrderGroups = o33Message.getSPECIMEN(spmIndex).getORDERReps();
                if (nbOfOrderGroups == NO_REP) {
                    entityManager.getTransaction().rollback();
                    return orlBuilder
                            .createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
                                    "ORDER group is missing in repetition " + spmIndex + " of SPECIMEN group", null);
                } else {
                    // add segments to response
                    orlBuilder.addSpecimenAndContainers(response.getRESPONSE().getSPECIMEN(spmIndex),
                            o33Message.getSPECIMEN(spmIndex),
                            selectedContainer.getSpecimen().getPlacerAssignedIdentifier(),
                            selectedContainer.getSpecimen().getFillerAssignedIdentifier());
                    // list orders relative to the current specimen/container
                    List<LabOrder> orders = new ArrayList<LabOrder>();
                    for (int orderGroupIndex = HL7FieldPosition.FIRST_REP; orderGroupIndex < nbOfOrderGroups; orderGroupIndex++) {
                        OML_O33_ORDER orderGroup = o33Message.getSPECIMEN(spmIndex).getORDER(orderGroupIndex);
                        Segment orc = orderGroup.getORC();
                        if (MessageDecoder.isEmpty(orc)) {
                            entityManager.getTransaction().rollback();
                            return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
                                    Receiver.SEVERITY_ERROR, "ORC segment is missing in repetition " + orderGroupIndex
                                            + " of ORDER group (SPECIMEN(" + spmIndex + "))", ANALYZER);
                        }
                        Segment obr = orderGroup.getOBSERVATION_REQUEST().getOBR();
                        Segment tq1 = orderGroup.getTIMING().getTQ1();
                        LabOrder messageOrder = MessageDecoder
                                .extractLabOrderFromSegments(orc, obr, tq1, simulatedActor, domain, creator,
                                        selectedEncounter);
                        messageOrder.setWorkOrder(false);
                        messageOrder.setMainEntity(false);
                        // the order retrieved from the database using numbers given in the message
                        LabOrder selectedOrder = AbstractOrder
                                .getOrderByNumbersByActor(LabOrder.class, messageOrder.getPlacerOrderNumber(),
                                        messageOrder.getFillerOrderNumber(), messageOrder.getPlacerGroupNumber(),
                                        simulatedActor, entityManager, false);
                        if (messageOrder.getOrderControlCode() == null) {
                            entityManager.getTransaction().rollback();
                            return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.REQUIRED_FIELD_MISSING,
                                    Receiver.SEVERITY_ERROR, "ORC-1 is required but missing", ANALYZER);
                        } else if (messageOrder.getOrderControlCode().equals(newControlCode)) {
                            if (selectedOrder != null) {
                                // order already exists with the same placer/filler order number ==> message is discarded and error returns to the sender
                                entityManager.getTransaction().rollback();
                                return orlBuilder
                                        .createNack(inMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
                                                "A awos with this WOS ID already exists", ANALYZER);
                            } else {
                                messageOrder.setOrderControlCode(HL7Constant.OK);
                                messageOrder.setOrderStatus(HL7Constant.SC);
                                orders.add(messageOrder.save(entityManager));
                                // add segments in response
                                orlBuilder.addOrderGroupAndUpdateControlCode(
                                        response.getRESPONSE().getSPECIMEN(spmIndex).getORDER(orderGroupIndex),
                                        o33Message.getSPECIMEN(spmIndex).getORDER(orderGroupIndex),
                                        messageOrder.getOrderControlCode(), messageOrder.getPlacerOrderNumber(),
                                        messageOrder.getFillerOrderNumber(), messageOrder.getPlacerGroupNumber(),
                                        false);
                            }
                        } else if (messageOrder.getOrderControlCode().equals(cancelControlCode)) {
                            // unknown order or order already started ==> UA/UC
                            if (selectedOrder == null) {
                                orlBuilder.addOrderGroupAndUpdateControlCode(
                                        response.getRESPONSE().getSPECIMEN(spmIndex).getORDER(orderGroupIndex),
                                        o33Message.getSPECIMEN(spmIndex).getORDER(orderGroupIndex), HL7Constant.UC, null, null,
                                        null, false);
                            } else if (!simulatedActor.getKeyword().equals(ANALYZER) && (selectedOrder.getOrderStatus()
                                    != null) && !selectedOrder.getOrderStatus().isEmpty()) {
                                selectedOrder.setOrderControlCode(HL7Constant.UC);
                                selectedOrder.save(entityManager);
                                orlBuilder.addOrderGroupAndUpdateControlCode(
                                        response.getRESPONSE().getSPECIMEN(spmIndex).getORDER(orderGroupIndex),
                                        o33Message.getSPECIMEN(spmIndex).getORDER(orderGroupIndex), HL7Constant.UC,
                                        selectedOrder.getPlacerOrderNumber(), selectedOrder.getFillerOrderNumber(),
                                        selectedOrder.getPlacerGroupNumber(), false);
                            } else {
                                // cancel accepted ==> CR
                                orlBuilder.addOrderGroupAndUpdateControlCode(
                                        response.getRESPONSE().getSPECIMEN(spmIndex).getORDER(orderGroupIndex),
                                        o33Message.getSPECIMEN(spmIndex).getORDER(orderGroupIndex), HL7Constant.CR,
                                        selectedOrder.getPlacerOrderNumber(), selectedOrder.getFillerOrderNumber(),
                                        selectedOrder.getPlacerGroupNumber(),
                                        simulatedActor.getKeyword().equals(ANALYZER));
                                selectedOrder.setOrderStatus(messageOrder.getOrderStatus());
                                selectedOrder.setOrderControlCode(HL7Constant.CR);
                                selectedOrder.setOrderResultStatus(MessageDecoder.getOrderResultStatus(obr));
                            }
                        } else {
                            entityManager.getTransaction().rollback();
                            return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.TABLE_VALUE_NOT_FOUND,
                                    Receiver.SEVERITY_ERROR,
                                    "Unexpected order control code: " + messageOrder.getOrderControlCode(), ANALYZER);
                        }
                    }
                    if (!orders.isEmpty()) {
                        if (selectedContainer.getSpecimen().getOrders() == null) {
                            selectedContainer.getSpecimen().setOrders(orders);
                        } else {
                            selectedContainer.getSpecimen().getOrders().addAll(orders);
                        }
                    }
                    selectedContainer.save(entityManager);
                }
            }
            return response;
        } else {
            return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
                    "No SPECIMEN group found", ANALYZER);
        }
    }

}
