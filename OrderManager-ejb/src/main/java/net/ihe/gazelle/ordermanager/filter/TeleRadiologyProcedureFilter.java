package net.ihe.gazelle.ordermanager.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyProcedure;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyProcedureQuery;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;

import java.util.Map;

/**
 * <b>Class Description : </b>AbstractOrderFilter<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 *
 *
 *
 */
public class TeleRadiologyProcedureFilter implements QueryModifier<TeleRadiologyProcedure> {

    private String fillerOrderNumber;
    private String placerOrderNumber;
    private Actor actor;
    private Domain domain;
    private String visitnumber;
    private String patientid;
    private Filter<TeleRadiologyProcedure> filter;

    public TeleRadiologyProcedureFilter(Domain domain, Actor simulatedActor) {
        this.domain = domain;
        this.actor = simulatedActor;
    }

    public Filter<TeleRadiologyProcedure> getFilter() {
        if (filter == null) {
            filter = new Filter<TeleRadiologyProcedure>(getHQLCriterionsForFilter());
        }
        return filter;
    }

    protected HQLCriterionsForFilter<TeleRadiologyProcedure> getHQLCriterionsForFilter() {
        TeleRadiologyProcedureQuery query = new TeleRadiologyProcedureQuery();
        HQLCriterionsForFilter<TeleRadiologyProcedure> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("firstname", query.teleRadiologyOrder().encounter().patient().firstName());
        criteria.addPath("lastname", query.teleRadiologyOrder().encounter().patient().lastName());
        criteria.addPath("actor", query.simulatedActor(), getActor());
        criteria.addPath("domain", query.domain(), getDomain());
        criteria.addPath("creator", query.creator());
        criteria.addPath("creationDate", query.lastChanged());
        criteria.addQueryModifier(this);
        return criteria;
    }

    public FilterDataModel<TeleRadiologyProcedure> getOrders() {
        return new FilterDataModel<TeleRadiologyProcedure>(getFilter()) {
            @Override
            protected Object getId(TeleRadiologyProcedure t) {
                return t.getId();
            }
        };
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<TeleRadiologyProcedure> hqlQueryBuilder, Map<String, Object> map) {
        TeleRadiologyProcedureQuery query = new TeleRadiologyProcedureQuery();
        if (getPlacerOrderNumber() != null && !getPlacerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.teleRadiologyOrder().placerOrderNumber().likeRestriction(getPlacerOrderNumber()));
        }
        if ((getFillerOrderNumber() != null) && !getFillerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.teleRadiologyOrder().fillerOrderNumber().likeRestriction(getFillerOrderNumber()));
        }
        if (getPatientid() != null && !getPatientid().isEmpty()){
            hqlQueryBuilder.addRestriction(query.teleRadiologyOrder().encounter().patient().patientIdentifiers().identifier().likeRestriction(getPatientid()));
        }
        if (getVisitnumber() != null && !getVisitnumber().isEmpty()){
            hqlQueryBuilder.addRestriction(query.teleRadiologyOrder().encounter().visitNumber().likeRestriction(getVisitnumber()));
        }
    }

    public void reset() {
        fillerOrderNumber = null;
        placerOrderNumber = null;
        visitnumber = null;
        patientid = null;
        getFilter().clear();
    }

    public String getFillerOrderNumber() {
        return fillerOrderNumber;
    }

    public void setFillerOrderNumber(String fillerOrderNumber) {
        this.fillerOrderNumber = fillerOrderNumber;
    }

    public String getPlacerOrderNumber() {
        return placerOrderNumber;
    }

    public void setPlacerOrderNumber(String placerOrderNumber) {
        this.placerOrderNumber = placerOrderNumber;
    }

    public String getVisitnumber() {
        return visitnumber;
    }

    public void setVisitnumber(String visitnumber) {
        this.visitnumber = visitnumber;
    }

    public String getPatientid() {
        return patientid;
    }

    public void setPatientid(String patientid) {
        this.patientid = patientid;
    }

    public Actor getActor() {
        return actor;
    }

    public Domain getDomain() {
        return domain;
    }
}
