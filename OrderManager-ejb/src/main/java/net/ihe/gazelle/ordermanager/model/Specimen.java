package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.annotations.Name;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <b>Class description</b> Specimen
 *
 * This class describes the specimen object.
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes Bretagne Atlantique
 * @version 1.0 - 2011, December 5th
 */

@Entity
@Name("specimen")
@Table(name = "om_specimen", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_specimen_sequence", sequenceName = "om_specimen_id_seq", allocationSize = 1)
public class Specimen extends CommonColumns implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -615062910804865680L;
    private static final String SUB_COMP_SEP_CHAR = "&";

    @Id
	@NotNull
	@GeneratedValue(generator = "om_specimen_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	/**
	 * SPM-2.1
	 */
	@Column(name = "placer_assigned_identifier")
	private String placerAssignedIdentifier;

	/**
	 * SPM-2.2
	 */
	@Column(name = "filler_assigned_identifier")
	private String fillerAssignedIdentifier;

	/**
	 * SPM-4
	 */
	@Column(name = "type")
	private String type;

	/**
	 * SPM-7
	 */
	@Column(name = "collection_method")
	private String collectionMethod;

	/**
	 * SPM-8
	 */
	@Column(name = "source_site")
	private String sourceSite;

	/**
	 * SPM-9
	 */
	@Column(name = "source_site_modifier")
	private String sourceSiteModifier;

	/**
	 * SPM-11
	 */
	@Column(name = "role")
	private String role;

	/**
	 * SPM-13
	 */
	@Column(name = "grouped_specimen_count")
	private Integer groupedSpecimenCount;

	/**
	 * SPM-16
	 */
	@Column(name = "risk_code")
	private String riskCode;

	/**
	 * SPM-17
	 */
	@Column(name = "collection_date_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date collectionDateTime;

	/**
	 * SPM-18
	 */
	@Column(name = "received_date_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date receivedDateTime;

	/**
	 * SPM-20
	 */
	@Column(name = "available")
	private boolean available;

	/**
	 * SPM-21
	 */
	@Column(name = "reject_reason")
	private String rejectReason;

	/**
	 * SPM-27
	 */
	@Column(name = "container_type")
	private String containerType;

	/**
	 * associated specimens and orders
	 */
	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name = "om_lab_order_specimen", joinColumns = @JoinColumn(name = "specimen_id"), inverseJoinColumns = @JoinColumn(name = "lab_order_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
			"specimen_id", "lab_order_id" }))
	private List<LabOrder> orders;

	/**
	 * associates specimens and containers
	 */
	@OneToMany(mappedBy = "specimen", cascade = CascadeType.MERGE, targetEntity = Container.class)
	private List<Container> containers;

	/**
	 * list of observations set/received for the specimen
	 */
	@OneToMany(mappedBy = "specimen", cascade = CascadeType.MERGE, targetEntity = Observation.class)
	@OrderBy("setId ASC")
	// in order to properly fill out the OBX segment, and to put them in the right order, we need to get the observations sorted according to the SetID attribute
	private List<Observation> observations;

	/**
	 * true if the message in which the specimen has been sent is OML_O33 / OML_O35 false otherwise
	 */
	@Column(name = "is_main_entity")
	private boolean mainEntity;

	/**
	 * Indicates whether the specimen is linked to an order or a work order
	 */
	@Column(name = "work_order")
	private boolean workOrder;

	/**
	 * <p>Constructor for Specimen.</p>
	 *
	 * @param isPartOfWorkOrder
	 *            TODO
	 */
	public Specimen(boolean isPartOfWorkOrder) {
		this.workOrder = isPartOfWorkOrder;
	}

	/**
	 * default constructor
	 */
	public Specimen() {
	}

	/**
	 * <p>Constructor for Specimen.</p>
	 *
	 * @param isMainEntity a boolean.
	 * @param isPartOfWorkOrder
	 *            TODO
	 * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
	 * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 */
	public Specimen(boolean isMainEntity, Domain domain, Actor simulatedActor, EntityManager entityManager,
			boolean isPartOfWorkOrder) {
		this.lastChanged = new Date();
		this.mainEntity = isMainEntity;
		this.simulatedActor = simulatedActor;
		this.domain = domain;
		if (this.simulatedActor.getKeyword().equals("OP") || this.simulatedActor.getKeyword().equals("LIP")) {
			this.placerAssignedIdentifier = NumberGenerator.getOrderNumberForActor(this.simulatedActor, entityManager,
                    SUB_COMP_SEP_CHAR);
		} else if (this.simulatedActor.getKeyword().equals("OF")) {
			this.fillerAssignedIdentifier = NumberGenerator.getOrderNumberForActor(this.simulatedActor, entityManager,
                    SUB_COMP_SEP_CHAR);
		} else if (!simulatedActor.getKeyword().startsWith("ANALYZER")) {
			this.placerAssignedIdentifier = NumberGenerator.getOrderNumberForActor(Actor.findActorWithKeyword("OP"),
					entityManager, SUB_COMP_SEP_CHAR);
			this.fillerAssignedIdentifier = NumberGenerator.getOrderNumberForActor(Actor.findActorWithKeyword("OF"),
					entityManager, SUB_COMP_SEP_CHAR);
		}
		if (isMainEntity) {
			this.orders = new ArrayList<LabOrder>();
		} else {
			this.orders = null;
		}
		this.containers = new ArrayList<Container>();
		this.workOrder = isPartOfWorkOrder;
	}

	/**
	 * <p>Constructor for Specimen.</p>
	 *
	 * @param isMainEntity a boolean.
	 * @param specimen a {@link net.ihe.gazelle.ordermanager.model.Specimen} object.
	 */
	public Specimen(boolean isMainEntity, Specimen specimen) {
		this.lastChanged = new Date();
		this.workOrder = true;
		this.mainEntity = isMainEntity;
		this.simulatedActor = Actor.findActorWithKeyword("OF");
		this.domain = Domain.getDomainByKeyword("LAB");
		this.placerAssignedIdentifier = specimen.getPlacerAssignedIdentifier();
		this.fillerAssignedIdentifier = specimen.getFillerAssignedIdentifier();
		this.available = specimen.isAvailable();
		this.collectionDateTime = specimen.getCollectionDateTime();
		this.containerType = specimen.getContainerType();
		this.receivedDateTime = specimen.getReceivedDateTime();
		this.rejectReason = null;
		this.riskCode = specimen.getRiskCode();
		this.sourceSite = specimen.getSourceSite();
		this.sourceSiteModifier = specimen.getSourceSiteModifier();
		this.type = specimen.getType();
	}

	/**
	 * <p>fillRandomly.</p>
	 */
	public void fillRandomly() {
		if (!this.simulatedActor.getKeyword().equals("ANALYZER_MGR")) {
			if (this.getCollectionDateTime() == null && !simulatedActor.getKeyword().equals("ANALYZER")) {
				this.setCollectionDateTime(new Date());
			}
			if (this.getRole() == null) {
				this.setRole(ValueSet.getRandomCodeFromValueSet("specimenRole"));
			}
			if (this.getReceivedDateTime() == null && !simulatedActor.getKeyword().equals("ANALYZER")) {
				this.setReceivedDateTime(new Date());
			}
		}
		if (this.getCollectionMethod() == null && !simulatedActor.getKeyword().equals("ANALYZER")) {
			this.setCollectionMethod(ValueSet.getRandomCodeFromValueSet("collectionMethod"));
		}
		if (this.getRiskCode() == null && !simulatedActor.getKeyword().equals("ANALYZER")) {
			this.setRiskCode(ValueSet.getRandomCodeFromValueSet("riskCode"));
		}
		if (this.getType() == null) {
			this.setType(ValueSet.getRandomCodeFromValueSet("specimenType"));
		}
	}

	/**
	 * <p>save.</p>
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.ordermanager.model.Specimen} object.
	 */
	public Specimen save(EntityManager entityManager) {
		Specimen specimen = entityManager.merge(this);
		entityManager.flush();
		return specimen;
	}

	/**
	 * <p>getSpecimenByAssignedIdentifiersByActorByDomain.</p>
	 *
	 * @param placerAssignedIdentifier a {@link java.lang.String} object.
	 * @param fillerAssignedIdentifier a {@link java.lang.String} object.
	 * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
	 * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
	 * @param isPartOfWorkOrder
	 *            TODO
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.ordermanager.model.Specimen} object.
	 */
	public static Specimen getSpecimenByAssignedIdentifiersByActorByDomain(String placerAssignedIdentifier,
			String fillerAssignedIdentifier, Actor simulatedActor, Domain domain, EntityManager entityManager,
			boolean isPartOfWorkOrder) {
		SpecimenQuery query = new SpecimenQuery(new HQLQueryBuilder<Specimen>(entityManager, Specimen.class));
		query.workOrder().eq(isPartOfWorkOrder);
		if ((placerAssignedIdentifier != null) && !placerAssignedIdentifier.isEmpty()) {
			query.placerAssignedIdentifier().eq(placerAssignedIdentifier);
		}
		if ((fillerAssignedIdentifier != null) && !fillerAssignedIdentifier.isEmpty()) {
			query.fillerAssignedIdentifier().eq(fillerAssignedIdentifier);
		}
		if (simulatedActor != null) {
			query.simulatedActor().eq(simulatedActor);
		}
		if (domain != null) {
			query.domain().eq(domain);
		}
		return query.getUniqueResult();
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((lastChanged == null) ? 0 : lastChanged.hashCode());
		result = (prime * result) + ((fillerAssignedIdentifier == null) ? 0 : fillerAssignedIdentifier.hashCode());
		result = (prime * result) + ((placerAssignedIdentifier == null) ? 0 : placerAssignedIdentifier.hashCode());
		result = (prime * result) + (workOrder ? 1231 : 1237);
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Specimen other = (Specimen) obj;
		if (lastChanged == null) {
			if (other.lastChanged != null) {
				return false;
			}
		} else if (!lastChanged.equals(other.lastChanged)) {
			return false;
		}
		if (fillerAssignedIdentifier == null) {
			if (other.fillerAssignedIdentifier != null) {
				return false;
			}
		} else if (!fillerAssignedIdentifier.equals(other.fillerAssignedIdentifier)) {
			return false;
		}
		if (placerAssignedIdentifier == null) {
			if (other.placerAssignedIdentifier != null) {
				return false;
			}
		} else if (!placerAssignedIdentifier.equals(other.placerAssignedIdentifier)) {
			return false;
		}
		if (workOrder != other.workOrder) {
			return false;
		}
		return true;
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>placerAssignedIdentifier</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getPlacerAssignedIdentifier() {
		return placerAssignedIdentifier;
	}

	/**
	 * <p>Setter for the field <code>placerAssignedIdentifier</code>.</p>
	 *
	 * @param placerAssignedIdentifier a {@link java.lang.String} object.
	 */
	public void setPlacerAssignedIdentifier(String placerAssignedIdentifier) {
		this.placerAssignedIdentifier = placerAssignedIdentifier;
	}

	/**
	 * <p>Getter for the field <code>fillerAssignedIdentifier</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFillerAssignedIdentifier() {
		return fillerAssignedIdentifier;
	}

	/**
	 * <p>Setter for the field <code>fillerAssignedIdentifier</code>.</p>
	 *
	 * @param fillerAssignedIdentifier a {@link java.lang.String} object.
	 */
	public void setFillerAssignedIdentifier(String fillerAssignedIdentifier) {
		this.fillerAssignedIdentifier = fillerAssignedIdentifier;
	}

	/**
	 * <p>Getter for the field <code>type</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getType() {
		return type;
	}

	/**
	 * <p>Setter for the field <code>type</code>.</p>
	 *
	 * @param type a {@link java.lang.String} object.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * <p>Getter for the field <code>collectionMethod</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCollectionMethod() {
		return collectionMethod;
	}

	/**
	 * <p>Setter for the field <code>collectionMethod</code>.</p>
	 *
	 * @param collectionMethod a {@link java.lang.String} object.
	 */
	public void setCollectionMethod(String collectionMethod) {
		this.collectionMethod = collectionMethod;
	}

	/**
	 * <p>Getter for the field <code>sourceSite</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSourceSite() {
		return sourceSite;
	}

	/**
	 * <p>Setter for the field <code>sourceSite</code>.</p>
	 *
	 * @param sourceSite a {@link java.lang.String} object.
	 */
	public void setSourceSite(String sourceSite) {
		this.sourceSite = sourceSite;
	}

	/**
	 * <p>Getter for the field <code>sourceSiteModifier</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSourceSiteModifier() {
		return sourceSiteModifier;
	}

	/**
	 * <p>Setter for the field <code>sourceSiteModifier</code>.</p>
	 *
	 * @param sourceSiteModifier a {@link java.lang.String} object.
	 */
	public void setSourceSiteModifier(String sourceSiteModifier) {
		this.sourceSiteModifier = sourceSiteModifier;
	}

	/**
	 * <p>Getter for the field <code>role</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getRole() {
		return role;
	}

	/**
	 * <p>Setter for the field <code>role</code>.</p>
	 *
	 * @param role a {@link java.lang.String} object.
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * <p>Getter for the field <code>riskCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getRiskCode() {
		return riskCode;
	}

	/**
	 * <p>Setter for the field <code>riskCode</code>.</p>
	 *
	 * @param riskCode a {@link java.lang.String} object.
	 */
	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}


    /**
     * <p>Getter for the field <code>collectionDateTime</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public final Date getCollectionDateTime() {
        if (collectionDateTime != null) {
            return (Date) collectionDateTime.clone();
        } else {
            return null;
        }
    }

    /**
     * <p>Setter for the field <code>collectionDateTime</code>.</p>
     *
     * @param spsStartDateTime a {@link java.util.Date} object.
     */
    public final void setCollectionDateTime(Date spsStartDateTime) {
        if (spsStartDateTime != null) {
            this.collectionDateTime = (Date) spsStartDateTime.clone();
        } else {
            this.collectionDateTime = null;
        }
    }

    /**
     * <p>Getter for the field <code>receivedDateTime</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public final Date getReceivedDateTime() {
        if (receivedDateTime != null) {
            return (Date) receivedDateTime.clone();
        } else {
            return null;
        }
    }

    /**
     * <p>Setter for the field <code>receivedDateTime</code>.</p>
     *
     * @param receivedDateTime a {@link java.util.Date} object.
     */
    public final void setReceivedDateTime(Date receivedDateTime) {
        if (receivedDateTime != null) {
            this.receivedDateTime = (Date) receivedDateTime.clone();
        } else {
            this.receivedDateTime = null;
        }
    }

		/**
		 * <p>isAvailable.</p>
		 *
		 * @return a boolean.
		 */
		public boolean isAvailable() {
		return available;
	}

	/**
	 * <p>Setter for the field <code>available</code>.</p>
	 *
	 * @param available a boolean.
	 */
	public void setAvailable(boolean available) {
		this.available = available;
	}

	/**
	 * <p>Getter for the field <code>rejectReason</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getRejectReason() {
		return rejectReason;
	}

	/**
	 * <p>Setter for the field <code>rejectReason</code>.</p>
	 *
	 * @param rejectReason a {@link java.lang.String} object.
	 */
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	/**
	 * <p>Getter for the field <code>containerType</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getContainerType() {
		return containerType;
	}

	/**
	 * <p>Setter for the field <code>containerType</code>.</p>
	 *
	 * @param containerType a {@link java.lang.String} object.
	 */
	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}

	/**
	 * <p>Setter for the field <code>mainEntity</code>.</p>
	 *
	 * @param mainEntity a boolean.
	 */
	public void setMainEntity(boolean mainEntity) {
		this.mainEntity = mainEntity;
	}

	/**
	 * <p>isMainEntity.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isMainEntity() {
		return mainEntity;
	}

	/**
	 * <p>Setter for the field <code>orders</code>.</p>
	 *
	 * @param orders a {@link java.util.List} object.
	 */
	public void setOrders(List<LabOrder> orders) {
		this.orders = orders;
	}

	/**
	 * <p>Getter for the field <code>orders</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<LabOrder> getOrders() {
		return orders;
	}

	/**
	 * <p>Getter for the field <code>containers</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Container> getContainers() {
		return containers;
	}

	/**
	 * <p>Setter for the field <code>containers</code>.</p>
	 *
	 * @param containers a {@link java.util.List} object.
	 */
	public void setContainers(List<Container> containers) {
		this.containers = containers;
	}

	/**
	 * <p>Setter for the field <code>workOrder</code>.</p>
	 *
	 * @param workOrder a boolean.
	 */
	public void setWorkOrder(boolean workOrder) {
		this.workOrder = workOrder;
	}

	/**
	 * <p>isWorkOrder.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isWorkOrder() {
		return workOrder;
	}

	/**
	 * <p>Setter for the field <code>observations</code>.</p>
	 *
	 * @param observations a {@link java.util.List} object.
	 */
	public void setObservations(List<Observation> observations) {
		this.observations = observations;
	}

	/**
	 * <p>Getter for the field <code>observations</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Observation> getObservations() {
		return observations;
	}

	/**
	 * <p>Getter for the field <code>groupedSpecimenCount</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getGroupedSpecimenCount() {
		return groupedSpecimenCount;
	}

	/**
	 * <p>Setter for the field <code>groupedSpecimenCount</code>.</p>
	 *
	 * @param groupedSpecimenCount a {@link java.lang.Integer} object.
	 */
	public void setGroupedSpecimenCount(Integer groupedSpecimenCount) {
		this.groupedSpecimenCount = groupedSpecimenCount;
	}

	/**
	 * <p>getEmptyObservationList.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Object> getEmptyObservationList(){
	    return new ArrayList<Object>();
    }
}
