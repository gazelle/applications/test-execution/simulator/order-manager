package net.ihe.gazelle.ordermanager.mwl.scp;

import org.apache.commons.io.IOUtils;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Extends OutputStream to store the output of the worklist to a buffer being print on screen
 *
 * @author aberge
 */
@Stateless
@Name("wlmscpfsLogger")
public class WlmscpfsLogger implements Runnable, WlmscpfsLoggerLocal {

    private static final String CHARSET_NAME = "UTF-8";
    private static final Logger LOGGER = LoggerFactory.getLogger(WlmscpfsLogger.class);

    private static final StringBuilder buffer;
    private static final int MAX_LENGTH = 50_000;
    private static final int OVERSIZE = 1000;
    private static final int START = 0;
    private static final long WAIT_TIME = 3000;
    private InputStream inputStream;
    private SimpleDateFormat sdf;

    static {
        buffer = new StringBuilder();
        buffer.ensureCapacity(MAX_LENGTH + OVERSIZE);
    }

    public WlmscpfsLogger(InputStream in) {
        super();
        this.inputStream = in;
        sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
    }

    public WlmscpfsLogger() {

    }

    @Override
    public void run() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName(CHARSET_NAME)));
        try {
            String line = reader.readLine();
            while (line != null) {
                if (inputStream.available() > 0) {
                    if (buffer.length() > MAX_LENGTH) {
                        buffer.delete(START, MAX_LENGTH);
                    }
                    if (!line.contains("I: Information") && !line.startsWith("I: /")
                            && !line.startsWith("I: Checking")) {
                        buffer.append('{');
                        buffer.append(sdf.format(new Date()));
                        buffer.append('}');
                        buffer.append(' ');
                        buffer.append(line);
                        buffer.append("\\n");
                    }
                } else {
                    Thread.sleep(WAIT_TIME);
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            IOUtils.closeQuietly(reader);
            IOUtils.closeQuietly(inputStream);
        } catch (InterruptedException e) {
            LOGGER.error("An error occurred when reading output from wlmscpfs: " + e.getMessage());
        }
    }

    @Override
    public String getLog(@Context HttpServletRequest req) {
        StringBuilder response = new StringBuilder();
        response.append("{\"logs\" : \"");
        response.append(buffer.toString());
        response.append("\"}");
        return response.toString();
    }

    @Override
    public Response hello(@Context HttpServletRequest req) {
        return Response.ok("hello").build();
    }
}
