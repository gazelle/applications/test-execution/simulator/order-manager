
/**
 * Copyright 2011 IHE International (http://www.ihe.net)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Nicolas LEFEBVRE/ INRIA Rennes - IHE development Project
 * @author Anne-Gaëlle BERGE / IHE Europe
 * @version $Id: $Id
 */
package net.ihe.gazelle.ordermanager.hl7.initiators;

import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message.QBP_Q11;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.utils.LAWQueryMessagesGenerator;
import net.ihe.gazelle.ordermanager.utils.QueryMode;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Name("queryForAWOSSender")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 30000)
public class QueryForAWOSSender implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8785889823341694952L;

    private static Logger log = LoggerFactory.getLogger(QueryForAWOSSender.class);

    private static Transaction currentTransaction = null;
    private static Actor simulatedActor = null;
    private static Domain selectedDomain = null;
    private static Actor sutActor = null;

    private static SelectItem[] queryModes = null;
    private List<TransactionInstance> hl7Messages = null;
    private QueryMode selectedQueryMode;
    private Container container = null;

    static {
        try {
            queryModes = new SelectItem[QueryMode.values().length];
            int index = 0;
            for (QueryMode mode : QueryMode.values()) {
                queryModes[index] = new SelectItem(mode, mode.getLabel());
                index++;
            }
            currentTransaction = Transaction.GetTransactionByKeyword("LAB-27");
            simulatedActor = Actor.findActorWithKeyword("ANALYZER");
            selectedDomain = Domain.getDomainByKeyword("LAB");
            sutActor = Actor.findActorWithKeyword("ANALYZER_MGR");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * <p>init.</p>
     */
    @Create
    public void init() {
        selectedQueryMode = QueryMode.WOS_ALL;
        hl7Messages = null;
        container = null;
    }

    /**
     * <p>queryModeChangeListener.</p>
     *
     * @param event a {@link javax.faces.event.ValueChangeEvent} object.
     */
    public void queryModeChangeListener(ValueChangeEvent event) {
        if (!((QueryMode) event.getNewValue()).equals(selectedQueryMode)) {
            selectedQueryMode = (QueryMode) event.getNewValue();
            log.info("new value: " + selectedQueryMode.getLabel());
            switch (selectedQueryMode) {
                case WOS_ALL:
                    container = null;
                    break;
                default:
                    container = new Container();
                    break;
            }
        }
    }

    /**
     * <p>validateParameter.</p>
     *
     * @param context a {@link javax.faces.context.FacesContext} object.
     * @param component a {@link javax.faces.component.UIComponent} object.
     * @param value a {@link java.lang.Object} object.
     * @throws javax.faces.validator.ValidatorException if any.
     */
    public void validateParameter(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        final String fieldId = component.getId();
        final String fieldValue = (String) value;
        if ((fieldValue != null) && !fieldValue.isEmpty()) {
            if (getEIFields().contains(fieldId) && !fieldValue.matches(Container.regex_EI)) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Field shall be formatted wih respect to the EI datatype: " + Container.regex_EI, null));
            } else if ((fieldId.equals("qpd5") || fieldId.equals("qpd7")) && !fieldValue.matches(Container.regex_NA)) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Field shall be formatted wih respect to the NA datatype: " + Container.regex_NA, null));
            } else if (!fieldValue.matches(Container.regex_CE)) { // qpd8
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Field shall be formatted wih respect to the CE datatype: " + Container.regex_CE, null));
            }
        }
    }

    private static List<String> getEIFields(){
        return Arrays.asList("qpd3", "qpd4", "qpd6", "qpd9");
    }

    /**
     * <p>sendMessage.</p>
     */
    public void sendMessage() {
        HL7V2ResponderSUTConfiguration sut = (HL7V2ResponderSUTConfiguration) Component.getInstance("selectedSUT");
        if (sut == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please, first select a system under test");
            return;
        } else if (selectedQueryMode == null){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please, first select a query mode");
            return;
        }
        QBP_Q11 messageToSend = LAWQueryMessagesGenerator.generateQBP_Q11(sut.getApplication(), sut.getFacility(),
                "OM_LAB_ANALYZER", "IHE", sut.getCharset().getHl7Code(), container, selectedQueryMode);

        try {
            String messageToSendString = null;
            PipeParser pipeParser = new PipeParser();
            messageToSendString = pipeParser.encode(messageToSend);

            if ((messageToSendString != null) && !messageToSendString.isEmpty()) {
                Initiator hl7Initiator = new Initiator(sut, "IHE", "OM_LAB_ANALYZER", simulatedActor,
                        currentTransaction, messageToSendString, "QBP^Q11^QBP_Q11", "LAB", sutActor);
                TransactionInstance instance = hl7Initiator.sendMessageAndGetTheHL7Message();
                hl7Messages = new ArrayList<TransactionInstance>();
                hl7Messages.add(instance);
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The message has not been built !");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to build the message for sending: " + e.getMessage());
        }
    }

    /**
     * <p>Getter for the field <code>hl7Messages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TransactionInstance> getHl7Messages() {
        return hl7Messages;
    }

    /**
     * <p>Setter for the field <code>hl7Messages</code>.</p>
     *
     * @param hl7Messages a {@link java.util.List} object.
     */
    public void setHl7Messages(List<TransactionInstance> hl7Messages) {
        this.hl7Messages = hl7Messages;
    }

    /**
     * <p>Getter for the field <code>selectedQueryMode</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.ordermanager.utils.QueryMode} object.
     */
    public QueryMode getSelectedQueryMode() {
        return selectedQueryMode;
    }

    /**
     * <p>Setter for the field <code>selectedQueryMode</code>.</p>
     *
     * @param selectedQueryMode a {@link net.ihe.gazelle.ordermanager.utils.QueryMode} object.
     */
    public void setSelectedQueryMode(QueryMode selectedQueryMode) {
        this.selectedQueryMode = selectedQueryMode;
    }

    /**
     * <p>Getter for the field <code>container</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.ordermanager.model.Container} object.
     */
    public Container getContainer() {
        return container;
    }

    /**
     * <p>Setter for the field <code>container</code>.</p>
     *
     * @param container a {@link net.ihe.gazelle.ordermanager.model.Container} object.
     */
    public void setContainer(Container container) {
        this.container = container;
    }

    /**
     * <p>Getter for the field <code>currentTransaction</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Getter for the field <code>selectedDomain</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public Domain getSelectedDomain() {
        return selectedDomain;
    }

    /**
     * <p>Getter for the field <code>sutActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSutActor() {
        return sutActor;
    }

    /**
     * <p>Getter for the field <code>queryModes</code>.</p>
     *
     * @return an array of {@link javax.faces.model.SelectItem} objects.
     */
    public SelectItem[] getQueryModes() {
        return queryModes.clone();
    }

}
