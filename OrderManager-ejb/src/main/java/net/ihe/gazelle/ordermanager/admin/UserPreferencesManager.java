package net.ihe.gazelle.ordermanager.admin;

import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.ordermanager.datamodel.UserPreferencesDataModel;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.IntegrationProfile;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import java.io.Serializable;
import java.util.Date;

/**
 * Class description : <b>UserPreferecesManager</b>
 * 
 * This class is a backing bean to handle logged in user's preferences. This bean as a SESSION scope which enable the application to retrieve user's information on each page
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */

@Stateful
@Name("userPreferencesManagerBean")
@Scope(ScopeType.SESSION)
@GenerateInterface("UserPreferencesManagerLocal")
public class UserPreferencesManager implements Serializable, UserPreferencesManagerLocal {

	
	private static Logger log = LoggerFactory.getLogger(UserPreferencesManager.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean seHEModeEnabled;

	private UserPreferences currentUser = null;
	private HL7V2ResponderSUTConfiguration selectedSUT;
	private IntegrationProfile selectedIntegrationProfile = null;
	private String stationAETitle = null;
	private boolean displayOnlyUsersObjects = false;
	private boolean hl7v251Option = false;
	
	public boolean isHL7v251OptionActivated(){
		Boolean activated = (Boolean) Component.getInstance("hl7v251Option");
		if (activated != null){
			return activated;
		} else {
			return false;
		}
	}

    public static UserPreferencesManagerLocal instance(){
        return (UserPreferencesManagerLocal) Component.getInstance("userPreferencesManagerBean");
    }
	
	/**
	 * @return the allUserPreferences
	 */
	@Override
	public UserPreferencesDataModel getAllUserPreferences() {
		return new UserPreferencesDataModel();
	}

	@Override
	public UserPreferences getCurrentUser() {
		return currentUser;
	}

	@Override
	public void setCurrentUser(UserPreferences currentUser) {
		this.currentUser = currentUser;
	}

	@Override
	public HL7V2ResponderSUTConfiguration getSelectedSUT() {
		return selectedSUT;
	}

	@Override
	public void setSelectedSUT(HL7V2ResponderSUTConfiguration selectedSUT) {
		this.selectedSUT = selectedSUT;
		if (currentUser != null) {
			currentUser.setSystemConfiguration(selectedSUT);
			currentUser = currentUser.save();
		}
		Contexts.getSessionContext().set("selectedSUT", selectedSUT);
	}

	@Override
	public IntegrationProfile getSelectedIntegrationProfile() {
		return selectedIntegrationProfile;
	}

	@Override
	public void setStationAETitle(String stationAETitle) {
		if (currentUser != null) {
			currentUser.setAeTitle(stationAETitle);
			currentUser = currentUser.save();
		}
		this.stationAETitle = stationAETitle;
	}

	@Override
	public String getStationAETitle() {
		return stationAETitle;
	}

	@Override
	public void setSelectedIntegrationProfile(IntegrationProfile selectedIntegrationProfile) {
		this.selectedIntegrationProfile = selectedIntegrationProfile;
		if (currentUser != null) {
			currentUser = currentUser.save();
		}
		Contexts.getSessionContext().set("integrationProfile", selectedIntegrationProfile);
	}

	/**
	 * @param displayOnlyUsersObjects
	 *            the displayOnlyUsersObjects to set
	 */
	@Override
	public void setDisplayOnlyUsersObjects(boolean displayOnlyUsersObjects) {
		this.displayOnlyUsersObjects = displayOnlyUsersObjects;
		if ((currentUser != null) && (currentUser.isViewOnlyMyObjects() != displayOnlyUsersObjects)) {
			currentUser.setViewOnlyMyObjects(displayOnlyUsersObjects);
			currentUser = currentUser.save();
		}
	}

	/**
	 * @return the displayOnlyUsersObjects
	 */
	@Override
	public boolean isDisplayOnlyUsersObjects() {
		return displayOnlyUsersObjects;
	}
	
	public boolean isHl7v251Option() {
		return hl7v251Option;
	}

	public void setHl7v251Option(boolean hl7v251Option) {
		this.hl7v251Option = hl7v251Option;
		if (currentUser != null) {
			currentUser.setHl7v251Option(hl7v251Option);
			currentUser = currentUser.save();
		}
		Contexts.getSessionContext().set("hl7v251Option", hl7v251Option);
	}

	@Override
	@Create
	public void getUserPreferences() {
		if (currentUser == null) {
			if (Identity.instance().isLoggedIn()) {
				String username = Identity.instance().getCredentials().getUsername();
				currentUser = UserPreferences.getPreferencesForUser(username);
				if (currentUser != null) {
					selectedSUT = currentUser.getSystemConfiguration();
					displayOnlyUsersObjects = currentUser.isViewOnlyMyObjects();
					stationAETitle = currentUser.getAeTitle();
					Contexts.getSessionContext().set("selectedSUT", selectedSUT);
					Contexts.getSessionContext().set("integrationProfile", selectedIntegrationProfile);
					Contexts.getSessionContext().set("hl7v251Option", hl7v251Option);
				} else {
					currentUser = new UserPreferences(username);
					setDefaultAttributes();
				}
				currentUser.setLastLogin(new Date());
				currentUser = currentUser.save();
			} else {
				currentUser = null;
				setDefaultAttributes();
			}
		}
		Contexts.getSessionContext().set("currentUser", currentUser);
		Boolean value = PreferenceService.getBoolean("SeHE_mode_enabled");
		seHEModeEnabled = (value != null && value); 
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
		log.info("Destroy UserPreferences...");
		currentUser = null;
		Contexts.getSessionContext().set("currentUser", null);
		setDefaultAttributes();
	}

	private void setDefaultAttributes() {
		displayOnlyUsersObjects = false;
	}

	@Override
	public String getPageUrlForConfig(String actorKeyword, String transactionKeyword) {
		StringBuilder url = new StringBuilder();
        synchronized (url) {
            url.append("/hl7Responders/configurationAndMessages.seam?actor=");
            url.append(actorKeyword);
            url.append("&transaction=");
            url.append(transactionKeyword);
            return url.toString();
        }
	}

	public boolean isSeHEModeEnabled() {
		return seHEModeEnabled;
	}
}
