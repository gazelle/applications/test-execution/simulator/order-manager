package net.ihe.gazelle.ordermanager.hl7.initiators;

/**
 * <b>Class Description : </b>SelectionType<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 */
public enum SelectionType {

    PATIENT,
    DDS,
    ENCOUNTER,
    ORDER, PROCEDURE, SPS, AWOS, SPECIMEN
}
