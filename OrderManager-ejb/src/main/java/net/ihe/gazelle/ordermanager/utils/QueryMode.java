package net.ihe.gazelle.ordermanager.utils;

public enum QueryMode {
    WOS_ALL("All Work", LAWOptions.LAW_QUERY_ALL, "WOS_ALL", "Work Order Step All"),
    CONTAINER_ID_ONLY("Specimen Container with barcode", LAWOptions.LAW_BIDIR, "WOS", "Work Order Step"),
    CARRIER_ID("Specimen Container in rack", LAWOptions.LAW_QUERY_RACK, "WOS_BY_RACK", "Work Order Step by rack"),
    TRAY_ID("Specimen Container in tray", LAWOptions.LAW_QUERY_TRAY, "WOS_BY_TRAY", "Work Order Step by tray"),
    ISOLATE("Isolate", LAWOptions.LAW_QUERY_ISOLATE, "WOS_BY_ISOLATE", "Work Order Step by isolate");

    String label;
    LAWOptions option;
    String queryNameId;
    String queryName;

    public static final String CODING_SYSTEM = "IHELAW";

    public String getQueryNameId() {
        return queryNameId;
    }

    public String getQueryName() {
        return queryName;
    }

    public LAWOptions getOption() {
        return option;
    }

    private QueryMode(String label, LAWOptions option, String queryNameId, String queryName) {
        this.label = label;
        this.option = option;
        this.queryName = queryName;
        this.queryNameId = queryNameId;
    }

    public String getLabel() {
        return this.label;
    }

    public static QueryMode getQueryMode(String queryType) {
        for (QueryMode mode : values()) {
            if (mode.getQueryNameId().equals(queryType)) {
                return mode;
            } else {
                continue;
            }
        }
        return null;
    }
}