package net.ihe.gazelle.ordermanager.hl7.responders;

import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;

/**
 * <p>TroCreator class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class TroCreator extends HttpServer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** {@inheritDoc} */
	@Override
	protected IHEDefaultHandler getHandler() {
		TroReceiver handler = new TroReceiver();
		return handler.newInstance();
	}

	/** {@inheritDoc} */
	@Override
	protected String getActorKeyword() {
		return "TRO_CREATOR";
	}

	/** {@inheritDoc} */
	@Override
	protected String getTransactionKeyword() {
		return "KSA-TRO";
	}

	/** {@inheritDoc} */
	@Override
	protected String getDomainKeyword() {
		return "SeHE";
	}

}
