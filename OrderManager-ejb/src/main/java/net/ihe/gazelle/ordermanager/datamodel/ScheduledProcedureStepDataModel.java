package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStep;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.Component;
import org.jboss.seam.security.Identity;

import java.util.Date;

/**
 * Class description : <b>RequestedProcedureDataModel</b>
 * <p>
 * This class is a data model for properly and quickly displaying the RequestedProcedure objects in the GUI
 *
 * @author Anne-Gaëlle Bergé - IHE Europe
 * @version 1.0 - 2012, April 18th
 *
 *
 *
 */
public class ScheduledProcedureStepDataModel extends FilterDataModel<ScheduledProcedureStep> {

    /**
     *
     */
    private static final long serialVersionUID = -5491788041051382192L;
    private String creator;
    private Date startDate;
    private Date endDate;
    private String placerNumber;
    private String fillerNumber;
    private Domain domain;
    private Actor simulatedActor;
    private Boolean stillScheduledProcedure;
    private String searchedVisitNumber;
    private String searchedPatientId;
    private Boolean worklistCreated;

    public ScheduledProcedureStepDataModel() {
        super(new ScheduledProcedureStepFilter());
    }

    public ScheduledProcedureStepDataModel(Date startDate, Date endDate, String placerNumber, String fillerNumber,
                                           Domain domain, Actor simulatedActor, String patientId, String visitNumber, Boolean stillScheduledProcedure,
                                           Boolean worklistCreated) {
        super(new ScheduledProcedureStepFilter());
        this.creator = null;
        if (Identity.instance().isLoggedIn()) {
            UserPreferences preferences = (UserPreferences) Component.getInstance("userPreferences");
            if (preferences.isViewOnlyMyObjects()) {
                this.creator = preferences.getUsername();
            }
        }
        if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        }
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        }
        this.placerNumber = placerNumber;
        this.fillerNumber = fillerNumber;
        this.domain = domain;
        this.simulatedActor = simulatedActor;
        this.searchedVisitNumber = visitNumber;
        this.searchedPatientId = patientId;
        this.stillScheduledProcedure = stillScheduledProcedure;
        this.worklistCreated = worklistCreated;
    }

    @Override
    public void appendFiltersFields(HQLQueryBuilder<ScheduledProcedureStep> hqlBuilder) {
        if ((creator != null) && !creator.isEmpty()) {
            hqlBuilder.addEq("creator", creator);
        }
        if (startDate != null) {
            hqlBuilder.addRestriction(HQLRestrictions.gt("lastChanged", startDate));
        }
        if (endDate != null) {
            hqlBuilder.addRestriction(HQLRestrictions.lt("lastChanged", endDate));
        }
        if ((placerNumber != null) && !placerNumber.isEmpty()) {
            hqlBuilder.addRestriction(HQLRestrictions.like("requestedProcedure.order.placerOrderNumber", placerNumber,
                    HQLRestrictionLikeMatchMode.ANYWHERE));
        }
        if ((fillerNumber != null) && !fillerNumber.isEmpty()) {
            hqlBuilder.addRestriction(HQLRestrictions.like("requestedProcedure.order.fillerOrderNumber", fillerNumber,
                    HQLRestrictionLikeMatchMode.ANYWHERE));
        }
        if (domain != null) {
            hqlBuilder.addEq("domain", domain);
        }
        if (simulatedActor != null) {
            hqlBuilder.addEq("simulatedActor", simulatedActor);
        }
        if ((stillScheduledProcedure != null) && stillScheduledProcedure) {
            hqlBuilder.addEq("requestedProcedure.orderStatus", "SC");
        }
        if ((searchedVisitNumber != null) && !searchedVisitNumber.isEmpty()) {
            hqlBuilder.addRestriction(HQLRestrictions.like("requestedProcedure.order.encounter.visitNumber",
                    searchedVisitNumber, HQLRestrictionLikeMatchMode.ANYWHERE));
        }
        if ((searchedPatientId != null) && !searchedPatientId.isEmpty()) {
            hqlBuilder.addRestriction(HQLRestrictions.like(
                    "requestedProcedure.order.encounter.patient.patientIdentifiers.identifier", searchedPatientId,
                    HQLRestrictionLikeMatchMode.ANYWHERE));
        }
        if (worklistCreated != null) {
            if (worklistCreated) {
                hqlBuilder.addRestriction(HQLRestrictions.isNotEmpty("worklist"));
            } else {
                hqlBuilder.addRestriction(HQLRestrictions.isEmpty("worklist"));
            }
        }
    }

    @Override
    protected Object getId(ScheduledProcedureStep t) {
        // TODO Auto-generated method stub
        return t.getId();
    }
}
