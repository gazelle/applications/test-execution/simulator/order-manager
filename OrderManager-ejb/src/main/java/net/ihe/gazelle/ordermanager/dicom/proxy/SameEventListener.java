package net.ihe.gazelle.ordermanager.dicom.proxy;

import net.ihe.gazelle.proxy.netty.channel.ProxySide;

import java.sql.Timestamp;

public abstract class SameEventListener<T> extends GazelleProxyEventListener<T, T> {

    public SameEventListener() {
        super();
    }

    protected abstract void saveMessage(Timestamp timeStamp, T message, String requesterIp, int requesterPort,
            int proxyPort, String responderIp, int responderPort, int requestChannelId, int responseChannelId,
            ProxySide side);

    public void onRequest(Timestamp timeStamp, T request, String requesterIp, int requesterPort, int proxyPort,
            String responderIp, int responderPort, int requestChannelId, int responseChannelId) {
        saveMessage(timeStamp, request, requesterIp, requesterPort, proxyPort, responderIp, responderPort,
                requestChannelId, responseChannelId, ProxySide.REQUEST);
    }

    public void onResponse(Timestamp timeStamp, T response, String requesterIp, int requesterPort, int proxyPort,
            String responderIp, int responderPort, int requestChannelId, int responseChannelId) {
        saveMessage(timeStamp, response, requesterIp, requesterPort, proxyPort, responderIp, responderPort,
                requestChannelId, responseChannelId, ProxySide.RESPONSE);
    }

}
