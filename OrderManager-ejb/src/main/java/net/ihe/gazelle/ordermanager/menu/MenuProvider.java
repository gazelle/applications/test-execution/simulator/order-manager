package net.ihe.gazelle.ordermanager.menu;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.pages.menu.Menu;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;

/**
 * <b>Class Description : </b>MenuProvider<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 10/06/16
 *
 *
 *
 */

@Name("menuProvider")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("MenuProviderLocal")
public class MenuProvider implements Serializable, MenuProviderLocal{

    public Menu getMenu(){
        return MainMenu.getMenu();
    }
}
