package net.ihe.gazelle.ordermanager.dicom.utils;

import com.pixelmed.dicom.Attribute;
import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.AttributeTag;
import com.pixelmed.dicom.DicomDictionary;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.dicom.DicomInputStream;
import com.pixelmed.dicom.SOPClassDescriptions;
import net.ihe.gazelle.ordermanager.dico.DicomCommands;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by aberge on 26/01/17.
 */
public final class DICOMDumpUtil {

    private static final String CHARSET_NAME = "UTF-8";
    private static final String DUMP_EXTENSION = ".dump";

    private static final Logger LOG = LoggerFactory.getLogger(DICOMDumpUtil.class);
    private static final DicomDictionary DICOM_DICTIONARY = new DicomDictionary();

    private DICOMDumpUtil() {

    }

    /**
     * Calls dcmdump tool to convert the DICOM data set into a readable text file
     */
    public static void convertDataSetToText(String filePath) throws InterruptedException {
        if (filePath != null) {
            Runtime runtime = Runtime.getRuntime();
            try {
                final Process dcmdump = runtime.exec(new String[]{"dcmdump", "-ll", "error", filePath});
                final String dumpFilePath = filePath.concat(DUMP_EXTENSION);
                // read the error output in a separate thread
                Thread dcmdumpThread = new Thread() {
                    @Override
                    public void run() {
                        convertDicomMessage(dcmdump, dumpFilePath);
                    }
                };
                dcmdumpThread.start();
                // wait for the thread to finish
                dcmdumpThread.join();
            } catch (IOException e) {
                LOG.error("Error while converting DICOM worklist to text file using dcmdump command", e);
            }
        }
    }

    private static void convertDicomMessage(Process dcmdump, String dumpFilePath) {
        BufferedReader errorStreamReader = null;
        BufferedReader streamReader = null;
        StringBuilder errorsBuffer = new StringBuilder();
        FileOutputStream dumpFos = null;
        try {
            errorStreamReader = new BufferedReader(new InputStreamReader(dcmdump.getErrorStream(), Charset.forName(CHARSET_NAME)));
            streamReader = new BufferedReader(new InputStreamReader(dcmdump.getInputStream(), Charset.forName(CHARSET_NAME)));
            String line;
            dumpFos = new FileOutputStream(dumpFilePath);
            while ((line = errorStreamReader.readLine()) != null) {
                if (line.startsWith("E:")) {
                    synchronized (errorsBuffer) {
                        errorsBuffer.append(line);
                        errorsBuffer.append('\n');
                    }
                }
            }
            while ((line = streamReader.readLine()) != null) {
                dumpFos.write(line.getBytes(Charset.forName(CHARSET_NAME)));
                dumpFos.write('\n');
            }
        } catch (IOException ioe) {
            LOG.error(ioe.getMessage(), ioe);
        } finally {
            IOUtils.closeQuietly(errorStreamReader);
            IOUtils.closeQuietly(dumpFos);
            IOUtils.closeQuietly(streamReader);
            if (errorsBuffer.length() > 0) {
                LOG.error(errorsBuffer.toString());
            }
        }
    }

    public static String dumpFileCommandSet(boolean compact, final byte[] commandSet) {
        if (commandSet != null) {
            AttributeList lattributeList = new AttributeList();

            try {
                lattributeList.read(new DicomInputStream(new ByteArrayInputStream(commandSet)));
                Set<AttributeTag> keys = lattributeList.keySet();
                Iterator<AttributeTag> iterator = keys.iterator();

                StringWriter sw = new StringWriter();
                BufferedWriter bw = new BufferedWriter(sw);

                while (iterator.hasNext()) {
                    AttributeTag tag = iterator.next();
                    Attribute attributeValue = lattributeList.get(tag);

                    if (!compact) {
                        bw.append("<p><span style=\"font-family: monospace;\">");
                        bw.append(StringEscapeUtils.escapeHtml(attributeValue.toString(DICOM_DICTIONARY)));
                        bw.append("</span></p>");
                        bw.newLine();
                    }

                    // affected sop class
                    if (tag.equals(DICOM_DICTIONARY.getTagFromName("AffectedSOPClassUID"))) {
                        extractAffectedSopClassUID(compact, bw, attributeValue);
                    }

                    // Command field
                    if (tag.equals(DICOM_DICTIONARY.getTagFromName("CommandField"))) {
                        extractCommandField(compact, bw, attributeValue);
                    }

                    // requested sop class
                    if (tag.equals(DICOM_DICTIONARY.getTagFromName("RequestedSOPClassUID"))) {
                        extractRequestedSopClassUID(compact, bw, attributeValue);
                    }
                    if (!compact && iterator.hasNext()) {
                        bw.append("<hr />");
                    }
                }
                bw.close();
                return sw.toString();

            } catch (IOException | DicomException e) {
                LOG.error(e.getMessage(), e);
            }
        }
        return "";
    }

    private static void extractRequestedSopClassUID(boolean compact, BufferedWriter bw, Attribute attributeValue) {
        try {
            String fieldValue = attributeValue.getDelimitedStringValuesOrEmptyString();
            String lrequestedSopClassUID = SOPClassDescriptions.getDescriptionFromUID(fieldValue);
            if (lrequestedSopClassUID == null) {
                lrequestedSopClassUID = fieldValue;
            }
            if (!compact) {
                bw.append("<p>Requested SOP class : <span style=\"font-family: monospace;\">");
                bw.append(StringEscapeUtils.escapeHtml(lrequestedSopClassUID));
                bw.append("</span></p>");
                bw.newLine();
            } else {
                bw.append("Req. <span style=\"font-family: monospace;\">");
                bw.append(StringEscapeUtils.escapeHtml(lrequestedSopClassUID));
                bw.append("</span><br />");
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private static void extractCommandField(boolean compact, BufferedWriter bw, Attribute attributeValue) {
        try {
            if (!compact) {
                bw.append("<p>Command fields : <span style=\"font-family: monospace;\">");
            } else {
                bw.append("Cmd. <span style=\"font-family: monospace;\">");
            }
            int[] integerValues = attributeValue.getIntegerValues();
            for (Integer fieldValue : integerValues) {
                String commandFieldTmp = DicomCommands.toString(fieldValue);
                bw.append(StringEscapeUtils.escapeHtml(commandFieldTmp));
                bw.append(" ");
            }
            if (!compact) {
                bw.append("</span></p>");
                bw.newLine();
            } else {
                bw.append("</span><br />");
            }
        } catch (DicomException | IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private static void extractAffectedSopClassUID(boolean compact, BufferedWriter bw, Attribute attributeValue) {
        try {
            String fieldValue = attributeValue.getDelimitedStringValuesOrEmptyString();
            String laffectedSopClassUID = SOPClassDescriptions.getDescriptionFromUID(fieldValue);
            if (laffectedSopClassUID == null) {
                laffectedSopClassUID = fieldValue;
            }
            if (!compact) {
                bw.append("<p>Affected SOP class : <span style=\"font-family: monospace;\">");
                bw.append(StringEscapeUtils.escapeHtml(laffectedSopClassUID));
                bw.append("</span></p>");
                bw.newLine();
            } else {
                bw.append("Aff. <span style=\"font-family: monospace;\">");
                bw.append(StringEscapeUtils.escapeHtml(laffectedSopClassUID));
                bw.append("</span><br />");
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public static String dumpFileCommandSet(final byte[] commandSet) {
        String commandField = null;
        if (commandSet != null) {
            AttributeList lattributeList = new AttributeList();

            try {
                lattributeList.read(new DicomInputStream(new ByteArrayInputStream(commandSet)));
                Set<AttributeTag> keys = lattributeList.keySet();
                Iterator<AttributeTag> iterator = keys.iterator();

                while (iterator.hasNext()) {
                    AttributeTag tag = iterator.next();
                    Attribute attributeValue = lattributeList.get(tag);

                    // Command field
                    if (tag.equals(DICOM_DICTIONARY.getTagFromName("CommandField"))) {
                        commandField = extractCommandField(attributeValue);
                    }
                }
            } catch (IOException | DicomException e) {
                LOG.error(e.getMessage(), e);
            }
        }
        return commandField;
    }

    private static String extractCommandField(Attribute attributeValue) {
        try {
            int[] integerValues = attributeValue.getIntegerValues();
            for (Integer fieldValue : integerValues) {
                return DicomCommands.toString(fieldValue);
            }
        } catch (DicomException e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }
}
