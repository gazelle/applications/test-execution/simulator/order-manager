package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>OrderFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class TeleRadiologyOrderFilter extends Filter<TeleRadiologyOrder> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3284358584889943102L;
	private static List<HQLCriterion<TeleRadiologyOrder, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<TeleRadiologyOrder, ?>>();
	}

	public TeleRadiologyOrderFilter() {
		super(TeleRadiologyOrder.class, defaultCriterions);
	}
}
