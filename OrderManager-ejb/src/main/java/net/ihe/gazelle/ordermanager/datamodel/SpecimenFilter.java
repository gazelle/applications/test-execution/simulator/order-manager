package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.Specimen;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>SpecimenFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, December 6th
 * 
 */
public class SpecimenFilter extends Filter<Specimen> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2269296136570764634L;
	private static List<HQLCriterion<Specimen, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<Specimen, ?>>();
	}

	public SpecimenFilter() {
		super(Specimen.class, defaultCriterions);
	}
}
