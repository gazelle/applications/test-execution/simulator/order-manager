package net.ihe.gazelle.ordermanager.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>LAWOptions<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 12/06/15
 *
 *
 *
 */
public enum LAWOptions {

    LAW_BIDIR("LAW_BIDIR", "Bi-directional communication", "ANALYZER", true, true, false),
    LAW_QUERY_ISOLATE("LAW_QUERY_ISOLATE", "Query by Isolate", "ANALYZER", true, false, false),
    LAW_QUERY_RACK("LAW_QUERY_RACK", "Query by Rack", "ANALYZER", true, false, false),
    LAW_QUERY_TRAY("LAW_QUERY_TRAY", "Query by Tray", "ANALYZER", true, false, false),
    LAW_QUERY_ALL("LAW_QUERY_ALL", "Query All", "ANALYZER", true, false, false),
    LAW_CONTRIB_SUB("LAW_CONTRIB_SUB", "Contribution Substances", "ANALYZER", false, false, true),
    LAW_DILUTIONS("LAW_DILUTIONS", "Dilutions", "ANALYZER", false, true, true),
    LAW_PAT_DEM("LAW_PAT_DEM", "Patient Demographics", "ANALYZER", false, true, true),
    LAW_REFLEX("LAW_REFLEX", "Reflex", "ANALYZER", false, false, true),
    LAW_RERUN("LAW_RERUN", "Rerun", "ANALYZER", false, false, true),
    LAW_AM_RR("LAW_AM_RR", "Analyzer Manager Rerun and Reflex", "ANALYZER_MGR", false, true, false),
    LAW_AM_RR_CONTROL("LAW_AM_RR_CONTROL", "Analyzer Manager Rerun and Reflex Control", "ANALYZEZR_MGR", false, true, false),
    LAW_AWOS_PRIORITY("LAW_AWOS_PRIORITY", "AWOS Priority", "ANALYZER", false, true, true),
    LAW_SPECIMEN("LAW_SPECIMEN", "Specimen Details", "ANALYZER", false, true, false),
    LAW_CONTAINER("LAW_CONTAINER", "Container Details", "ANALYZER", false, true, false),
    LAW_MASS_SPEC("LAW_MASS_SPEC", "Mass spectrometry", "ANALYZER", false, false, true),
    LAW_PRIOR_RES("LAW_PRIOR_RES", "Prior Results", "ANALYZER", false, true, false),
    LAW_REL_OBS("LAW_REL_OBS", "Related Observations", "ANALYZER", false, true, false),
    LAW_RESULT_EXT("LAW_RESULT_EXT", "External Result", "ANALYZER", false, false, true),
    LAW_POOL_AN("LAW_POOL_AN", "Pooling on Analyzer", null, false, true, true),
    LAW_POOL_NOAN("LAW_POOL_NOAN", "Pooling outside of the Analyzer", null, false, true, true);

    LAWOptions(String keyword, String displayName, String actorKeyword, boolean lab27, boolean lab28, boolean lab29) {
        this.keyword = keyword;
        this.displayName = displayName;
        this.actorKeyword = actorKeyword;
        this.usedInLab27 = lab27;
        this.usedInLab28 = lab28;
        this.usedInLab29 = lab29;
    }

    String keyword;
    String displayName;
    String actorKeyword;
    boolean usedInLab27;
    boolean usedInLab28;
    boolean usedInLab29;

    public boolean isUsedInLab27() {
        return usedInLab27;
    }

    public boolean isUsedInLab28() {
        return usedInLab28;
    }

    public boolean isUsedInLab29() {
        return usedInLab29;
    }

    public String getActorKeyword() {
        return actorKeyword;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getKeyword() {
        return keyword;
    }

    public static List<LAWOptions> getOptionsForActor(String actorKeyword, String transactionKeyword) {
        List<LAWOptions> items = new ArrayList<LAWOptions>();
        for (LAWOptions option : values()) {
            if ((option.getActorKeyword() == null || (actorKeyword == null || option.getActorKeyword().equals(actorKeyword)))
                    && option.useableForTransaction(transactionKeyword)) {
                items.add(option);
            }
        }
        return items;
    }

    private boolean useableForTransaction(String transactionKeyword) {
        if (transactionKeyword != null) {
            if (transactionKeyword.equals("LAB-27")) {
                return isUsedInLab27();
            } else if (transactionKeyword.equals("LAB-28")) {
                return isUsedInLab28();
            } else if (transactionKeyword.equals("LAB-29")) {
                return isUsedInLab29();
            }
        }
        return false;
    }

    public static LAWOptions getOptionByKeyword(String optionKeyword) {
        for (LAWOptions option : values()) {
            if (option.getKeyword().equals(optionKeyword)) {
                return option;
            } else {
                continue;
            }
        }
        return null;
    }
}
