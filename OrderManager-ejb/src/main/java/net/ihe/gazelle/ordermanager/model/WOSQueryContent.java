package net.ihe.gazelle.ordermanager.model;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.QPD;
import net.ihe.gazelle.ordermanager.utils.QueryMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity(name = "wosQueryContent")
@Table(name = "om_wos_query_content", schema = "public")
@SequenceGenerator(name = "om_wos_query_content_sequence", sequenceName = "om_wos_query_content_id_seq", allocationSize = 1)
public class WOSQueryContent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2741960103872020864L;

    @Id
    @GeneratedValue(generator = "om_wos_query_content_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "request")
    private String requester;

    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(name = "query_type")
    private String queryType;

    @Column(name = "container_id")
    private String containerId;

    @Column(name = "tray_id")
    private String trayId;

    @Column(name = "position_in_tray")
    private String positionInTray;

    @Column(name = "carrier_id")
    private String carrierId;

    @Column(name = "position_in_carrier")
    private String positionInCarrier;

    @Column(name = "location")
    private String location;

    @Column(name = "primary_container_id")
    private String primaryContainerId;

    @ManyToMany
    @JoinTable(name = "om_wos_query_orders", joinColumns = @JoinColumn(name = "wos_query_content_id"), inverseJoinColumns = @JoinColumn(name = "wos_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "wos_query_content_id", "wos_id"}))
    private List<LabOrder> matchingOrders;

    public WOSQueryContent() {

    }

    public WOSQueryContent(QPD qpd, String requester) throws HL7Exception {
        this.requester = requester;
        this.timestamp = new Date();
        this.queryType = qpd.getMessageQueryName().getIdentifier().getValue();
        this.containerId = qpd.getSAC3ContainerIdentifier().encode();
        this.trayId = qpd.getSAC13TrayIdentifier().encode();
        this.positionInTray = qpd.getSAC11PositionInTray().encode();
        this.positionInCarrier = qpd.getSAC11PositionInCarrier().encode();
        this.carrierId = qpd.getSAC10CarrierIdentification().encode();
        this.location = qpd.getSAC15Location().encode();
        this.primaryContainerId = qpd.getQpd9_SAC4PrimaryParentContainerIdentifier().encode();
    }

    public void save(EntityManager entityManager) {
        entityManager.merge(this);
        entityManager.flush();
    }

    public void updateMatchingOrders() {
        QueryMode queryMode = QueryMode.getQueryMode(queryType);
        if (queryMode != null) {
            LabOrderQuery query = new LabOrderQuery();
            String[] controlCodes = {"NW", "UA"}; // only not accepted orders can be broadcast
            query.simulatedActor().keyword().eq("ANALYZER_MGR");
            query.orderControlCode().in(Arrays.asList(controlCodes));
            switch (queryMode) {
                case CARRIER_ID:
                    if ((this.carrierId != null) && !this.carrierId.isEmpty()) {
                        query.specimens().containers().carrierIdentifier().eq(this.carrierId);
                    }
                    break;
                case CONTAINER_ID_ONLY:
                    if ((this.containerId != null) && !this.containerId.isEmpty()) {
                        query.specimens().containers().containerIdentifier().eq(this.containerId);
                    }
                    break;
                case ISOLATE:
                    if ((this.containerId != null) && !this.containerId.isEmpty()) {
                        query.specimens().containers().containerIdentifier().eq(this.containerId);
                    }
                    if (this.primaryContainerId != null && !this.primaryContainerId.isEmpty()) {
                        query.specimens().containers().primaryContainerIdentifier().eq(this.primaryContainerId);
                    }
                    break;
                case TRAY_ID:
                    if ((this.trayId != null) && !this.trayId.isEmpty()) {
                        query.specimens().containers().trayIdentifier().eq(this.trayId);
                    }
                    break;
                default:
                    // WOS_ALL falls in this case since no more filter applies
                    break;
            }
            if ((this.positionInCarrier != null) && !this.positionInCarrier.isEmpty()) {
                query.specimens().containers().positionInCarrier().eq(this.positionInCarrier);
            }
            if ((this.positionInTray != null) && !this.positionInTray.isEmpty()) {
                query.specimens().containers().positionInTray().eq(this.positionInTray);
            }
            this.matchingOrders = query.getList();
            this.save(EntityManagerService.provideEntityManager());
        }
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    public Date getTimestamp() {
        if (timestamp != null) {
            return (Date) timestamp.clone();
        } else {
            return null;
        }
    }

    public void setTimestamp(Date timestamp) {
        if (timestamp != null) {
            this.timestamp = (Date) timestamp.clone();
        } else {
            this.timestamp = null;
        }
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getTrayId() {
        return trayId;
    }

    public void setTrayId(String trayId) {
        this.trayId = trayId;
    }

    public String getPositionInTray() {
        return positionInTray;
    }

    public void setPositionInTray(String positionInTray) {
        this.positionInTray = positionInTray;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getPositionInCarrier() {
        return positionInCarrier;
    }

    public void setPositionInCarrier(String positionInCarrier) {
        this.positionInCarrier = positionInCarrier;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getId() {
        return id;
    }

    public List<LabOrder> getMatchingOrders() {
        return matchingOrders;
    }

    public void setMatchingOrders(List<LabOrder> matchingOrders) {
        this.matchingOrders = matchingOrders;
    }

    public String getPrimaryContainerId() {
        return primaryContainerId;
    }

    public void setPrimaryContainerId(String primaryContainerId) {
        this.primaryContainerId = primaryContainerId;
    }
}
