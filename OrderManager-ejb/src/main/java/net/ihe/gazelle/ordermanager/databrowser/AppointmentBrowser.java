package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ordermanager.model.Appointment;
import net.ihe.gazelle.ordermanager.model.AppointmentQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.util.Map;

/**
 * <p>AppointmentBrowser class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("appointmentBrowser")
@Scope(ScopeType.PAGE)
public class AppointmentBrowser extends DataBrowser<Appointment> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4255867730713758661L;

	/** {@inheritDoc} */
	@Override
	public void modifyQuery(HQLQueryBuilder<Appointment> queryBuilder, Map<String, Object> map) {
		// TODO Auto-generated method stub
	}

    /** {@inheritDoc} */
    @Override
    public Filter<Appointment> getFilter() {
        if (filter == null){
            filter = new Filter<Appointment>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return filter;
    }

    /** {@inheritDoc} */
    @Override
	protected HQLCriterionsForFilter<Appointment> getHQLCriterionsForFilter() {
		AppointmentQuery query = new AppointmentQuery();
		HQLCriterionsForFilter<Appointment> criterions = query.getHQLCriterionsForFilter();
		criterions.addPath("creator", query.creator());
		criterions.addPath("lastChanged", query.lastChanged());
		return criterions;
	}

	/** {@inheritDoc} */
	@Override
	public String getPermanentLink(Integer objectId) {
		// TODO Auto-generated method stub
		return null;
	}

    /** {@inheritDoc} */
    @Override
    public FilterDataModel<Appointment> getDataModel() {
        return new FilterDataModel<Appointment>(getFilter()) {
            @Override
            protected Object getId(Appointment appointment) {
                return appointment.getId();
            }
        };
    }

}
