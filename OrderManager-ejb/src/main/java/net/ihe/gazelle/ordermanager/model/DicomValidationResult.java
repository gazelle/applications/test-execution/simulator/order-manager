package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Name("dicomValidationResult")
@Table(name = "om_dicom_validation_result", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_dicom_validation_result_sequence", sequenceName = "om_dicom_validation_result_id_seq", allocationSize = 1)
public class DicomValidationResult implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3347552865909358622L;

    @Id
    @GeneratedValue(generator = "om_dicom_validation_result_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    @NotNull
    private Integer id;

    @ManyToOne(targetEntity = DicomMessage.class)
    @JoinColumn(name = "dicom_message_id")
    private DicomMessage dicomMessage;

    @Column(name = "oid")
    private String oid;

    @Column(name = "validation_date")
    private String validationDate;

    @Column(name = "validation_status")
    private String validationStatus;

    @Column(name = "permanent_link")
    private String permanentLink;

    public DicomValidationResult() {
        super();
    }

    public DicomValidationResult(DicomMessage selectedMessage, String oid) {
        this.dicomMessage = selectedMessage;
        this.oid = oid;
    }

    public DicomValidationResult save(){
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        DicomValidationResult result = entityManager.merge(this);
        entityManager.flush();
        return result;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DicomMessage getDicomMessage() {
        return dicomMessage;
    }

    public void setDicomMessage(DicomMessage dicomMessage) {
        this.dicomMessage = dicomMessage;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(String validationDate) {
        this.validationDate = validationDate;
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }

    public String getPermanentLink() {
        return permanentLink;
    }

    public void setPermanentLink(String permanentLink) {
        this.permanentLink = permanentLink;
    }
}
