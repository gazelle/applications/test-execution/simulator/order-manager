package net.ihe.gazelle.ordermanager.hl7.responders;

import net.ihe.gazelle.HL7Common.responder.AbstractServer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.Transactional;

import javax.ejb.DependsOn;

/**
 * Class description : <b>OrderFillerServer</b>
 * 
 * This class is a backing bean started at deployment time. It opens multiple ports and route the received messages to the appropriate handler
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */

@Startup(depends = {"entityManager"})
@DependsOn({"entityManager"})
@Name("orderFillerServer")
@Scope(ScopeType.APPLICATION)
public class OrderFillerServer extends AbstractServer<OrderFiller> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4146460687716413787L;

    @Create
    @Override
    @Transactional
	public void startServers() {
		Actor simulatedActor = Actor.findActorWithKeyword("OF", entityManager);
		handler = new OrderFiller();
		startServers(simulatedActor, null, null);
	}

}
