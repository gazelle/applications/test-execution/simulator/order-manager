package net.ihe.gazelle.ordermanager.hl7.initiators;

import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfigurationQuery;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.admin.UserPreferencesManager;
import net.ihe.gazelle.ordermanager.filter.EncounterFilter;
import net.ihe.gazelle.ordermanager.filter.PatientFilter;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import net.ihe.gazelle.ordermanager.utils.EncounterGeneration;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;
import org.jboss.seam.security.Identity;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <b>Class description</b> AbstractSender This abstract class is used as a basis for most of the HL7 initiators
 * 
 *
 *
 * @author Anne-Gaëlle Bergé / IHE-Europe
 *
 * @version 1.0 - February, 28th 2012
 * 
 */
public abstract class AbstractSender extends EncounterGeneration {

	public static final String RADKEYWORD = "RAD";

	private static Log log = Logging.getLog(AbstractSender.class);

	protected Domain selectedDomain;
	protected Actor simulatedActor;
	protected PatientFilter patientFilter;
	protected EncounterFilter encounterFilter;
	protected List<TransactionInstance> hl7Messages = null;
	protected UserPreferences userPreferences;
	protected String sutActorKeyword;
	protected boolean displayCountryList;
	protected boolean encounterComesFromDispatch;
	protected String sendingFacility;
	protected String sendingApplication;
	// not used in JSF
	protected String username;
	protected Map<String, String> urlParams;
	protected boolean selectPatient;
    private SelectionType selectionType;

	/**
	 * this method is used to send the message to the SUT
	 */
	public abstract void sendMessage();

	/**
	 * This method is called when the user hit the button "randomly fill order"
	 */
	public abstract void fillOrderRandomly();

	/**
	 * methods called when the selected message structure changes, or when the selected action changes, also called after reseting filters... 
	 * This methods must update the content of the display
	 * datamodel
	 */
	public abstract void onChangeActionEvent();

	public String getSequenceDiagramPath(){
		StringBuilder builder = new StringBuilder("/diag/");
		builder.append(selectedDomain.getKeyword());
		builder.append("_");
		builder.append(simulatedActor.getKeyword());
		builder.append("_");
		builder.append(sutActorKeyword);
		builder.append("_send");
		if (UserPreferencesManager.instance().isHL7v251OptionActivated()){
			builder.append("_b");
		}
		builder.append(".png");
		return builder.toString();
	}
	/**
	 * Initialize the page using the parameters give in URL
	 */
	public void initializePage() {
		urlParams = new HashMap<String, String>(FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap());
		// retrieve userPreferences
		if (Identity.instance().isLoggedIn()) {
			userPreferences = (UserPreferences) Component.getInstance("currentUser");
			if (userPreferences != null) {
				username = userPreferences.getUsername();
			}
		}
		// get URL parameters
		if (urlParams.containsKey("domain")) {
			selectedDomain = Domain.getDomainByKeyword(urlParams.get("domain"));
		} else {
			selectedDomain = Domain.getDomainByKeyword(RADKEYWORD);
			FacesMessages.instance().add(
					"domain parameter is expected but missing in the URL, Radiology will be used as default domain");
		}
		if (urlParams.containsKey("actor")) {
			String actorKeyword = urlParams.get("actor");
			simulatedActor = Actor.findActorWithKeyword(actorKeyword);
		} else {
			// default behavior is OP
			simulatedActor = Actor.findActorWithKeyword("OP");
		}
		// check if an encounter is given as parameter, if so, retrieve the encounter
		if (urlParams.containsKey("encounterId")) {
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			selectEncounter(entityManager.find(Encounter.class, Integer.decode(urlParams.get("encounterId"))));
			encounterComesFromDispatch = true;
		} else {
			encounterComesFromDispatch = false;
		}
		setDisplayCountryList(false);
		SimulatorResponderConfigurationQuery query = new SimulatorResponderConfigurationQuery();
		Boolean seHEMode = PreferenceService.getBoolean("SeHE_mode_enabled");
		if (seHEMode != null && seHEMode){
			query.domain().keyword().eq("SeHE");
		} else {
			query.domain().keyword().eq(selectedDomain.getKeyword());
		}
		query.simulatedActor().eq(simulatedActor);
		SimulatorResponderConfiguration conf = query.getUniqueResult();
		if (conf != null){
			if (conf.getHl7Protocol().equals(HL7Protocol.HTTP)){
				sendingApplication = conf.getUrl();
			}else{
				sendingApplication = conf.getReceivingApplication();
			}
			sendingFacility = conf.getReceivingFacility();
		} else {
			sendingApplication = "OM_" + selectedDomain.getKeyword() + "_" + simulatedActor.getKeyword();
			sendingFacility = "IHE";
		}
		selectPatient = true;
        selectionType = SelectionType.ENCOUNTER;
	}

	/**
	 * 
	 * @return list actions
	 */
	public abstract List<SelectItem> listAvailableActions();

	/**
	 * 
	 */
	public void createNewPatientAndEncounter() {
		setSelectedPatient(null);
		generateAnEncounter();
		displayCountryList = false;
	}

	public void displayAvailableCountries() {
		displayCountryList = true;
		setSelectedPatient(null);
		setSelectedEncounter(null);
		initializeGenerator();
        selectionType = SelectionType.DDS;
	}

	/**
	 * 
	 */
	public void displayPatientsList() {
		selectionType = SelectionType.PATIENT;
		// OM-23
		displayCountryList = false;
	}

	/**
	 * creates a new encounter for the selected patient. This method may be overridden to add the object (order/result) creation for this encounter
	 * 
	 * @param inPatient
	 */
	public void createANewEncounterForPatient(Patient inPatient) {
		setSelectedPatient(inPatient);
		generateAnEncounter();
	}


	/**
	 * hides the patient creation panel This method may be overridden to update the datamodel content displayed then
	 */
	public void cancelPatientCreation() {
		displayCountryList = false;
		selectionType = SelectionType.ENCOUNTER;
	}

	/**
	 * Set the given encounter as selectedEncounter and create the associated object This method may be overridden to add the object (order/result) creation for this encounter
	 * 
	 * @param inEncounter
	 */
	public void selectEncounter(Encounter inEncounter) {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		setSelectedEncounter(inEncounter);
        if (inEncounter.getPatient() != null) {
            setSelectedPatient(entityManager.find(Patient.class, inEncounter.getPatient().getId()));
        }else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No demographic linked with this encounter !");
        }
	}

	public abstract void skipPatientSelection();

	/***************************************
	 * GETTERS and SETTERS
	 ***************************************/
	public Domain getSelectedDomain() {
		return selectedDomain;
	}

	public void setSelectedDomain(Domain selectedDomain) {
		this.selectedDomain = selectedDomain;
	}

	public Actor getSimulatedActor() {
		return simulatedActor;
	}

	public void setSimulatedActor(Actor simulatedActor) {
		this.simulatedActor = simulatedActor;
	}

	public List<TransactionInstance> getHl7Messages() {
		return hl7Messages;
	}

	public void setHl7Messages(List<TransactionInstance> hl7Messages) {
		this.hl7Messages = hl7Messages;
		if (hl7Messages == null) {
			onChangeActionEvent();
			selectPatient = true;
		}
	}

	public void setUserPreferences(UserPreferences userPreferences) {
		this.userPreferences = userPreferences;
	}

	public UserPreferences getUserPreferences() {
		return userPreferences;
	}


	public void setSutActorKeyword(String sutActorKeyword) {
		this.sutActorKeyword = sutActorKeyword;
	}

	public String getSutActorKeyword() {
		return sutActorKeyword;
	}

	public void setDisplayCountryList(boolean displayCountryList) {
		this.displayCountryList = displayCountryList;
	}

	public boolean isDisplayCountryList() {
		return displayCountryList;
	}


	public String getSendingFacility() {
		return sendingFacility;
	}

	public void setSendingFacility(String sendingFacility) {
		this.sendingFacility = sendingFacility;
	}

	public String getSendingApplication() {
		return sendingApplication;
	}

	public void setSendingApplication(String sendingApplication) {
		this.sendingApplication = sendingApplication;
	}

	public String getRadKeyword() {
		return RADKEYWORD;
	}

	public boolean isSelectPatient() {
		return selectPatient;
	}

	public void setSelectPatient(boolean selectPatient) {
		this.selectPatient = selectPatient;
	}

    public PatientFilter getPatientFilter(){
        if (patientFilter == null){
            patientFilter = new PatientFilter();
        }
        return patientFilter;
    }

    public EncounterFilter getEncounterFilter(){
        if (encounterFilter == null){
            encounterFilter = new EncounterFilter();
        }
        return encounterFilter;
    }


    public SelectionType getSelectionType() {
        return selectionType;
    }

    public void setSelectionType(SelectionType selectionType) {
        this.selectionType = selectionType;
    }
}
