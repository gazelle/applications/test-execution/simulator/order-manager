package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>OrderFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class OrderFilter extends Filter<Order> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3284358584889943102L;
	private static List<HQLCriterion<Order, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<Order, ?>>();
	}

	public OrderFilter() {
		super(Order.class, defaultCriterions);
	}
}
