package net.ihe.gazelle.ordermanager.action;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * <p>PageUrlProvider class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("pageUrlProvider")
@Scope(ScopeType.APPLICATION)
public class PageUrlProvider {

	/**
	 * <p>getUrlForAwosSender.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUrlForAwosSender() {
		return "/hl7Initiators/awosSender.seam?workOrder=false&actor=ANALYZER_MGR&domain=LAB";
	}

	/**
	 * <p>getUrlForLabOrderSender.</p>
	 *
	 * @param actorKeyword a {@link java.lang.String} object.
	 * @param inDomainKeyword a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String getUrlForLabOrderSender(String actorKeyword, String inDomainKeyword) {
		String url = "/hl7Initiators/labOrderSender.seam?";
		if (actorKeyword.equals("workOrder")) {
			url = url.concat("workOrder=true");
			actorKeyword = "OF";
		} else if (actorKeyword.equals("ANALYZER_MGR")) {
			url = "/hl7Initiators/awosSender.seam?workOrder=false";
		} else {
			url = url.concat("workOrder=false");
		}
		url = url.concat("&actor=");
		url = url.concat(actorKeyword);
		url = url.concat("&domain=" + inDomainKeyword);
		return url;
	}

	/**
	 * <p>getUrlForOrderSender.</p>
	 *
	 * @param actorKeyword a {@link java.lang.String} object.
	 * @param domain a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String getUrlForOrderSender(String actorKeyword, String domain) {
		String url = "/hl7Initiators/orderSender.seam?actor=";
		url = url.concat(actorKeyword);
		url = url.concat("&domain=" + domain);
		return url;
	}

	/**
	 * <p>getUrlForProcedureSender.</p>
	 *
	 * @param domainKeyword a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String getUrlForProcedureSender(String domainKeyword) {
		String url = "/hl7Initiators/procedureSender.seam?actor=OF&domain=" + domainKeyword;
		return url;
	}

	/**
	 * <p>getUrlForOrderResultSender.</p>
	 *
	 * @param actorKeyword a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String getUrlForOrderResultSender(String actorKeyword) {
		return "/hl7Initiators/orderResultSender.seam?domain=LAB&actor=" + actorKeyword;
	}
}
