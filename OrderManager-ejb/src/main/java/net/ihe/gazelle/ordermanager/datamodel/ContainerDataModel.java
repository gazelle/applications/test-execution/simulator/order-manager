package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.Component;
import org.jboss.seam.security.Identity;

import java.util.Date;

/**
 * Class description : <b>ContainerDataModel</b>
 * 
 * This class is a data model for properly and quickly displaying the container objects in the GUI
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, December 6th
 * 
 */

public class ContainerDataModel extends FilterDataModel<Container> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8047045666817095216L;

	/**
	 * 
	 */
	private String creator;
	private Date startDate;
	private Date endDate;
	private String patientId;
	private boolean isPartOfWorkOrder;
	private Actor actor;
	private Specimen relatedSpecimen;

	public ContainerDataModel() {
		super(new ContainerFilter());
	}

	public ContainerDataModel(Specimen relatedSpecimen) {
		super(new ContainerFilter());
		this.relatedSpecimen = relatedSpecimen;
		this.creator = null;
		this.startDate = null;
		this.endDate = null;
		this.patientId = null;
		if (relatedSpecimen != null) {
			this.isPartOfWorkOrder = relatedSpecimen.isWorkOrder();
		}
	}

	public ContainerDataModel(Date startDate, Date endDate, String patientId, boolean isPartOfWorkOrder,
			Actor simulaterActor) {
		super(new ContainerFilter());
		this.creator = null;
		if (Identity.instance().isLoggedIn()) {
			UserPreferences preferences = (UserPreferences) Component.getInstance("userPreferences");
			if (preferences.isViewOnlyMyObjects()) {
				this.creator = preferences.getUsername();
			}
		}
		if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        }
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        }
		this.patientId = patientId;
		this.isPartOfWorkOrder = isPartOfWorkOrder;
		this.relatedSpecimen = null;
		this.actor = simulaterActor;
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<Container> hqlBuilder) {
		hqlBuilder.addEq("workOrder", isPartOfWorkOrder);
		if (relatedSpecimen != null) {
			hqlBuilder.addEq("specimen", relatedSpecimen);
		}
		if ((creator != null) && !creator.isEmpty()) {
			hqlBuilder.addEq("creator", creator);
		}
		if (startDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.gt("lastChanged", startDate));
		}
		if (endDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.lt("lastChanged", endDate));
		}
		if ((patientId != null) && !patientId.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("patient.patientIdentifiers.identifier", patientId,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (actor != null) {
			hqlBuilder.addEq("simulatedActor", actor);
		}
	}
@Override
        protected Object getId(Container t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
