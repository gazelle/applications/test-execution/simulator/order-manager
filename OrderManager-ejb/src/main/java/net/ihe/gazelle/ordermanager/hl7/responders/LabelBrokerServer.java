package net.ihe.gazelle.ordermanager.hl7.responders;

import net.ihe.gazelle.HL7Common.responder.AbstractServer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.Transactional;

import javax.ejb.DependsOn;

/**
 * Class description : <b>LabelBrokerServer</b>
 * 
 * This class is a backing bean started at deployment time. It opens multiple ports and route the received messages to the appropriate handler
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - KEREVAL
 *
 * @version 1.0 - 2016, August 18th
 * 
 */

@Name("labelBrokerServer")
@Startup(depends = {"entityManager"})
@DependsOn({"entityManager"})
@Scope(ScopeType.APPLICATION)
public class LabelBrokerServer extends AbstractServer<LabelBroker> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @Create
    @Override
    @Transactional
	public void startServers() {
		Actor simulatedActor = Actor.findActorWithKeyword("LB", entityManager);
		handler = new LabelBroker();
		startServers(simulatedActor, null, null);
	}

}
