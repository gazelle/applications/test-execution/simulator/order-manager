package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>UserPreferences<br>
 * <br>
 *
 * This class describes the preferences of the logged in users. Login is done using CAS.
 *
 * UserPreferences possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id in the database</li>
 * <li><b>username</b> username of the logged in user (the one from TM)</li>
 * <li><b>domain</b> preferred domain</li>
 * <li><b>integrationProfile</b> preferred integration profile</li>
 * <li><b>systemConfiguration</b> preferred SUT</li>
 * <li><b>lastLogin</b> date/time of the last visit in the application</li>
 * <li><b>aeTitle</b> preferred AE Title</li>
 * <li><b>viewOnlyMyObjects</b> indicates whether the user prefer to see only the objects created by him/her</b></li>
 * </ul>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2011, November 17th
 */
@Entity
@Name("userPreferences")
@Table(name = "om_user_preferences", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "username"))
@SequenceGenerator(name = "om_user_preferences_sequence", sequenceName = "om_user_preferences_id_seq", allocationSize = 1)
public class UserPreferences implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "om_user_preferences_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@Column(name = "username")
	private String username;

	@ManyToOne(targetEntity = net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration.class)
	@JoinColumn(name = "system_configuration_id")
	private HL7V2ResponderSUTConfiguration systemConfiguration;

	@Column(name = "last_login")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLogin;

	@Column(name = "ae_title")
	private String aeTitle;

	@Column(name = "view_only_my_objects")
	private boolean viewOnlyMyObjects;
	
	@Column(name="hl7v251_option")
	private boolean hl7v251Option;

	/**
	 * <p>Constructor for UserPreferences.</p>
	 */
	public UserPreferences() {
		this.hl7v251Option = false;
	}

	/**
	 * <p>Constructor for UserPreferences.</p>
	 *
	 * @param username a {@link java.lang.String} object.
	 */
	public UserPreferences(String username) {
		this.username = username;
		this.hl7v251Option = false;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Getter for the field <code>username</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * <p>Setter for the field <code>username</code>.</p>
	 *
	 * @param username a {@link java.lang.String} object.
	 */
	public void setUsername(String username) {
		this.username = username;
	}

    /**
     * <p>Getter for the field <code>systemConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     */
    public HL7V2ResponderSUTConfiguration getSystemConfiguration() {
        return systemConfiguration;
    }

    /**
     * <p>Setter for the field <code>systemConfiguration</code>.</p>
     *
     * @param systemConfiguration a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     */
    public void setSystemConfiguration(HL7V2ResponderSUTConfiguration systemConfiguration) {
        this.systemConfiguration = systemConfiguration;
    }

	/**
	 * <p>Setter for the field <code>lastLogin</code>.</p>
	 *
	 * @param lastLogin
	 *            the lastLogin to set
	 */
	public void setLastLogin(Date lastLogin) {
        if (lastLogin != null) {
            this.lastLogin = (Date) lastLogin.clone();
        } else {
            this.lastLogin = null;
        }
	}

	/**
	 * <p>Getter for the field <code>lastLogin</code>.</p>
	 *
	 * @return the lastLogin
	 */
	public Date getLastLogin() {
        if (lastLogin != null) {
            return (Date) lastLogin.clone();
        } else {
            return null;
        }
	}

	/**
	 * <p>Setter for the field <code>aeTitle</code>.</p>
	 *
	 * @param aeTitle
	 *            the aeTitle to set
	 */
	public void setAeTitle(String aeTitle) {
		this.aeTitle = aeTitle;
	}

	/**
	 * <p>Getter for the field <code>aeTitle</code>.</p>
	 *
	 * @return the aeTitle
	 */
	public String getAeTitle() {
		return aeTitle;
	}

	/**
	 * <p>getPreferencesForUser.</p>
	 *
	 * @param username a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.ordermanager.model.UserPreferences} object.
	 */
	public static UserPreferences getPreferencesForUser(String username) {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		HQLQueryBuilder<UserPreferences> builder = new HQLQueryBuilder<UserPreferences>(entityManager,
				UserPreferences.class);
		builder.addEq("username", username);
		List<UserPreferences> preferences = builder.getList();
		if ((preferences != null) && !preferences.isEmpty()) {
			return preferences.get(0);
		} else {
			return null;
		}
	}

	/**
	 * <p>Setter for the field <code>viewOnlyMyObjects</code>.</p>
	 *
	 * @param viewOnlyMyObjects
	 *            the viewOnlyMyObjects to set
	 */
	public void setViewOnlyMyObjects(boolean viewOnlyMyObjects) {
		this.viewOnlyMyObjects = viewOnlyMyObjects;
	}

	/**
	 * <p>isViewOnlyMyObjects.</p>
	 *
	 * @return the viewOnlyMyObjects
	 */
	public boolean isViewOnlyMyObjects() {
		return viewOnlyMyObjects;
	}

	/**
	 * <p>save.</p>
	 *
	 * @return a {@link net.ihe.gazelle.ordermanager.model.UserPreferences} object.
	 */
	public UserPreferences save() {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		UserPreferences preference = entityManager.merge(this);
		entityManager.flush();
		Contexts.getSessionContext().set("userPreferences", preference);
		return preference;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserPreferences other = (UserPreferences) obj;
		if (username == null) {
			if (other.username != null) {
				return false;
			}
		} else if (!username.equals(other.username)) {
			return false;
		}
		return true;
	}

	/**
	 * <p>isHl7v251Option.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isHl7v251Option() {
		return hl7v251Option;
	}

	/**
	 * <p>Setter for the field <code>hl7v251Option</code>.</p>
	 *
	 * @param hl7v251Option a boolean.
	 */
	public void setHl7v251Option(boolean hl7v251Option) {
		this.hl7v251Option = hl7v251Option;
	}

}
