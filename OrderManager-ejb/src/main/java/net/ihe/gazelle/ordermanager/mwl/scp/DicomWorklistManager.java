package net.ihe.gazelle.ordermanager.mwl.scp;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.dicom.proxy.DicomEventListener;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.ConnectionConfigSimple;
import net.ihe.gazelle.proxy.netty.protocols.dicom.DicomProxy;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.model.ApplicationConfigurationQuery;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.DependsOn;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class description : <b>DicomHandler</b> This class is used as a kind of proxy, DICOM messages for the SCP are received in this "server" and forward to the dcmtk SCP
 *
 * @author aberge
 */

@Name("dicomWorklistManager")
@Startup(depends = {"entityManager"})
@DependsOn({"entityManager"})
@Scope(ScopeType.APPLICATION)
@GenerateInterface("DicomWorklistManagerLocal")
public class DicomWorklistManager implements Serializable, DicomWorklistManagerLocal {

    /**
     *
     */
    private static final long serialVersionUID = -1703582502032106361L;
    private DicomProxy dicomProxy;
    private List<String> errorMessages;
    private Integer proxyPort;
    private Integer remotePort;
    private String remoteHost;
    private String dataSetDirectory;
    private boolean wlmscpfsStarted;
    private boolean proxyStarted;
    private String worklistsBasedir;
    private Transaction simulatedTransaction;

    public Process getWlmscpfs() {
        return wlmscpfs;
    }

    private Process wlmscpfs;
    private Actor simulatedActor;
    private ApplicationConfiguration wlmscpfsPid;

    private static Logger log = LoggerFactory.getLogger(DicomWorklistManager.class);

    /**
     * Starts the worklist server at deployment time
     */

    @Create
    @Transactional
    public void onStartUp() {
        start(true);
    }

    @Override
    public void start(boolean force) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Boolean radiologyEnabled = ApplicationConfiguration.getBooleanValue("radiology_enabled");
        Boolean eyecareEnabled = ApplicationConfiguration.getBooleanValue("eyecare_enabled");
        if ((radiologyEnabled != null && radiologyEnabled) || (eyecareEnabled != null && eyecareEnabled)) {
            log.info("Configuring and starting the worklist");
            errorMessages = new ArrayList<String>();
            wlmscpfsStarted = false;
            proxyStarted = false;
            wlmscpfs = null;
            dataSetDirectory = ApplicationConfiguration.getValueOfVariable("dcm_data_set_basedir", entityManager);
            remoteHost = ApplicationConfiguration.getValueOfVariable("wlmscpfs_host", entityManager);
            worklistsBasedir = ApplicationConfiguration.getValueOfVariable("worklists_basedir", entityManager);
            simulatedActor = Actor.findActorWithKeyword("OF", entityManager);
            wlmscpfsPid = getWlmscpfsPID(entityManager);
            simulatedTransaction = Transaction.GetTransactionByKeyword("RAD-5", entityManager);
            if (force) {
                updateWlmscpfsPid(null, entityManager);
            }
            String value = ApplicationConfiguration.getValueOfVariable("dicom_proxy_port", entityManager);
            if (value != null && !value.isEmpty()) {
                try {
                    proxyPort = Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    errorMessages
                            .add("DICOM proxy cannot start: dicom_proxy_port is either missing in app_configuration or not well formed. An int is expected");
                    proxyPort = null;
                    return;
                }
            } else {
                errorMessages
                        .add("DICOM proxy cannot start: dicom_proxy_port is missing in app_configuration");
                return;
            }
            value = ApplicationConfiguration.getValueOfVariable("wlmscpfs_port", entityManager);
            if (value != null && !value.isEmpty()) {
                try {
                    remotePort = Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    errorMessages
                            .add("DICOM proxy cannot start: wlmscpfs_port is either missing in app_configuration or not well formed. An int is expected");
                    remotePort = null;
                    return;
                }
            } else {
                errorMessages
                        .add("DICOM proxy cannot start: wlmscpfs_port is missing in app_configuration");
                return;
            }
            if (wlmscpfsPid.getValue() == null || wlmscpfsPid.getValue().isEmpty()) {
                startWlmscpfs(false);
                entityManager.merge(wlmscpfsPid);
                entityManager.flush();
            } else {
                log.info("Worklist is already started with PID " + wlmscpfsPid.getValue());
                wlmscpfsStarted = true;
            }
            if (wlmscpfsStarted) {
                startProxyChannel();
            }
            if (proxyStarted) {
                log.info("DICOM proxy has been successfully started");
            }
        } else {
            log.info("DICOM worklist will not be started because it is not needed");
        }
    }

    private ApplicationConfiguration getWlmscpfsPID(EntityManager entityManager) {
        ApplicationConfigurationQuery query = new ApplicationConfigurationQuery(entityManager);
        query.variable().eq("wlmscpfs_pid");
        ApplicationConfiguration pid = query.getUniqueResult();
        if (pid == null) {
            pid = new ApplicationConfiguration("wlmscpfs_pid", null);
            pid = entityManager.merge(pid);
            entityManager.flush();
        }
        return pid;
    }

    /**
     * launches wlmscpfs binary
     */
    @Override
    public void startWlmscpfs(boolean calledFromGui) {
        if ((worklistsBasedir == null) || (remotePort == null)) {
            errorMessages
                    .add("DICOM proxy cannot start: both dcm_data_set_basedir and wlmscpfs_host entries are required in app_configuration table");
            wlmscpfsStarted = false;
        } else {
            try {
                String[] commandLine = {"wlmscpfs", "--log-level", "info", "--disable-file-reject", "-dfp",worklistsBasedir, remotePort.toString()};
                ProcessBuilder builder = new ProcessBuilder();
                builder.command(Arrays.asList(commandLine));
                builder.redirectErrorStream(true);

                wlmscpfs = builder.start();

                // now looking for the PID of the created process
                Class<? extends Process> proc = wlmscpfs.getClass();
                try {
                    Field field = proc.getDeclaredField("pid");
                    field.setAccessible(true);
                    Integer pid = (Integer) field.get(wlmscpfs);
                    if (calledFromGui) {
                        updateWlmscpfsPid(pid.toString(), null);
                    } else {
                        wlmscpfsPid.setValue(pid.toString());
                    }
                } catch (RuntimeException e) {
                    log.error("Application has not be able to save PID: " + e.getMessage());
                } catch (NoSuchFieldException e){
                    log.error("Cannot find wlmscpfs attribute: " + e.getMessage());
                } catch (IllegalAccessException e){
                    log.error("Cannot access wlmscpfs attribute: " + e.getMessage());
                }

                // input and error streams are merged
                InputStream inStream = wlmscpfs.getInputStream();
                WlmscpfsLogger logger = new WlmscpfsLogger(inStream);

                Thread thread4Logger = new Thread(logger, "WlmscpfsLogger");
                thread4Logger.start();

            } catch (IOException e) {
                wlmscpfsStarted = false;
            }
            wlmscpfsStarted = true;
        }
    }

    /**
     * stops wlmscpfs application
     */
    @Override
    public void stopWlmscpfs() {
        if (wlmscpfsStarted && (wlmscpfs != null)) {
            wlmscpfs.destroy();
            wlmscpfsStarted = false;
            updateWlmscpfsPid(null, null);
        }
        wlmscpfsStarted = false;
    }

    private void updateWlmscpfsPid(String value, EntityManager em) {
        wlmscpfsPid.setValue(value);
        EntityManager entityManager = em;
        if (entityManager == null) {
            entityManager = EntityManagerService.provideEntityManager();
        }
        entityManager.merge(wlmscpfsPid);
        entityManager.flush();
    }

    /**
     * opens the DICOM channel between the Order Manager and the worklist manager
     */
    @Override
    public void startProxyChannel() {
        if (wlmscpfsStarted && (remotePort != null) && (remoteHost != null)) {
            log.info("starting DICOM proxy in OrderManager tool");
            final DicomEventListener dicomProxyEvent = new DicomEventListener(simulatedActor, simulatedTransaction);
            ConnectionConfig connectionConfig = new ConnectionConfigSimple(proxyPort, remoteHost, remotePort,
                    ChannelType.DICOM);
            dicomProxy = new DicomProxy(dicomProxyEvent, connectionConfig, dataSetDirectory);
            try {
                dicomProxy.start();
                proxyStarted = true;
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                errorMessages.add(e.getMessage());
                proxyStarted = false;
            }
        } else {
            errorMessages.add("Application has not been able to start the DICOM proxy");
        }
    }

    /**
     * closes the DICOM channel
     */
    @Override
    public void stopProxyChannel() {
        if (proxyStarted) {
            log.info("Stopping DICOM proxy !");
            dicomProxy.stop();
            proxyStarted = false;
        } else {
            log.info("DICOM channel is already closed");
        }
    }

    @Override
    @Destroy
    public void stop() {
        stopProxyChannel();
        stopWlmscpfs();
    }

    @Override
    public void restartService() {
        stop();
        start(false);
    }

    @Override
    public List<String> getErrorMessages() {
        return errorMessages;
    }

    @Override
    public Integer getProxyPort() {
        return proxyPort;
    }

    @Override
    public Integer getRemotePort() {
        return remotePort;
    }

    @Override
    public String getRemoteHost() {
        return remoteHost;
    }

    @Override
    public String getDataSetDirectory() {
        return dataSetDirectory;
    }

    @Override
    public boolean isWlmscpfsStarted() {
        return wlmscpfsStarted;
    }

    @Override
    public boolean isProxyStarted() {
        return proxyStarted;
    }

    @Override
    public String getWorklistsBasedir() {
        return worklistsBasedir;
    }
}
