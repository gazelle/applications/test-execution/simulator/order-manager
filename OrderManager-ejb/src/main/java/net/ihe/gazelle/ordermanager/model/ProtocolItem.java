package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Name("protocolItem")
@Table(name = "om_protocol_item", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_protocol_item_sequence", sequenceName = "om_protocol_item_id_seq", allocationSize = 1)
public class ProtocolItem extends CommonColumns implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4521914481362106038L;

	@Id
	@NotNull
	@GeneratedValue(generator = "om_protocol_item_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@Column(name = "contrast_agent_code")
	private String contrastAgentCode;

	/**
	 * OBR-4-4
	 */
	@Column(name = "protocol_code")
	private String protocolCode;

	/**
	 * OBR-4-5
	 */
	@Column(name = "protocol_code_meaning")
	private String protocolCodeMeaning;

	/**
	 * OBR-4-6
	 */
	@Column(name = "coding_scheme")
	private String codingScheme;

	@ManyToOne
	@JoinColumn(name = "scheduled_procedure_step_id")
	private ScheduledProcedureStep scheduledProcedureStep;

	public ProtocolItem() {
		super();
	}

	public ProtocolItem(Actor simulatedActor, String creator, Domain domain) {
		super(simulatedActor, creator, domain);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContrastAgentCode() {
		return contrastAgentCode;
	}

	public void setContrastAgentCode(String contrastAgentCode) {
		this.contrastAgentCode = contrastAgentCode;
	}

	public String getProtocolCode() {
		return protocolCode;
	}

	public void setProtocolCode(String protocolCode) {
		this.protocolCode = protocolCode;
	}

	public String getProtocolCodeMeaning() {
		return protocolCodeMeaning;
	}

	public void setProtocolCodeMeaning(String protocolCodeMeaning) {
		this.protocolCodeMeaning = protocolCodeMeaning;
	}

	public String getCodingScheme() {
		return codingScheme;
	}

	public void setCodingScheme(String codingScheme) {
		this.codingScheme = codingScheme;
	}

	public ScheduledProcedureStep getScheduledProcedureStep() {
		return scheduledProcedureStep;
	}

	public void setScheduledProcedureStep(ScheduledProcedureStep scheduledProcedureStep) {
		this.scheduledProcedureStep = scheduledProcedureStep;
	}

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!super.equals(o)) return false;
        if (getClass() != o.getClass()) return false;

        ProtocolItem that = (ProtocolItem) o;

        if (protocolCode != null ? !protocolCode.equals(that.protocolCode) : that.protocolCode != null) return false;
        if (protocolCodeMeaning != null ? !protocolCodeMeaning.equals(that.protocolCodeMeaning) : that.protocolCodeMeaning != null)
            return false;
        if (codingScheme != null ? !codingScheme.equals(that.codingScheme) : that.codingScheme != null) return false;
        return scheduledProcedureStep != null ? scheduledProcedureStep.equals(that.scheduledProcedureStep) : that.scheduledProcedureStep == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (protocolCode != null ? protocolCode.hashCode() : 0);
        result = 31 * result + (protocolCodeMeaning != null ? protocolCodeMeaning.hashCode() : 0);
        result = 31 * result + (codingScheme != null ? codingScheme.hashCode() : 0);
        result = 31 * result + (scheduledProcedureStep != null ? scheduledProcedureStep.hashCode() : 0);
        return result;
    }
}
