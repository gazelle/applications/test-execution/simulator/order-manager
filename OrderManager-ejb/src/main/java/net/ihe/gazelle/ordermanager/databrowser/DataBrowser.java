package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.annotations.Create;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

/**
 * Class description : <b>DataBrowser</b>
 * 
 * This class is a backing bean to handle the pages which display the database content and more especially the registered patients, orders, worklists and appointments
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - IHE EUROPE
 *
 * @version 2.0 - 2012, November 5th
 * 
 */

public abstract class DataBrowser<T> implements Serializable, QueryModifier<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4171216182420781142L;

	protected ObjectType objectType;
	protected Filter<T> filter;
	protected Domain domain;
	protected Actor actor;
	protected String fillerOrderNumber;
	protected String placerOrderNumber;
	protected String patientId;
	protected String visitNumber;
	protected Integer containerId;

	@Create
	public void initializePage() {
		Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		domain = Domain.getDomainByKeyword(urlParams.get("domainKey"));
		actor = Actor.findActorWithKeyword(urlParams.get("actorKey"));
		objectType = ObjectType.getObjectTypeByKeyword(urlParams.get("objectType"));
		if (urlParams.get("containerId") != null) {
			containerId = Integer.decode(urlParams.get("containerId"));
		} else {
			containerId = null;
		}
	}

	public ObjectType getObjectType() {
		return objectType;
	}

	public void reset() {
		getFilter().clear();
		fillerOrderNumber = null;
		placerOrderNumber = null;
		patientId = null;
		visitNumber = null;
	}

	public abstract  Filter<T> getFilter();

	protected abstract HQLCriterionsForFilter<T> getHQLCriterionsForFilter();

	public abstract String getPermanentLink(Integer objectId);

	public String getPermanentLink(Domain inDomain, Integer objectId) {
		this.domain = inDomain;
		return getPermanentLink(objectId);
	}

	@SuppressWarnings("unchecked")
	public abstract FilterDataModel<T> getDataModel();

	public Domain getDomain() {
		return domain;
	}

	public Actor getActor() {
		return actor;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getVisitNumber() {
		return visitNumber;
	}

	public void setVisitNumber(String visitNumber) {
		this.visitNumber = visitNumber;
	}

	public String getFillerOrderNumber() {
		return fillerOrderNumber;
	}

	public void setFillerOrderNumber(String fillerOrderNumber) {
		this.fillerOrderNumber = fillerOrderNumber;
	}

	public String getPlacerOrderNumber() {
		return placerOrderNumber;
	}

	public void setPlacerOrderNumber(String placerOrderNumber) {
		this.placerOrderNumber = placerOrderNumber;
	}

}
