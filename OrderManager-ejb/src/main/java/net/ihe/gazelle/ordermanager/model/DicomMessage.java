package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.evsclient.connector.api.EVSClientServletConnector;
import net.ihe.gazelle.evsclient.connector.model.EVSClientValidatedObject;
import net.ihe.gazelle.ordermanager.dicom.utils.DICOMDumpUtil;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.apache.commons.httpclient.methods.multipart.FilePartSource;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class description : <b>DicomMessage</b>
 * <p>
 * This class is the entity used to store received and set DICOM messages
 *
 * @author aberge
 */
@Entity
@Name("dicomMessage")
@Table(name = "om_dicom_message", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_dicom_request_sequence", sequenceName = "om_dicom_request_id_seq", allocationSize = 1)
public class DicomMessage implements Comparable<DicomMessage>, Serializable, EVSClientValidatedObject {

    /**
     *
     */
    private static final long serialVersionUID = 1720390674255306980L;
    public static final String DUMP_EXTENSION = ".dump";
    public static final String OBJECT_TYPE = "DICOM";

    private static final Logger LOG = LoggerFactory.getLogger(DicomMessage.class);
    public static final String QUESTION_MARK = "?";
    public static final String FIRST_INDEX = "0";

    /**
     *
     */


    @Id
    @NotNull
    @GeneratedValue(generator = "om_dicom_request_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "from_host_port")
    private String from;

    @Column(name = "to_host_port")
    private String to;

    @ManyToOne(fetch = FetchType.LAZY)
    private Connection connection;

    @Column(name = "file_command_set")
    @Basic(fetch = FetchType.LAZY)
    private byte[] commandSet;

    /**
     * true if it is an incoming message, false for an outcoming message
     */
    @Column(name = "is_request")
    private boolean isRequest;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(name = "transfert_syntax")
    @Lob
    @Type(type = "text")
    private String transferSyntax;

    @Column(name = "dicom_commandfield")
    private String commandField;

    @OneToMany(targetEntity = DicomValidationResult.class)
    private List<DicomValidationResult> validationResults;

    @ManyToOne
    @JoinColumn(name = "simulated_actor_id")
    private Actor simulatedActor;

    @ManyToOne
    @JoinColumn(name = "simulated_transaction_id")
    private Transaction transaction;

    @Column(name = "index")
    private String index;

    @Column(name = "index_int")
    private int indexInt;

    public DicomMessage(Timestamp timeStamp, String requesterIp,
                        String responderIp, ProxySide side) {
        setTimestamp(timeStamp);
        this.isRequest = side.isRequest();
        this.from = side.isRequest()? requesterIp:responderIp;
        this.to = side.isRequest()? responderIp:requesterIp;

    }

    public DicomMessage() {

    }

    public int getIndexInt() {
        return indexInt;
    }

    public void setIndexInt(int inIndexInt) {
        this.indexInt = inIndexInt;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
        if (QUESTION_MARK.equals(index)) {
            setIndex(FIRST_INDEX);
        }
        setIndexInt(Integer.parseInt(index));
    }

    public String getAttributesForCommandSetString() {
        return DICOMDumpUtil.dumpFileCommandSet(false, getCommandSet());
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public boolean isRequest() {
        return isRequest;
    }

    public void setRequest(boolean isRequest) {
        this.isRequest = isRequest;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public final void setTimestamp(Date timestamp) {
        if (timestamp != null) {
            this.timestamp = (Date) timestamp.clone();
        } else {
            this.timestamp = null;
        }
    }

    public Date getTimestamp() {
        if (timestamp != null) {
            return (Date) timestamp.clone();
        } else {
            return null;
        }
    }

    public void setTransferSyntax(String transferSyntax) {
        this.transferSyntax = transferSyntax;
    }

    public String getTransferSyntax() {
        return transferSyntax;
    }

    public void setCommandSet(byte[] fileCommandSet) {
        if (fileCommandSet != null) {
            this.commandSet = fileCommandSet.clone();
            commandField = DICOMDumpUtil.dumpFileCommandSet(commandSet);
        } else {
            this.commandSet = null;
        }
    }

    public byte[] getCommandSet() {
        if (commandSet != null) {
            return commandSet.clone();
        } else {
            return null;
        }
    }

    public String getCommandField() {
        return commandField;
    }

    public void setCommandField(String commandField) {
        this.commandField = commandField;
    }

    public String getAttributesForCommandSet() {
        return DICOMDumpUtil.dumpFileCommandSet(false, getCommandSet());
    }

    public void setValidationResults(List<DicomValidationResult> validationResults) {
        if (validationResults != null) {
            this.validationResults = new ArrayList<DicomValidationResult>(validationResults);
        } else {
            this.validationResults = null;
        }
    }

    public List<DicomValidationResult> getValidationResults() {
        if (validationResults == null) {
            validationResults = new ArrayList<DicomValidationResult>();
        }
        return new ArrayList<DicomValidationResult>(validationResults);
    }

    public Connection getConnection() {
        return connection;
    }

    public String getDumpFilePath() {
        if (filePath != null) {
            return filePath.concat(DUMP_EXTENSION);
        } else {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }
        if (getClass() != o.getClass()) {
            return false;
        }

        DicomMessage that = (DicomMessage) o;

        if (isRequest != that.isRequest) {
            return false;
        }
        if (from != null ? !from.equals(that.from) : (that.from != null)) {
            return false;
        }
        if (to != null ? !to.equals(that.to) : (that.to != null)) {
            return false;
        }
        if (connection != null ? !connection.equals(that.connection) : (that.connection != null)) {
            return false;
        }
        return timestamp != null ? timestamp.equals(that.timestamp) : (that.timestamp == null);

    }

    @Override
    public int hashCode() {
        int result = from != null ? from.hashCode() : 0;
        result = 31 * result + (to != null ? to.hashCode() : 0);
        result = 31 * result + (connection != null ? connection.hashCode() : 0);
        result = 31 * result + (isRequest ? 1 : 0);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        return result;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    public void setSimulatedActor(Actor simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     * <p>
     * <p>The implementor must ensure <tt>sgn(x.compareTo(y)) ==
     * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
     * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
     * <tt>y.compareTo(x)</tt> throws an exception.)
     * <p>
     * <p>The implementor must also ensure that the relation is transitive:
     * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
     * <tt>x.compareTo(z)&gt;0</tt>.
     * <p>
     * <p>Finally, the implementor must ensure that <tt>x.compareTo(y)==0</tt>
     * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
     * all <tt>z</tt>.
     * <p>
     * <p>It is strongly recommended, but <i>not</i> strictly required that
     * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
     * class that implements the <tt>Comparable</tt> interface and violates
     * this condition should clearly indicate this fact.  The recommended
     * language is "Note: this class has a natural ordering that is
     * inconsistent with equals."
     * <p>
     * <p>In the foregoing description, the notation
     * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
     * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
     * <tt>0</tt>, or <tt>1</tt> according to whether the value of
     * <i>expression</i> is negative, zero or positive.
     *
     * @param o the object to be compared.
     *
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     *
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     */
    @Override
    public int compareTo(DicomMessage o) {
        if (getTimestamp().equals(o.getTimestamp())) {
            return getId().compareTo(o.getId());
        }
        return getTimestamp().compareTo(o.getTimestamp());
    }

    @Override
    public String getUniqueKey() {
        Integer objectId = getId();
        return objectId.toString();
    }

    @Override
    public PartSource getPartSource() {
        if (getFilePath() != null) {
            File dicomFile = new File(getFilePath());
            try {
                return new FilePartSource(getType(), dicomFile);
            } catch (FileNotFoundException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public String getType() {
        return OBJECT_TYPE;
    }

    public void validate() {
        String evsClientUrl = ApplicationConfiguration.getValueOfVariable("evs_client_url");
        String toolOid = ApplicationConfiguration.getValueOfVariable("tool_instance_oid");
        if (evsClientUrl == null) {
            LOG.error("evs_client_url is missing in app_configuration table");
        } else if (toolOid == null) {
            LOG.error("tool_instance_oid is missing in app_configuration table");
        } else {
            ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
            EVSClientServletConnector.sendToValidation(this, extContext, evsClientUrl, toolOid);
        }
    }
}
