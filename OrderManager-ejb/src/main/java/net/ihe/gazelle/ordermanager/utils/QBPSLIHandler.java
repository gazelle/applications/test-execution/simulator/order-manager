package net.ihe.gazelle.ordermanager.utils;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.segment.QPD;
import net.ihe.gazelle.ordermanager.model.EncounterQuery;
import net.ihe.gazelle.ordermanager.model.PatientQuery;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.SpecimenQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>QBPSLIHandler<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 19/08/16
 *
 *
 *
 */
public class QBPSLIHandler{

    private static Logger log = LoggerFactory.getLogger(QBPSLIHandler.class);

    private QBPSLIHandler(){

    }

    public static List<Specimen> findSpecimensForParameters(QPD parameters) throws HL7Exception {
       log.info("number of fields in query: " + parameters.numFields());
        SpecimenQuery query = new SpecimenQuery();
        query.addRestriction(HQLRestrictions.or(query.simulatedActor().keyword().eqRestriction("LIP"),
                query.simulatedActor().keyword().eqRestriction("OF")));
        if (!parameters.getPatientID().isEmpty()){
            String identifier = getTrimmedParam(parameters.getPatientID().encode());
            PatientQuery patQuery = new PatientQuery();
            patQuery.patientIdentifiers().identifier().like(identifier, HQLRestrictionLikeMatchMode.START);
            List<Integer> patients = patQuery.id().getListDistinct();
            if (patients == null || patients.isEmpty()){
                return null;
            } else {
                query.orders().encounter().patient().id().in(patients);
            }
        }
        if (!parameters.getPatientVisitNumber().isEmpty()){
            String visitnumber = getTrimmedParam(parameters.getPatientVisitNumber().encode());
            EncounterQuery encQuery = new EncounterQuery();
            encQuery.visitNumber().like(visitnumber, HQLRestrictionLikeMatchMode.START);
            List<Integer> encounters = encQuery.id().getListDistinct();
            if (encounters == null || encounters.isEmpty()){
                return null;
            } else {
                query.orders().encounter().id().in(encounters);
            }
        }
        if (!parameters.getFillerOrderNumber().isEmpty()){
            String fon = getTrimmedParam(parameters.getFillerOrderNumber().encode());
            query.orders().fillerOrderNumber().eq(fon);
        }
        if (!parameters.getPlacerOrderNumber().isEmpty()){
            String pon = getTrimmedParam(parameters.getPlacerOrderNumber().encode());
            query.orders().placerOrderNumber().eq(pon);
        }
        if (!parameters.getPlacerGroupNumber().isEmpty()){
            String pgn = getTrimmedParam(parameters.getPlacerGroupNumber().encode());
            query.orders().placerGroupNumber().eq(pgn);
        }
        if (!parameters.getSearchPeriod().getRangeStartDateTime().isEmpty()){
            Date start = parameters.getSearchPeriod().getRangeStartDateTime().getTime().getValueAsDate();
            query.lastChanged().ge(start);
        }
        if (!parameters.getSearchPeriod().getRangeEndDateTime().isEmpty()){
            Date end = parameters.getSearchPeriod().getRangeEndDateTime().getTime().getValueAsDate();
            query.lastChanged().le(end);
        }
        return query.getList();
    }

    private static String getTrimmedParam(String param){
        return param.trim();
    }
}
