package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public abstract class CommonColumns implements Serializable {

	@ManyToOne(targetEntity=Domain.class)
	@JoinColumn(name = "domain_id")
	protected Domain domain;

	@ManyToOne(targetEntity=Actor.class)
	@JoinColumn(name = "simulated_actor_id")
	protected Actor simulatedActor;

	@Column(name = "last_changed")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastChanged;

	@Column(name = "creator")
	protected String creator;

	public CommonColumns() {
		this.lastChanged = new Date();
	}

	public CommonColumns(Actor simulatedActor, String username, Domain domain) {
		this.simulatedActor = simulatedActor;
		this.domain = domain;
		this.creator = username;
		this.lastChanged = new Date();
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public Actor getSimulatedActor() {
		return simulatedActor;
	}

	public void setSimulatedActor(Actor simulatedActor) {
		this.simulatedActor = simulatedActor;
	}

    public void setLastChanged(Date lastChanged) {
        if (lastChanged != null) {
            this.lastChanged = (Date) lastChanged.clone();
        } else {
            this.lastChanged = null;
        }
    }

    public Date getLastChanged() {
        if (lastChanged != null) {
            return (Date) lastChanged.clone();
        } else {
            return null;
        }
    }

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		if (simulatedActor != null) {
			result = (prime * result)
					+ ((simulatedActor.getKeyword() == null) ? 0 : simulatedActor.getKeyword().hashCode());
		}
		if (domain != null) {
			result = (prime * result) + ((domain.getKeyword() == null) ? 0 : domain.getKeyword().hashCode());
		}
		result = (prime * result) + ((lastChanged == null) ? 0 : lastChanged.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CommonColumns other = (CommonColumns) obj;
		if (domain == null) {
			if (other.domain != null) {
				return false;
			}
		} else if (!domain.getKeyword().equals(other.domain.getKeyword())) {
			return false;
		}
		if (simulatedActor == null) {
			if (other.simulatedActor != null) {
				return false;
			}
		} else if (!simulatedActor.getKeyword().equals(other.simulatedActor.getKeyword())) {
			return false;
		}
		return true;
	}
}
