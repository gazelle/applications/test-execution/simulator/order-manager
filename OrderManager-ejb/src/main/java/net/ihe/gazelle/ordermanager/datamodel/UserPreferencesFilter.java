package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.UserPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>UserPreferencesFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class UserPreferencesFilter extends Filter<UserPreferences> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 477268598889230353L;
	private static List<HQLCriterion<UserPreferences, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<UserPreferences, ?>>();
	}

	public UserPreferencesFilter() {
		super(UserPreferences.class, defaultCriterions);
	}
}
