package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.util.Arrays;
import java.util.Date;

/**
 * Class description : <b>LabelBroker</b>
 * <p/>
 * This class is the handler for messages received by the Label Broker in the context of the LAB-61 transaction
 *
 * @author Anne-Gaëlle Bergé - KEREVAL
 * @version 2016, August 18th
 *
 *
 *
 */
public class LabelBroker extends OrderReceiver {

    private static Log log = Logging.getLog(LabelBroker.class);

    @Override
    public IHEDefaultHandler newInstance() {
        return new LabelBroker();
    }

    @Override
    public Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception {

        // start hibernate transaction
        simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-61", entityManager);
        domain = Domain.getDomainByKeyword("LAB", entityManager);
        Message response;
        try {
            // check if we must accept the message
            response = Receiver.generateNack(incomingMessage, serverApplication, serverFacility, Arrays.asList("OML"),
                    Arrays.asList("O33"), hl7Charset);
            // if no NACK is generated, process the message
            if (response == null) {
                if (messageType == null) {
                    log.error("Message type is empty");
                } else if (messageType.contains("OML")) {
                    response = processLAB61Transaction(incomingMessage);
                } else {
                    log.error("This kind of message is not supported, we should not reach this line !");
                }
            }
            setSutActor(Actor.findActorWithKeyword("LIP", entityManager));
            setResponseMessageType(HL7MessageDecoder.getMessageType(response));
            return response;
        } catch (Exception e) {
            throw new HL7Exception(e);
        }
    }

    private Message processLAB61Transaction(Message incomingMessage) {
        return processOMLMessage(incomingMessage, "NW", null, null, null);
    }

}
