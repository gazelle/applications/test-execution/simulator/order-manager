package net.ihe.gazelle.ordermanager.dicom.utils;

import net.ihe.gazelle.ordermanager.model.WorklistEntry;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;

public class WorklistConverter {

    public static final String UTF_8 = "UTF-8";
    public static final int START = 0;
    private static Log log = Logging.getLog(WorklistConverter.class);

	private WorklistEntry worklistEntry;
	private String errorMessage;

	public WorklistConverter(WorklistEntry worklistEntry) {
		this.worklistEntry = worklistEntry;
		this.errorMessage = null;
	}

	/**
	 * calls xml2dcm from DCMTK toolkit
	 * 
	 * @return true if the conversion is successful
	 */
	public boolean convertToDicom() {
		try {
			ProcessBuilder builder = new ProcessBuilder(new String[] { "xml2dcm", "-ll", "error",
					worklistEntry.getXMLFilePath(), worklistEntry.getWLFilePath() });
			final Process xml2dcm = builder.start();

			new Thread() {
				@Override
				public void run() {
					try {
						BufferedReader reader = new BufferedReader(new InputStreamReader(xml2dcm.getErrorStream(), Charset.forName(UTF_8)));
						String line = "";
						StringBuilder errorsBuffer = new StringBuilder();
                        readLine(reader, errorsBuffer);
                    } catch (IOException ioe) {
						log.error(ioe.getMessage(), ioe);
					}
				}
			}.start();
		} catch (IOException e) {
			errorMessage = "Error while converting XML file to DICOM worklist using xml2dcm command";
			log.error(errorMessage, e);
		}
		return true;
	}

    private void readLine(BufferedReader reader, StringBuilder errorsBuffer) throws IOException {
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("E:")) {
                    errorsBuffer.append(line);
                    errorsBuffer.append("<br/>");
                }
            }
        } finally {
            reader.close();
            if (errorsBuffer.length() > 0) {
                errorMessage = errorsBuffer.toString();
            } else {
                errorMessage = null;
            }
        }
    }

    /**
	 * calls dcmdump from DCMTK toolkit
	 */
	public boolean convertToDump() {
		try {
			ProcessBuilder builder = new ProcessBuilder(new String[] { "dcmdump", worklistEntry.getWLFilePath() });
			builder.redirectErrorStream(true);
			final Process dcmdump = builder.start();
			// read the standard and error outputs in a separate thread
			new Thread() {
				@Override
				public void run() {
					try {
                        InputStreamReader errorIS = new InputStreamReader(dcmdump.getErrorStream(), Charset.forName(UTF_8));
						BufferedReader reader = new BufferedReader(errorIS);
						String line;
						StringBuilder errorsBuffer = new StringBuilder();
						readLine(reader, errorsBuffer);
						// read the standard output to fill the dump file with received data
						File dumpFile = new File(worklistEntry.getDumpFilePath());
                        InputStreamReader isReader = new InputStreamReader(dcmdump.getInputStream(), Charset.forName(UTF_8));
						reader = new BufferedReader(isReader);
						OutputStream out = new FileOutputStream(dumpFile);
						try {
							while ((line = reader.readLine()) != null) {
								line = line.concat("\n");
								out.write(line.getBytes(Charset.forName(UTF_8)), START, line.length());
							}
						} finally {
							out.close();
							reader.close();
                            errorIS.close();
                            isReader.close();
						}

					} catch (IOException ioe) {
						log.error(ioe.getMessage(), ioe);
					}
				}
			}.start();
		} catch (IOException e) {
			errorMessage = "Error while dumping DICOM file";
			log.error(errorMessage, e);
			return false;
		}
		return true;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
