
/**
 * Copyright 2011 IHE International (http://www.ihe.net)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Nicolas LEFEBVRE/ INRIA Rennes - IHE development Project
 * @author Anne-Gaëlle BERGE / IHE Europe
 * @version $Id: $Id
 */
package net.ihe.gazelle.ordermanager.hl7.initiators;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message.QBP_Q11;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.SpecimenQuery;
import net.ihe.gazelle.ordermanager.utils.LBLQueryMessageGenerator;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Name("queryForLabel")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 30000)
public class QueryForLabel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8785889823341694952L;
    private static final String REGEX_CX = "([a-zA-Z0-9_\\.]*\\^){3}([a-zA-Z0-9_\\.]*&?)([0-9_\\.]+&[a-zA-Z0-9_\\.]+)?((\\^)([a-zA-Z0-9_\\.]*)){0,3}";

    private static Logger log = LoggerFactory.getLogger(QueryForLabel.class);

    private static Transaction currentTransaction = null;
    private static Actor simulatedActor = null;
    private static Domain selectedDomain = null;
    private static Actor sutActor = null;

    private List<TransactionInstance> hl7Messages = null;
    private String patientId = null;
    private String visitNumber = null;
    private String placerGroupNumber = null;
    private String placerOrderNumber = null;
    private String fillerOrderNumber = null;
    private Date startDate = null;
    private Date endDate = null;
    private List<Specimen> receivedRequests;

    static {
        try {
            currentTransaction = Transaction.GetTransactionByKeyword("LAB-62");
            simulatedActor = Actor.findActorWithKeyword("LB");
            selectedDomain = Domain.getDomainByKeyword("LAB");
            sutActor = Actor.findActorWithKeyword("LIP");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * <p>init.</p>
     */
    @Create
    public void init() {
        hl7Messages = null;
        receivedRequests = null;
    }

    /**
     * <p>resetParameters.</p>
     */
    public void resetParameters() {
        patientId = null;
        visitNumber = null;
        placerGroupNumber = null;
        placerOrderNumber = null;
        fillerOrderNumber = null;
        startDate = null;
        endDate = null;
    }

    /**
     * <p>validateParameter.</p>
     *
     * @param context a {@link javax.faces.context.FacesContext} object.
     * @param component a {@link javax.faces.component.UIComponent} object.
     * @param value a {@link java.lang.Object} object.
     * @throws javax.faces.validator.ValidatorException if any.
     */
    public void validateParameter(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        final String fieldId = component.getId();
        final String fieldValue = (String) value;
        if ((fieldValue != null) && !fieldValue.isEmpty()) {
            if ((fieldId.equals("qpd3") || fieldId.equals("qpd4"))
                    && !fieldValue.matches(REGEX_CX)) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Field shall be formatted wih respect to the CX datatype: at least CX-1 and CX-4-1 (or CX-4-2 and CX-4-3) shall be populated", null));
            } else if ((fieldId.equals("qpd5") || fieldId.equals("qpd6") || fieldId.equals("qpd7"))
                    && !fieldValue.matches(Container.regex_EI)) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Field shall be formatted wih respect to the EI datatype: " + Container.regex_EI, null));
            }
        }
    }

    /**
     * <p>sendMessage.</p>
     */
    public void sendMessage() {
        Date currentDate = new Date();
        HL7V2ResponderSUTConfiguration sut = (HL7V2ResponderSUTConfiguration) Component.getInstance("selectedSUT");
        if (sut == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please, first select a system under test");
            return;
        }
        QBP_Q11 messageToSend = LBLQueryMessageGenerator.generateQBP_SLI(sut, "OM_LAB_LB", "IHE", patientId,
                visitNumber, placerGroupNumber, placerOrderNumber, fillerOrderNumber, startDate, endDate);
        String messageToSendString;
        PipeParser pipeParser = new PipeParser();
        try {
            if (messageToSend != null) {
                messageToSendString = pipeParser.encode(messageToSend);
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Message has not been built properly, cannot send it");
                return;
            }
        } catch (HL7Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The tool has not been able to send the message: " + e.getMessage());
            return;
        }
        Initiator hl7Initiator = new Initiator(sut, "IHE", "OM_LAB_LB", simulatedActor,
                currentTransaction, messageToSendString, "QBP^SLI^QBP_Q11", "LAB", sutActor);
        TransactionInstance instance;
        try {
            instance = hl7Initiator.sendMessageAndGetTheHL7Message();
            hl7Messages = new ArrayList<TransactionInstance>();
            hl7Messages.add(instance);
        } catch (HL7Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error occurred when sending the message: " + e.getMessage());
            return;
        }
        if (instance != null && instance.getResponse() != null
                && instance.getResponse().getContent() != null) {
            MessageDecoder.extractLabelsFromQueryResponse(instance.getResponse().getContentAsString(),
                    selectedDomain, simulatedActor, sut.getSystemName());
            getReceivedRequests(currentDate, sut.getSystemName());
        }
    }

    private void getReceivedRequests(Date currentDate, String systemName) {
        SpecimenQuery query = new SpecimenQuery();
        query.simulatedActor().eq(simulatedActor);
        query.domain().eq(selectedDomain);
        query.creator().eq(systemName);
        query.lastChanged().ge(currentDate);
        receivedRequests = query.getListNullIfEmpty();
    }

    /**
     * <p>Getter for the field <code>hl7Messages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TransactionInstance> getHl7Messages() {
        return hl7Messages;
    }

    /**
     * <p>Setter for the field <code>hl7Messages</code>.</p>
     *
     * @param hl7Messages a {@link java.util.List} object.
     */
    public void setHl7Messages(List<TransactionInstance> hl7Messages) {
        this.hl7Messages = hl7Messages;
    }

    /**
     * <p>Getter for the field <code>currentTransaction</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Getter for the field <code>selectedDomain</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public Domain getSelectedDomain() {
        return selectedDomain;
    }

    /**
     * <p>Getter for the field <code>sutActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSutActor() {
        return sutActor;
    }

    /**
     * <p>Getter for the field <code>patientId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPatientId() {
        return patientId;
    }

    /**
     * <p>Setter for the field <code>patientId</code>.</p>
     *
     * @param patientId a {@link java.lang.String} object.
     */
    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    /**
     * <p>Getter for the field <code>visitNumber</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getVisitNumber() {
        return visitNumber;
    }

    /**
     * <p>Setter for the field <code>visitNumber</code>.</p>
     *
     * @param visitNumber a {@link java.lang.String} object.
     */
    public void setVisitNumber(String visitNumber) {
        this.visitNumber = visitNumber;
    }

    /**
     * <p>Getter for the field <code>placerGroupNumber</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPlacerGroupNumber() {
        return placerGroupNumber;
    }

    /**
     * <p>Setter for the field <code>placerGroupNumber</code>.</p>
     *
     * @param placerGroupNumber a {@link java.lang.String} object.
     */
    public void setPlacerGroupNumber(String placerGroupNumber) {
        this.placerGroupNumber = placerGroupNumber;
    }

    /**
     * <p>Getter for the field <code>placerOrderNumber</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPlacerOrderNumber() {
        return placerOrderNumber;
    }

    /**
     * <p>Setter for the field <code>placerOrderNumber</code>.</p>
     *
     * @param placerOrderNumber a {@link java.lang.String} object.
     */
    public void setPlacerOrderNumber(String placerOrderNumber) {
        this.placerOrderNumber = placerOrderNumber;
    }

    /**
     * <p>Getter for the field <code>fillerOrderNumber</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFillerOrderNumber() {
        return fillerOrderNumber;
    }

    /**
     * <p>Setter for the field <code>fillerOrderNumber</code>.</p>
     *
     * @param fillerOrderNumber a {@link java.lang.String} object.
     */
    public void setFillerOrderNumber(String fillerOrderNumber) {
        this.fillerOrderNumber = fillerOrderNumber;
    }

    /**
     * <p>Getter for the field <code>startDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getStartDate() {
        if (this.startDate != null) {
            return (Date) startDate.clone();
        } else {
            return null;
        }
    }

    /**
     * <p>Setter for the field <code>startDate</code>.</p>
     *
     * @param startDate a {@link java.util.Date} object.
     */
    public final void setStartDate(Date startDate) {
        if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        }else {
            this.startDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>endDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getEndDate() {
        if (this.endDate != null) {
            return (Date) endDate.clone();
        } else {
            return null;
        }
    }

    /**
     * <p>Setter for the field <code>endDate</code>.</p>
     *
     * @param endDate a {@link java.util.Date} object.
     */
    public final void setEndDate(Date endDate) {
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        } else {
            this.endDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>receivedRequests</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Specimen> getReceivedRequests() {
        return receivedRequests;
    }
}
