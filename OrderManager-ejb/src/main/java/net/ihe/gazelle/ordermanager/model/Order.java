package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.ordermanager.datamodel.RequestedProcedureDataModel;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Class description : <b>Order</b>
 * 
 * This class describes the Order object, it extends the AbstractOrder class
 * 
 * This class defines the following attributes
 * <ul>
 * <li><b>id</b> unique identifier in the database</li>
 * <li><b>dangerCode</b></li>
 * <li><b>specimenSource</b></li>
 * <li><b>transportationMode</b></li>
 * <li><b>transportArranged</b></li>
 * <li><b>relevantClinicalInfo</b></li>
 * <li><b>worklists</b> are the worklists related to this order</li>
 * <li><b>appointments</b> are the appointments related to this order</li>
 * <li><b>accessionNumber</b> unique identifier</li>
 * <ul>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.1 - 2011, December 5th
 * 
 */

@Entity
@Name("order")
@Table(name = "om_order", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_order_sequence", sequenceName = "om_order_id_seq", allocationSize = 1)
public class Order extends AbstractOrder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1350584879611319224L;

	public static final String STATUS_UNKNOWN = "Unknown";

	@Id
	@NotNull
	@GeneratedValue(generator = "om_order_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	/**
	 * OBR-15
	 */
	@Column(name = "specimen_source")
	private String specimenSource;

	/**
	 * OBR-30
	 */
	@Column(name = "transportation_mode")
	private String transportationMode;

	/**
	 * OBR-41
	 */
	@Column(name = "transport_arranged")
	private String transportArranged;

	@Column(name = "accession_number")
	private String accessionNumber;

	/**
	 * appointments related to this order
	 */
	@OneToMany(mappedBy = "order")
	private List<Appointment> appointments;

	/**
	 * procedure requested for this order
	 */
	@OneToMany(mappedBy = "order")
	private List<RequestedProcedure> requestedProcedures;

	/**
	 * Default constructor
	 */
	public Order() {

	}

	/**
	 * Instanciate a new order and compute an order placer/filler number according the case
	 * 
	 * @param inDomain
	 * @param inSimulatedActor
	 * @param entityManager
	 */
	public Order(Domain inDomain, Actor inSimulatedActor, Encounter inEncounter, EntityManager entityManager) {
		this.simulatedActor = inSimulatedActor;
		this.domain = inDomain;
		if (this.simulatedActor.getKeyword().equals("OP")) {
			this.setPlacerOrderNumber(NumberGenerator.getOrderNumberForActor(this.simulatedActor, entityManager));
			this.setPlacerGroupNumber(NumberGenerator.getOrderNumberForActor(simulatedActor, entityManager));
		} else if (this.simulatedActor.getKeyword().equals("OF")) {
			this.setFillerOrderNumber(NumberGenerator.getOrderNumberForActor(this.simulatedActor, entityManager));
		}
		this.lastChanged = new Date();
		this.setEncounter(inEncounter);
		this.setOrderStatus(null);
	}

	/**
	 * Saves the order and returns the merged object
	 * 
	 * @param entityManager
	 * @return the saved order
	 */
	public Order save(EntityManager entityManager) {
		return super.save(Order.class, entityManager);
	}

	@Override
	public void fillOrderRandomly(Domain inDomain, Actor simulatedActor) {
		if (this.getQuantityTiming() == null) {
			this.setQuantityTiming(ValueSet.getRandomCodeFromValueSet("quantityTiming"));
		}
		if (this.getEnteredBy() == null) {
			this.setEnteredBy(ValueSet.getRandomCodeFromValueSet("doctor"));
		}
		if (this.getEnteringOrganization() == null) {
			this.setEnteringOrganization(ValueSet.getRandomCodeFromValueSet("enteringOrganization"));
		}
		if (this.getTransactionDate() == null) {
			this.setTransactionDate(new Date());
		}
		if (this.getOrderingProvider() == null) {
			this.setOrderingProvider(this.getEnteredBy());
		}
		if (this.getTechnician() == null) {
			this.setTechnician(this.getEnteredBy());
		}
		if (this.getUniversalServiceId() == null) {
			if (inDomain.getKeyword().equals("EYE")) {
				this.setUniversalServiceId(ValueSet.getRandomCodeFromValueSet("eyeUniversalServiceId"));
			} else {
				this.setUniversalServiceId(ValueSet.getRandomCodeFromValueSet("radUniversalServiceId"));
			}
		}
		if (this.getTransportationMode() == null) {
			this.setTransportationMode(ValueSet.getRandomCodeFromValueSet("transportationMode"));
		}
		if (this.getTransportArranged() == null) {
			this.setTransportArranged(ValueSet.getRandomCodeFromValueSet("transportArranged"));
		}
		if (this.getStartDateTime() == null) {
			this.setStartDateTime(new Date());
		}
	}

	/**
	 * 
	 * @param placerOrderNumber
	 * @param fillerOrderNumber
	 * @param simulatedActor
	 * @param entityManager
	 * @return the order
	 */
	public static Order getOrderByNumbersByActor(String placerOrderNumber, String fillerOrderNumber,
			Actor simulatedActor, EntityManager entityManager) {
		return getOrderByNumbersByActor(Order.class, placerOrderNumber, fillerOrderNumber, null, simulatedActor,
				entityManager, null);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSpecimenSource() {
		return specimenSource;
	}

	public void setSpecimenSource(String specimenSource) {
		this.specimenSource = specimenSource;
	}

	public String getTransportationMode() {
		return transportationMode;
	}

	public void setTransportationMode(String transportationMode) {
		this.transportationMode = transportationMode;
	}

	public String getTransportArranged() {
		return transportArranged;
	}

	public void setTransportArranged(String transportArranged) {
		this.transportArranged = transportArranged;
	}

	/**
	 * @param appointments
	 *            the appointments to set
	 */
	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	/**
	 * @return the appointments
	 */
	public List<Appointment> getAppointments() {
		return appointments;
	}

	public void setAccessionNumber(String accessionNumber) {
		this.accessionNumber = accessionNumber;
	}

	public String getAccessionNumber() {
		return accessionNumber;
	}

	public void setRequestedProcedures(List<RequestedProcedure> requestedProcedures) {
		this.requestedProcedures = requestedProcedures;
	}

	public List<RequestedProcedure> getRequestedProcedures() {
		return requestedProcedures;
	}

	public RequestedProcedureDataModel getRequestedProceduresAsDataModel() {
		return new RequestedProcedureDataModel(null, null, null, null, null, null, null, null, null, this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = (prime * result) + ((accessionNumber == null) ? 0 : accessionNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Order other = (Order) obj;
		if (accessionNumber == null) {
			if (other.accessionNumber != null) {
				return false;
			}
		} else if (!accessionNumber.equals(other.accessionNumber)) {
			return false;
		}
		return true;
	}

}
