package net.ihe.gazelle.ordermanager.dicom.utils;

import net.ihe.gazelle.ordermanager.dico.LocalDICOMDico;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.ProtocolItem;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStep;
import net.ihe.gazelle.ordermanager.model.UIDGenerator;
import net.ihe.gazelle.ordermanager.model.WorklistEntry;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class XMLWorklistBuilder {

    private static final String ELEMENT = "element";
    private static final String SEQUENCE = "sequence";
    private static final String ITEM = "item";
    private static final String DICOM_STAT = "STAT";
    private static final String DICOM_ROUTINE = "ROUTINE";
    private static final String DICOM_MEDIUM = "MEDIUM";
    private static final String HL7_STAT = "S";
    private static final String HL7_ROUTINE = "R";
    private static final String HL7_MEDIUM = "T";
    private static final String DICOM_HIGH = "HIGH";
    private static final String VR_SEQUENCE = "SQ";
    private static Log log = Logging.getLog(XMLWorklistBuilder.class);

    private WorklistEntry worklistEntry;

    public XMLWorklistBuilder(WorklistEntry worklistEntry) {
        this.worklistEntry = worklistEntry;
    }

    public boolean build() {
        try {
            ScheduledProcedureStep scheduledProcedureStep = worklistEntry.getScheduledProcedureStep();
            RequestedProcedure requestedProcedure = scheduledProcedureStep.getRequestedProcedure();
            Order order = requestedProcedure.getOrder();
            Patient patient = order.getEncounter().getPatient();
            Encounter encounter = order.getEncounter();

            Element root = new Element("file-format");
            // meta-header
            Element metaHeader = new Element("meta-header");
            metaHeader.setAttribute("xfer", "1.2.840.10008.1.2.1");
            metaHeader.setAttribute("name", "Little Endian Explicit");
            metaHeader.addContent(createElement(ELEMENT, LocalDICOMDico.TRANSFER_SYNTAX_UID,
                    "1.2.840.10008.1.2.1"));
            root.addContent(metaHeader);
            // data-set
            Element dataSet = new Element("data-set");
            dataSet.setAttribute("xfer", "1.2.840.10008.1.2.1");
            dataSet.setAttribute("name", "Little Endian Explicit");
            // character set encoding = UTF-8
            dataSet.setContent(createElement(ELEMENT, LocalDICOMDico.SPECIFIC_CHARACTER_SET, "ISO_IR 100"));
            // patient name
            StringBuilder value = new StringBuilder();
            synchronized (value) {
                if (patient.getLastName() != null) {
                    value.append(patient.getLastName());
                }
                value.append('^');
                if (patient.getFirstName() != null) {
                    value.append(patient.getFirstName());
                }
            }
            dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_NAME, value.toString()));
            // patient ID
            List<PatientIdentifier> pids = PatientIdentifier.getIdentifiersForPatient(patient);
            if (pids != null) {
                String[] pidComponent = pids.get(0).getIdentifier().split("\\^");
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_ID, pidComponent[0]));
                if (pidComponent.length > 3) {
                    dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.ISSUER_OF_PATIENT_ID,
                            pidComponent[3].replace("&ISO", "")));
                }
                if (pids.size() > 1) {
                    Element sequence = createElement(SEQUENCE, LocalDICOMDico.OTHER_PID_SEQUENCE, null);
                    for (int index = 1; index < pids.size(); index++) {
                        pidComponent = pids.get(index).getIdentifier().split("\\^");
                        Element item = new Element(ITEM);
                        item.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_ID, pidComponent[0]));
                        item.addContent(createElement(ELEMENT, LocalDICOMDico.ISSUER_OF_PATIENT_ID,
                                pidComponent[3].replace("&ISO", "")));
                        sequence.addContent(item);
                    }
                    dataSet.addContent(sequence);
                }
            }
            // patient birth date & time (return key type = 2)
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat sdfTime = new SimpleDateFormat("HHmm");
            if (patient.getDateOfBirth() != null) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_BIRTH_DATE,
                        sdfDate.format(patient.getDateOfBirth())));
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_BIRTH_TIME,
                        sdfTime.format(patient.getDateOfBirth())));
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_AGE, patient.getPatientAge().toString()));
            } else {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_BIRTH_DATE, null));
            }
            // patient gender (return key type = 2)
            if ((patient.getGenderCode() == null) || patient.getGenderCode().equals("U")) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_SEX, null));
            } else if (patient.getGenderCode().equals("F") || patient.getGenderCode().equals("M")
                    || patient.getGenderCode().equals("O")) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_SEX, patient.getGenderCode()));
            } else {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_SEX, "O"));
            }
            // patient weight (return key type = 2)
            if (patient.getWeight() != null) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_WEIGHT, patient.getWeight()
                        .toString()));
            }
            if (patient.getSize() != null) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_SIZE, patient.getSize().toString()));
            }
            // allergies
            if (patient.getContrastAllergies() != null) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.ALLERGIES, patient.getContrastAllergies()));
            }
            // study instance UID
            dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.STUDY_INSTANCE_UID, requestedProcedure.getStudyInstanceUID()));
            // patient state
            if (order.getDangerCode() != null) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_STATE, order.getDangerCode()));
            }
            // medical alerts
            if (order.getRelevantClinicalInfo() != null) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.MEDICAL_ALERTS,
                        order.getRelevantClinicalInfo()));
            }
            // current patient location (return key type = 2)
            dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.CURRENT_PATIENT_LOCATION,
                    encounter.getAssignedPatientLocation()));

            // visit identification (return key type = 2)
            if (encounter.getVisitNumber() != null) {
                String visitNumber[] = encounter.getVisitNumber().split("\\^");
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.ADMISSION_ID, visitNumber[0]));
                if (visitNumber.length > 3) {
                    dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.ISSUER_OF_ADMISSION_ID,
                            visitNumber[3].replace("&ISO", "")));
                } else {
                    dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.ISSUER_OF_ADMISSION_ID, null));
                }
            } else {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.ADMISSION_ID, null));
            }
            dataSet.addContent(createReferencedSequence(LocalDICOMDico.REFERENCED_PATIENT_SEQUENCE, null));
            // accession number
            dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.ACCESSION_NUMBER,
                    order.getAccessionNumber()));
            // requesting physician (return key type = 2)
            dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.REQUESTING_PHYSICIAN,
                    ValueSet.getDisplayNameForCode("doctor", order.getOrderingProvider())));

            // referring physician (return key type = 2)
            dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.REFERRING_PHYSICIAN_NAME,
                    encounter.getReferringDoctor()));

            // placer Issuer and Number
            if (order.getPlacerOrderNumber() != null) {
                String[] placerNumberElements = order.getPlacerOrderNumber().split("\\^");
                String placerNumber = placerNumberElements[0];
                if (placerNumberElements.length > 1) {
                    placerNumber = placerNumber.concat("^").concat(placerNumberElements[1]);
                }
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PLACER_ORDER_NUMBER_IMAGING_SERVICE_REQUEST, placerNumber));
            } else {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PLACER_ORDER_NUMBER_IMAGING_SERVICE_REQUEST, null));
            }
            // filler Issuer and Number
            if (order.getFillerOrderNumber() != null) {
                String[] fillerNumberElements = order.getFillerOrderNumber().split("\\^");
                String fillerNumber = fillerNumberElements[0];
                if (fillerNumberElements.length > 1) {
                    fillerNumber = fillerNumber.concat("^").concat(fillerNumberElements[1]);
                }
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.FILLER_ORDER_NUMBER_IMAGING_SERVICE_REQUEST, fillerNumber));
            } else {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.FILLER_ORDER_NUMBER_IMAGING_SERVICE_REQUEST, null));
            }
            // entered by
            if (order.getEnteredBy() != null) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.ORDER_ENTERED_BY,
                        ValueSet.getDisplayNameForCode("doctor", order.getEnteredBy())));
            }
            // order entering location
            if (order.getEnteringOrganization() != null) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.ORDER_ENTERER_ORGANIZATION,
                        order.getEnteringOrganization()));
            }
            // order callback phone number
            if (order.getCallBackPhoneNumber() != null) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.ORDER_CALLBACK_PHONE_NUMBER,
                        order.getCallBackPhoneNumber()));
            }

            // sequence: Scheduled Procedure Step
            Element sequence = createElement(SEQUENCE, LocalDICOMDico.SCHEDULED_PROCEDURE_STEP_SEQUENCE, null);
            Element item = new Element(ITEM);
            item.addContent(createElement(ELEMENT, LocalDICOMDico.SCHEDULED_STATION_AE_TITLE,
                    worklistEntry.getStationAETitle()));
            if (scheduledProcedureStep.getSpsStartDateTime() != null) {
                item.addContent(createElement(ELEMENT, LocalDICOMDico.SCHEDULED_PROCEDURE_STEP_START_DATE,
                        sdfDate.format(scheduledProcedureStep.getSpsStartDateTime())));
                item.addContent(createElement(ELEMENT, LocalDICOMDico.SCHEDULED_PROCEDURE_STEP_START_TIME,
                        sdfTime.format(scheduledProcedureStep.getSpsStartDateTime())));
            }
            item.addContent(createElement(ELEMENT, LocalDICOMDico.MODALITY,
                    scheduledProcedureStep.getModality()));

            // scheduled Performing Physician's name
            item.addContent(createElement(ELEMENT, LocalDICOMDico.SCHEDULED_PERFORMING_PHYSICIAN_NAME,
                    ValueSet.getDisplayNameForCode("doctor", order.getTechnician())));

            // scheduled procedure step description
            if (scheduledProcedureStep.getSpsDescription() != null) {
                item.addContent(createElement(ELEMENT, LocalDICOMDico.SCHEDULED_PROCEDURE_STEP_DESCRIPTION,
                        scheduledProcedureStep.getSpsDescription()));
            }
            // scheduled protocol code sequence
            if (scheduledProcedureStep.getProtocolItems() != null) {
                Element protocolCodeSequence = createElement(SEQUENCE, LocalDICOMDico.SCHEDULED_PROTOCOL_CODE_SEQUENCE, null);
                for (ProtocolItem protocol : scheduledProcedureStep.getProtocolItems()) {
                    protocolCodeSequence.addContent(createItem(protocol.getProtocolCode(), protocol.getCodingScheme(),
                            protocol.getProtocolCodeMeaning()));
                }
                item.addContent(protocolCodeSequence);
            }
            item.addContent(createElement(ELEMENT, LocalDICOMDico.SCHEDULED_PROCEDURE_STEP_ID,
                    scheduledProcedureStep.getScheduledProcedureID()));
            // link sequence to data-set
            sequence.addContent(item);
            dataSet.addContent(sequence);
            // end of sequence
            // requested procedure
            dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.REQUESTED_PROCEDURE_ID,
                    requestedProcedure.getRequestedProcedureID()));
            // reason for requested procedure
            if (order.getReasonForStudy() != null) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.REASON_FOR_REQUESTING_PROCEDURE,
                        order.getReasonForStudy()));
            }
            // requested procedure
            dataSet.addContent(createCodeSequenceWithSingleItem(LocalDICOMDico.REQUESTED_PROCEDURE_CODE_SEQUENCE,
                    requestedProcedure.getCode(), requestedProcedure.getCodingScheme(),
                    requestedProcedure.getCodeMeaning()));
            dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.REQUESTED_PROCEDURE_DESCRIPTION,
                    requestedProcedure.getDescription()));
            dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.STUDY_INSTANCE_UID,
                    requestedProcedure.getStudyInstanceUID()));
            dataSet.addContent(createReferencedSequence(LocalDICOMDico.REFERENCED_STUDY_SEQUENCE, null));

            // transport arrangements
            dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.PATIENT_TRANSPORT_ARRANGEMENTS,
                    ValueSet.getDisplayNameForCode("transportArranged", order.getTransportArranged())));
            // priority (return key type = 2)
            if (order.getQuantityTiming() == null) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.REQUESTED_PROCEDURE_PRIORITY, null));
            } else if (order.getQuantityTiming().equals(HL7_STAT)) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.REQUESTED_PROCEDURE_PRIORITY, DICOM_STAT));
            } else if (order.getQuantityTiming().equals(HL7_ROUTINE)) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.REQUESTED_PROCEDURE_PRIORITY, DICOM_ROUTINE));
            } else if (order.getQuantityTiming().equals(HL7_MEDIUM)) {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.REQUESTED_PROCEDURE_PRIORITY, DICOM_MEDIUM));
            } else {
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.REQUESTED_PROCEDURE_PRIORITY, DICOM_HIGH));
            }

            if (requestedProcedure.getAdmittingDiagnosticCode() != null && !requestedProcedure.getAdmittingDiagnosticCode().isEmpty()) {
                Concept diagConcept = ValueSet.getConceptForCode("ADMITTING_DIAG", requestedProcedure.getAdmittingDiagnosticCode());
                dataSet.addContent(createElement(ELEMENT, LocalDICOMDico.ADMITTING_DIAGNOSES_DESCRIPTION, diagConcept.getDisplayName()));
                Element diagnosisSequence = createElement(SEQUENCE, LocalDICOMDico.ADMITTING_DIAGNOSES_CODE_SEQUENCE, null);
                diagnosisSequence.addContent(createItem(diagConcept));
                dataSet.addContent(diagnosisSequence);
            }

            // link data-set to root element of the xml file
            root.addContent(dataSet);
            Document xmlWorklist = new Document(root);
            try {
                XMLOutputter out = new XMLOutputter(Format.getPrettyFormat());
                out.output(xmlWorklist, new FileOutputStream(worklistEntry.getXMLFilePath()));
            } catch (IOException e) {
                log.error("Cannot create XML file with filepath: " + worklistEntry.getXMLFilePath(), e);
            }
        } catch (RuntimeException e) {
            log.error("An error occurred during worklist generation as XML", e);
            return false;
        }
        return true;
    }

    protected Element createElement(String elementName, String tag, String valueRepresentation, String name,
                                    String value) {
        Element element = new Element(elementName);
        element.setAttribute("tag", tag);
        element.setAttribute("vr", valueRepresentation);
        element.setAttribute("name", name);
        if (value != null) {
            element.setText(value);
        }
        return element;
    }

    protected Element createElement(String elementName, LocalDICOMDico dicomTag, String value) {
        return createElement(elementName, dicomTag.getKey(), dicomTag.getValueRepresentation(), dicomTag.getKeyword(), value);
    }

    protected Element createCodeSequenceWithSingleItem(String sequenceTag, String sequenceName, String codeValue,
                                                       String codingScheme, String codeMeaning) {
        Element sequence = createElement(SEQUENCE, sequenceTag, VR_SEQUENCE, sequenceName, null);
        Element item = createItem(codeValue, codingScheme, codeMeaning);
        sequence.addContent(item);
        return sequence;
    }

    protected Element createCodeSequenceWithSingleItem(LocalDICOMDico sequenceTag, String codeValue, String codingScheme, String codeMeaning){
        return createCodeSequenceWithSingleItem(sequenceTag.getKey(), sequenceTag.getKeyword(), codeValue, codingScheme, codeMeaning);
    }

    protected Element createItem(String codeValue, String codingScheme, String codeMeaning) {
        Element item = new Element(ITEM);
        item.addContent(createElement(ELEMENT, LocalDICOMDico.CODE_VALUE, codeValue));
        item.addContent(createElement(ELEMENT, LocalDICOMDico.CODING_SCHEME, codingScheme));
        item.addContent(createElement(ELEMENT, LocalDICOMDico.CODE_MEANING, codeMeaning));
        return item;
    }

    protected Element createItem(Concept concept) {
        return createItem(concept.getCode(), concept.getCodeSystemName(), concept.getDisplayName());
    }

    protected Element createReferencedSequence(String sequenceTag, String sequenceName, String classUID) {
        Element sequence = createElement(SEQUENCE, sequenceTag, VR_SEQUENCE, sequenceName, null);
        if (classUID != null) {
            Element item = new Element(ITEM);
            item.addContent(createElement(ELEMENT, LocalDICOMDico.REFERENCED_SOP_CLASS_UID, classUID));
            item.addContent(createElement(ELEMENT, LocalDICOMDico.REFERENCED_SOP_INSTANCE_UID,
                    UIDGenerator.getNextUIDForTag("(0008,1155)")));
            sequence.addContent(item);
        }
        return sequence;
    }

    protected Element createReferencedSequence(LocalDICOMDico referencedSequenceTag, String classUID){
        return createReferencedSequence(referencedSequenceTag.getKey(), referencedSequenceTag.getKeyword(), classUID);
    }
}
