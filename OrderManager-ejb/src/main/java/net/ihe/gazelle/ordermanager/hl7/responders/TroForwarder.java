package net.ihe.gazelle.ordermanager.hl7.responders;

import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;

public class TroForwarder extends HttpServer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected IHEDefaultHandler getHandler() {
		TroReceiver handler = new TroReceiver();
		return handler.newInstance();
	}

	@Override
	protected String getActorKeyword() {
		return "TRO_FORWARDER";
	}

	@Override
	protected String getTransactionKeyword() {
		return "KSA-TRO";
	}

	@Override
	protected String getDomainKeyword() {
		return "SeHE";
	}

}
