package net.ihe.gazelle.ordermanager.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.ordermanager.model.LabOrderQuery;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.SpecimenQuery;
import net.ihe.gazelle.simulator.common.tf.model.Actor;

import java.util.List;
import java.util.Map;

/**
 * <b>Class Description : </b>ContainerFilter<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 01/07/16
 *
 *
 *
 */
public class SpecimenFilter implements QueryModifier<Specimen> {

    private Actor simulatedActor;
    private boolean workOrder;
    private Boolean single;
    private Boolean mainEntity;
    private String placerIdentifier;
    private String fillerIdentifier;
    private boolean hideDeliveredLabels;


    private Filter<Specimen> filter;

    public SpecimenFilter(Boolean isMainEntity,
                          boolean isPartOfWorkOrder, Actor simulatedActor, Boolean searchForSingleContainers){
        this.simulatedActor = simulatedActor;
        this.workOrder = isPartOfWorkOrder;
        this.single = searchForSingleContainers;
        this.mainEntity = isMainEntity;
        this.hideDeliveredLabels = false;
    }

    public Filter<Specimen> getFilter(){
        if (filter == null){
            filter = new Filter<Specimen>(getHQLCriterionsForFilter());
        }
        return filter;
    }

    public FilterDataModel<Specimen> getSpecimens(){
        return new FilterDataModel<Specimen>(getFilter()) {
            @Override
            protected Object getId(Specimen specimen) {
                return specimen.getId();
            }
        };
    }

    public void reset(){
        getFilter().clear();
        placerIdentifier = null;
        fillerIdentifier = null;
    }

    private HQLCriterionsForFilter<Specimen> getHQLCriterionsForFilter() {
        SpecimenQuery query = new SpecimenQuery();
        HQLCriterionsForFilter<Specimen> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("creator", query.creator());
        criteria.addPath("creationDate", query.lastChanged());
        criteria.addPath("actor", query.simulatedActor(), simulatedActor);
        criteria.addPath("workOrder", query.workOrder(), workOrder);
        criteria.addPath("mainEntity", query.mainEntity(), mainEntity);
        criteria.addQueryModifier(this);
        return criteria;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<Specimen> hqlQueryBuilder, Map<String, Object> map) {
        SpecimenQuery query = new SpecimenQuery();
        if (placerIdentifier != null && ! placerIdentifier.isEmpty()){
            hqlQueryBuilder.addRestriction(query.placerAssignedIdentifier().likeRestriction(placerIdentifier));
        }
        if (fillerIdentifier != null && !fillerIdentifier.isEmpty()){
            hqlQueryBuilder.addRestriction(query.fillerAssignedIdentifier().likeRestriction(fillerIdentifier));
        }
        if (hideDeliveredLabels){
            LabOrderQuery oquery = new LabOrderQuery();
            oquery.simulatedActor().keyword().eq(simulatedActor.getKeyword());
            oquery.orderControlCode().eq("SC");
            List<Integer> ids = oquery.id().getListDistinct();
            if (ids != null) {
                hqlQueryBuilder.addRestriction(query.orders().id().ninRestriction(ids));
            }
        }
        if (single != null){
            hqlQueryBuilder.addRestriction(query.containers().mainEntity().eqRestriction(single));
        }
    }

    public String getFillerIdentifier() {
        return fillerIdentifier;
    }

    public void setFillerIdentifier(String fillerIdentifier) {
        this.fillerIdentifier = fillerIdentifier;
    }

    public String getPlacerIdentifier() {
        return placerIdentifier;
    }

    public void setPlacerIdentifier(String placerIdentifier) {
        this.placerIdentifier = placerIdentifier;
    }

    public boolean isHideDeliveredLabels() {
        return hideDeliveredLabels;
    }

    public void setHideDeliveredLabels(boolean hideDeliveredLabels) {
        this.hideDeliveredLabels = hideDeliveredLabels;
    }
}
