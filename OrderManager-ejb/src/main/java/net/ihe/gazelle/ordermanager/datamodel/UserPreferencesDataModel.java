package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.ordermanager.model.UserPreferences;

import java.util.Date;

/**
 * Class description : <b>UserPreferencesDataModel</b>
 *
 * This class is a data model for properly and quickly displaying the user preferences objects in the GUI
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 * @version 1.0 - 2011, November 17th
 */
public class UserPreferencesDataModel extends FilterDataModel<UserPreferences> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -175267672525040589L;

	/**
	 * <p>Constructor for UserPreferencesDataModel.</p>
	 */
	public UserPreferencesDataModel() {
		super(new UserPreferencesFilter());
	}

	/**
	 * <p>Constructor for UserPreferencesDataModel.</p>
	 *
	 * @param creator a {@link java.lang.String} object.
	 * @param startDate a {@link java.util.Date} object.
	 * @param endDate a {@link java.util.Date} object.
	 * @param patientId a {@link java.lang.String} object.
	 */
	public UserPreferencesDataModel(String creator, Date startDate, Date endDate, String patientId) {
		super(new UserPreferencesFilter());
	}
/** {@inheritDoc} */
@Override
        protected Object getId(UserPreferences t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
