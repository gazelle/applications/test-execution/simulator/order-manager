package net.ihe.gazelle.ordermanager.utils;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Primitive;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.Type;
import ca.uhn.hl7v2.model.v251.datatype.CX;
import ca.uhn.hl7v2.model.v251.segment.PID;
import ca.uhn.hl7v2.model.v25.message.ORL_O22;
import ca.uhn.hl7v2.model.v25.message.ORL_O34;
import ca.uhn.hl7v2.model.v25.message.ORL_O36;
import ca.uhn.hl7v2.model.v25.segment.ORC;
import ca.uhn.hl7v2.model.v251.message.OMG_O19;
import ca.uhn.hl7v2.model.v251.segment.IPC;
import ca.uhn.hl7v2.model.v251.segment.TQ1;
import ca.uhn.hl7v2.parser.EncodingNotSupportedException;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message.ORL_O42;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.group.RSP_K11_ORDER;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.group.RSP_K11_SPECIMEN;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message.RSP_K11;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.segment.MSA;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.segment.QAK;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.segment.SAC;
import net.ihe.gazelle.ordermanager.dico.HL7FieldPosition;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Observation;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.ProtocolItem;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStep;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>MessageDecoder<br>
 * <br>
 * <p/>
 * This class gathers methods to extract the relevant information from the received messages
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2011, November 17th
 *
 *
 *
 */

public class MessageDecoder {

    private static Logger log = LoggerFactory.getLogger(MessageDecoder.class);

    private MessageDecoder() {

    }

    /**
     * takes the MSH-9 field and build the message type of the given messages
     *
     * @param message
     * @return the message type
     * */
    public static String buildMessageType(Message message) {
        Terser t = new Terser(message);
        StringBuilder type = new StringBuilder();
        try {
            type.append(t.get("/.MSH-9-1"));
            type.append('^');
        } catch (HL7Exception e) {
            log.error("MSH-9-1 not found");
        }
        try {
            type.append(t.get("/.MSH-9-2"));
            type.append('^');
        } catch (HL7Exception e) {
            log.error("MSH-9-2 not found");
        }
        try {
            type.append(t.get("/.MSH-9-3"));
        } catch (HL7Exception e) {
            log.error("MSH-9-3 not found");
        }
        return type.toString();
    }

    /**
     * create a Patient object filled with the information contained in the given PID segment
     *
     * @param pid
     * @return the patient
     */
    public static Patient extractPatientFromPID(Segment pid) {
        Patient patient = new Patient();
        patient.setCreationDate(new Date());
        try {
            // patient's first name PID-5-2
            patient.setFirstName(Terser.get(pid, HL7FieldPosition.PATIENT_NAME, HL7FieldPosition.FIRST_REP,
                    HL7FieldPosition.FIRST_NAME, HL7FieldPosition.VALUE));
            // patient's last name PID-5-1-1
            patient.setLastName(Terser.get(pid, HL7FieldPosition.PATIENT_NAME, HL7FieldPosition.FIRST_REP,
                    HL7FieldPosition.LAST_NAME, HL7FieldPosition.VALUE));
            try {
                String alternateFirstName = Terser.get(pid, HL7FieldPosition.PATIENT_NAME, HL7FieldPosition.ALT_NAME_REP,
                        HL7FieldPosition.FIRST_NAME, HL7FieldPosition.VALUE);
                if (alternateFirstName != null && !alternateFirstName.isEmpty()) {
                    patient.setAlternateFirstName(alternateFirstName);
                }
            } catch (HL7Exception e) {
                log.debug("Alternate first name not provided");
            }
            try {
                String alternateLastName = Terser.get(pid, HL7FieldPosition.PATIENT_NAME, HL7FieldPosition.ALT_NAME_REP,
                        HL7FieldPosition.LAST_NAME, HL7FieldPosition.VALUE);
                if (alternateLastName != null && !alternateLastName.isEmpty()) {
                    patient.setAlternateLastName(alternateLastName);
                }
            } catch (HL7Exception e) {
                log.debug("Alternate last name not provided");
            }
            // mother's maiden name
            patient.setMotherMaidenName(Terser.get(pid, HL7FieldPosition.MOTHER_MAIDEN_NAME,
                    HL7FieldPosition.FIRST_REP, HL7FieldPosition.LAST_NAME, HL7FieldPosition.VALUE));
            // date/time of birth
            String dobString = Terser.get(pid, HL7FieldPosition.DATE_OF_BIRTH, HL7FieldPosition.FIRST_REP,
                    HL7FieldPosition.TS_DATE_TIME, HL7FieldPosition.VALUE);
            if (dobString != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                try {
                    patient.setDateOfBirth(sdf.parse(dobString));
                } catch (ParseException e) {
                    patient.setDateOfBirth((String) null);
                }
            }
            // patient's gender PID-8
            patient.setGenderCode(Terser.get(pid, HL7FieldPosition.GENDER_CODE, HL7FieldPosition.FIRST_REP, HL7FieldPosition.VALUE, HL7FieldPosition.VALUE));
            // patient's race PID-10
            patient.setRaceCode(Terser.get(pid, 10, 0, 1, 1));
            // patient's religion PID-17
            patient.setReligionCode(Terser.get(pid, 17, 0, 1, 1));
            // patient's address PID-11
            PatientAddress address = new PatientAddress(patient);
            address.setStreet(Terser.get(pid, 11, 0, 1, 1));
            address.setZipCode(Terser.get(pid, 11, 0, 5, 1));
            address.setState(Terser.get(pid, 11, 0, 4, 1));
            address.setCountryCode(Terser.get(pid, 11, 0, 6, 1));
            address.setCity(Terser.get(pid, 11, 0, 3, 1));
            patient.addPatientAddress(address);
            return patient;
        } catch (HL7Exception e) {
            return null;
        }
    }

    /**
     * @param inMessage
     * @return the order
     */
    public static Order extractNewOrderFromORMWithTerser(Message inMessage) {
        Order order = new Order();
        order.setLastChanged(new Date());
        Terser terser = new Terser(inMessage);
        try {
            // ORC
            Segment orc = terser.getSegment("/.ORC");
            String placerOrderNumber = orc.getField(2, 0).encode();
            if ((placerOrderNumber != null) && !placerOrderNumber.isEmpty()) {
                order.setPlacerOrderNumber(placerOrderNumber);
            }
            String fillerOrderNumber = orc.getField(3, 0).encode();
            if ((fillerOrderNumber != null) && !fillerOrderNumber.isEmpty()) {
                order.setFillerOrderNumber(fillerOrderNumber);
            }
            String placerGroupNumber = orc.getField(4, 0).encode();
            if ((placerGroupNumber != null) && !placerGroupNumber.isEmpty()) {
                order.setPlacerGroupNumber(placerGroupNumber);
            }
            order.setOrderControlCode(Terser.get(orc, 1, 0, 1, 1));
            order.setCallBackPhoneNumber(Terser.get(orc, 14, 0, 1, 1));
            order.setEnteredBy(Terser.get(orc, 10, 0, 1, 1));
            order.setEnteringOrganization(Terser.get(orc, 17, 0, 1, 1));
            order.setQuantityTiming(Terser.get(orc, 7, 0, 6, 1));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            try {
                order.setTransactionDate(sdf.parse(Terser.get(orc, 9, 0, 1, 1)));
            } catch (NullPointerException e) {
                // parse(String) method raises a NPE if the given string is null
                order.setTransactionDate(null);
            }
            order.setOrderingProvider(Terser.get(orc, 12, 0, 1, 1));
            try {
                order.setStartDateTime(sdf.parse(Terser.get(orc, 7, 0, 4, 1)));
            } catch (NullPointerException e) {
                // parse(String) method raises a NPE if the given string is null
                order.setStartDateTime(null);
            }

            // OBR
            Segment obr = terser.getSegment("/.OBR");
            order.setUniversalServiceId(Terser.get(obr, 4, 0, 1, 1));
            order.setDangerCode(Terser.get(obr, 12, 0, 1, 1));
            order.setSpecimenSource(Terser.get(obr, 15, 0, 1, 1));
            order.setTransportationMode(Terser.get(obr, 30, 0, 1, 1));
            order.setReasonForStudy(Terser.get(obr, 31, 0, 1, 2));
            order.setTransportArranged(Terser.get(obr, 41, 0, 1, 1));
            order.setRelevantClinicalInfo(Terser.get(obr, 13, 0, 1, 1));
            order.setTechnician(Terser.get(obr, 34, 0, 1, 1));
            order.setAccessionNumber(Terser.get(obr, 18, 0, 1, 1));
            order.setOrderResultStatus(Terser.get(obr, 25, 0, 1, 1));

            // OMG case
            if (inMessage instanceof OMG_O19) {
                TQ1 tq1 = ((OMG_O19) inMessage).getORDER().getTIMING().getTQ1();
                order.setQuantityTiming(tq1.getPriority(0).getIdentifier().getValue());
                order.setStartDateTime(tq1.getStartDateTime().getTime().getValueAsDate());
            }
            return order;
        } catch (HL7Exception e) {
            return order;
        } catch (ParseException e) {
            log.error("Unable to parse date " + e.getMessage());
            return order;
        }
    }

    /**
     * @param orc
     * @param obr
     * @return the order
     */
    public static Order extractOrderFromORCAndOBR(Segment orc, Segment obr) {
        Order order = new Order();
        order.setLastChanged(new Date());
        try {
            // ORC
            String placerOrderNumber = orc.getField(2, 0).encode();
            if ((placerOrderNumber != null) && !placerOrderNumber.isEmpty()) {
                order.setPlacerOrderNumber(placerOrderNumber);
            }
            String fillerOrderNumber = orc.getField(3, 0).encode();
            if ((fillerOrderNumber != null) && !fillerOrderNumber.isEmpty()) {
                order.setFillerOrderNumber(fillerOrderNumber);
            }
            String placerGroupNumber = orc.getField(4, 0).encode();
            if ((placerGroupNumber != null) && !placerGroupNumber.isEmpty()) {
                order.setPlacerGroupNumber(placerGroupNumber);
            }
            order.setOrderControlCode(Terser.get(orc, 1, 0, 1, 1));
            order.setCallBackPhoneNumber(Terser.get(orc, 14, 0, 1, 1));
            order.setEnteredBy(Terser.get(orc, 10, 0, 1, 1));
            order.setEnteringOrganization(Terser.get(orc, 17, 0, 1, 1));
            order.setQuantityTiming(Terser.get(orc, 7, 0, 6, 1));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            try {
                order.setTransactionDate(sdf.parse(Terser.get(orc, 9, 0, 1, 1)));
            } catch (NullPointerException e) {
                // parse(String) method raises a NPE if the given string is null
                order.setTransactionDate(null);
            }
            order.setOrderingProvider(Terser.get(orc, 12, 0, 1, 1));
            try {
                order.setStartDateTime(sdf.parse(Terser.get(orc, 7, 0, 4, 1)));
            } catch (NullPointerException e) {
                // parse(String) method raises a NPE if the given string is null
                order.setStartDateTime(null);
            }

            // OBR
            order.setUniversalServiceId(Terser.get(obr, 4, 0, 1, 1));
            order.setDangerCode(Terser.get(obr, 12, 0, 1, 1));
            order.setSpecimenSource(Terser.get(obr, 15, 0, 1, 1));
            order.setTransportationMode(Terser.get(obr, 30, 0, 1, 1));
            order.setReasonForStudy(Terser.get(obr, 31, 0, 1, 2));
            order.setTransportArranged(Terser.get(obr, 41, 0, 1, 1));
            order.setRelevantClinicalInfo(Terser.get(obr, 13, 0, 1, 1));
            order.setTechnician(Terser.get(obr, 34, 0, 1, 1));
            order.setAccessionNumber(Terser.get(obr, 18, 0, 1, 1));
            order.setOrderResultStatus(Terser.get(obr, 25, 0, 1, 1));
            return order;
        } catch (HL7Exception e) {
            return order;
        } catch (ParseException e) {
            log.error("Unable to parse date " + e.getMessage());
            return order;
        }
    }

    /**
     * @param orc
     * @param obr
     * @return the requested procedure
     */
    public static RequestedProcedure extractProcedureFromOrderGroup(Segment orc, Segment obr) {
        RequestedProcedure procedure = new RequestedProcedure();
        try {
            procedure.setOrderControlCode(Terser.get(orc, 1, 0, 1, 1));
            procedure.setOrderStatus(Terser.get(orc, 5, 0, 1, 1));
            procedure.setCode(Terser.get(obr, 44, 0, 1, 1));
            procedure.setCodeMeaning(Terser.get(obr, 44, 0, 2, 1));
            procedure.setCodingScheme(Terser.get(obr, 44, 0, 3, 1));
            procedure.setDescription(Terser.get(obr, 44, 0, 5, 1));
            procedure.setRequestedProcedureID(Terser.get(obr, 19, 0, 1, 1));
        } catch (HL7Exception e) {
            log.error("An error occurred when creating RequestedProcedure", e);
        }
        return procedure;
    }

    /**
     * @param obr
     * @param orc
     * @return the scheduled procedure step
     */
    public static ScheduledProcedureStep extractSPSFromOBRAndORC(Segment obr, Segment orc) {
        ScheduledProcedureStep sps = new ScheduledProcedureStep();
        try {
            sps.setModality(Terser.get(obr, 24, 0, 1, 1));
            sps.setScheduledProcedureID(Terser.get(obr, 20, 0, 1, 1));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            try {
                sps.setSpsStartDateTime(sdf.parse(Terser.get(orc, 7, 0, 4, 1)));
            } catch (NullPointerException e) {
                log.error("SPS start date/time is not specified");
            } catch (ParseException e) {
                log.error("Cannot parse given SPS start date/time");
            }
        } catch (HL7Exception e) {
            log.error("An error occurred when creating ScheduledProcedureStep", e);
        }
        return sps;
    }

    /**
     * @param obr
     * @return the protocol item
     */
    public static ProtocolItem extractProtocolFromOBR(Segment obr) {
        ProtocolItem protocol = new ProtocolItem();
        try {
            protocol.setProtocolCode(Terser.get(obr, 4, 0, 4, 1));
            protocol.setProtocolCodeMeaning(Terser.get(obr, 4, 0, 5, 1));
            protocol.setCodingScheme(Terser.get(obr, 4, 0, 6, 1));
        } catch (HL7Exception e) {
            log.error("An error occurred when creating ProtocolItem", e);
        }
        return protocol;
    }



    /**
     * Encode the PID segment and check its length is strictly greated than 3. That means that the segment is not empty.
     *
     * @param message
     * @param segmentName
     * @return true if the segment is present in the message
     */
    public static boolean isSegmentPresent(Message message, String segmentName) {
        try {
            Terser terser = new Terser(message);
            Segment segment = terser.getSegment("/." + segmentName);
            if (segment != null) {
                int segmentLength = segment.encode().length();
                return (segmentLength > segmentName.length());
            } else {
                return false;
            }
        } catch (HL7Exception e) {
            log.error("cannot retrieve/encode segment with name " + segmentName, e);
            return false;
        }
    }

    /**
     * Takes the PV1 segment and extract visit number, referring doctor, patient's class and location
     *
     * @param message
     * @return the encounter
     */
    public static Encounter extractEncounterFromPV1(Message message) {
        Encounter encounter = new Encounter();
        encounter.setCreationDate(new Date());
        try {
            Terser terser = new Terser(message);
            Segment pv1 = terser.getSegment("/.PV1");
            encounter.setVisitNumber(pv1.getField(19, 0).encode());
            encounter.setPatientClassCode(Terser.get(pv1, 2, 0, 1, 1));
            encounter.setReferringDoctorCode(Terser.get(pv1, 8, 0, 1, 1));
            encounter.setAssignedPatientLocation(pv1.getField(3, 0).encode());
            return encounter;
        } catch (HL7Exception e) {
            log.error("Cannot parse the PV1 segment properly", e);
            return encounter;
        }
    }

    /**
     * @param orc
     * @param obr
     * @param tq1
     * @param actor
     * @param domain
     * @param creator
     * @param selectedEncounter
     * @return the lab order
     */
    public static LabOrder extractLabOrderFromSegments(Segment orc, Segment obr, Segment tq1, Actor actor,
                                                       Domain domain, String creator, Encounter selectedEncounter) {
        LabOrder order = new LabOrder();
        order.setLastChanged(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            if (orc != null) {
                if (!(actor.getKeyword().equals("ANALYZER_MGR") || (actor.getKeyword().equals("ANALYZER")))) {
                    String placerOrderNumber = obr.getField(2, 0).encode();
                    if ((placerOrderNumber != null) && !placerOrderNumber.isEmpty()) {
                        order.setPlacerOrderNumber(placerOrderNumber);
                    }
                    String fillerOrderNumber = orc.getField(3, 0).encode();
                    if ((fillerOrderNumber != null) && !fillerOrderNumber.isEmpty()) {
                        order.setFillerOrderNumber(fillerOrderNumber);
                    }
                }
                String placerGroupNumber = orc.getField(4, 0).encode();
                if ((placerGroupNumber != null) && !placerGroupNumber.isEmpty()) {
                    order.setPlacerGroupNumber(placerGroupNumber);
                }
                // parent
                Type[] parents = orc.getField(8);
                if (parents.length > 0) {
                    List<String> listOfParents = new ArrayList<String>();
                    for (Type parent : parents) {
                        listOfParents.add(parent.encode());
                    }
                    order.setParent(listOfParents);
                }
                order.setOrderControlCode(Terser.get(orc, 1, 0, 1, 1));
                order.setCallBackPhoneNumber(Terser.get(orc, 14, 0, 1, 1));
                order.setEnteredBy(Terser.get(orc, 10, 0, 1, 1));
                order.setEnteringOrganization(Terser.get(orc, 17, 0, 1, 1));
                try {
                    order.setTransactionDate(sdf.parse(Terser.get(orc, 9, 0, 1, 1)));
                } catch (NullPointerException e) {
                    // parse(String) method raises a NPE if the given string is null
                    order.setTransactionDate(null);
                }
                order.setOrderingProvider(Terser.get(orc, 12, 0, 1, 1));
                order.setOrderStatus(Terser.get(orc, 5, 0, 1, 1));
                try {
                    order.setExpectedAvailabilityDateTime(sdf.parse(Terser.get(orc, 27, 0, 1, 1)));
                } catch (NullPointerException e) {
                    // parse(String) method raises a NPE if the given string is null
                    order.setExpectedAvailabilityDateTime(null);
                }
            }
            if (tq1 != null) {
                // TQ1 quantity/Timing
                try {
                    order.setStartDateTime(sdf.parse(Terser.get(tq1, 7, 0, 1, 1)));
                } catch (NullPointerException e) {
                    // parse(String) method raises a NPE if the given string is null
                    order.setStartDateTime(null);
                }
                order.setQuantityTiming(Terser.get(tq1, 9, 0, 1, 1));
            }
            if (obr != null) {
                // OBR
                if (actor.getKeyword().equals("ANALYZER_MGR") || actor.getKeyword().equals("ANALYZER")) {
                    String wosID = obr.getField(2, 0).encode();
                    if ((wosID != null) && !wosID.isEmpty()) {
                        order.setPlacerOrderNumber(wosID);
                    }
                    String fillerOrderNumber = obr.getField(3, 0).encode();
                    if ((fillerOrderNumber != null) && !fillerOrderNumber.isEmpty()) {
                        order.setFillerOrderNumber(fillerOrderNumber);
                    }
                }
                order.setUniversalServiceId(Terser.get(obr, 4, 0, 1, 1));
                order.setReasonForStudy(Terser.get(obr, 31, 0, 1, 2));
                order.setTechnician(Terser.get(obr, 34, 0, 1, 1));
                order.setDiagnosticServiceSectionID(Terser.get(obr, 24, 0, 1, 1));
                order.setResultCopiesTo(Terser.get(obr, 28, 0, 1, 1));
                order.setOrderResultStatus(Terser.get(obr, 25, 0, 1, 1));
            }
            order.setSimulatedActor(actor);
            order.setDomain(domain);
            order.setCreator(creator);
            order.setEncounter(selectedEncounter);
            return order;
        } catch (HL7Exception e) {
            return order;
        } catch (ParseException e) {
            log.error("Unable to parse date " + e.getMessage(), e);
            return order;
        }
    }

    public static Specimen extractSpecimenFromSegment(Segment spm, String creator, Actor actor, Domain domain) {
        Specimen specimen = new Specimen(false);
        try {
            String specimenID = spm.getField(2, 0).encode();
            if (specimenID != null) {
                if (!specimenID.contains("^")) {
                    specimen.setPlacerAssignedIdentifier(specimenID);
                } else {
                    String[] identifiers = specimenID.split("\\^");
                    if (identifiers.length == 1) {
                        specimen.setFillerAssignedIdentifier(identifiers[0]);
                    } else {
                        specimen.setPlacerAssignedIdentifier(identifiers[0]);
                        specimen.setFillerAssignedIdentifier(identifiers[1]);
                    }
                }
            }
            specimen.setType(Terser.get(spm, 4, 0, 1, 1));
            specimen.setCollectionMethod(Terser.get(spm, 7, 0, 1, 1));
            specimen.setSourceSite(Terser.get(spm, 8, 0, 1, 1));
            specimen.setSourceSiteModifier(Terser.get(spm, 9, 0, 1, 1));
            specimen.setRole(Terser.get(spm, 11, 0, 1, 1));
            specimen.setRiskCode(Terser.get(spm, 16, 0, 1, 1));
            String available = Terser.get(spm, 20, 0, 1, 1);
            if ((available != null) && available.equals("Y")) {
                specimen.setAvailable(true);
            } else {
                specimen.setAvailable(false);
            }
            specimen.setRejectReason(Terser.get(spm, 21, 0, 1, 1));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            try {
                specimen.setCollectionDateTime(sdf.parse(Terser.get(spm, 17, 0, 1, 1)));
            } catch (NullPointerException e) {
                specimen.setCollectionDateTime(null);
            }
            try {
                specimen.setReceivedDateTime(sdf.parse(Terser.get(spm, 18, 0, 1, 1)));
            } catch (NullPointerException e) {
                specimen.setReceivedDateTime(null);
            }
            specimen.setLastChanged(new Date());
            specimen.setCreator(creator);
            specimen.setSimulatedActor(actor);
            specimen.setDomain(domain);
        } catch (HL7Exception e) {
            log.error("Unable to parse SPM segment", e);
        } catch (ParseException e) {
            log.error("Unable to parse date " + e.getMessage(), e);
        }

        return specimen;
    }

    /**
     * !!! by default the workOrder attribute is set to false (this case occurs more often !)
     *
     * @param sac
     * @param actor
     * @param specimen
     * @param creator
     * @param domain
     * @return the container
     */
    public static Container extractContainerFromSAC(Segment sac, Actor actor, Specimen specimen, String creator,
                                                    Domain domain) {
        Container container = new Container(false);
        try {
            container.setContainerIdentifier(sac.getField(3, 0).encode());
            container.setPrimaryContainerIdentifier(sac.getField(4, 0).encode());
            container.setCarrierIdentifier(sac.getField(10, 0).encode());
            container.setPositionInCarrier(sac.getField(11, 0).encode());
            container.setTrayIdentifier(sac.getField(13, 0).encode());
            container.setPositionInTray(sac.getField(14, 0).encode());
            container.setLocation(sac.getField(15, 0).encode());
            container.setCapType(sac.getField(26, 0).encode());
        } catch (HL7Exception e) {
            log.error("An error occurred when parsing SAC segment", e);
        }
        container.setLastChanged(new Date());
        container.setSimulatedActor(actor);
        container.setSpecimen(specimen);
        container.setCreator(creator);
        container.setDomain(domain);
        return container;
    }

    /**
     * @param inMessage
     * @return the list of patient identifiers
     */
    public static List<PatientIdentifier> extractPatientIdentifiersFromMessage25(Message inMessage) {
        Terser terser = new Terser(inMessage);
        ca.uhn.hl7v2.model.v25.segment.PID pidSegment;
        try {
            pidSegment = (ca.uhn.hl7v2.model.v25.segment.PID) terser.getSegment("/.PID");
        } catch (HL7Exception e) {
            log.error("Cannot extract PID segment from message");
            return null;
        }
        ca.uhn.hl7v2.model.v25.datatype.CX[] identifiers = pidSegment.getPatientIdentifierList();
        if ((identifiers != null) && (identifiers.length > 0)) {
            List<PatientIdentifier> patientIdentifiers = new ArrayList<PatientIdentifier>();
            for (int index = 0; index < identifiers.length; index++) {
                PatientIdentifier pid = extractPatientIdenfierFromCX(identifiers[index]);
                patientIdentifiers.add(pid);
            }
            return patientIdentifiers;
        } else {
            return null;
        }
    }

    private static List<PatientIdentifier> extractPatientIdentifiersFromPID(Segment segment) {
        try {
            Type[] identifierList = segment.getField(3);
            if (identifierList.length > 0) {
                List<PatientIdentifier> identifiers = new ArrayList<PatientIdentifier>();
                for (int index = 0; index < identifierList.length; index++) {
                    Type cx = identifierList[index];
                    PatientIdentifier pid = extractPatientIdenfierFromCX(cx);
                    if (pid != null) {
                        identifiers.add(pid);
                    }
                }
                return identifiers;
            } else {
                return null;
            }
        } catch (HL7Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    private static PatientIdentifier extractPatientIdenfierFromCX(Type field) {
        try {
            Primitive cx1 = Terser.getPrimitive(field, HL7FieldPosition.FIRST_COMP, HL7FieldPosition.VALUE);
            StringBuilder stringId = new StringBuilder(cx1.encode());
            stringId.append("^^^");
            try {
                Primitive hd1 = Terser.getPrimitive(field, HL7FieldPosition.CX_ASSIGNING_AUTH, HL7FieldPosition.HD_NAME);
                stringId.append(hd1.encode());
                stringId.append('&');
                Primitive hd2 = Terser.getPrimitive(field, HL7FieldPosition.CX_ASSIGNING_AUTH, HL7FieldPosition.HD_ID);
                stringId.append(hd2.encode());
                stringId.append('&');
                Primitive hd3 = Terser.getPrimitive(field, HL7FieldPosition.CX_ASSIGNING_AUTH, HL7FieldPosition.HD_ID_TYPE);
                stringId.append(hd3.encode());
            } catch (HL7Exception e) {
                log.error("cannot encode assigning authority");
            }
            PatientIdentifier pid = new PatientIdentifier();
            pid.setIdentifier(stringId.toString());
            Primitive typeCode = Terser.getPrimitive(field, HL7FieldPosition.CX_ID_TYPE, HL7FieldPosition.VALUE);
            pid.setIdentifierTypeCode(typeCode.encode());
            return pid;
        } catch (HL7Exception e) {
            log.error(e.getMessage());
            return null;
        }

    }

    /**
     * @param inMessage
     * @return the list of patient identifiers
     */
    public static List<PatientIdentifier> extractPatientIdentifiersFromPID251(Message inMessage) {
        Terser terser = new Terser(inMessage);
        ca.uhn.hl7v2.model.v251.segment.PID pidSegment;
        try {
            pidSegment = (ca.uhn.hl7v2.model.v251.segment.PID) terser.getSegment("/.PID");
        } catch (HL7Exception e) {
            log.error("Cannot extract PID segment from message");
            return null;
        }
        ca.uhn.hl7v2.model.v251.datatype.CX[] identifiers = pidSegment.getPatientIdentifierList();
        if ((identifiers != null) && (identifiers.length > 0)) {
            List<PatientIdentifier> patientIdentifiers = new ArrayList<PatientIdentifier>();
            for (int index = 0; index < identifiers.length; index++) {
                StringBuilder stringId = new StringBuilder(identifiers[index].getCx1_IDNumber().getValue());
                stringId.append("^^^");
                try {
                    stringId.append(identifiers[index].getAssigningAuthority().encode().replace("^", "&"));
                } catch (HL7Exception e) {
                    log.error("cannot encode assigning authority");
                }
                PatientIdentifier pid = new PatientIdentifier();
                pid.setIdentifier(stringId.toString());
                pid.setIdentifierTypeCode(identifiers[index].getIdentifierTypeCode().getValue());
                patientIdentifiers.add(pid);
            }
            return patientIdentifiers;
        } else {
            return null;
        }
    }

    /**
     * Encodes the segment and checks its length. If length is greater than 3 (name of the segment is three characters long), return false else return true
     *
     * @param segment
     * @return true if the segment is empty
     */
    public static boolean isEmpty(Segment segment) {
        try {
            return (!(segment.encode().length() > 3));
        } catch (HL7Exception e) {
            return true;
        }
    }

    /**
     * parses the acknowldgement message received and update the contained orders
     *
     * @param ackString
     * @param simulatedActor
     */
    public static void decodeAcknowledgement(String ackString, Actor simulatedActor) {
        if (ackString != null) {
            PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();
            Message ackMessage;
            try {
                if (simulatedActor.getKeyword().equals("ANALYZER_MGR")) {
                    ackMessage = pipeParser.parseForSpecificPackage(ackString, "net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message");
                } else {
                    ackMessage = pipeParser.parse(ackString);
                }
            } catch (EncodingNotSupportedException e) {
                log.error("Unable to encode the received message", e);
                return;
            } catch (HL7Exception e) {
                log.error("Unable to encode the received message", e);
                return;
            } catch (Exception e) {
                log.error("Unable to find the message ack class in the specified package!", e);
                return;
            }

            EntityManager entityManager = EntityManagerService.provideEntityManager();
            if (ackMessage instanceof ORL_O22) {
                ORL_O22 orlMessage = (ORL_O22) ackMessage;
                int nbOfOrders = orlMessage.getRESPONSE().getPATIENT().getORDERReps();
                if (nbOfOrders > 0) {
                    for (int index = 0; index < nbOfOrders; index++) {
                        try {
                            ORC orc = orlMessage.getRESPONSE().getPATIENT().getORDER(index).getORC();
                            updateOrder(orc, simulatedActor, entityManager);
                        } catch (HL7Exception e) {
                            log.error(
                                    "Cannot retrieve lab order information in repetition " + index + " of ORDER group");
                        }
                    }
                }
            } else if (ackMessage instanceof ORL_O34) {
                ORL_O34 orlMessage = (ORL_O34) ackMessage;
                int nbOfSpecimens = orlMessage.getRESPONSE().getPATIENT().getSPECIMENReps();
                if (nbOfSpecimens > 0) {
                    for (int index = 0; index < nbOfSpecimens; index++) {
                        int nbOfOrders = orlMessage.getRESPONSE().getPATIENT().getSPECIMEN(index).getORDERReps();
                        if (nbOfOrders > 0) {
                            for (int orderIndex = 0; orderIndex < nbOfOrders; orderIndex++) {
                                try {
                                    ORC orc = orlMessage.getRESPONSE().getPATIENT().getSPECIMEN(index)
                                            .getORDER(orderIndex).getORC();
                                    updateOrder(orc, simulatedActor, entityManager);
                                } catch (HL7Exception e) {
                                    log.error("Cannot retrieve lab order in repetition SPECIMEN[" + index + "]/ORDER["
                                            + orderIndex + "]");
                                }
                            }
                        }
                    }
                }
            } else if (ackMessage instanceof ca.uhn.hl7v2.model.v251.message.ORL_O34) {
                ca.uhn.hl7v2.model.v251.message.ORL_O34 orlMessage = (ca.uhn.hl7v2.model.v251.message.ORL_O34) ackMessage;
                int nbOfSpecimens = orlMessage.getRESPONSE().getPATIENT().getSPECIMENReps();
                if (nbOfSpecimens > 0) {
                    for (int index = 0; index < nbOfSpecimens; index++) {
                        int nbOfOrders = orlMessage.getRESPONSE().getPATIENT().getSPECIMEN(index).getORDERReps();
                        if (nbOfOrders > 0) {
                            for (int orderIndex = 0; orderIndex < nbOfOrders; orderIndex++) {
                                try {
                                    ca.uhn.hl7v2.model.v251.segment.ORC orc = orlMessage.getRESPONSE().getPATIENT()
                                            .getSPECIMEN(index).getORDER(orderIndex).getORC();
                                    ca.uhn.hl7v2.model.v251.segment.OBR obr = orlMessage.getRESPONSE().getPATIENT()
                                            .getSPECIMEN(index).getORDER(orderIndex).getOBSERVATION_REQUEST().getOBR();
                                    updateOrder(orc, obr, simulatedActor, entityManager);
                                } catch (HL7Exception e) {
                                    log.error("Cannot retrieve lab order in repetition SPECIMEN[" + index + "]/ORDER["
                                            + orderIndex + "]");
                                }
                            }
                        }
                    }
                }
            } else if (ackMessage instanceof ORL_O42) {
                ORL_O42 orlMessage = (ORL_O42) ackMessage;
                int nbOfSpecimens = orlMessage.getRESPONSE().getSPECIMENReps();
                if (nbOfSpecimens > 0) {
                    for (int index = 0; index < nbOfSpecimens; index++) {
                        int nbOfOrders = orlMessage.getRESPONSE().getSPECIMEN(index).getORDERReps();
                        if (nbOfOrders > 0) {
                            for (int orderIndex = 0; orderIndex < nbOfOrders; orderIndex++) {
                                try {
                                    net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.ORC orc = orlMessage.getRESPONSE()
                                            .getSPECIMEN(index).getORDER(orderIndex).getORC();
                                    net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.OBR obr = orlMessage.getRESPONSE()
                                            .getSPECIMEN(index).getORDER(orderIndex).getOBSERVATION_REQUEST().getOBR();
                                    updateOrder(orc, obr, simulatedActor, entityManager);
                                } catch (HL7Exception e) {
                                    log.error("Cannot retrieve lab order in repetition SPECIMEN[" + index + "]/ORDER["
                                            + orderIndex + "]");
                                }
                            }
                        }
                    }
                }
            } else if (ackMessage instanceof ORL_O36) {
                ORL_O36 orlMessage = (ORL_O36) ackMessage;
                int nbOfSpecimens = orlMessage.getRESPONSE().getPATIENT().getSPECIMENReps();
                if (nbOfSpecimens > 0) {
                    for (int index = 0; index < nbOfSpecimens; index++) {
                        int nbOfContainers = orlMessage.getRESPONSE().getPATIENT().getSPECIMEN(index)
                                .getSPECIMEN_CONTAINERReps();
                        if (nbOfContainers > 0) {
                            for (int containerIndex = 0; containerIndex < nbOfContainers; containerIndex++) {
                                int nbOfOrders = orlMessage.getRESPONSE().getPATIENT().getSPECIMEN(index)
                                        .getSPECIMEN_CONTAINER(containerIndex).getORDERReps();
                                if (nbOfOrders > 0) {
                                    for (int orderIndex = 0; orderIndex < nbOfOrders; orderIndex++) {
                                        try {
                                            ORC orc = orlMessage.getRESPONSE().getPATIENT().getSPECIMEN(index)
                                                    .getSPECIMEN_CONTAINER(containerIndex).getORDER(orderIndex)
                                                    .getORC();
                                            updateOrder(orc, simulatedActor, entityManager);
                                        } catch (HL7Exception e) {
                                            log.error("Cannot retrieve lab order in repetition SPECIMEN[" + index
                                                    + "]/CONTAINER[" + containerIndex + "]/ORDER[" + orderIndex + "]");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                log.error("decodeAcknowledgement::not an expected message type !");
            }
        }

    }

    /**
     * @param orc            (HL7v2.5)
     * @param simulatedActor
     * @param entityManager
     * @throws HL7Exception
     */
    private static void updateOrder(ORC orc, Actor simulatedActor, EntityManager entityManager) throws HL7Exception {
        String placerNumber = orc.getPlacerOrderNumber().encode();
        String fillerNumber = orc.getFillerOrderNumber().encode();
        String controlCode = orc.getOrderControl().getValue();
        LabOrder labOrder = null;
        if (simulatedActor.getKeyword().equals("OP")) {
            labOrder = AbstractOrder
                    .getOrderByNumbersByActor(LabOrder.class, placerNumber, null, null, simulatedActor, entityManager,
                            null);
            if (labOrder != null) {
                labOrder.setFillerOrderNumber(fillerNumber);
                labOrder.setOrderControlCode(controlCode);
                labOrder.save(entityManager);
            }
        } else {
            labOrder = AbstractOrder
                    .getOrderByNumbersByActor(LabOrder.class, null, fillerNumber, null, simulatedActor, entityManager,
                            null);
            if (labOrder != null) {
                labOrder.setPlacerOrderNumber(placerNumber);
                labOrder.setOrderControlCode(controlCode);
                labOrder.save(entityManager);
            }
        }
    }

    /**
     * @param orc            (HL7v2.5.1)
     * @param simulatedActor
     * @param entityManager
     * @throws HL7Exception
     */
    private static void updateOrder(Segment orc, Segment obr, Actor simulatedActor,
                                    EntityManager entityManager) throws HL7Exception {
        String placerNumber = orc.getField(2, 0).encode();
        String fillerNumber = orc.getField(3, 0).encode();
        String controlCode = orc.getField(1, 0).encode();
        LabOrder labOrder = null;
        if (simulatedActor.getKeyword().equals("ANALYZER_MGR") || simulatedActor.getKeyword().equals("ANALYZER")) {
            placerNumber = obr.getField(2, 0).encode();
            fillerNumber = obr.getField(3, 0).encode();
        }
        labOrder = AbstractOrder
                .getOrderByNumbersByActor(LabOrder.class, placerNumber, fillerNumber, null, simulatedActor,
                        entityManager, null);
        if (labOrder != null) {
            labOrder.setPlacerOrderNumber(placerNumber);
            labOrder.setFillerOrderNumber(fillerNumber);
            labOrder.setOrderControlCode(controlCode);
            labOrder.save(entityManager);
        }

    }

    public static Observation extractObservationFromOBX(Segment obx, Actor simulatedActor, Domain domain,
                                                        String creator) {
        Observation observation = new Observation(domain, simulatedActor, creator);
        try {
            // OBX-1
            String setId = Terser.get(obx, 1, 0, 1, 1);
            try {
                observation.setSetId(Integer.decode(setId));
            } catch (NumberFormatException e) {
                observation.setSetId(null);
            }
            observation.setValueType(Terser.get(obx, 2, 0, 1, 1));
            observation.setIdentifier(Terser.get(obx, 3, 0, 1, 1));
            observation.setSubID(Terser.get(obx, 4, 0, 1, 1));
            observation.setValue(Terser.get(obx, 5, 0, 1, 1));
            observation.setUnits(Terser.get(obx, 6, 0, 1, 1));
            observation.setReferencesRange(Terser.get(obx, 7, 0, 1, 1));
            // TODO observation.setAbnormalFlags(abnormalFlags)
            observation.setResultStatus(Terser.get(obx, 11, 0, 1, 1));
            observation.setAccessChecks(Terser.get(obx, 13, 0, 1, 1));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            try {
                observation.setObservationDateTime(sdf.parse(Terser.get(obx, 14, 0, 1, 1)));
            } catch (NullPointerException e) {
                observation.setObservationDateTime(null);
            } catch (ParseException e) {
                observation.setObservationDateTime(null);
            }
            observation.setProducerId(obx.getField(15, 0).encode());
            observation.setResponsibleObserver(obx.getField(16, 0).encode());
            observation.setObservationMethod(obx.getField(17, 0).encode());
            observation.setEquipment(obx.getField(18, 0).encode());
            try {
                observation.setAnalysisDateTime(sdf.parse(Terser.get(obx, 19, 0, 1, 1)));
            } catch (NullPointerException e) {
                observation.setAnalysisDateTime(null);
            } catch (ParseException e) {
                observation.setAnalysisDateTime(null);
            }
        } catch (HL7Exception e) {
            log.error("An error occurred when extracted observation from OBX segment");
        }
        return observation;
    }

    /**
     * @param obr
     * @return the order result status
     */
    public static String getOrderResultStatus(Segment obr) {
        try {
            String newOrderResultStatus = Terser.get(obr, 25, 0, 1, 1);
            if ((newOrderResultStatus != null) && !newOrderResultStatus.isEmpty()) {
                return newOrderResultStatus;
            } else {
                return null;
            }
        } catch (HL7Exception e) {
            log.error("Cannot get OBR-25");
            return null;
        }
    }

    public static String getOrderStatus(Segment orc) {
        try {
            String newOrderResultStatus = Terser.get(orc, 5, 0, 1, 1);
            if ((newOrderResultStatus != null) && !newOrderResultStatus.isEmpty()) {
                return newOrderResultStatus;
            } else {
                return null;
            }
        } catch (HL7Exception e) {
            log.error("Cannot get ORC-5");
            return null;
        }
    }

    public static PatientIdentifier buildPatientIdentifier(CX cx) {
        StringBuilder stringId = new StringBuilder(cx.getCx1_IDNumber().getValue());
        stringId.append("^^^");
        try {
            stringId.append(cx.getAssigningAuthority().encode().replace("^", "&"));
        } catch (HL7Exception e) {
            log.error("cannot encode assigning authority");
        }
        PatientIdentifier pid = new PatientIdentifier();
        pid.setIdentifier(stringId.toString());
        pid.setIdentifierTypeCode(cx.getIdentifierTypeCode().getValue());
        return pid;
    }

    public static RequestedProcedure extractProcedureFromOrderGroup(Segment orc, Segment obr, IPC ipc) {
        RequestedProcedure procedure = extractProcedureFromOrderGroup(orc, obr);
        try {
            procedure.setStudyInstanceUID(ipc.getStudyInstanceUID().encode());
            procedure.setRequestedProcedureID(ipc.getRequestedProcedureID().encode());
        } catch (HL7Exception e) {
            log.error(e.getMessage(), e);
        }
        return procedure;
    }

    public static ScheduledProcedureStep extractSPSFromOBRAndORC(Segment tq1, IPC ipc) {
        ScheduledProcedureStep sps = new ScheduledProcedureStep();
        try {
            sps.setScheduledProcedureID(ipc.getScheduledProcedureStepID().encode());
            sps.setModality(ipc.getModality().getIdentifier().getValue());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            sps.setSpsStartDateTime(sdf.parse(Terser.get(tq1, 7, 0, 1, 1)));
        } catch (HL7Exception e) {
            log.error(e.getMessage(), e);
        } catch (ParseException e) {
            log.error("TQ1-7 is not formatted as expected", e);
        }
        return sps;
    }

    public static ProtocolItem extractProtocolFromOBR(IPC ipc) {
        ProtocolItem protocol = new ProtocolItem();
        protocol.setProtocolCode(ipc.getProtocolCode(0).getIdentifier().getValue());
        protocol.setProtocolCodeMeaning(ipc.getProtocolCode(0).getText().getValue());
        protocol.setCodingScheme(ipc.getProtocolCode(0).getNameOfCodingSystem().getValue());
        return protocol;
    }

    public static void extractLabelsFromQueryResponse(String contentAsString, Domain selectedDomain, Actor simulatedActor, String creator) {
        Parser parser = PipeParser.getInstanceWithNoValidation();
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        try {
            RSP_K11 response = (RSP_K11) parser.parseForSpecificPackage(contentAsString, "net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message");
            // first check if the LIP returned requests
            MSA msa = response.getMSA();
            QAK qak = response.getQAK();
            if (msa.getMsa1_AcknowledgmentCode().getValue().equals("AA")) {
                if (qak.getQueryResponseStatus().getValue().equals("NF")) {
                    log.info("QAK-2 = NF, no request to import at Label Broker");
                } else if (qak.getQueryResponseStatus().getValue().equals("OK")) {
                    // start parsing
                    Patient selectedPatient = getPatientFromRSPK11(entityManager, response);
                    Encounter selectedEncounter = getEncounterFromRSPK11(entityManager, response, selectedPatient);
                    if (selectedEncounter != null) {
                        extractOrdersForRSPK11(response, selectedEncounter, simulatedActor, selectedDomain, creator);
                    } else {
                        // we do not need the patient to be persisted
                        entityManager.getTransaction().rollback();
                    }
                }
            }
        } catch (HL7Exception e) {
            log.error(e.getMessage());
        }
    }

    private static void extractOrdersForRSPK11(RSP_K11 response, Encounter selectedEncounter, Actor simulatedActor, Domain selectedDomain, String creator) {
        try {
            List<RSP_K11_SPECIMEN> specimenGroup = response.getPATIENT().getSPECIMENAll();
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            for (RSP_K11_SPECIMEN spmgroup : specimenGroup) {
                Specimen specimen = extractSpecimenFromSegment(spmgroup.getSPM(), creator, simulatedActor, selectedDomain);
                specimen.setMainEntity(true);
                List<SAC> sacList = spmgroup.getSACAll();
                specimen.setContainers(new ArrayList<Container>());
                for (SAC sac : sacList) {
                    Container container = extractContainerFromSAC(sac, simulatedActor, specimen, creator, selectedDomain);
                    container.setMainEntity(false);
                    specimen.getContainers().add(container);
                }
                List<RSP_K11_ORDER> orderGroups = spmgroup.getORDERAll();
                specimen.setOrders(new ArrayList<LabOrder>());
                for (RSP_K11_ORDER group : orderGroups) {
                    LabOrder labOrder = extractLabOrderFromSegments(group.getORC(), group.getOBSERVATION_REQUEST().getOBR(),
                            group.getTIMING(0).getTQ1(), simulatedActor, selectedDomain, creator, selectedEncounter);
                    labOrder.setMainEntity(false);
                    specimen.getOrders().add(labOrder);
                }
                specimen.save(entityManager);
            }
        } catch (HL7Exception e) {
            log.error(e.getMessage());
        }
    }

    private static Encounter getEncounterFromRSPK11(EntityManager entityManager, RSP_K11 response, Patient selectedPatient) {
        Encounter encounter = extractEncounterFromPV1(response);
        Encounter selectedEncounter;
        if (encounter != null && (encounter.getVisitNumber() != null && !encounter.getVisitNumber().isEmpty())) {
            selectedEncounter = Encounter.getEncounterByVisitNumber(encounter.getVisitNumber(), entityManager);
            if (selectedEncounter == null) {
                encounter.setPatient(selectedPatient);
                return encounter.save(entityManager);
            } else if (!selectedEncounter.getPatient().getId().equals(selectedPatient.getId())) {
                log.warn("Inconsistency: visit number is already linked to another patient");
                return null;
            } else {
                return selectedEncounter;
            }
        } else {
            log.warn("Cannot extract data from response, no encounter provided");
            return null;
        }
    }

    private static Patient getPatientFromRSPK11(EntityManager entityManager, RSP_K11 response) {
        net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.segment.PID pidSeg = response.getPATIENT().getPID();
        Patient patient = extractPatientFromPID(pidSeg);
        Patient selectedPatient = null;
        List<PatientIdentifier> identifiers = extractPatientIdentifiersFromPID(pidSeg);
        if (identifiers != null && !identifiers.isEmpty()) {
            for (PatientIdentifier pid : identifiers) {
                PatientIdentifier pidFromDB = pid.getStoredPatientIdentifier(entityManager);
                if (pidFromDB == null) {
                    continue;
                } else {
                    selectedPatient = pidFromDB.getPatient();
                    break;
                }
            }
            if (selectedPatient == null) {
                selectedPatient = storePartient(patient, identifiers);
            }
        }
        return selectedPatient;
    }

    private static Patient storePartient(Patient patient, List<PatientIdentifier> identifiers) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        patient = patient.save(entityManager);
        patient.setPatientIdentifiers(new ArrayList<PatientIdentifier>());
        for (PatientIdentifier identifier : identifiers) {
            identifier.setPatient(patient);
            identifier = identifier.save(entityManager);
            patient.getPatientIdentifiers().add(identifier);
        }
        return patient;
    }
}
