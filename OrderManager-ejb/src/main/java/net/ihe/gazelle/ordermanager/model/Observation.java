package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Name("observation")
@Table(name = "om_observation", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_observation_sequence", sequenceName = "om_observation_id_seq", allocationSize = 1)
public class Observation extends CommonColumns implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Logger
    private static Log log = Logging.getLog(Observation.class);

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(generator = "om_observation_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    private Integer id;

    @Column(name = "set_id")
    private Integer setId;

    /**
     * OBX-2 taken from the list of 'valueType' in SVS
     */
    @Column(name = "value_type")
    private String valueType;

    /**
     * OBX-4
     */
    @Column(name = "sub_id")
    // in LAW: subID is set at runId.representationId where
    // both runId and representationId are a positive Integer starting from 1
    private String subID;

    @Transient
    private Integer runId;

    @Transient
    private Integer representationId;

    /**
     * OBX-5
     */
    @Lob
    @Type(type = "text")
    @Column(name = "value")
    private String value;

    /**
     * OBX-3
     */
    @Column(name = "identifier")
    private String identifier;

    /**
     * OBX-6
     */
    @Column(name = "units")
    private String units;

    /**
     * OBX-7
     */
    @Column(name = "references_range")
    private String referencesRange;

    /**
     * OBX-8 only codes are stored there, display names are available in SVS 'abnormalFlag'
     */
    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "om_observation_flag", joinColumns = @JoinColumn(name = "abnormal_flag_id"))
    @Column(name = "element")
    private List<String> abnormalFlags;

    /**
     * OBX-11 only the code is stored there, display name is available in SVS 'resultStatus'
     */
    @Column(name = "result_status")
    private String resultStatus;

    /**
     * OBX-13
     */
    @Column(name = "access_checks")
    private String accessChecks;

    /**
     * OBX-14
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "observation_date_time")
    private Date observationDateTime;

    /**
     * OBX-16 taken from the list of 'doctor' in SVS
     */
    @Column(name = "responsible_observer")
    private String responsibleObserver;

    /**
     * OBX-17
     */
    @Column(name = "observation_method")
    private String observationMethod;

    /**
     * OBX-18
     */
    @Column(name = "equipment")
    private String equipment;

    /**
     * OBX-15
     */
    @Column(name = "producer_id")
    private String producerId;

    /**
     * OBX-19
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "analysis_date_time")
    private Date analysisDateTime;

    /**
     * OBX-15
     */
    @Column(name = "observation_type")
    private String observationType;


    @ManyToOne
    @JoinColumn(name = "order_id")
    private LabOrder order;

    @ManyToOne
    @JoinColumn(name = "specimen_id")
    private Specimen specimen;

    @OneToMany(mappedBy = "observation", targetEntity = net.ihe.gazelle.ordermanager.model.NoteAndComment.class, cascade = CascadeType.MERGE)
    private List<NoteAndComment> comments;

    @Transient
    private String selectedAbnormalFlag;

    @Transient
    private Boolean includeInMessage;

    /**
     * Constructors
     */
    public Observation() {

    }

    public Observation(Domain domain, Actor simulatedActor, String creator) {
        super(simulatedActor, creator, domain);
    }

    public static List<Observation> getObservationFiltered(EntityManager entityManager, Actor simulatedActor,
                                                           Domain domain, LabOrder order, Specimen specimen, Integer setId) {
        ObservationQuery query = new ObservationQuery(
                new HQLQueryBuilder<Observation>(entityManager, Observation.class));
        if (simulatedActor != null) {
            query.simulatedActor().eq(simulatedActor);
        }
        if (domain != null) {
            query.domain().eq(domain);
        }
        if (order != null) {
            query.order().eq(order);
        }
        if (specimen != null) {
            query.specimen().eq(specimen);
        }
        if (setId != null) {
            query.setId().eq(setId);
        }
        return query.getListNullIfEmpty();
    }

    public Observation save(EntityManager entityManager) {
        Observation observation = entityManager.merge(this);
        entityManager.flush();
        return observation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getReferencesRange() {
        return referencesRange;
    }

    public void setReferencesRange(String referencesRange) {
        this.referencesRange = referencesRange;
    }

    public List<String> getAbnormalFlags() {
        return abnormalFlags;
    }

    public void setAbnormalFlags(List<String> abnormalFlags) {
        this.abnormalFlags = abnormalFlags;
    }

    public String getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(String resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getAccessChecks() {
        return accessChecks;
    }

    public void setAccessChecks(String accessChecks) {
        this.accessChecks = accessChecks;
    }

    public Date getObservationDateTime() {
        if (observationDateTime != null) {
            return (Date) observationDateTime.clone();
        }else{
            return null;
        }
    }

    public void setObservationDateTime(Date observationDateTime) {
        if (observationDateTime != null) {
            this.observationDateTime = (Date) observationDateTime.clone();
        } else {
            this.observationDateTime = null;
        }
    }

    public String getResponsibleObserver() {
        return responsibleObserver;
    }

    public void setResponsibleObserver(String responsileObserver) {
        this.responsibleObserver = responsileObserver;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getProducerId() {
        return producerId;
    }

    public void setProducerId(String producerId) {
        this.producerId = producerId;
    }

    public Date getAnalysisDateTime() {
        if (analysisDateTime != null) {
            return (Date) analysisDateTime.clone();
        } else {
            return null;
        }
    }

    public void setAnalysisDateTime(Date analysisDateTime) {
        if (analysisDateTime != null) {
            this.analysisDateTime = (Date) analysisDateTime.clone();
        } else {
            this.analysisDateTime = null;
        }
    }

    public LabOrder getOrder() {
        return order;
    }

    public void setOrder(LabOrder order) {
        this.order = order;
    }

    public Specimen getSpecimen() {
        return specimen;
    }

    public void setSpecimen(Specimen specimen) {
        this.specimen = specimen;
    }

    public void setSubID(String subID) {
        this.subID = subID;
        if ((this.subID != null) && !this.subID.isEmpty() && (this.subID.indexOf('.') >= 0)) {
            String[] parts = this.subID.split("\\.");
            this.runId = Integer.decode(parts[0]);
            this.representationId = Integer.decode(parts[1]);
        } else if ((this.subID != null) && StringUtils.isNumeric(this.subID)) {
            this.runId = Integer.decode(subID);
        } else {
            runId = null;
        }
    }

    public String getSubID() {
        if (runId != null) {
            this.subID = runId.toString();
            if (representationId != null) {
                String representationIdString = representationId.toString();
                this.subID = this.subID.concat("." + representationIdString);
            }
        }
        return subID;
    }

    public void setObservationMethod(String observationMethod) {
        this.observationMethod = observationMethod;
    }

    public String getObservationMethod() {
        return observationMethod;
    }

    public void setComments(List<NoteAndComment> comments) {
        this.comments = comments;
    }

    public List<NoteAndComment> getComments() {
        return comments;
    }

    public void setSelectedAbnormalFlag(String selectedAbnormalFlag) {
        if (this.abnormalFlags == null) {
            this.abnormalFlags = new ArrayList<String>();
        }
        if (!this.abnormalFlags.contains(selectedAbnormalFlag)) {
            this.abnormalFlags.add(selectedAbnormalFlag);
        }
        this.selectedAbnormalFlag = null;
    }

    public String getSelectedAbnormalFlag() {
        return selectedAbnormalFlag;
    }

    public Integer getRunId() {
        if (this.runId == null) {
            if ((this.subID != null) && this.subID.matches("[0-9]+(\\.[0.9]+)?")) {
                int end = this.subID.indexOf('.');
                if (end > 0) {
                    this.runId = Integer.decode(this.subID.substring(0, end));
                } else {
                    this.runId = Integer.decode(this.subID);
                }
            }
        }
        return runId;
    }

    public void setRunId(Integer runId) {
        this.runId = runId;
    }

    public Integer getRepresentationId() {
        return representationId;
    }

    public void setRepresentationId(Integer representationId) {
        this.representationId = representationId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = (prime * result) + ((identifier == null) ? 0 : identifier.hashCode());
        result = (prime * result) + ((order == null) ? 0 : order.hashCode());
        result = (prime * result) + ((subID == null) ? 0 : subID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Observation other = (Observation) obj;
        if (identifier == null) {
            if (other.identifier != null) {
                return false;
            }
        } else if (!identifier.equals(other.identifier)) {
            return false;
        }
        if (order == null) {
            if (other.order != null) {
                return false;
            }
        } else if (!order.equals(other.order)) {
            return false;
        }
        if (subID == null) {
            if (other.subID != null) {
                return false;
            }
        } else if (!subID.equals(other.subID)) {
            return false;
        }
        return true;
    }

    public Boolean getIncludeInMessage() {
        if (this.includeInMessage == null) {
            this.includeInMessage = true;
        }
        return includeInMessage;
    }

    // indicates if the observation must be enclosed within the result message sent to the SUT
    public void setIncludeInMessage(Boolean includeInMessage) {
        this.includeInMessage = includeInMessage;
    }

    public void setSetId(Integer setId) {
        this.setId = setId;
    }

    public Integer getSetId() {
        return this.setId;
    }


    public String getObservationType() {
        return observationType;
    }

    public void setObservationType(String observationType) {
        this.observationType = observationType;
    }
}
