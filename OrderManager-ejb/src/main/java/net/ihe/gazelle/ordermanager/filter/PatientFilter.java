package net.ihe.gazelle.ordermanager.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientQuery;

import java.util.Map;

/**
 * <b>Class Description : </b>PatientFilter<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 *
 *
 *
 */
public class PatientFilter implements QueryModifier<Patient> {

    private Filter<Patient> filter;
    private String patientid;

    private HQLCriterionsForFilter<Patient> getHQLCriterionsForPatients() {
        PatientQuery query = new PatientQuery();
        HQLCriterionsForFilter<Patient> criterions = query.getHQLCriterionsForFilter();
        criterions.addPath("firstname", query.firstName());
        criterions.addPath("lastname", query.lastName());
        criterions.addPath("creator", query.creator());
        criterions.addPath("creationDate", query.creationDate());
        criterions.addQueryModifier(this);
        return criterions;
    }

    public Filter<Patient> getFilter() {
        if (filter == null){
            filter = new Filter<Patient>(getHQLCriterionsForPatients());
        }
        return filter;
    }

    public FilterDataModel<Patient> getPatientsFiltered(){
        return new FilterDataModel<Patient>(getFilter()) {
            @Override
            protected Object getId(Patient patient) {
                return patient.getId();
            }
        };
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<Patient> hqlQueryBuilder, Map<String, Object> map) {
        if (patientid != null && !patientid.isEmpty()){
            PatientQuery query = new PatientQuery();
            hqlQueryBuilder.addRestriction(query.patientIdentifiers().identifier().likeRestriction(patientid));
        }
    }

    public String getPatientid() {
        return patientid;
    }

    public void setPatientid(String patientid) {
        this.patientid = patientid;
    }

    public void reset(){
        patientid = null;
        getFilter().clear();
    }
}
