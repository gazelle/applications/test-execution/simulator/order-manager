package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Name("noteAndComment")
@Table(name = "om_note_and_comment", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_note_and_comment_sequence", sequenceName = "om_note_and_comment_id_seq", allocationSize = 1)
public class NoteAndComment extends CommonColumns implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6356730976566043946L;

	@Id
	@GeneratedValue(generator = "om_note_and_comment_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	/**
	 * NTE-1 (sequenceId)
	 */
	@Column(name = "set_id")
	private Integer setID;

	/**
	 * NTE-2 (source of comment) coded in SVS 'sourceOfComment'
	 */
	@Column(name = "source")
	private String source;

	/**
	 * NTE-3 (comment)
	 */
	@Column(name = "comment")
	@Lob
    @Type(type = "text")
	private String comment;

	/**
	 * NTE-4 (comment type) coded in SVS 'commentType'
	 */
	@Column(name = "type")
	private String type;

	@ManyToOne
	@JoinColumn(name = "observation_id")
	private Observation observation;

	public NoteAndComment() {
		super();
	}

	public NoteAndComment(Actor simulatedActor, String username, Domain domain) {
		super(simulatedActor, username, domain);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSetID() {
		return setID;
	}

	public void setSetID(Integer setID) {
		this.setID = setID;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Observation getObservation() {
		return observation;
	}

	public void setObservation(Observation observation) {
		this.observation = observation;
	}

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!super.equals(o)) return false;
        if (getClass() != o.getClass()) return false;

        NoteAndComment that = (NoteAndComment) o;

        if (setID != null ? !setID.equals(that.setID) : that.setID != null) return false;
        if (source != null ? !source.equals(that.source) : that.source != null) return false;
        if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        return observation != null ? observation.equals(that.observation) : that.observation == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (setID != null ? setID.hashCode() : 0);
        result = 31 * result + (source != null ? source.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (observation != null ? observation.hashCode() : 0);
        return result;
    }
}
