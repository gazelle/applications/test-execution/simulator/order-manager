package net.ihe.gazelle.ordermanager.utils;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message.QBP_Q11;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.segment.MSH;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.segment.QPD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * <b>Class Description : </b>LBLQueryMessageGenerator<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 28/07/16
 */
public class LBLQueryMessageGenerator {

    private static Logger log = LoggerFactory.getLogger(LBLQueryMessageGenerator.class);

    private LBLQueryMessageGenerator(){

    }

    /**
     * <p>generateQBP_SLI.</p>
     *
     * @param sut a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     * @param sendingApplication a {@link java.lang.String} object.
     * @param sendingFacility a {@link java.lang.String} object.
     * @param pid a {@link java.lang.String} object.
     * @param visitNumber a {@link java.lang.String} object.
     * @param pgn a {@link java.lang.String} object.
     * @param pon a {@link java.lang.String} object.
     * @param fon a {@link java.lang.String} object.
     * @param startDate a {@link java.util.Date} object.
     * @param endDate a {@link java.util.Date} object.
     * @return a {@link net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message.QBP_Q11} object.
     */
    public static QBP_Q11 generateQBP_SLI(HL7V2ResponderSUTConfiguration sut, String sendingApplication,
                                          String sendingFacility, String pid, String visitNumber, String pgn, String pon, String fon,
                                          Date startDate, Date endDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
        QBP_Q11 message = new QBP_Q11();
        try {
            fillMSHSegment(message.getMSH(), sut.getApplication(), sut.getFacility(), sendingApplication, sendingFacility,
                    "QBP", "SLI", "QBP_Q11", "2.5", sdf, sut.getCharset().getHl7Code());
            QPD qpd = message.getQPD();
            if (pid != null && !pid.isEmpty()) {
                qpd.getPatientID().parse(pid);
            }
            if (visitNumber != null && !visitNumber.isEmpty()) {
                qpd.getPatientVisitNumber().parse(visitNumber);
            }
            if (pgn != null && !pgn.isEmpty()) {
                qpd.getPlacerGroupNumber().parse(pgn);
            }
            if (pon != null && !pon.isEmpty()){
                qpd.getPlacerOrderNumber().parse(pon);
            }
            if (fon != null && !fon.isEmpty()){
                qpd.getFillerOrderNumber().parse(fon);
            }
            if (startDate != null){
                qpd.getSearchPeriod().getRangeStartDateTime().getTime().setValue(sdf.format(startDate));
            }
            if (endDate != null){
                qpd.getSearchPeriod().getRangeEndDateTime().getTime().setValue(sdf.format(endDate));
            }
            qpd.getMessageQueryName().getCe1_Identifier().setValue("SLI");
            qpd.getMessageQueryName().getCe2_Text().setValue("Specimen Labeling Instructions");
            qpd.getMessageQueryName().getCe3_NameOfCodingSystem().setValue("IHE_LABTF");
            qpd.getQueryTag().setValue(sdf.format(new Date()));
            message.getRCP().getQueryPriority().setValue("I");
            message.getRCP().getResponseModality().getIdentifier().setValue("R");
        } catch (HL7Exception e) {
            log.error(e.getMessage());
            return null;
        }
        return message;
    }

    private static void fillMSHSegment(MSH mshSegment,
                                       String receivingApplication, String receivingFacility,
                                       String sendingApplication, String sendingFacility,
                                       String messageCode, String triggerEvent,
                                       String msgStructure,
                                       String hl7Version,
                                       SimpleDateFormat sdf, String characterSet) throws DataTypeException {
        try {
            mshSegment.getFieldSeparator().setValue("|");
            mshSegment.getEncodingCharacters().setValue("^~\\&");
            mshSegment.getSendingApplication().getNamespaceID().setValue(sendingApplication);
            mshSegment.getSendingFacility().getNamespaceID().setValue(sendingFacility);
            mshSegment.getReceivingApplication().getNamespaceID().setValue(receivingApplication);
            mshSegment.getReceivingFacility().getNamespaceID().setValue(receivingFacility);
            mshSegment.getDateTimeOfMessage().getTime().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getMessageType().getMessageCode().setValue(messageCode);
            mshSegment.getMessageType().getTriggerEvent().setValue(triggerEvent);
            mshSegment.getMessageType().getMessageStructure().setValue(msgStructure);
            mshSegment.getProcessingID().getProcessingID().setValue("P");
            mshSegment.getMessageControlID().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getVersionID().getVersionID().setValue(hl7Version);
            if (characterSet != null) {
                mshSegment.getCharacterSet().setValue(characterSet);
            }
        } catch (DataTypeException e) {
            throw new DataTypeException("An error occurred when filling MSH segment: " + e.getMessage(), e);
        }
    }
}
