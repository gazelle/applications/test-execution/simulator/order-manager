package net.ihe.gazelle.ordermanager.action;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.SCPConfiguration;
import net.ihe.gazelle.ordermanager.model.SCPConfigurationQuery;
import net.ihe.gazelle.simulator.sut.action.AbstractSystemConfigurationManager;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.simulator.sut.model.UsageQuery;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Name("scpConfigurationManager")
@Scope(ScopeType.PAGE)
public class SCPConfigurationManager extends AbstractSystemConfigurationManager<SCPConfiguration> implements Serializable, QueryModifier<SCPConfiguration>, UserAttributeCommon {

    @In(value="gumUserService")
    private UserService userService;
    @Override
    protected HQLCriterionsForFilter<SCPConfiguration> getHQLCriterionsForFilter() {
        SCPConfigurationQuery query = new SCPConfigurationQuery();
        HQLCriterionsForFilter<SCPConfiguration> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("name", query.name());
        criteria.addPath("system", query.systemName());
        criteria.addPath("owner", query.owner());
        criteria.addQueryModifier(this);
        return criteria;
    }

    @Override
    public void copySelectedConfiguration(SCPConfiguration inConfiguration) {
        if (inConfiguration != null) {
            selectedSystemConfiguration = new SCPConfiguration(inConfiguration);
            editSelectedConfiguration();
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No configuration selected");
        }
    }

    @Override
    public void addConfiguration() {
        selectedSystemConfiguration = new SCPConfiguration();
        displayEditPanel = true;
        displayListPanel = false;
    }

    @Override
    public void deleteSelectedSystemConfiguration() {
        if (selectedSystemConfiguration != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            SCPConfiguration configToRemove = entityManager.find(
                    SCPConfiguration.class, selectedSystemConfiguration.getId());
            entityManager.remove(configToRemove);
            entityManager.flush();
            selectedSystemConfiguration = null;
            reset();
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "SUT configuration successfully deleted");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No SUT selected for deletion");
        }
    }

    @Override
    public List<Usage> getPossibleListUsages() {
        UsageQuery query = new UsageQuery();
        query.transaction().keyword().eq("RAD-5");
        return query.getList();
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<SCPConfiguration> hqlQueryBuilder, Map<String, Object> map) {
        SCPConfigurationQuery query = new SCPConfigurationQuery();
        if (!Identity.instance().isLoggedIn()) {
            hqlQueryBuilder.addRestriction(query.isPublic().eqRestriction(true));
            hqlQueryBuilder.addRestriction(query.isAvailable().eqRestriction(true));
        } else if (!Identity.instance().hasRole("admin_role")) {
            hqlQueryBuilder.addRestriction(query.isAvailable().eqRestriction(true));
            hqlQueryBuilder.addRestriction(HQLRestrictions.or(query.isPublic().eqRestriction(true),
                    query.owner().eqRestriction(Identity.instance().getCredentials().getUsername())));
        }
        if (filteredUsage != null){
            hqlQueryBuilder.addRestriction(query.listUsages().id().eqRestriction(filteredUsage.getId()));
        }
    }

    @Override
    public String getUserName(String s) {
        return userService.getUserDisplayNameWithoutException(s);
    }
}
