package net.ihe.gazelle.ordermanager.filter;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.LabOrderQuery;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <b>Class Description : </b>OrderFilter<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 */
public class LabOrderFilter extends AbstractOrderFilter<LabOrder> {

    private Boolean isMainEntity;
    private boolean workOrder;

    /**
     * <p>Constructor for LabOrderFilter.</p>
     *
     * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param newOrderControlCode a {@link java.lang.String} object.
     * @param isOrderAlreadyScheduled a {@link java.lang.Boolean} object.
     */
    public LabOrderFilter(Domain domain, Actor simulatedActor, String newOrderControlCode, Boolean isOrderAlreadyScheduled) {
        super(domain, simulatedActor, newOrderControlCode, isOrderAlreadyScheduled);
    }

    /**
     * <p>Constructor for LabOrderFilter.</p>
     *
     * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param newOrderControlCode a {@link java.lang.String} object.
     * @param isMainEntity a {@link java.lang.Boolean} object.
     * @param isWorkOrder a boolean.
     */
    public LabOrderFilter(Domain domain, Actor simulatedActor, String newOrderControlCode, Boolean isMainEntity, boolean isWorkOrder) {
        super(domain, simulatedActor, newOrderControlCode, null);
        this.isMainEntity = isMainEntity;
        this.workOrder = isWorkOrder;
    }

    /** {@inheritDoc} */
    @Override
    protected HQLCriterionsForFilter<LabOrder> getHQLCriterionsForFilter() {
        LabOrderQuery query = new LabOrderQuery();
        HQLCriterionsForFilter<LabOrder> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("firstname", query.encounter().patient().firstName());
        criteria.addPath("lastname", query.encounter().patient().lastName());
        criteria.addPath("actor", query.simulatedActor(), getActor());
        criteria.addPath("domain", query.domain(), getDomain());
        criteria.addPath("creator", query.creator());
        criteria.addPath("creationDate", query.lastChanged());
        criteria.addQueryModifier(this);
        return criteria;
    }

    /** {@inheritDoc} */
    @Override
    protected Integer getObjectId(LabOrder order) {
        return order.getId();
    }

    /** {@inheritDoc} */
    @Override
    public void modifyQuery(HQLQueryBuilder<LabOrder> hqlQueryBuilder, Map<String, Object> map) {
        LabOrderQuery query = new LabOrderQuery();
        hqlQueryBuilder.addRestriction(query.workOrder().eqRestriction(workOrder));
        if (getPlacerOrderNumber() != null && !getPlacerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.placerOrderNumber().likeRestriction(getPlacerOrderNumber()));
        }
        if ((getFillerOrderNumber() != null) && !getFillerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.fillerOrderNumber().likeRestriction(getFillerOrderNumber()));
        }
        if (getPatientid() != null && !getPatientid().isEmpty()){
            hqlQueryBuilder.addRestriction(query.encounter().patient().patientIdentifiers().identifier().likeRestriction(getPatientid()));
        }
        if (getVisitnumber() != null && !getVisitnumber().isEmpty()){
            hqlQueryBuilder.addRestriction(query.encounter().visitNumber().likeRestriction(getVisitnumber()));
        }
        if ((getNewOrderControlCode() != null) && !getNewOrderControlCode().isEmpty()) {
            if (getNewOrderControlCode().equals("DC")) {
                hqlQueryBuilder.addRestriction(query.orderStatus().eqRestriction("IP"));
                hqlQueryBuilder.addRestriction(query.orderControlCode().neqRestriction("DC"));
            } else if (getNewOrderControlCode().equals("CA") && !workOrder) {
                hqlQueryBuilder.addRestriction(query.orderStatus().isNullRestriction());
                hqlQueryBuilder.addRestriction(HQLRestrictions.or(query.orderControlCode().eqRestriction("OK"),
                        query.orderControlCode().eqRestriction("NA")));
            } else if (getNewOrderControlCode().equals("CA") && workOrder) {
                hqlQueryBuilder.addRestriction(query.orderControlCode().eqRestriction("NW"));
            } else if (getNewOrderControlCode().equals("OC") || getNewOrderControlCode().equals("SC")) {
                hqlQueryBuilder.addRestriction(HQLRestrictions.or(query.orderStatus().isNullRestriction(),
                        query.orderStatus().neqRestriction("CA")));
                List<String> controlCodeIn = Arrays.asList("OK", "NA", "UC");
                hqlQueryBuilder.addRestriction(query.orderControlCode().inRestriction(controlCodeIn));
            }
        }
        if (isMainEntity != null) {
            hqlQueryBuilder.addRestriction(query.mainEntity().eqRestriction(isMainEntity));
        }
    }
}
