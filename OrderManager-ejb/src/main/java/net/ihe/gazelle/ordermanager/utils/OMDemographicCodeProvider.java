package net.ihe.gazelle.ordermanager.utils;

import net.ihe.gazelle.patient.AbstractPatient;
import net.ihe.gazelle.patient.DemographicCodeProvider;
import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.international.LocaleSelector;

import javax.faces.model.SelectItem;
import java.util.List;
import java.util.Locale;

/**
 * <b>Class Description : </b>OMDemographicCodeProvider<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 */

@Name("omDemographicCodeProvider")
@Scope(ScopeType.EVENT)
public class OMDemographicCodeProvider implements DemographicCodeProvider {

    private static final String RELIGION = "RELIGION";
    private static final String RACE = "RACE";
    private static final String GENDER = "HL70001";

    /** {@inheritDoc} */
    @Override
    public String getReligion(AbstractPatient patient) {
        if (patient.getReligionCode() != null) {
            return ValueSetProvider.getInstance().getDisplayNameForCode(RELIGION, patient.getReligionCode());
        } else {
            return null;
        }
    }

    /** {@inheritDoc} */
    @Override
    public String getRace(AbstractPatient patient) {
        if (patient.getRaceCode() != null) {
            return ValueSetProvider.getInstance().getDisplayNameForCode(RACE, patient.getRaceCode());
        } else {
            return null;
        }
    }

    /** {@inheritDoc} */
    @Override
    public String getGender(AbstractPatient patient) {
        if (patient.getGenderCode() != null) {
            return ValueSetProvider.getInstance().getDisplayNameForCode(GENDER, patient.getGenderCode());
        } else {
            return null;
        }
    }

    /** {@inheritDoc} */
    @Override
    public String getCountry(AbstractPatient patient) {
        if (patient.getCountryCode() != null) {
            Locale locale = new Locale(LocaleSelector.instance().getLanguage(), patient.getCountryCode());
            return locale.getDisplayCountry(locale);
        } else {
            return null;
        }
    }

    /** {@inheritDoc} */
    @Override
    public List<SelectItem> getAllReligions() {
        return ValueSetProvider.getInstance().getListOfConcepts(RELIGION);
    }

    /** {@inheritDoc} */
    @Override
    public List<SelectItem> getAllRaces() {
        return ValueSetProvider.getInstance().getListOfConcepts(RACE);
    }

    /** {@inheritDoc} */
    @Override
    public List<SelectItem> getAllGenders() {
        return ValueSetProvider.getInstance().getListOfConcepts(GENDER);
    }
}
