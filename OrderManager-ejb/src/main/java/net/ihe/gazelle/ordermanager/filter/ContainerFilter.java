package net.ihe.gazelle.ordermanager.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.ContainerQuery;
import net.ihe.gazelle.simulator.common.tf.model.Actor;

import java.util.Map;

/**
 * <b>Class Description : </b>ContainerFilter<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 01/07/16
 *
 *
 *
 */
public class ContainerFilter implements QueryModifier<Container> {

    private Actor simulatedActor;
    private boolean workOrder;
    private String containerId;
    private String rackId;
    private String trayId;
    private String primaryId;

    private Filter<Container> filter;

    public ContainerFilter(boolean workOrder, Actor simulatedActor){
        this.simulatedActor = simulatedActor;
        this.workOrder = workOrder;
    }

    public Filter<Container> getFilter(){
        if (filter == null){
            filter = new Filter<Container>(getHQLCriterionsForFilter());
        }
        return filter;
    }

    public FilterDataModel<Container> getContainers(){
        return new FilterDataModel<Container>(getFilter()) {
            @Override
            protected Object getId(Container container) {
                return container.getId();
            }
        };
    }

    public void reset(){

    }

    private HQLCriterionsForFilter<Container> getHQLCriterionsForFilter() {
        ContainerQuery query = new ContainerQuery();
        HQLCriterionsForFilter<Container> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("creator", query.creator());
        criteria.addPath("creationDate", query.lastChanged());
        criteria.addPath("actor", query.simulatedActor(), simulatedActor);
        criteria.addPath("workOrder", query.workOrder(), workOrder);
        criteria.addQueryModifier(this);
        return criteria;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<Container> hqlQueryBuilder, Map<String, Object> map) {
        ContainerQuery query = new ContainerQuery();
        if (containerId != null && !containerId.isEmpty()){
            query.addRestriction(query.containerIdentifier().likeRestriction(containerId));
        }
        if (rackId != null && !rackId.isEmpty()){
            query.addRestriction(query.carrierIdentifier().likeRestriction(rackId));
        }
        if (trayId != null && !trayId.isEmpty()){
            query.addRestriction(query.trayIdentifier().likeRestriction(trayId));
        }
        if (primaryId != null && !primaryId.isEmpty()){
            query.addRestriction(query.primaryContainerIdentifier().likeRestriction(primaryId));
        }
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getRackId() {
        return rackId;
    }

    public void setRackId(String rackId) {
        this.rackId = rackId;
    }

    public String getTrayId() {
        return trayId;
    }

    public void setTrayId(String trayId) {
        this.trayId = trayId;
    }

    public String getPrimaryId() {
        return primaryId;
    }

    public void setPrimaryId(String primaryId) {
        this.primaryId = primaryId;
    }
}
