package net.ihe.gazelle.ordermanager.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "om_proxy_connection")
public class Connection implements java.io.Serializable {

    private static final long serialVersionUID = 6402109080942012360L;

    @Id
    @GeneratedValue
    private Integer id;

    @OneToMany(mappedBy = "connection")
    @OrderBy("dateReceived")
    private Set<DicomMessage> messages;

    private String uuid;

    private transient List<DicomMessage> sortedMessages = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Set<DicomMessage> getMessages() {
        return messages;
    }

    public void setMessages(Set<DicomMessage> messages) {
        this.messages = messages;
    }

    public List<DicomMessage> getSortedMessages() {
        if (sortedMessages == null) {
            sortedMessages = new ArrayList<DicomMessage>(getMessages());
            Collections.sort(sortedMessages);
        }
        return sortedMessages;
    }

    @Override
    public String toString() {
        return "Connection [id=" + id + ", uuid=" + uuid + "]";
    }

}
