package net.ihe.gazelle.ordermanager.menu;

import net.ihe.gazelle.common.pages.menu.Menu;
import net.ihe.gazelle.common.pages.menu.MenuEntry;
import net.ihe.gazelle.common.pages.menu.MenuGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>MainMenu<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 10/06/16
 *
 *
 *
 */
public class MainMenu {

    private static MenuGroup MENU = null;

    private MainMenu() {
        super();
    }

    public static final MenuGroup getMenu() {
        synchronized (MainMenu.class) {
            if (MENU == null) {
                ArrayList<Menu> children = new ArrayList<Menu>();

                children.add(getEyecare());
                children.add(getLaboratory());
                children.add(getRadiology());
                children.add(getSeHE());
                children.add(getSUT());
                children.add(getMessages());
                children.add(new MenuEntry(OrderManagerPages.DATA_BROWSER));
                children.add(new MenuEntry(OrderManagerPages.VALUE_SETS));
                MENU = new MenuGroup(null, children);
            }
        }
        return MENU;
    }

    public static final MenuGroup refreshMenu() {
      synchronized (MainMenu.class) {
          MENU = null;
      }
        return getMenu();
    }

    private static Menu getEyecare() {
        List<Menu> eyecare = new ArrayList<Menu>();
        eyecare.add(new MenuEntry(OrderManagerPages.EYE_IM_CONF));
        eyecare.add(getEyecareOF());
        eyecare.add(getEyecareOP());
        return new MenuGroup(OrderManagerPages.EYECARE, eyecare);
    }

    private static Menu getRadiology() {
        List<Menu> radiology = new ArrayList<Menu>();
        radiology.add(getAcquisitionModality());
        radiology.add(new MenuEntry(OrderManagerPages.RAD_IM_CONF));
        radiology.add(getRadiologyOF());
        radiology.add(getRadiologyOP());
        return new MenuGroup(OrderManagerPages.RADIOLOGY, radiology);
    }

    private static Menu getEyecareOF() {
        List<Menu> eyecareOFChildren = new ArrayList<Menu>();
        eyecareOFChildren.add(new MenuEntry(OrderManagerPages.EYE_OF_RAD3));
        eyecareOFChildren.add(new MenuEntry(OrderManagerPages.EYE_OF_RAD4));
        /*eyecareOFChildren.add(new MenuEntry(OrderManagerPages.EYE_OF_RAD48));*/
        eyecareOFChildren.add(new MenuEntry(OrderManagerPages.EYE_OF_WORKLIST));
        eyecareOFChildren.add(new MenuEntry(OrderManagerPages.EYE_OF_RAD5));
        eyecareOFChildren.add(new MenuEntry(OrderManagerPages.EYE_OF_CONF));
        return new MenuGroup(OrderManagerPages.OF, eyecareOFChildren);
    }

    private static Menu getEyecareOP() {
        List<Menu> eyecareOP = new ArrayList<Menu>();
        eyecareOP.add(new MenuEntry(OrderManagerPages.EYE_OP_RAD2));
        eyecareOP.add(new MenuEntry(OrderManagerPages.EYE_OP_CONF));
        return new MenuGroup(OrderManagerPages.OP, eyecareOP);
    }

    private static Menu getRadiologyOF() {
        List<Menu> radiologyOF = new ArrayList<Menu>();
        radiologyOF.add(new MenuEntry(OrderManagerPages.RAD_OF_RAD3));
        radiologyOF.add(new MenuEntry(OrderManagerPages.RAD_OF_RAD4));
        /*radiologyOF.add(new MenuEntry(OrderManagerPages.RAD_OF_RAD48));*/
        radiologyOF.add(new MenuEntry(OrderManagerPages.RAD_OF_WORKLIST));
        radiologyOF.add(new MenuEntry(OrderManagerPages.RAD_OF_RAD5));
        radiologyOF.add(new MenuEntry(OrderManagerPages.RAD_OF_CONF));
        return new MenuGroup(OrderManagerPages.OF, radiologyOF);
    }

    private static Menu getRadiologyOP() {
        List<Menu> radiologyOP = new ArrayList<Menu>();
        radiologyOP.add(new MenuEntry(OrderManagerPages.RAD_OP_RAD2));
        radiologyOP.add(new MenuEntry(OrderManagerPages.RAD_OP_CONF));
        return new MenuGroup(OrderManagerPages.OP, radiologyOP);
    }

    private static Menu getAcquisitionModality() {
        List<Menu> radiologyMOD = new ArrayList<Menu>();
        radiologyMOD.add(new MenuEntry(OrderManagerPages.RAD_MOD_RAD5));
        return new MenuGroup(OrderManagerPages.MOD, radiologyMOD);
    }

    private static Menu getLaboratory() {
        List<Menu> laboratory = new ArrayList<Menu>();
        laboratory.add(getAnalyzer());
        laboratory.add(getAnalyzerManager());
        laboratory.add(getAutomationManager());
        laboratory.add(getLaboratoryLB());
        laboratory.add(getLaboratoryLIP());
        laboratory.add(getLaboratoryOF());
        laboratory.add(getLaboratoryOP());
        laboratory.add(new MenuEntry(OrderManagerPages.LAB_ORT_CONF));
        return new MenuGroup(OrderManagerPages.LAB, laboratory);
    }

    private static Menu getLaboratoryLB() {
        List<Menu> lb = new ArrayList<Menu>();
        lb.add(new MenuEntry(OrderManagerPages.LAB_LB_LAB62));
        lb.add(new MenuEntry(OrderManagerPages.LAB_LB_LAB63));
        lb.add(new MenuEntry(OrderManagerPages.LAB_LB_CONF));
        return new MenuGroup(OrderManagerPages.LB, lb);
    }

    private static Menu getLaboratoryLIP() {
        List<Menu> lip = new ArrayList<Menu>();
        lip.add(new MenuEntry(OrderManagerPages.LAB_LIP_LAB61));
        lip.add(new MenuEntry(OrderManagerPages.LAB_LIP_CONF));
        return new MenuGroup(OrderManagerPages.LIP, lip);
    }

    private static Menu getLaboratoryOP() {
        List<Menu> op = new ArrayList<Menu>();
        op.add(new MenuEntry(OrderManagerPages.LAB_OP_LAB1));
        op.add(new MenuEntry(OrderManagerPages.LAB_OP_CONF));
        return new MenuGroup(OrderManagerPages.OP, op);
    }

    private static Menu getLaboratoryOF() {
        List<Menu> of = new ArrayList<Menu>();
        of.add(new MenuEntry(OrderManagerPages.LAB_OF_LAB1));
        of.add(new MenuEntry(OrderManagerPages.LAB_OF_LAB3));
        of.add(new MenuEntry(OrderManagerPages.LAB_OF_LAB4));
        of.add(new MenuEntry(OrderManagerPages.LAB_OF_CONF));
        return new MenuGroup(OrderManagerPages.OF, of);
    }

    private static Menu getAutomationManager() {
        List<Menu> am = new ArrayList<Menu>();
        am.add(new MenuEntry(OrderManagerPages.LAB_AM_LAB5));
        am.add(new MenuEntry(OrderManagerPages.LAB_AM_CONF));
        return new MenuGroup(OrderManagerPages.AM, am);
    }

    private static Menu getAnalyzerManager() {
        List<Menu> analyzerMgr = new ArrayList<Menu>();
        analyzerMgr.add(new MenuEntry(OrderManagerPages.LAB_ANMGR_LAB27));
        analyzerMgr.add(new MenuEntry(OrderManagerPages.LAB_ANMGR_LAB28));
        analyzerMgr.add(new MenuEntry(OrderManagerPages.LAB_ANMGR_CONF));
        return new MenuGroup(OrderManagerPages.ANALYZER_MGR, analyzerMgr);
    }

    private static Menu getAnalyzer() {
        List<Menu> analyzer = new ArrayList<Menu>();
        analyzer.add(new MenuEntry(OrderManagerPages.LAB_AN_LAB27));
        analyzer.add(new MenuEntry(OrderManagerPages.LAB_AN_LAB29));
        analyzer.add(new MenuEntry(OrderManagerPages.LAB_AN_CONF));
        return new MenuGroup(OrderManagerPages.ANALYZER, analyzer);
    }

    private static Menu getSUT() {
        List<Menu> sut = new ArrayList<Menu>();
        sut.add(new MenuEntry(OrderManagerPages.SUT_HL7_CONF));
        sut.add(new MenuEntry(OrderManagerPages.SUT_DICOM_CONF));
        return new MenuGroup(OrderManagerPages.SUT, sut);
    }

    private static Menu getMessages() {
        List<Menu> messages = new ArrayList<Menu>();
        messages.add(new MenuEntry(OrderManagerPages.HL7_MESSAGES));
        messages.add(new MenuEntry(OrderManagerPages.DICOM_MESSAGES));
        return new MenuGroup(OrderManagerPages.MESSAGES, messages);
    }

    private static Menu getSeHE() {
        List<Menu> sehe = new ArrayList<Menu>();
        sehe.add(getTroCreator());
        sehe.add(new MenuEntry(OrderManagerPages.SEHE_TRO_FORWARDER));
        sehe.add(getTroFulfiller());
        return new MenuGroup(OrderManagerPages.SEHE, sehe);
    }

    private static Menu getTroCreator() {
        List<Menu> creator = new ArrayList<Menu>();
        creator.add(new MenuEntry(OrderManagerPages.SEHE_CREATOR_MANAGE_ORDER));
        creator.add(new MenuEntry(OrderManagerPages.SEHE_TRO_CREATOR_CONF));
        return new MenuGroup(OrderManagerPages.SEHE_TRO_CREATOR, creator);
    }

    private static Menu getTroFulfiller() {
        List<Menu> fulfiller = new ArrayList<Menu>();
        fulfiller.add(new MenuEntry(OrderManagerPages.SEHE_FULFILLER_MANAGE_ORDER));
        fulfiller.add(new MenuEntry(OrderManagerPages.SEHE_TRO_FULFILLER_CONF));
        return new MenuGroup(OrderManagerPages.SEHE_TRO_FULFILLER, fulfiller);
    }
}
