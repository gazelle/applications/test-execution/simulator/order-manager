package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>RequestedProcedureFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class RequestedProcedureFilter extends Filter<RequestedProcedure> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5518097161896274749L;
	private static List<HQLCriterion<RequestedProcedure, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<RequestedProcedure, ?>>();
	}

	public RequestedProcedureFilter() {
		super(RequestedProcedure.class, defaultCriterions);
	}
}
