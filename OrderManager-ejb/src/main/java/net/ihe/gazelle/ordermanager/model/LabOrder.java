package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.annotations.Name;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <b>Class description</b> LabOrder
 * <p>
 * This class describes the order exchanged between the Order Placer and the Order Filler from the laboratory and pathology domains.
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes Bretagne Atlantique
 * @version 1.0 - 2011, December 5th
 *
 *
 *
 */
@Entity
@Name("labOrder")
@Table(name = "om_lab_order", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_lab_order_sequence", sequenceName = "om_lab_order_id_seq", allocationSize = 1)
public class LabOrder extends AbstractOrder implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6878497027607459027L;

    @Id
    @NotNull
    @GeneratedValue(generator = "om_lab_order_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "is_work_order")
    private boolean workOrder;

    /**
     * ORC-27
     */
    @Column(name = "expected_availability_date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expectedAvailabilityDateTime;

    /**
     * OBR-24
     */
    @Column(name = "diagnostic_service_section_id")
    private String diagnosticServiceSectionID;

    /**
     * OBR-28
     */
    @Column(name = "result_copies_to")
    private String resultCopiesTo;

    @ManyToMany(mappedBy = "orders", targetEntity = Specimen.class, cascade = CascadeType.MERGE)
    private List<Specimen> specimens;

    @ManyToOne
    @JoinColumn(name = "container_id")
    private Container container;

    @OneToMany(mappedBy = "order", targetEntity = Observation.class, cascade = CascadeType.MERGE)
    @OrderBy("setId ASC")
    // in order to properly fill out the OBX segment, and to put them in the right order, we need to get the observations sorted according to the SetID attribute
    private List<Observation> observations;

    /**
     * true if the message in which the order has been sent is OML_O21 false otherwise
     */
    @Column(name = "is_main_entity")
    private boolean mainEntity;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "om_order_parent_id", joinColumns = @JoinColumn(name = "om_parent_id"))
    @Column(name = "element")
    private List<String> parent;

    /**
     * Default constructor
     */
    public LabOrder() {
        super();
        this.setOrderStatus(STATUS_UNKNOWN);
    }

    /**
     * @param inDomain
     * @param inSimulatedActor
     * @param inEncounter
     * @param entityManager
     * @param isWorkOrder
     */
    public LabOrder(Domain inDomain, Actor inSimulatedActor, Encounter inEncounter, EntityManager entityManager,
                    boolean isMainEntity, boolean isWorkOrder) {
        this.workOrder = isWorkOrder;
        this.simulatedActor = inSimulatedActor;
        this.domain = inDomain;
        if (this.simulatedActor.getKeyword().equals("OP") || this.simulatedActor.getKeyword().equals("LIP")) {
            this.setPlacerOrderNumber(NumberGenerator.getOrderNumberForActor(this.simulatedActor, entityManager));
            this.setPlacerGroupNumber(NumberGenerator.getOrderNumberForActor(this.simulatedActor, entityManager));
            this.setOrderStatus(null);
        } else if (this.simulatedActor.getKeyword().equals("ANALYZER_MGR")) {
            this.setPlacerOrderNumber(NumberGenerator.getOrderNumberForActor(this.simulatedActor, entityManager));
            this.setPlacerGroupNumber(NumberGenerator.getOrderNumberForActor(
                    Actor.findActorWithKeyword("OP", entityManager), entityManager));
            this.setOrderStatus(null);
        } else if (!this.simulatedActor.getKeyword().equals("ANALYZER_MGR")) {
            this.setFillerOrderNumber(NumberGenerator.getOrderNumberForActor(this.simulatedActor, entityManager));
        }
        if (this.simulatedActor.getKeyword().equals("ANALYZER")) {
            this.setPlacerOrderNumber("\"\"");
            this.setOrderControlCode("OK");
        }
        if (isWorkOrder) {
            this.setPlacerOrderNumber(NumberGenerator.getOrderNumberForActor(Actor.findActorWithKeyword("OP"),
                    entityManager));
            this.setPlacerGroupNumber(NumberGenerator.getOrderNumberForActor(Actor.findActorWithKeyword("OP"),
                    entityManager));
        }
        this.lastChanged = new Date();
        this.setEncounter(inEncounter);
        this.mainEntity = isMainEntity;
        if (isMainEntity) {
            this.specimens = new ArrayList<Specimen>();
            this.container = null;
        }
    }

    /**
     * Creates a work order from the given selected order
     *
     * @param order
     */
    public LabOrder(LabOrder order, boolean isMainEntity) {
        this.workOrder = true;
        this.mainEntity = isMainEntity;
        this.domain = order.getDomain();
        this.setFillerOrderNumber(order.getFillerOrderNumber());
        if (order.getPlacerOrderNumber() != null) {
            this.setPlacerOrderNumber(order.getPlacerOrderNumber());
        } else {
            this.setPlacerOrderNumber(NumberGenerator.getOrderNumberForActor(Actor.findActorWithKeyword("OP"),
                    EntityManagerService.provideEntityManager()));
        }
        if (order.getPlacerGroupNumber() != null) {
            this.setPlacerGroupNumber(order.getPlacerGroupNumber());
        } else {
            this.setPlacerGroupNumber(NumberGenerator.getOrderNumberForActor(Actor.findActorWithKeyword("OP"),
                    EntityManagerService.provideEntityManager()));
        }
        this.simulatedActor = Actor.findActorWithKeyword("OF");
        this.lastChanged = new Date();
        this.setEncounter(order.getEncounter());
        this.expectedAvailabilityDateTime = null;
        this.resultCopiesTo = order.getResultCopiesTo();
        this.setCallBackPhoneNumber(order.getCallBackPhoneNumber());
        this.diagnosticServiceSectionID = order.getDiagnosticServiceSectionID();
        this.setEnteredBy(order.getEnteredBy());
        this.setEnteringOrganization(order.getEnteringOrganization());
        this.setOrderControlCode("NW");
        this.setOrderStatus(order.getOrderStatus());
        this.setOrderingProvider(order.getOrderingProvider());
        this.setQuantityTiming(order.getQuantityTiming());
        this.setReasonForStudy(order.getReasonForStudy());
        this.setStartDateTime(order.getStartDateTime());
        this.setTechnician(order.getTechnician());
        this.setUniversalServiceId(order.getUniversalServiceId());
        this.setTransactionDate(order.getTransactionDate());
    }

    /**
     * @param entityManager
     * @return the saved order
     */
    public LabOrder save(EntityManager entityManager) {
        return super.save(LabOrder.class, entityManager);
    }

    @Override
    public void fillOrderRandomly(Domain inDomain, Actor simulatedActor) {
        if (this.getQuantityTiming() == null) {
            if (simulatedActor.getKeyword().equals("ANALYZER") || simulatedActor.getKeyword().equals("ANALYZER_MGR")) {
                this.setQuantityTiming(ValueSet.getRandomCodeFromValueSet("quantityTiming4law"));
            } else {
                this.setQuantityTiming(ValueSet.getRandomCodeFromValueSet("quantityTiming"));
            }
        }
        if (this.getEnteredBy() == null) {
            this.setEnteredBy(ValueSet.getRandomCodeFromValueSet("doctor"));
        }
        if (this.getEnteringOrganization() == null) {
            this.setEnteringOrganization(ValueSet.getRandomCodeFromValueSet("enteringOrganization"));
        }
        if (this.getTransactionDate() == null) {
            this.setTransactionDate(new Date());
        }
        if (this.getOrderingProvider() == null && !simulatedActor.getKeyword().equals("ANALYZER")) {
            this.setOrderingProvider(this.getEnteredBy());
        }
        if (this.getTechnician() == null) {
            this.setTechnician(this.getEnteredBy());
        }
        if (this.getUniversalServiceId() == null) {
            this.setUniversalServiceId(ValueSet.getRandomCodeFromValueSet("labUniversalServiceId"));
        }
        // this field is not valued in LAB-4 transaction
        if (simulatedActor.getKeyword().equals("OF") && (this.expectedAvailabilityDateTime == null) && !this.workOrder) {
            this.setExpectedAvailabilityDateTime(new Date());
        }
        if (this.getStartDateTime() == null) {
            this.setStartDateTime(new Date());
        }
        if (simulatedActor.getKeyword().equals("OF") && (this.diagnosticServiceSectionID == null)) {
            this.setDiagnosticServiceSectionID(ValueSet.getRandomCodeFromValueSet("diagnosticService"));
        }
        if (this.resultCopiesTo == null) {
            this.setResultCopiesTo(ValueSet.getRandomCodeFromValueSet("doctor"));
        }
    }

    /*******************************************
     * GETTERS and SETTERS
     *******************************************/
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Specimen> getSpecimens() {
        return specimens;
    }

    public void setSpecimens(List<Specimen> specimens) {
        this.specimens = specimens;
    }

    public void setMainEntity(boolean mainEntity) {
        this.mainEntity = mainEntity;
    }

    public boolean isMainEntity() {
        return mainEntity;
    }

    public void setContainer(Container container) {
        this.container = container;
    }

    public Container getContainer() {
        return container;
    }

    public final void setExpectedAvailabilityDateTime(Date expectedAvailabilityDateTime) {
        if (expectedAvailabilityDateTime != null) {
            this.expectedAvailabilityDateTime = (Date) expectedAvailabilityDateTime.clone();
        } else {
            this.expectedAvailabilityDateTime = null;
        }
    }

    public Date getExpectedAvailabilityDateTime() {
        if (expectedAvailabilityDateTime != null) {
            return (Date) expectedAvailabilityDateTime.clone();
        } else {
            return null;
        }
    }


    public void setDiagnosticServiceSectionID(String diagnosticServiceSectionID) {
        this.diagnosticServiceSectionID = diagnosticServiceSectionID;
    }

    public String getDiagnosticServiceSectionID() {
        return diagnosticServiceSectionID;
    }

    public void setResultCopiesTo(String resultCopiesTo) {
        this.resultCopiesTo = resultCopiesTo;
    }

    public String getResultCopiesTo() {
        return resultCopiesTo;
    }

    public boolean isWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(boolean workOrder) {
        this.workOrder = workOrder;
    }

    public void setObservations(List<Observation> observations) {
        this.observations = observations;
    }

    public List<Observation> getObservations() {
        return observations;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = (prime * result) + (workOrder ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LabOrder other = (LabOrder) obj;
        if (workOrder != other.workOrder) {
            return false;
        }
        return true;
    }

    public List<String> getParent() {
        return parent;
    }

    public void setParent(List<String> parent) {
        this.parent = parent;
    }

}
