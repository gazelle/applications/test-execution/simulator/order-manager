package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyProcedure;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>RequestedProcedureFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class TeleRadiologyProcedureFilter extends Filter<TeleRadiologyProcedure> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5518097161896274749L;
	private static List<HQLCriterion<TeleRadiologyProcedure, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<TeleRadiologyProcedure, ?>>();
	}

	public TeleRadiologyProcedureFilter() {
		super(TeleRadiologyProcedure.class, defaultCriterions);
	}
}
