package net.ihe.gazelle.ordermanager.dicom.proxy;

import net.ihe.gazelle.ordermanager.model.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class ConnectionManager {

    public static final long HOUR_IN_MILLIS = 3_600_000;
    private static final Logger LOG = LoggerFactory.getLogger(ConnectionManager.class);

    static final Map<Integer, Connection> connections = Collections
            .synchronizedMap(new HashMap<Integer, Connection>());

    private static Date lastConnectionDate = new Date();

    static final Map<Integer, String> uuids = Collections.synchronizedMap(new HashMap<Integer, String>());

    private ConnectionManager() {
        // private
    }

    public static String retrieveUuid(Integer channelId) {
        return retrieveUuid(channelId, null);
    }

    private static String retrieveUuid(Integer requestChannelId, Integer responseChannelId) {
        String uuid = retrieveConnectionUuidFromKwnownConnections(requestChannelId, responseChannelId);
        if (uuid == null) {
            synchronized (ConnectionManager.class) {
                uuid = retrieveConnectionUuidFromKwnownConnections(requestChannelId, responseChannelId);
                if (uuid == null) {
                    uuid = createUuid(requestChannelId, responseChannelId);
                }
            }
        }
        return uuid;
    }

    private static String createUuid(Integer requestChannelId, Integer responseChannelId) {
        String uuid = UUID.randomUUID().toString();
        uuids.put(requestChannelId, uuid);
        if (responseChannelId != null) {
            uuids.put(responseChannelId, uuid);
        }
        return uuid;
    }

    private static String retrieveConnectionUuidFromKwnownConnections(Integer requestChannelId,
            Integer responseChannelId) {
        String uuid = uuids.get(requestChannelId);
        if (uuid == null && responseChannelId != null) {
            uuid = uuids.get(responseChannelId);
        }
        return uuid;
    }

    static Connection retrieveConnection(EntityManager entityManager, Integer requestChannelId,
            Integer responseChannelId) {
        LOG.debug("Retrieving connection for channel ids " + requestChannelId + " / " + responseChannelId);
        clearConnectionsIfNoActivity();
        Connection connection = retrieveConnectionFromKwnownConnections(requestChannelId, responseChannelId);
        if (connection == null) {
            synchronized (ConnectionManager.class) {
                connection = retrieveConnectionFromKwnownConnections(requestChannelId, responseChannelId);
                if (connection == null) {
                    connection = createConnection(entityManager, requestChannelId, responseChannelId);
                }
            }
        }
        LOG.debug("Got connection " + connection + " for channel ids " + requestChannelId + " / " + responseChannelId);
        return connection;
    }

    /**
     * Clear the map if no connection was received in the last hour
     */
    private static void clearConnectionsIfNoActivity() {
        Date current = new Date();
        long lastCall = lastConnectionDate.getTime();
        if (lastCall < (current.getTime() - HOUR_IN_MILLIS)){
            connections.clear();
            uuids.clear();
        }
        synchronized (ConnectionManager.class) {
            lastConnectionDate = current;
        }
    }


    private static Connection createConnection(EntityManager entityManager, Integer requestChannelId,
            Integer responseChannelId) {
        Connection connection = new Connection();
        connection.setUuid(retrieveUuid(requestChannelId, responseChannelId));
        entityManager.persist(connection);
        connections.put(requestChannelId, connection);
        if (responseChannelId != null) {
            connections.put(responseChannelId, connection);
        }
        return connection;
    }

    private static Connection retrieveConnectionFromKwnownConnections(Integer requestChannelId,
            Integer responseChannelId) {
        Connection connection = connections.get(requestChannelId);
        if (connection == null && responseChannelId != null) {
            connection = connections.get(responseChannelId);
        }
        return connection;
    }


}
