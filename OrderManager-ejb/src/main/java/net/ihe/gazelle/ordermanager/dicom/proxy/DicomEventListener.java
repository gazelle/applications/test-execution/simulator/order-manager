package net.ihe.gazelle.ordermanager.dicom.proxy;

import jp.digitalsensation.ihej.transactionmonitor.dicom.messageexchange.DimseMessage;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.ordermanager.model.Connection;
import net.ihe.gazelle.ordermanager.model.DicomMessage;
import net.ihe.gazelle.proxy.netty.channel.ProxySide;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import javax.persistence.EntityManager;
import java.sql.Timestamp;

public class DicomEventListener extends SameEventListener<DimseMessage> {


    private Actor simulatedActor;
    private Transaction simulatedTransaction;
    private Connection currentConnection;

    public DicomEventListener(Actor inSimulatedActor, Transaction transaction) {
        super();
        this.simulatedActor = inSimulatedActor;
        this.simulatedTransaction = transaction;
    }

    @Override
    protected void saveMessage(final Timestamp timeStamp, final DimseMessage dicom, final String requesterIp,
                               final int requesterPort, final int proxyPort, final String responderIp, final int responderPort,
                               final int requestChannelId, final int responseChannelId, final ProxySide side) {
        // Messages are coming from different threads!
        synchronized (this) {

            try {
                HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                    @Override
                    public Object performAction(EntityManager entityManager, Object... context) throws Exception {

                        DicomMessage messageToStore = new DicomMessage(timeStamp, requesterIp,
                                responderIp, side);
                        messageToStore.setSimulatedActor(simulatedActor);
                        messageToStore.setTransaction(simulatedTransaction);
                        if (dicom.getDataSet() != null) {
                            String pathToStore = dicom.getDataSet().getAbsolutePath();
                            messageToStore.setFilePath(pathToStore);
                        }

                        if (dicom.getCommandSet() != null && dicom.getCommandSet().length > 0) {
                            messageToStore.setCommandSet(dicom.getCommandSet());
                        }
                        messageToStore.setTransferSyntax(dicom.getTransferSyntax());
                        processMessage(entityManager, messageToStore, requestChannelId, responseChannelId);
                        currentConnection = messageToStore.getConnection();
                        return null;
                    }
                });
            } catch (HibernateFailure e) {
                // Already logged
            }
        }
    }

    public Connection getCurrentConnection() {
        return currentConnection;
    }
}
