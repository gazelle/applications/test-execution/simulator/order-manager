package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.datamodel.TeleRadiologyProcedureDataModel;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.hibernate.annotations.IndexColumn;
import org.jboss.seam.annotations.Name;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Name("teleRadiologyOrder")
@Table(name = "om_tele_radiology_order", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_tele_radiology_order_sequence", sequenceName = "om_tele_radiology_order_id_seq", allocationSize = 1)
public class TeleRadiologyOrder extends AbstractOrder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(generator = "om_tele_radiology_order_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	private Integer id;

	/**
	 * OBR-20
	 */
	@Column(name = "views")
	private String views;

	/**
	 * OBR-44 (component 1-3)
	 */
	@Column(name = "procedure_code")
	private String procedureCode;
	
	/**
	 * OBR-19 : source of imaging Study
	 */
	@Column(name="imaging_study_source")
	private String imagingStudySource;
	
	@Column(name="comment")
	private String comment;
	
	@Column(name="abort_reason")
	private String abortReason;

	/**
	 * OBR-44 (component 5)
	 */
	@Column(name = "procedure_description")
	private String procedureDescription;

	@OneToMany(mappedBy = "teleRadiologyOrder", targetEntity = TeleRadiologyProcedure.class, cascade = CascadeType.MERGE)
	private List<TeleRadiologyProcedure> teleRadiologyProcedures;
	
	@ElementCollection(targetClass=java.lang.String.class)
	@IndexColumn(name = "accession_identifier")
	private List<String> relatedStudies;

	public TeleRadiologyOrder() {
		super();
	}

	public TeleRadiologyOrder(Domain inDomain, Actor inSimulatedActor, Encounter inEncounter,
			EntityManager entityManager) {
		super(inSimulatedActor, null, inDomain);
		this.simulatedActor = inSimulatedActor;
		this.domain = inDomain;
		this.setFillerOrderNumber(NumberGenerator
                .getOrderNumberForActor(Actor.findActorWithKeyword("OF"), entityManager));
		this.setPlacerOrderNumber(NumberGenerator
                .getOrderNumberForActor(Actor.findActorWithKeyword("OP"), entityManager));
		this.lastChanged = new Date();
		this.setEncounter(inEncounter);
		this.setOrderStatus(null);
	}

	@Override
	public void fillOrderRandomly(Domain inDomain, Actor simulatedActor) {
		if (this.getQuantityTiming() == null) {
			this.setQuantityTiming(ValueSet.getRandomCodeFromValueSet("quantityTiming"));
		}
		if (this.getEnteredBy() == null) {
			this.setEnteredBy(ValueSet.getRandomCodeFromValueSet("doctor"));
		}
		if (this.getEnteringOrganization() == null) {
			this.setEnteringOrganization(ValueSet.getRandomCodeFromValueSet("enteringOrganization"));
		}
		if (this.getTransactionDate() == null) {
			this.setTransactionDate(new Date());
		}
		if (this.getOrderingProvider() == null) {
			this.setOrderingProvider(this.getEnteredBy());
		}
		if (this.getTechnician() == null) {
			this.setTechnician(this.getEnteredBy());
		}
		if (this.getUniversalServiceId() == null) {
			this.setUniversalServiceId(ValueSet.getRandomCodeFromValueSet("troUniversalServiceId"));
		}
		if (this.getStartDateTime() == null) {
			this.setStartDateTime(new Date());
		}
		if (this.getProcedureCode() == null) {
			this.setProcedureCode(ValueSet.getRandomCodeFromValueSet("troProcedure"));
		}
	}
	
	public TeleRadiologyProcedureDataModel getProceduresAsDataModel() {
		return new TeleRadiologyProcedureDataModel(this);
	}
	
	public TeleRadiologyOrder save(){
		return save(TeleRadiologyOrder.class, EntityManagerService.provideEntityManager());
	}
	
	public TeleRadiologyOrder save(EntityManager entityManager){
		return save(TeleRadiologyOrder.class, entityManager);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getViews() {
		return views;
	}

	public void setViews(String views) {
		this.views = views;
	}

	public String getProcedureCode() {
		return procedureCode;
	}

	public void setProcedureCode(String procedureCode) {
		this.procedureCode = procedureCode;
	}

	public String getProcedureDescription() {
		return procedureDescription;
	}

	public void setProcedureDescription(String procedureDescription) {
		this.procedureDescription = procedureDescription;
	}

	public List<TeleRadiologyProcedure> getTeleRadiologyProcedures() {
		return teleRadiologyProcedures;
	}

	public void setTeleRadiologyProcedures(List<TeleRadiologyProcedure> teleRadiologyProcedures) {
		this.teleRadiologyProcedures = teleRadiologyProcedures;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((procedureCode == null) ? 0 : procedureCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeleRadiologyOrder other = (TeleRadiologyOrder) obj;
		if (procedureCode == null) {
			if (other.procedureCode != null)
				return false;
		} else if (!procedureCode.equals(other.procedureCode))
			return false;
		return true;
	}

	public static TeleRadiologyOrder getOrderByOrderFillerNumber(String fillerOrderNumber, Actor simulatedActor, EntityManager entityManager) {
		TeleRadiologyOrderQuery query = new TeleRadiologyOrderQuery(entityManager);
		query.simulatedActor().eq(simulatedActor);
		query.fillerOrderNumber().eq(fillerOrderNumber);
		return query.getUniqueResult();
	}

	public String getImagingStudySource() {
		return imagingStudySource;
	}

	public void setImagingStudySource(String imagingStudySource) {
		this.imagingStudySource = imagingStudySource;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<String> getRelatedStudies() {
		return relatedStudies;
	}

	public void setRelatedStudies(List<String> relatedStudies) {
		this.relatedStudies = relatedStudies;
	}

	public String getAbortReason() {
		return abortReason;
	}

	public void setAbortReason(String abortReason) {
		this.abortReason = abortReason;
	}
	
}