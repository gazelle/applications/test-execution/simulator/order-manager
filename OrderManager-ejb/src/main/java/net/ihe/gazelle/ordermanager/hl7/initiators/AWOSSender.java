package net.ihe.gazelle.ordermanager.hl7.initiators;


import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.filter.LabOrderFilter;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.WOSQueryContent;
import net.ihe.gazelle.ordermanager.utils.LAWOptions;
import net.ihe.gazelle.ordermanager.utils.LAWSpecimenRole;
import net.ihe.gazelle.ordermanager.utils.LabMessageBuilder;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Class description : <b>LabOrderSender</b>
 * 
 * This class is the backing bean for the page /hl7Intiators/labOrderSender.xhtml, the latter enables the user to create a new order, cancel, replace, update or discontinue it and sends the message to
 * his/her system under test
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, December 6th
 * 
 */

@Name("awosSender")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 30000)
public class AWOSSender extends AbstractOrderSender<LabOrder> implements Serializable, UserAttributeCommon {

	private static Logger log = LoggerFactory.getLogger(AWOSSender.class);

	private static final long serialVersionUID = -3833081015406182145L;

	public static final String SPECIMEN_CENTRIC = "OML^O33^OML_O33";

	private Specimen selectedSpecimen;
	private Container selectedContainer;
	private Integer numberOfContainers;
	private Container container;
	private boolean editOrder;
	private boolean editSpecimen;
	private boolean expendPanel;
	private List<LAWOptions> availableOptionsForLAW = null;
	private List<LAWOptions> selectedOptions = null;

	@In(value="gumUserService")
	private UserService userService;

	@Override
    @Create
	public void initializePage(){
		super.initializePage();
		availableOptionsForLAW = LAWOptions.getOptionsForActor(null, "LAB-28");
		selectedOptions = new ArrayList<LAWOptions>();
	}

	public boolean isOptionSupported(String optionKeyword){
		LAWOptions option = LAWOptions.getOptionByKeyword(optionKeyword);
		if (option == null){
			return false;
		} else {
            return selectedOptions.contains(option);
		}
	}

	/**
	 * This class is also used to create a new specimen or container depending on the context
	 */
	@Override
	public void createANewOrder() {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		// one specimen - several containers - several orders
		selectedSpecimen = new Specimen(true, selectedDomain, simulatedActor, entityManager, workOrder);
		selectedSpecimen.setCreator(username);

		// Initialize the new container
		container = new Container();
		container.setOrders(null);
		container.setDomain(Domain.getDomainByKeyword("LAB"));
		container.setSimulatedActor(simulatedActor);
		container.setLastChanged(new Date());

		String queryId = urlParams.get("wosQuery");

		if (queryId != null) {
			try {
				WOSQueryContent wosQuery = entityManager.find(WOSQueryContent.class, Integer.decode(queryId));
				if (wosQuery != null) {
					container.setCarrierIdentifier(wosQuery.getCarrierId());
					container.setContainerIdentifier(wosQuery.getContainerId());
					container.setTrayIdentifier(wosQuery.getTrayId());
					container.setPositionInCarrier(wosQuery.getPositionInCarrier());
					container.setPositionInTray(wosQuery.getPositionInTray());
					container.setLocation(wosQuery.getLocation());
					container.setPrimaryContainerIdentifier(wosQuery.getPrimaryContainerId());
				} else {
					FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot retrieve WOS Query with id " + queryId);
				}
			} catch (NumberFormatException e) {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The provided queryId is not a properly formatted Integer");
			}
		} else {
			log.info("wosQuery not provided");
		}

		selectedSpecimen.getContainers().add(container);

		numberOfContainers = 1;
		editOrder = false;
		editSpecimen = true;
		selectedOrder = null;
		selectedContainer = null;
	}

	@Override
	public void onChangeActionEvent() {
		if (orderControlCode.equals(NEW)) {
			setSelectionType(SelectionType.ENCOUNTER);
		} else if (orderControlCode.equals(CANCEL)) {
			encounterComesFromDispatch = false;
            setSelectionType(SelectionType.ORDER);
			if (simulatedActor.getKeyword().equals(OP_KEYWORD)) {
				orderFilter = new LabOrderFilter(selectedDomain, simulatedActor, "CA", null, false);
			}
			// retrieve the list of work orders which can be cancelled
			else if (workOrder) {
				orderFilter = new LabOrderFilter(selectedDomain, simulatedActor, null, true);
			} else {
				orderFilter = new LabOrderFilter(selectedDomain, simulatedActor, "OC", null, false);
			}
		} else if (orderControlCode.equals(UPDATE)) {
			encounterComesFromDispatch = false;
            setSelectionType(SelectionType.ORDER);
			orderFilter = new LabOrderFilter(selectedDomain, simulatedActor, "SC", null, false);
		} else {
			// those cases are not yet processed
			log.info("This case is not yet handled by the simulator");
		}
		if (!encounterComesFromDispatch) {
			setSelectedEncounter(null);
			setSelectedPatient(null);
			selectedOrder = null;
			selectedSpecimen = null;
			selectedContainer = null;
		}

	}

	@Override
	public void sendMessage() {
		Transaction transaction = Transaction.GetTransactionByKeyword("LAB-28");
		encounterComesFromDispatch = false;
		HL7V2ResponderSUTConfiguration sut = (HL7V2ResponderSUTConfiguration) Component.getInstance("selectedSUT");
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		if (sut == null) {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No SUT selected ! please select the system to test and retry");
			return;
		}
		if (orderControlCode.equals(NEW)) {
			selectedSpecimen = selectedSpecimen.save(entityManager);
		} else // For Cancel case
		{
			selectedOrder.setOrderResultStatus("X");
			selectedOrder.setOrderControlCode("CA");
			selectedOrder = selectedOrder.save(entityManager);
			setSelectedEncounter(selectedOrder.getEncounter());
		}
		// build message
		LabMessageBuilder messageGenerator = new LabMessageBuilder(transaction.getKeyword(),
				simulatedActor.getKeyword(), getSelectedEncounter(), selectedOrder, selectedSpecimen, sut,
				sendingApplication, sendingFacility, SPECIMEN_CENTRIC, selectedOptions);

		try {
			String messageToSend = messageGenerator.generateMessage();
			if (messageToSend != null) {
				Initiator hl7Initiator = new Initiator(sut, sendingFacility, sendingApplication, simulatedActor,
						transaction, messageToSend, SPECIMEN_CENTRIC, "LAB", Actor.findActorWithKeyword(sutActorKeyword));

				TransactionInstance message = hl7Initiator.sendMessageAndGetTheHL7Message();
				if (message == null) {
					hl7Messages = null;
				} else {
					hl7Messages = new ArrayList<TransactionInstance>();
                    hl7Messages.add(message);
					// examine response: check order control code
					String messageToDecode = message.getReceivedMessageContent();
					MessageDecoder.decodeAcknowledgement(messageToDecode, simulatedActor);
				}
			} else {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The message has not been built !");
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to build the message for sending: " + e.getMessage());
		}
		selectedOrder = null;
		editOrder = false;
		editSpecimen = false;
		selectedSpecimen = null;
	}

	public void fillSpecimenRandomly() {
		selectedSpecimen.fillRandomly();
		selectedSpecimen.setRole(LAWSpecimenRole.P.getCode());
	}

	private void createContainersForSpecimen() {
		container.setSpecimen(selectedSpecimen);
		selectedSpecimen.setContainers(new ArrayList<Container>());
		selectedSpecimen.getContainers().add(container);
	}

	public void removeSpecimenFromOrder(Specimen inSpecimen) {
		selectedOrder.getSpecimens().remove(inSpecimen);
	}

	public void removeOrderFromSpecimen(LabOrder inOrder) {
		selectedSpecimen.getOrders().remove(inOrder);
	}

	//
	public void createOrderForSpecimen() {
		// Need to check the conditional usage. See the W.3.10 part of the LAW IHE TF for more details.
		boolean containerDescriptionIsOk = true;
		if (container.getContainerIdentifier().isEmpty() && container.getPrimaryContainerIdentifier().isEmpty()) {
			FacesMessages
					.instance()
					.addFromResourceBundle(StatusMessage.Severity.ERROR,
							"gazelle.ordermanager.PleaseFillAtLeastTheContainerIdentifierAndOrThePrimaryContainerIdentifierFields");
			containerDescriptionIsOk = false;
		} else {

			if ((!container.getPrimaryContainerIdentifier().isEmpty() && container.getContainerIdentifier().isEmpty())
					&& container.getCarrierIdentifier().isEmpty() && container.getTrayIdentifier().isEmpty()) {
				FacesMessages
						.instance()
						.add(StatusMessage.Severity.ERROR,
						        "Since the primary container identifier is provided but not the container identifier, carrier identifier or tray identifier shall be populated");
				containerDescriptionIsOk = false;
			} else if (!container.getPrimaryContainerIdentifier().isEmpty()
					&& !container.getContainerIdentifier().isEmpty()
					&& (!container.getCarrierIdentifier().isEmpty() || !container.getTrayIdentifier().isEmpty())) {
				FacesMessages
						.instance()
						.add("Since both the primary container identifier and the container identifier are provided, all other attributes are not supported");
				containerDescriptionIsOk = false;
			}
			if (!container.getCarrierIdentifier().isEmpty() && !container.getTrayIdentifier().isEmpty()) {
				FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR,
						"gazelle.ordermanager.YoucantFillTheCarrierIdentifierAndTheTrayIdentifierFields");
				containerDescriptionIsOk = false;
			}
			if (!container.getCarrierIdentifier().isEmpty() && container.getPositionInCarrier().isEmpty()) {
				FacesMessages
						.instance()
						.addFromResourceBundle(StatusMessage.Severity.ERROR,
                                "gazelle.ordermanager.IfYouFillTheCarrierIdentifierFieldYouMustFillThePositionInCarrierFieldToo");
				containerDescriptionIsOk = false;
			} else if (container.getCarrierIdentifier().isEmpty() && !container.getPositionInCarrier().isEmpty()) {
				FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR,
                        "If the carrier identifier is not populated, the position in carrier is not supported");
				containerDescriptionIsOk = false;
			}

			if (!container.getTrayIdentifier().isEmpty() && container.getPositionInTray().isEmpty()) {
				FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR,
                        "gazelle.ordermanager.IfYouFillTheTrayIdentifierFieldYouMustFillThePositionInTrayFieldToo");
				containerDescriptionIsOk = false;
			} else if (container.getTrayIdentifier().isEmpty() && !container.getPositionInTray().isEmpty()) {
				FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR,
                        "If the tray identifier is not populated, the position in tray is not supported");
				containerDescriptionIsOk = false;
			}
		}
		if (!containerDescriptionIsOk) {
			return;
		} else {
			editSpecimen = false;
			createContainersForSpecimen();
			selectedOrder = new LabOrder(selectedDomain, simulatedActor, getSelectedEncounter(),
					EntityManagerService.provideEntityManager(), false, workOrder);
			selectedOrder.setCreator(username);
			selectedOrder.setSpecimens(new ArrayList<Specimen>());
			selectedOrder.getSpecimens().add(selectedSpecimen);
			selectedOrder.setOrderControlCode("NW");
			expendPanel = false;
			editOrder = true;
		}
	}

	public void addOrderToSpecimen() {
		if (selectedSpecimen.getOrders() == null) {
			selectedSpecimen.setOrders(new ArrayList<LabOrder>());
		}
		selectedSpecimen.getOrders().add(selectedOrder);
		createOrderForSpecimen();
		expendPanel = true;
		editOrder = false;
		selectedOrder = null;
	}

	public void addOrderToSpecimenAndSendMessage() {
		addOrderToSpecimen();
		sendMessage();
	}

	public void removeContainerFromSpecimen(Container inContainer) {
		selectedSpecimen.getContainers().remove(inContainer);
	}

	@Override
	public void fillOrderRandomly() {
		selectedOrder.fillOrderRandomly(selectedDomain, simulatedActor);
	}

	public SelectItem[] getListOfRoles() {
		return LAWSpecimenRole.getSpecimenRoleList();
	}

	@Override
	public List<SelectItem> getAvailableOrderStatuses() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(null, ResourceBundle.instance().getString("gazelle.ordermanager.PleaseSelect")));
		items.add(new SelectItem("A", ResourceBundle.instance().getString("gazelle.ordermanager.SomeResultsAvailable")));
		items.add(new SelectItem("CA", ResourceBundle.instance().getString("gazelle.ordermanager.OrderCancelled")));
		items.add(new SelectItem("CM", ResourceBundle.instance().getString("gazelle.ordermanager.OrderCompleted")));
		items.add(new SelectItem("IP", ResourceBundle.instance().getString("gazelle.ordermanager.OrderInProgressUnsp")));
		items.add(new SelectItem("SC", ResourceBundle.instance().getString("gazelle.ordermanager.OrderInProgressSch")));
		return items;
	}

	@Override
	public List<SelectItem> listAvailableActions() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(NEW, "Broadcast new AWOS"));
		items.add(new SelectItem(CANCEL, "Cancel AWOS"));
		return items;
	}

	public void validateParameter(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		final String fieldId = component.getId();
		final String fieldValue = (String) value;
		if ((fieldValue != null) && !fieldValue.isEmpty()) {
			if (getEIFields().contains(fieldId) && !fieldValue.matches(Container.regex_EI)) {
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Field shall be formatted wih respect to the EI datatype: " + Container.regex_EI, null));
			} else if ((fieldId.equals("sac11") || fieldId.equals("sac14")) && !fieldValue.matches(Container.regex_NA)) {
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Field shall be formatted wih respect to the NA datatype: " + Container.regex_NA, null));
			} else if (!fieldValue.matches(Container.regex_CE)) { // sac15
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Field shall be formatted wih respect to the CE datatype: " + Container.regex_CE, null));
			}
		}
	}

	private static List<String> getEIFields(){
        return Arrays.asList("sac3", "sac4", "sac10", "sac13");
    }

	/**
	 * GETTERS and SETTERS
	 */

	public LabOrder getSelectedOrder() {
		return selectedOrder;
	}

	public void setSelectedOrder(LabOrder selectedOrder) {
		this.selectedOrder = selectedOrder;
	}

	public Specimen getSelectedSpecimen() {
		return selectedSpecimen;
	}

	public void setSelectedSpecimen(Specimen selectedSpecimen) {
		this.selectedSpecimen = selectedSpecimen;
	}

	public Container getSelectedContainer() {
		return selectedContainer;
	}

	public void setSelectedContainer(Container selectedContainer) {
		this.selectedContainer = selectedContainer;
	}

	public Integer getNumberOfContainers() {
		return numberOfContainers;
	}

	public void setNumberOfContainers(Integer numberOfContainers) {
		this.numberOfContainers = numberOfContainers;
	}

	public String getSPECIMEN_CENTRIC() {
		return SPECIMEN_CENTRIC;
	}

	public boolean isEditOrder() {
		return editOrder;
	}

	public void setEditOrder(boolean editOrder) {
		this.editOrder = editOrder;
	}

	public boolean isEditSpecimen() {
		return editSpecimen;
	}

	public void setEditSpecimen(boolean editSpecimen) {
		this.editSpecimen = editSpecimen;
	}

	public void setExpendPanel(boolean expendPanel) {
		this.expendPanel = expendPanel;
	}

	public boolean isExpendPanel() {
		return expendPanel;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public List<LAWOptions> getAvailableOptionsForLAW() {
		return availableOptionsForLAW;
	}

	public List<LAWOptions> getSelectedOptions() {
		return selectedOptions;
	}

	public void setSelectedOptions(List<LAWOptions> selectedOptions) {
	    log.info("setting options");
        this.selectedOptions = selectedOptions;
	}

	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}
}
