package net.ihe.gazelle.ordermanager.mwl.scu;

import com.pixelmed.dicom.ApplicationEntityAttribute;
import com.pixelmed.dicom.Attribute;
import com.pixelmed.dicom.AttributeFactory;
import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.AttributeTag;
import com.pixelmed.dicom.CodeStringAttribute;
import com.pixelmed.dicom.DateAttribute;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.dicom.LongStringAttribute;
import com.pixelmed.dicom.PersonNameAttribute;
import com.pixelmed.dicom.SequenceAttribute;
import com.pixelmed.dicom.SequenceItem;
import com.pixelmed.dicom.ShortStringAttribute;
import com.pixelmed.dicom.TagFromName;
import com.pixelmed.dicom.TimeAttribute;
import com.pixelmed.dicom.UniqueIdentifierAttribute;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.dico.ModalityWorklistInformationModelDictionary;
import net.ihe.gazelle.ordermanager.model.Connection;
import net.ihe.gazelle.ordermanager.model.DicomMessage;
import net.ihe.gazelle.ordermanager.model.DicomMessageQuery;
import net.ihe.gazelle.ordermanager.model.SCPConfiguration;
import net.ihe.gazelle.ordermanager.model.SCPConfigurationQuery;
import net.ihe.gazelle.simulator.common.action.ApplicationConfigurationManagerLocal;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Name("worklistProviderTester")
@Scope(ScopeType.PAGE)
public class WorklistProviderTester extends ModalityWorklistInformationModelDictionary implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3694976697081031654L;

    private static final String TAG_PATIENT_NAME = TagFromName.PatientName.toString();
    private static final String TAG_PATIENT_ID = TagFromName.PatientID.toString();
    private static final String TAG_ACCESSION_NUMBER = TagFromName.AccessionNumber.toString();
    private static final String TAG_REQ_PROC_ID = TagFromName.RequestedProcedureID.toString();
    private static final String TAG_START_DATE = TagFromName.ScheduledProcedureStepStartDate.toString();
    private static final String TAG_MODALITY = TagFromName.Modality.toString();
    private static final String TAG_AE_TITLE = TagFromName.ScheduledStationAETitle.toString();

    private static final String QUERY_BY_PATIENT_KEYS = TAG_PATIENT_NAME + " "
            + TAG_PATIENT_ID + " " + TAG_ACCESSION_NUMBER + " "
            + TAG_REQ_PROC_ID;
    private static final String BROAD_WORLIST_QUERY_KEYS = TAG_START_DATE + " "
            + TAG_MODALITY + " " + TAG_AE_TITLE;
    private static final String ACTIVITY_SUPPLIER_QUERY = TAG_START_DATE + " " + TAG_MODALITY;

    private static Logger log = LoggerFactory.getLogger(WorklistProviderTester.class);

    private List<String> listOfTagsByName;
    private SCPConfiguration selectedSUT;
    private AttributeList attributes;
    private String attributeTag;
    private String attributeTagName;
    private String attributeValue;
    private SequenceAttribute selectedSequence;
    private Connection connectionId;
    private String keysToHighlight = null;

    public WorklistProviderTester() {
        super();
    }

    public List<SCPConfiguration> getAvailableSUT() {
        SCPConfigurationQuery query = new SCPConfigurationQuery();
        query.isAvailable().eq(true);
        query.listUsages().transaction().keyword().eq("RAD-5");
        ApplicationConfigurationManagerLocal manager = (ApplicationConfigurationManagerLocal) Component
                .getInstance("applicationConfigurationManager");
        if (!manager.isUserAllowedAsAdmin()) {
            if (Identity.instance().isLoggedIn()) {
                query.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("isPublic", true),
                        HQLRestrictions.eq("creator", Identity.instance().getCredentials().getUsername())));
            } else {
                query.isPublic().eq(true);
            }
        }
        return query.getList();
    }

    @Create
    public void initializeAttributeList() {
        HashMap<AttributeTag, Attribute> requiredAttributeTags = new HashMap<AttributeTag, Attribute>();

        // ScheduledProcedureStepSequence
        SequenceAttribute sequence = new SequenceAttribute(TagFromName.ScheduledProcedureStepSequence);
        Map<AttributeTag, Attribute> sequenceItems = new HashMap<AttributeTag, Attribute>();
        sequenceItems.put(TagFromName.ScheduledStationAETitle, new ApplicationEntityAttribute(
                TagFromName.ScheduledStationAETitle));
        sequenceItems.put(TagFromName.ScheduledProcedureStepStartDate, new DateAttribute(
                TagFromName.ScheduledProcedureStepStartDate));
        sequenceItems.put(TagFromName.ScheduledProcedureStepStartTime, new TimeAttribute(
                TagFromName.ScheduledProcedureStepStartTime));
        sequenceItems.put(TagFromName.Modality, new CodeStringAttribute(TagFromName.Modality));
        sequenceItems.put(TagFromName.ScheduledProcedureStepID, new ShortStringAttribute(
                TagFromName.ScheduledProcedureStepID));
        // ScheduledProtocolCodeSequence
        SequenceAttribute sequence2 = new SequenceAttribute(TagFromName.ScheduledProtocolCodeSequence);
        Map<AttributeTag, Attribute> sequence2Items = new HashMap<AttributeTag, Attribute>();
        sequence2Items.put(TagFromName.CodeValue, new ShortStringAttribute(TagFromName.CodeValue));
        sequence2Items.put(TagFromName.CodingSchemeDesignator, new ShortStringAttribute(
                TagFromName.CodingSchemeDesignator));
        sequence2Items.put(TagFromName.CodeMeaning, new LongStringAttribute(TagFromName.CodeMeaning));
        AttributeList sequence2Attributes = new AttributeList();
        sequence2Attributes.putAll(sequence2Items);
        sequence2.addItem(sequence2Attributes);
        sequenceItems.put(TagFromName.ScheduledProtocolCodeSequence, sequence2);
        // end sub sequence
        sequenceItems.put(TagFromName.ScheduledProcedureStepDescription, new LongStringAttribute(
                TagFromName.ScheduledProcedureStepDescription));
        AttributeList sequenceAttributes = new AttributeList();
        sequenceAttributes.putAll(sequenceItems);
        sequence.addItem(sequenceAttributes);
        requiredAttributeTags.put(TagFromName.ScheduledProcedureStepSequence, sequence);
        // end sequence
        requiredAttributeTags.put(TagFromName.RequestedProcedureDescription, new LongStringAttribute(
                TagFromName.RequestedProcedureDescription));
        requiredAttributeTags.put(TagFromName.RequestedProcedureID, new ShortStringAttribute(
                TagFromName.RequestedProcedureID));
        requiredAttributeTags.put(TagFromName.StudyInstanceUID, new UniqueIdentifierAttribute(
                TagFromName.StudyInstanceUID));
        requiredAttributeTags.put(TagFromName.AccessionNumber, new ShortStringAttribute(TagFromName.AccessionNumber));
        requiredAttributeTags.put(TagFromName.ReferringPhysicianName, new PersonNameAttribute(
                TagFromName.ReferringPhysicianName));
        requiredAttributeTags.put(TagFromName.PatientName, new PersonNameAttribute(TagFromName.PatientName));
        requiredAttributeTags.put(TagFromName.PatientID, new LongStringAttribute(TagFromName.PatientID));
        requiredAttributeTags.put(TagFromName.PatientBirthDate, new DateAttribute(TagFromName.PatientBirthDate));
        requiredAttributeTags.put(TagFromName.PatientSex, new CodeStringAttribute(TagFromName.PatientSex));

        attributes = new AttributeList();
        attributes.putAll(requiredAttributeTags);

        // initialize list of tags
        Set<String> tags = tagByName.keySet();
        String[] arrayOfTags = tags.toArray(new String[tags.size()]);
        listOfTagsByName = Arrays.asList(arrayOfTags);
        Collections.sort(listOfTagsByName);
    }

    public GazelleTreeNodeImpl<Object> getQueryAsTree() {
        GazelleTreeNodeImpl<Object> treeRoot = new GazelleTreeNodeImpl<Object>();
        int index = 0;
        for (Attribute attribute : attributes.values()) {
            GazelleTreeNodeImpl<Object> node = new GazelleTreeNodeImpl<Object>();
            if (attribute instanceof SequenceAttribute) {
                SequenceAttribute sequence = (SequenceAttribute) attribute;
                treeRoot.addChild(index, processSequence(sequence));
            } else {
                node.setData(attribute);
                treeRoot.addChild(index, node);
            }
            index++;
        }
        return treeRoot;
    }

    private GazelleTreeNodeImpl<Object> processSequence(SequenceAttribute sequence) {
        GazelleTreeNodeImpl<Object> sequenceNode = new GazelleTreeNodeImpl<Object>();
        sequenceNode.setData(sequence);
        int index = 0;
        for (int i = 0; i < sequence.getNumberOfItems(); i++) {
            SequenceItem item = sequence.getItem(i);
            for (Attribute itemAttribute : item.getAttributeList().values()) {
                GazelleTreeNodeImpl<Object> itemNode = new GazelleTreeNodeImpl<Object>();
                if (itemAttribute instanceof SequenceAttribute) {
                    sequenceNode.addChild(index, processSequence((SequenceAttribute) itemAttribute));
                } else {
                    itemNode.setData(itemAttribute);
                    sequenceNode.addChild(index, itemNode);
                }
                index++;
            }
        }
        return sequenceNode;
    }

    public void addAnAttribute() {
        try {
            Attribute newAttribute = buildAttributeFromTagName();
            attributes.put(newAttribute.getTag(), newAttribute);
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Attribute successfully added to the query");
        } catch (DicomException e) {
            FacesMessages.instance().add(attributeTag + " is not a valid DICOM tag !");
            log.info(e.getMessage(), e);
        }
        attributeTag = null;
    }

    public void addAnAttributeByTagName() {
        if ((attributeTagName != null) && tagByName.containsKey(attributeTagName)) {
            AttributeTag selectedTag = (AttributeTag) tagByName.get(attributeTagName);
            try {
                attributes.put(selectedTag, AttributeFactory.newAttribute(selectedTag));
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Attribute successfully added to the query");
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Cannot create attribute for tag with name " + attributeTagName + ": " + e.getMessage());
                log.error(e.getMessage(), e);
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Selected tag is either null either unknown from the dictionary");
        }
        attributeTagName = null;
    }

    public void addAnAttributeByTagNameToSequence() {
        if ((attributeTagName != null) && tagByName.containsKey(attributeTagName)) {
            AttributeTag selectedTag = (AttributeTag) tagByName.get(attributeTagName);
            try {
                AttributeList attributeForSequence = new AttributeList();
                attributeForSequence.put(selectedTag, AttributeFactory.newAttribute(selectedTag));
                selectedSequence.addItem(attributeForSequence);
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Attribute successfully added to the query");
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Cannot create attribute for tag with name " + attributeTagName + ": " + e.getMessage());
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Selected tag is either null either unknown from the dictionary");
        }
        attributeTagName = null;
        selectedSequence = null;
    }

    private Attribute buildAttributeFromTagName() throws DicomException {
        if ((attributeTag != null) && !attributeTag.isEmpty()) {
            AttributeTag tag = new AttributeTag(attributeTag);
            Attribute newAttribute = AttributeFactory.newAttribute(tag);
            return newAttribute;
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "given tag is null or empty, cannot create attribute");
        }
        return null;
    }

    public void addAnAttributeToSequence() {
        try {
            Attribute newAttribute = buildAttributeFromTagName();
            if (newAttribute != null) {
                AttributeList newAttributeList = new AttributeList();
                newAttributeList.put(newAttribute.getTag(), newAttribute);
                selectedSequence.addItem(newAttributeList);
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Attribute successfully added to the sequence");
            }
        } catch (DicomException e) {
            String selectedSequenceTag = selectedSequence.getTag().toString();
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Unable to add tag " + attributeTag + " to sequence " + selectedSequenceTag + ": "
                            + e.getMessage());
        }
        attributeTag = null;
        selectedSequence = null;
        attributeTagName = null;
    }

    public void setAttributeValue(Attribute attribute) {
        if ((attributeValue != null) && (attribute != null)) {
            try {
                attribute.addValue(attributeValue);
            } catch (DicomException e) {
                String attributeTag = attribute.getTag().toString();
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Cannot set value for tag " + attributeTag + ": " + e.getMessage());
                log.error(e.getMessage(), e);
            }
            attributeValue = null;
        }
    }

    public void removeAttribute(Attribute inAttribute) {
        if (inAttribute != null) {
            Attribute attributeFromList = attributes.get(inAttribute.getTag());
            if ((attributeFromList != null) && (attributeFromList.equals(inAttribute))) {
                attributes.remove(inAttribute.getTag());
            } else {
                for (AttributeTag tag : attributes.keySet()) {
                    if ((attributes.get(tag) instanceof SequenceAttribute)
                            && removeAttributeFromSequence((SequenceAttribute) attributes.get(tag), inAttribute)) {
                        break;
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    private boolean removeAttributeFromSequence(SequenceAttribute sequenceAttribute, Attribute inAttribute) {
        for (int i = 0; i < sequenceAttribute.getNumberOfItems(); i++) {
            SequenceItem item = sequenceAttribute.getItem(i);
            Attribute attributeFromList = item.getAttributeList().get(inAttribute.getTag());
            if ((attributeFromList != null) && (attributeFromList.equals(inAttribute))) {
                item.getAttributeList().remove(inAttribute.getTag());
                return true;
            } else {
                for (AttributeTag tag : item.getAttributeList().keySet()) {
                    if ((item.getAttributeList().get(tag) instanceof SequenceAttribute)
                            && removeAttributeFromSequence((SequenceAttribute) attributes.get(tag), inAttribute)) {
                        return true;
                    } else {
                        continue;
                    }
                }
            }
        }
        return false;
    }

    public void emptyAttribute(Attribute inAttribute) {
        if (inAttribute != null) {
            try {
                inAttribute.removeValues();
            } catch (DicomException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to remove values from attribute: " + e.getMessage());
            }
        }
    }

    public void executeQuery() {
        if (selectedSUT == null) {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "Please, first select a system under test and retry");
            return;
        } else {
            try {
                FindModalityWorklistSCU scu = new FindModalityWorklistSCU(selectedSUT, attributes);
                connectionId = scu.send();
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Exchanged messages are displayed below");
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
            }
        }
    }

    public List<SelectItem> getHighlightModes() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        items.add(new SelectItem(null, "None"));
        items.add(new SelectItem(QUERY_BY_PATIENT_KEYS, "Keys for Query By Patient"));
        items.add(new SelectItem(BROAD_WORLIST_QUERY_KEYS, "Keys for Broad Worklist Queries"));
        items.add(new SelectItem(ACTIVITY_SUPPLIER_QUERY, "Keys for Radiopharmaceutical Activity Supplier Query"));
        return items;
    }

    public List<DicomMessage> getDicomMessages() {
        DicomMessageQuery query = new DicomMessageQuery();
        query.connection().id().eq(connectionId.getId());
        query.timestamp().order(true);
        return query.getListDistinct();
    }

    public String getStyleClassForNode(AttributeTag tag) {
        if ((keysToHighlight != null) && !keysToHighlight.isEmpty() && (tag != null)
                && keysToHighlight.contains(tag.toString())) {
            return "highlightedAttribute";
        } else {
            return "basicAttribute";
        }
    }

    public void performAnotherTest() {
        connectionId = null;
    }

    public SCPConfiguration getSelectedSUT() {
        return selectedSUT;
    }

    public void setSelectedSUT(SCPConfiguration selectedSUT) {
        this.selectedSUT = selectedSUT;
    }

    public AttributeList getAttributes() {
        return attributes;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public String getAttributeTag() {
        return attributeTag;
    }

    public void setAttributeTag(String attributeTag) {
        this.attributeTag = attributeTag;
    }

    public SequenceAttribute getSelectedSequence() {
        return selectedSequence;
    }

    public void setSelectedSequence(SequenceAttribute selectedSequence) {
        this.selectedSequence = selectedSequence;
        if (selectedSequence != null) {
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Use the right-hand panel to select the tag to add to this sequence");
        }
    }

    public List<String> getListOfTagsByName() {
        return listOfTagsByName;
    }

    public String getAttributeTagName() {
        return attributeTagName;
    }

    public void setAttributeTagName(String attributeTagName) {
        this.attributeTagName = attributeTagName;
    }

    public Connection getConnectionId() {
        return connectionId;
    }

    public String getKeysToHighlight() {
        return keysToHighlight;
    }

    public void setKeysToHighlight(String keysToHighlight) {
        this.keysToHighlight = keysToHighlight;
    }

    public String getQUERY_BY_PATIENT_KEYS() {
        return QUERY_BY_PATIENT_KEYS;
    }

    public String getBROAD_WORLIST_QUERY_KEYS() {
        return BROAD_WORLIST_QUERY_KEYS;
    }

}
