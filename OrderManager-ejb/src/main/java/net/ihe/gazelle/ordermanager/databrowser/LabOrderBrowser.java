package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.LabOrderQuery;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.util.Map;

@Name("labOrderBrowser")
@Scope(ScopeType.PAGE)
public class LabOrderBrowser extends DataBrowser<LabOrder> implements UserAttributeCommon {

	private static final long serialVersionUID = -5844151780076105391L;
    private Integer selectedOrderId = null;

	@In(value="gumUserService")
	private UserService userService;

	@Override
	public void modifyQuery(HQLQueryBuilder<LabOrder> queryBuilder, Map<String, Object> map) {
		if ((fillerOrderNumber != null) && !fillerOrderNumber.isEmpty()) {
			queryBuilder.addLike("fillerOrderNumber", fillerOrderNumber);
		}
		if ((placerOrderNumber != null) && !placerOrderNumber.isEmpty()) {
			queryBuilder.addLike("placerOrderNumber", placerOrderNumber);
		}
		if ((patientId != null) && !patientId.isEmpty()) {
			queryBuilder.addLike("encounter.patient.patientIdentifiers.identifier", patientId);
		}
		if ((visitNumber != null) && !visitNumber.isEmpty()) {
			queryBuilder.addLike("encounter.visitNumber", visitNumber);
		}
	}

    @Override
    public Filter<LabOrder> getFilter() {
        if (filter == null){
            filter = new Filter<LabOrder>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return filter;
    }

    @Override
	protected HQLCriterionsForFilter<LabOrder> getHQLCriterionsForFilter() {
		LabOrderQuery query = new LabOrderQuery();
		HQLCriterionsForFilter<LabOrder> criterions = query.getHQLCriterionsForFilter();
		criterions.addPath("actor", query.simulatedActor(), actor);
		criterions.addPath("domain", query.domain(), domain);
		criterions.addPath("lastChanged", query.lastChanged());
		criterions.addPath("creator", query.creator());
		criterions.addPath("mainEntity", query.mainEntity(), true);
		if (objectType.equals(ObjectType.LABORDER)) {
			criterions.addPath("isWorkOrder", query.workOrder(), false);
		} else if (objectType.equals(ObjectType.WORKORDER)) {
			criterions.addPath("isWorkOrder", query.workOrder(), true);
		}
		criterions.addQueryModifier(this);
		return criterions;
	}

	@Override
	public String getPermanentLink(Integer objectId) {
		return "/browsing/labOrder.seam?domain=LAB&id=" + objectId;
	}

    @Override
    public FilterDataModel<LabOrder> getDataModel() {
        return new FilterDataModel<LabOrder>(getFilter()) {
            @Override
            protected Object getId(LabOrder labOrder) {
                return labOrder.getId();
            }
        };
    }

    public String getSendResultTitle() {
		if (actor.getKeyword().equals("OF")) {
			return "Send results to ORT";
		} else if (actor.getKeyword().equals("AM")) {
			return "Send results to OF";
		} else {
			return null;
		}
	}

	public String sendResults(Integer orderId) {
		if (objectType.equals(ObjectType.WORKORDER)) {
			return "/hl7Initiators/orderResultSender.seam?domain=LAB&actor=AM&labOrderId=" + orderId;
		} else {
			return "/hl7Initiators/orderResultSender.seam?domain=LAB&actor=OF&labOrderId=" + orderId;
		}
	}

	public String createWorkOrder(Integer orderId) {
		return "/hl7Initiators/workOrderSender.seam?domain=LAB&orderId=" + orderId;
	}

    public Integer getSelectedOrderId() {
        return selectedOrderId;
    }

    public void setSelectedOrderId(Integer selectedOrderId) {
        this.selectedOrderId = selectedOrderId;
    }

	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}
}
