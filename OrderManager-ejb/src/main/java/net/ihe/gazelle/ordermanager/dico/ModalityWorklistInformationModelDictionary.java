package net.ihe.gazelle.ordermanager.dico;

import com.pixelmed.dicom.AttributeTag;
import com.pixelmed.dicom.DicomDictionary;
import com.pixelmed.dicom.TagFromName;

import java.util.HashMap;
import java.util.TreeSet;

/**
 * This class defines a Dicom Dictionary which defines all the DICOM attribute which can appear in a Modality Worklist Information Model object
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ModalityWorklistInformationModelDictionary extends DicomDictionary {

	/**
	 * <p>Constructor for ModalityWorklistInformationModelDictionary.</p>
	 */
	public ModalityWorklistInformationModelDictionary() {
		super();
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * Concrete sub-classes implement this method to create a list of all tags in the dictionary.
	 * </p>
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void createTagList() {
		tagList = new TreeSet<AttributeTag>(); // sorted, based on AttributeTag's implementation of Comparable

		tagList.add(TagFromName.ScheduledProcedureStepSequence);
		tagList.add(TagFromName.ScheduledStationAETitle);
		tagList.add(TagFromName.ScheduledProcedureStepStartDate);
		tagList.add(TagFromName.ScheduledProcedureStepStartTime);
		tagList.add(TagFromName.Modality);
		tagList.add(TagFromName.ScheduledPerformingPhysicianName);
		tagList.add(TagFromName.ScheduledProcedureStepDescription);
		tagList.add(TagFromName.ScheduledStationName);
		tagList.add(TagFromName.ScheduledProcedureStepLocation);
		tagList.add(TagFromName.ScheduledProtocolCodeSequence);
		tagList.add(TagFromName.CodeValue);
		tagList.add(TagFromName.CodingSchemeVersion);
		tagList.add(TagFromName.CodingSchemeDesignator);
		tagList.add(TagFromName.CodeMeaning);
		tagList.add(TagFromName.ProtocolContextSequence);
		tagList.add(TagFromName.ValueType);
		tagList.add(TagFromName.ConceptNameCodeSequence);
		tagList.add(TagFromName.DateTime);
		tagList.add(TagFromName.PersonName);
		tagList.add(TagFromName.TextValue);
		tagList.add(TagFromName.ConceptCodeSequence);
		tagList.add(TagFromName.NumericValue);
		tagList.add(TagFromName.MeasurementUnitsCodeSequence);
		tagList.add(TagFromName.PreMedication);
		tagList.add(TagFromName.ScheduledProcedureStepID);
		tagList.add(TagFromName.RequestedContrastAgent);
		tagList.add(TagFromName.ScheduledProcedureStepStatus);
		tagList.add(TagFromName.ScheduledSpecimenSequence);
		tagList.add(TagFromName.ContainerIdentifier);
		tagList.add(TagFromName.ContainerTypeCodeSequence);
		tagList.add(TagFromName.SpecimenDescriptionSequence);
		tagList.add(TagFromName.SpecimenIdentifier);
		tagList.add(TagFromName.SpecimenUID);
		tagList.add(TagFromName.SpecimenPreparationSequence);
		tagList.add(TagFromName.RequestedProcedureID);
		tagList.add(TagFromName.RequestedProcedureDescription);
		tagList.add(TagFromName.RequestedProcedureCodeSequence);
		tagList.add(TagFromName.StudyInstanceUID);
		tagList.add(TagFromName.StudyDate);
		tagList.add(TagFromName.StudyTime);
		tagList.add(TagFromName.ReferencedStudySequence);
		tagList.add(TagFromName.ReferencedSOPClassUID);
		tagList.add(TagFromName.ReferencedSOPInstanceUID);
		tagList.add(TagFromName.RequestedProcedurePriority);
		tagList.add(TagFromName.PatientTransportArrangements);
		tagList.add(TagFromName.AccessionNumber);
		tagList.add(TagFromName.RequestingPhysician);
		tagList.add(TagFromName.ReferringPhysicianName);
		tagList.add(TagFromName.AdmissionID);
		tagList.add(TagFromName.CurrentPatientLocation);
		tagList.add(TagFromName.ReferencedPatientSequence);
		tagList.add(TagFromName.PatientName);
		tagList.add(TagFromName.PatientID);
		tagList.add(TagFromName.PatientBirthDate);
		tagList.add(TagFromName.PatientSex);
		tagList.add(TagFromName.PatientPrimaryLanguageCodeSequence);
		tagList.add(TagFromName.PatientPrimaryLanguageModifierCodeSequence);
		tagList.add(TagFromName.PatientWeight);
		tagList.add(TagFromName.ConfidentialityConstraintOnPatientDataDescription);
		tagList.add(TagFromName.PatientState);
		tagList.add(TagFromName.PregnancyStatus);
		tagList.add(TagFromName.MedicalAlerts);
		tagList.add(TagFromName.Allergies);
		tagList.add(TagFromName.SpecialNeeds);
		tagList.add(TagFromName.PertinentDocumentsSequence);
		tagList.add(TagFromName.PurposeOfReferenceCodeSequence);
		tagList.add(TagFromName.DocumentTitle);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * Concrete sub-classes implement this method to create a map of tags from attribute names for each tag in the dictionary.
	 * </p>
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void createTagByName() {
		tagByName = new HashMap<String, AttributeTag>();

		tagByName.put("ScheduledProcedureStepSequence", TagFromName.ScheduledProcedureStepSequence);
		tagByName.put("ScheduledStationAETitle", TagFromName.ScheduledStationAETitle);
		tagByName.put("ScheduledProcedureStepStartDate", TagFromName.ScheduledProcedureStepStartDate);
		tagByName.put("ScheduledProcedureStepStartTime", TagFromName.ScheduledProcedureStepStartTime);
		tagByName.put("Modality", TagFromName.Modality);
		tagByName.put("ScheduledPerformingPhysicianName", TagFromName.ScheduledPerformingPhysicianName);
		tagByName.put("ScheduledProcedureStepDescription", TagFromName.ScheduledProcedureStepDescription);
		tagByName.put("ScheduledStationName", TagFromName.ScheduledStationName);
		tagByName.put("ScheduledProcedureStepLocation", TagFromName.ScheduledProcedureStepLocation);
		tagByName.put("ScheduledProtocolCodeSequence", TagFromName.ScheduledProtocolCodeSequence);
		tagByName.put("CodeValue", TagFromName.CodeValue);
		tagByName.put("CodingSchemeVersion", TagFromName.CodingSchemeVersion);
		tagByName.put("CodingSchemeDesignator", TagFromName.CodingSchemeDesignator);
		tagByName.put("CodeMeaning", TagFromName.CodeMeaning);
		tagByName.put("ProtocolContextSequence", TagFromName.ProtocolContextSequence);
		tagByName.put("ValueType", TagFromName.ValueType);
		tagByName.put("ConceptNameCodeSequence", TagFromName.ConceptNameCodeSequence);
		tagByName.put("DateTime", TagFromName.DateTime);
		tagByName.put("PersonName", TagFromName.PersonName);
		tagByName.put("TextValue", TagFromName.TextValue);
		tagByName.put("ConceptCodeSequence", TagFromName.ConceptCodeSequence);
		tagByName.put("NumericValue", TagFromName.NumericValue);
		tagByName.put("MeasurementUnitsCodeSequence", TagFromName.MeasurementUnitsCodeSequence);
		tagByName.put("PreMedication", TagFromName.PreMedication);
		tagByName.put("ScheduledProcedureStepID", TagFromName.ScheduledProcedureStepID);
		tagByName.put("RequestedContrastAgent", TagFromName.RequestedContrastAgent);
		tagByName.put("ScheduledProcedureStepStatus", TagFromName.ScheduledProcedureStepStatus);
		tagByName.put("ScheduledSpecimenSequence", TagFromName.ScheduledSpecimenSequence);
		tagByName.put("ContainerIdentifier", TagFromName.ContainerIdentifier);
		tagByName.put("ContainerTypeCodeSequence", TagFromName.ContainerTypeCodeSequence);
		tagByName.put("SpecimenDescriptionSequence", TagFromName.SpecimenDescriptionSequence);
		tagByName.put("SpecimenIdentifier", TagFromName.SpecimenIdentifier);
		tagByName.put("SpecimenUID", TagFromName.SpecimenUID);
		tagByName.put("SpecimenPreparationSequence", TagFromName.SpecimenPreparationSequence);
		tagByName.put("RequestedProcedureID", TagFromName.RequestedProcedureID);
		tagByName.put("RequestedProcedureDescription", TagFromName.RequestedProcedureDescription);
		tagByName.put("RequestedProcedureCodeSequence", TagFromName.RequestedProcedureCodeSequence);
		tagByName.put("StudyInstanceUID", TagFromName.StudyInstanceUID);
		tagByName.put("StudyDate", TagFromName.StudyDate);
		tagByName.put("StudyTime", TagFromName.StudyTime);
		tagByName.put("ReferencedStudySequence", TagFromName.ReferencedStudySequence);
		tagByName.put("ReferencedSOPClassUID", TagFromName.ReferencedSOPClassUID);
		tagByName.put("ReferencedSOPInstanceUID", TagFromName.ReferencedSOPInstanceUID);
		tagByName.put("RequestedProcedurePriority", TagFromName.RequestedProcedurePriority);
		tagByName.put("PatientTransportArrangements", TagFromName.PatientTransportArrangements);
		tagByName.put("AccessionNumber", TagFromName.AccessionNumber);
		tagByName.put("RequestingPhysician", TagFromName.RequestingPhysician);
		tagByName.put("ReferringPhysicianName", TagFromName.ReferringPhysicianName);
		tagByName.put("AdmissionID", TagFromName.AdmissionID);
		tagByName.put("CurrentPatientLocation", TagFromName.CurrentPatientLocation);
		tagByName.put("ReferencedPatientSequence", TagFromName.ReferencedPatientSequence);
		tagByName.put("PatientName", TagFromName.PatientName);
		tagByName.put("PatientID", TagFromName.PatientID);
		tagByName.put("PatientBirthDate", TagFromName.PatientBirthDate);
		tagByName.put("PatientSex", TagFromName.PatientSex);
		tagByName.put("PatientPrimaryLanguageCodeSequence", TagFromName.PatientPrimaryLanguageCodeSequence);
		tagByName.put("PatientPrimaryLanguageModifierCodeSequence",
				TagFromName.PatientPrimaryLanguageModifierCodeSequence);
		tagByName.put("PatientWeight", TagFromName.PatientWeight);
		tagByName.put("ConfidentialityConstraintOnPatientDataDescription",
				TagFromName.ConfidentialityConstraintOnPatientDataDescription);
		tagByName.put("PatientState", TagFromName.PatientState);
		tagByName.put("PregnancyStatus", TagFromName.PregnancyStatus);
		tagByName.put("MedicalAlerts", TagFromName.MedicalAlerts);
		tagByName.put("Allergies", TagFromName.Allergies);
		tagByName.put("SpecialNeeds", TagFromName.SpecialNeeds);
		tagByName.put("PertinentDocumentsSequence", TagFromName.PertinentDocumentsSequence);
		tagByName.put("PurposeOfReferenceCodeSequence", TagFromName.PurposeOfReferenceCodeSequence);
		tagByName.put("DocumentTitle", TagFromName.DocumentTitle);
	}
}
