package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.Connection;
import net.ihe.gazelle.ordermanager.model.DicomMessage;

import java.util.Date;

/**
 * Class description : <b>DicomMessageDataModel</b>
 * <p>
 * This class is a data model for properly and quickly displaying the DicomMessage objects in the GUI
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 * @version 1.0 - 2011, November 25th
 */
public class DicomMessageDataModel extends FilterDataModel<DicomMessage> {

    /**
     *
     */
    private static final long serialVersionUID = -718059090406031370L;
    private Date startDate;
    private Date endDate;
    private Connection connectionId;
    private Integer currentMessageId;

    /**
     * <p>Constructor for DicomMessageDataModel.</p>
     */
    public DicomMessageDataModel() {
        super(new DicomMessageFilter());
    }

    /**
     * <p>Constructor for DicomMessageDataModel.</p>
     *
     * @param connectionId a {@link net.ihe.gazelle.ordermanager.model.Connection} object.
     * @param currentMessageId a {@link java.lang.Integer} object.
     */
    public DicomMessageDataModel(Connection connectionId, Integer currentMessageId) {
        super(new DicomMessageFilter());
        this.startDate = null;
        this.endDate = null;
        this.connectionId = connectionId;
        this.currentMessageId = currentMessageId;
    }

    /**
     * <p>Constructor for DicomMessageDataModel.</p>
     *
     * @param startDate a {@link java.util.Date} object.
     * @param endDate a {@link java.util.Date} object.
     * @param patientId a {@link java.lang.String} object.
     */
    public DicomMessageDataModel(Date startDate, Date endDate, String patientId) {
        super(new DicomMessageFilter());
        setStartDate(startDate);
        setEndDate(endDate);
        this.connectionId = null;
        this.currentMessageId = null;
    }

    /** {@inheritDoc} */
    @Override
    public void appendFiltersFields(HQLQueryBuilder<DicomMessage> hqlBuilder) {
        if (startDate != null) {
            hqlBuilder.addRestriction(HQLRestrictions.gt("timestamp", startDate));
        }
        if (endDate != null) {
            hqlBuilder.addRestriction(HQLRestrictions.lt("timestamp", endDate));
        }
        if (connectionId != null) {
            hqlBuilder.addEq("connection", connectionId);
        }
        if (currentMessageId != null) {
            hqlBuilder.addRestriction(HQLRestrictions.neq("id", currentMessageId));
        }
    }

    /**
     * <p>Getter for the field <code>startDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getStartDate() {
        if (this.startDate != null) {
            return (Date) startDate.clone();
        } else {
            return null;
        }
    }

    /**
     * <p>Setter for the field <code>startDate</code>.</p>
     *
     * @param startDate a {@link java.util.Date} object.
     */
    public final void setStartDate(Date startDate) {
        if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        } else {
            this.startDate = null;
        }
    }

    /**
     * <p>Getter for the field <code>endDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getEndDate() {
        if (this.endDate != null) {
            return (Date) endDate.clone();
        } else {
            return null;
        }
    }

    /**
     * <p>Setter for the field <code>endDate</code>.</p>
     *
     * @param endDate a {@link java.util.Date} object.
     */
    public final void setEndDate(Date endDate) {
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        } else {
            this.endDate = null;
        }
    }

    /**
     * <p>Setter for the field <code>connectionId</code>.</p>
     *
     * @param connectionId a {@link net.ihe.gazelle.ordermanager.model.Connection} object.
     */
    public void setConnectionId(Connection connectionId) {
        this.connectionId = connectionId;
    }

    /**
     * <p>Getter for the field <code>connectionId</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.ordermanager.model.Connection} object.
     */
    public Connection getConnectionId() {
        return connectionId;
    }

    /**
     * <p>Setter for the field <code>currentMessageId</code>.</p>
     *
     * @param currentMessageId a {@link java.lang.Integer} object.
     */
    public void setCurrentMessageId(Integer currentMessageId) {
        this.currentMessageId = currentMessageId;
    }

    /**
     * <p>Getter for the field <code>currentMessageId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getCurrentMessageId() {
        return currentMessageId;
    }

    /** {@inheritDoc} */
    @Override
    protected Object getId(DicomMessage t) {
        // TODO Auto-generated method stub
        return t.getId();
    }
}
