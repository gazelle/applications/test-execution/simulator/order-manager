package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.v25.group.ORU_R01_ORDER_OBSERVATION;
import ca.uhn.hl7v2.model.v25.message.ORU_R01;
import ca.uhn.hl7v2.model.v25.message.OUL_R22;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Observation;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class OrderResultTracker extends OrderReceiver {

    private static Log log = Logging.getLog(OrderResultTracker.class);

    private static final String R01 = "R01";


    @Override
    public IHEDefaultHandler newInstance() {
        return new OrderResultTracker();
    }

    @Override
    public Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception {
        Message response;
        orderControlCode = null;
        try {
            // check if we must accept the message
            response = Receiver.generateNack(incomingMessage, serverApplication, serverFacility,
                    Arrays.asList("ORU", "OUL"), Arrays.asList(R01, "R22"), hl7Charset);

            // if no NACK is generated, process the message
            if (response == null) {
                if (messageType == null) {
                    log.error("Message type is empty");
                } else if (messageType.contains("ORU")) {
                    // process message ORU^R01^ORU_R01
                    response = processORUMessage(incomingMessage);
                } else if (messageType.contains("OUL")) {
                    // process message for LAB
                    response = processOULR22Message((OUL_R22) incomingMessage);
                } else {
                    log.error("This kind of message is not supported, we should not reach this line !");
                }
            }
            setSutActor(Actor.findActorWithKeyword("OF", entityManager));
            setResponseMessageType(HL7MessageDecoder.getMessageType(response));
        } catch (HL7Exception e) {
            throw new HL7Exception(e);
        } catch (Exception e) {
            throw new ReceivingApplicationException(e);
        }
        return response;
    }

    /**
     * @param incomingMessage
     * @return
     */
    private Message processORUMessage(Message incomingMessage) {
        if (incomingMessage instanceof ORU_R01) {
            ORU_R01 oruMessage = (ORU_R01) incomingMessage;
            Patient selectedPatient = null;
            String creator = sendingApplication + "_" + sendingFacility;
            Domain domain = Domain.getDomainByKeyword("LAB", entityManager);
            // extract patient
            if (!MessageDecoder.isEmpty(oruMessage.getPATIENT_RESULT().getPATIENT().getPID())) {
                Patient pidPatient = MessageDecoder.extractPatientFromPID(oruMessage.getPATIENT_RESULT().getPATIENT().getPID());
                List<PatientIdentifier> pidPatientIdentifiers;
                pidPatientIdentifiers = MessageDecoder.extractPatientIdentifiersFromMessage25(oruMessage);
                List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
                for (PatientIdentifier pid : pidPatientIdentifiers) {
                    PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
                    if (existingPID != null) {
                        if (selectedPatient == null) {
                            selectedPatient = existingPID.getPatient();
                        } else if (selectedPatient.getId().equals(existingPID.getPatient().getId())) {
                            // OK: nothing to do
                            continue;
                        } else {
                            // error patients with same identifiers mismatch
                            // TODO create a good ACK
                            return Receiver.buildNack(serverApplication, serverFacility, sendingApplication,
                                    sendingFacility, R01, AcknowledgmentCode.AE, "The patient identifiers are inconsistent", null,
                                    Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
                                    hl7Charset, hl7Version);
                        }
                    } else {
                        pid = pid.save(entityManager);
                        identifiersToAdd.add(pid);
                    }
                }
                // if none of the identifiers is already known by the simulator, we save the patient contained in the message
                if (selectedPatient == null) {
                    // save the patient
                    pidPatient.setCreator(creator);
                    selectedPatient = pidPatient.save(entityManager);
                }
                for (PatientIdentifier pid : identifiersToAdd) {
                    pid.setPatient(selectedPatient);
                    pid.save(entityManager);
                }
            }
            // then, extract encounter
            Encounter selectedEncounter = null;
            if ((selectedPatient != null)
                    && !MessageDecoder.isEmpty(oruMessage.getPATIENT_RESULT().getPATIENT().getVISIT().getPV1())) {
                // check existance of PV1 segment
                Encounter pv1Encounter = null;
                pv1Encounter = MessageDecoder.extractEncounterFromPV1(oruMessage);
                if (pv1Encounter.getVisitNumber() != null) {
                    // check the encounter exists
                    selectedEncounter = Encounter.getEncounterByVisitNumber(pv1Encounter.getVisitNumber(),
                            entityManager);
                    if ((selectedEncounter != null)
                            && (!selectedEncounter.getPatient().getId().equals(selectedPatient.getId()))) {
                        return Receiver.buildNack(serverApplication, serverFacility, sendingApplication,
                                sendingFacility, R01, AcknowledgmentCode.AE, "This visit number is already used for another patient",
                                null, Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
                                hl7Charset, hl7Version);
                    }
                    // if not rattach the encounter to the selected patient
                    else if (selectedEncounter == null) {
                        selectedEncounter = pv1Encounter;
                        selectedEncounter.setPatient(selectedPatient);
                        selectedEncounter.setCreator(creator);
                        selectedEncounter = selectedEncounter.save(entityManager);
                    }
                    // else selectedEncounter is the one retrieved from the database
                }
                // PV1-19 should not be null
                else {
                    return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
                            R01, AcknowledgmentCode.AE, "PV1-19 is required but empty", null, Receiver.REQUIRED_FIELD_MISSING,
                            Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
                }
            } else {
                selectedEncounter = new Encounter();
                selectedEncounter.setPatient(selectedPatient);
                selectedEncounter.setCreator(creator);
                selectedEncounter = selectedEncounter.save(entityManager);
            }
            // extract order/specimen/observation
            int nbOfOrders = oruMessage.getPATIENT_RESULT().getORDER_OBSERVATIONReps();
            if (nbOfOrders > 0) {
                for (int orderIndex = 0; orderIndex < nbOfOrders; orderIndex++) {
                    // ORDER_OBSERVATION begin
                    ORU_R01_ORDER_OBSERVATION orderObservationGroup = oruMessage.getPATIENT_RESULT()
                            .getORDER_OBSERVATION(orderIndex);
                    Segment orc = orderObservationGroup.getORC();
                    Segment obr = orderObservationGroup.getOBR();
                    Segment tq1 = orderObservationGroup.getTIMING_QTY().getTQ1();
                    if (!MessageDecoder.isEmpty(orc) && !MessageDecoder.isEmpty(obr)) {
                        LabOrder messageOrder = MessageDecoder.extractLabOrderFromSegments(orc, obr, tq1,
                                simulatedActor, domain, creator, selectedEncounter);
                        messageOrder.setMainEntity(true);
                        messageOrder.setWorkOrder(false);
                        LabOrder selectedOrder = AbstractOrder.getOrderByNumbersByActor(LabOrder.class,
                                messageOrder.getPlacerOrderNumber(), messageOrder.getFillerOrderNumber(), null,
                                simulatedActor, entityManager, false);
                        if (selectedOrder == null) {
                            selectedOrder = messageOrder;
                        }
                        // OBSERVATIONS
                        int nbOfObservations = orderObservationGroup.getOBSERVATIONReps();
                        if (nbOfObservations > 0) {
                            for (int obsIndex = 0; obsIndex < nbOfObservations; obsIndex++) {
                                // OBSERVATION begin
                                Segment obx = orderObservationGroup.getOBSERVATION(obsIndex).getOBX();
                                Observation messageObservation = MessageDecoder.extractObservationFromOBX(obx,
                                        simulatedActor, domain, creator);
                                if (selectedOrder.getId() != null) {
                                    List<Observation> observations = Observation.getObservationFiltered(entityManager,
                                            simulatedActor, domain, selectedOrder, null, null);
                                    if (observations != null) {
                                        selectedOrder.getObservations().remove(observations.get(0));
                                        messageObservation.setId(observations.get(0).getId());
                                    }
                                }
                                messageObservation.setOrder(selectedOrder);
                                if (selectedOrder.getObservations() == null) {
                                    selectedOrder.setObservations(new ArrayList<Observation>());
                                }
                                selectedOrder.getObservations().add(messageObservation);
                                // OBSERVATION end
                            }
                        }
                        // SPECIMENS
                        int nbOfSpecimens = orderObservationGroup.getSPECIMENReps();
                        if (nbOfSpecimens > 0) {
                            for (int spmIndex = 0; spmIndex < nbOfSpecimens; spmIndex++) {
                                Segment spm = orderObservationGroup.getSPECIMEN(spmIndex).getSPM();
                                Specimen messageSpecimen = MessageDecoder.extractSpecimenFromSegment(spm, creator,
                                        simulatedActor, domain);
                                Specimen selectedSpecimen = Specimen.getSpecimenByAssignedIdentifiersByActorByDomain(
                                        messageSpecimen.getPlacerAssignedIdentifier(),
                                        messageSpecimen.getFillerAssignedIdentifier(), simulatedActor, domain,
                                        entityManager, false);
                                if (selectedSpecimen == null) {
                                    selectedSpecimen = messageSpecimen;
                                }
                                // OBSERVATIONS
                                int nbOfSpmObservations = orderObservationGroup.getSPECIMEN(spmIndex).getOBXReps();
                                if (nbOfSpmObservations > 0) {
                                    for (int spmobxIndex = 0; spmobxIndex < nbOfSpmObservations; spmobxIndex++) {
                                        // OBSERVATION begin
                                        Segment obx = orderObservationGroup.getSPECIMEN(spmIndex).getOBX(spmobxIndex);
                                        Observation messageObservation = MessageDecoder.extractObservationFromOBX(obx,
                                                simulatedActor, domain, creator);
                                        if (selectedSpecimen.getId() != null) {
                                            List<Observation> observations = Observation
                                                    .getObservationFiltered(entityManager, simulatedActor, domain,
                                                            null, selectedSpecimen, null);
                                            if (observations != null) {
                                                selectedSpecimen.getObservations().remove(observations.get(0));
                                                messageObservation.setId(observations.get(0).getId());
                                            }
                                        }
                                        messageObservation.setSpecimen(selectedSpecimen);
                                        if (selectedSpecimen.getObservations() == null) {
                                            selectedSpecimen.setObservations(new ArrayList<Observation>());
                                        }
                                        selectedSpecimen.getObservations().add(messageObservation);
                                        // OBSERVATION end
                                    }
                                }
                                if ((selectedSpecimen.getId() != null) && (selectedOrder.getId() != null)
                                        && (selectedOrder.getSpecimens() != null)
                                        && selectedOrder.getSpecimens().contains(selectedSpecimen)) {
                                    int specimenIndex = selectedOrder.getSpecimens().indexOf(selectedSpecimen);
                                    selectedOrder.getSpecimens().set(specimenIndex, selectedSpecimen);
                                } else if (selectedOrder.getSpecimens() == null) {
                                    selectedOrder.setSpecimens(new ArrayList<Specimen>());
                                    selectedOrder.getSpecimens().add(selectedSpecimen);
                                } else {
                                    selectedOrder.getSpecimens().add(selectedSpecimen);
                                }
                                if ((selectedSpecimen.getId() != null) && (selectedOrder.getId() != null)
                                        && (selectedSpecimen.getOrders() != null)
                                        && selectedSpecimen.getOrders().contains(selectedOrder)) {
                                    int orderIntIndex = selectedSpecimen.getOrders().indexOf(selectedOrder);
                                    selectedSpecimen.getOrders().set(orderIntIndex, selectedOrder);
                                } else if (selectedSpecimen.getOrders() == null) {
                                    selectedSpecimen.setOrders(new ArrayList<LabOrder>());
                                    selectedSpecimen.getOrders().add(selectedOrder);
                                } else {
                                    selectedSpecimen.getOrders().add(selectedOrder);
                                }
                            }
                        }
                        selectedOrder = selectedOrder.save(entityManager);
                        log.info("new order id: " + selectedOrder.getId());
                    } else {
                        return Receiver.buildNack(serverApplication, serverFacility, sendingApplication,
                                sendingFacility, R01, AcknowledgmentCode.AE,
                                "Both OBR and ORC segment are required in ORDER_OBSERVATION group", null,
                                Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
                                hl7Version);
                    }

                }

            } else {
                return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
                        R01, AcknowledgmentCode.AE, "No ORDER_OBSERVATION group found, one repetition at least is required", null,
                        Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
                        hl7Version);
            }
            // every goes well, send AA ack
            return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility, R01,
                    AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
        } else {
            return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, R01,
                    AcknowledgmentCode.AE, "R01 trigger event requires ORU_R01 message structure", null, Receiver.INTERNAL_ERROR,
                    Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
        }
    }

}
