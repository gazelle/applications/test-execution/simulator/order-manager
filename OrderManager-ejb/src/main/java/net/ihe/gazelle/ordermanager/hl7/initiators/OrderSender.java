package net.ihe.gazelle.ordermanager.hl7.initiators;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v231.message.ORR_O02;
import ca.uhn.hl7v2.model.v251.message.ORG_O20;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.validation.impl.NoValidation;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.admin.UserPreferencesManager;
import net.ihe.gazelle.ordermanager.filter.OrderFilter;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.utils.IHERadMessageBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Class description : <b>OrderSender</b>
 * 
 * This class is the backing bean for the page /hl7Intiators/orderSender.xhtml, the latter enables the user to create a new order, cancel, replace, update or discontinue it and sends the message to
 * his/her system under test
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */

@Name("orderSender")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 30000)
public class OrderSender extends AbstractOrderSender<Order> implements Serializable, UserAttributeCommon {

	private static Logger log = LoggerFactory.getLogger(OrderSender.class);

	private static final long serialVersionUID = 7140822192208806858L;

	/**
	 * selectedPatient selectedEncounter selectedCountry patientRequired selectedDomain simulatedActor orderControlCode ... are declared in AbstractOrderSender class
	 */

    private List<HL7V2ResponderSUTConfiguration> availableSUTs;

	@In(value="gumUserService")
	private UserService userService;


    @Create
    public void init(){
        super.initializePage();
    }

    /**
	 * Depending of the order control selected, create the datamodel for orderFilter, encounters or patients
	 */
	@Override
	public void onChangeActionEvent() {
		if (orderControlCode.equals(NEW)) {
			orderFilter = null;
            setSelectionType(SelectionType.ENCOUNTER);
		} else if (orderControlCode.equals(CANCEL)) {
			encounterComesFromDispatch = false;
            setSelectionType(SelectionType.ORDER);
			if (simulatedActor.getKeyword().equals(OP_KEYWORD)) {
				orderFilter = new OrderFilter(selectedDomain, simulatedActor, "CA", null);
			} else {
				orderFilter = new OrderFilter(selectedDomain, simulatedActor, "OC", null);
			}
		} else if (orderControlCode.equals(DISCONTINUE)) {
			encounterComesFromDispatch = false;
            setSelectionType(SelectionType.ORDER);
			orderFilter = new OrderFilter(selectedDomain, simulatedActor, "DC", null);
		} else if (orderControlCode.equals(UPDATE)) {
			encounterComesFromDispatch = false;
            setSelectionType(SelectionType.ORDER);
			orderFilter = new OrderFilter(selectedDomain, simulatedActor, "SC", null);
		}
		// else those cases are not yet processed
		if (!encounterComesFromDispatch) {
			setSelectedEncounter(null);
			setSelectedPatient(null);
			selectedOrder = null;
		}
	}

	@Override
	public void displayAvailableCountries() {
		super.displayAvailableCountries();
		orderFilter = null;
		selectedOrder = null;
	}

	@Override
	public void createANewOrder() {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		selectedOrder = new Order(selectedDomain, simulatedActor, getSelectedEncounter(), entityManager);
		if (userPreferences != null) {
			selectedOrder.setCreator(userPreferences.getUsername());
		}
		if (simulatedActor.getKeyword().equals("OP")) {
			selectedOrder.setOrderControlCode("NW");
		} else {
			selectedOrder.setOrderControlCode("SN");
		}
	}

	@Override
	public void sendMessage() {
		HL7V2ResponderSUTConfiguration sut = (HL7V2ResponderSUTConfiguration) Component.getInstance("selectedSUT");
		if (sut == null) {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No SUT selected ! please select the system to test and retry");
			return;
		}
		encounterComesFromDispatch = false;
		// first save the order with the appropriate attributes
		if (orderControlCode.equals(NEW)) {
			if ((selectedOrder.getSpecimenSource() != null) && selectedOrder.getSpecimenSource().equals("null")) {
				selectedOrder.setSpecimenSource(null);
			}
		} else if (orderControlCode.equals(UPDATE)) {
			selectedOrder.setOrderControlCode("SC");
			selectedOrder.setOrderStatus(newOrderStatus);
			selectedOrder.setOrderResultStatus(newResultStatus);
		} else if (orderControlCode.equals(CANCEL)) {
			if (simulatedActor.getKeyword().equals(OF_KEYWORD)) {
				selectedOrder.setOrderControlCode("OC");
				selectedOrder.setOrderStatus("CA");
			} else {
				selectedOrder.setOrderControlCode("CA");
			}
		} else if (orderControlCode.equals(DISCONTINUE)) {
			selectedOrder.setOrderControlCode("DC");
		} else {
			// replace
			selectedOrder.setOrderControlCode("RP");
		}
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		selectedOrder = selectedOrder.save(entityManager);

		// build message
		Transaction transaction;
		String messageType;
		if (simulatedActor.getKeyword().equals(OP_KEYWORD)) {
			transaction = Transaction.GetTransactionByKeyword("RAD-2");
		} else {
			transaction = Transaction.GetTransactionByKeyword("RAD-3");
		}
		if (UserPreferencesManager.instance().isHL7v251OptionActivated()) {
			messageType = "OMG^O19^OMG_O19";
		}else{
			messageType = "ORM^O01^ORM_O01";
		}
		IHERadMessageBuilder generator = new IHERadMessageBuilder(transaction.getKeyword(), simulatedActor.getKeyword(),
				selectedOrder, sut, sendingApplication, sendingFacility,
				UserPreferencesManager.instance().isHL7v251OptionActivated());
		try {
			String messageStringToSend = generator.generateMessage();
			Initiator hl7Initiator = new Initiator(sut, sendingFacility, sendingApplication, simulatedActor,
					transaction, messageStringToSend, messageType + " (" + selectedOrder.getOrderControlCode() + ")",
					selectedDomain.getKeyword(), Actor.findActorWithKeyword(sutActorKeyword));
			try {
				TransactionInstance currentMessage = hl7Initiator.sendMessageAndGetTheHL7Message();
				if (currentMessage != null) {
					hl7Messages = new ArrayList<TransactionInstance>();
                    hl7Messages.add(currentMessage);
                    // if OF + NEW = extract the placer order number from the OP's response
                    updateOrderControlCode(entityManager, currentMessage);
				} else {
					hl7Messages = null;
				}
            } catch (HL7Exception e) {
				log.error("Cannot send message due to " + e.getMessage());
				FacesMessages.instance().add(e.getMessage());
				hl7Messages = null;
			}
		} catch (Exception e) {
			log.error("Cannot build message due to " + e.getMessage());
			FacesMessages.instance().add(e.getMessage());
			hl7Messages = null;
			return;
		}
	}

    private void updateOrderControlCode(EntityManager entityManager, TransactionInstance currentMessage) throws HL7Exception {
        if (simulatedActor.getKeyword().equals("OF") && orderControlCode.equals(NEW)) {
            PipeParser parser = new PipeParser();
            parser.setValidationContext(new NoValidation());
            Message response = parser.parse(currentMessage.getReceivedMessageContent());
            if (response instanceof ORR_O02) {
                ORR_O02 orrMessage = (ORR_O02) parser.parseForSpecificPackage(
                        currentMessage.getReceivedMessageContent(), "ca.uhn.hl7v2.model.v231.message");
                selectedOrder.setPlacerOrderNumber(orrMessage.getPIDNTEORCOBRRQDRQ1RXOODSODTNTECTI()
                        .getORCOBRRQDRQ1RXOODSODTNTECTI().getORC().getPlacerOrderNumber().encode());
                selectedOrder.save(entityManager);
            } else if (response instanceof ORG_O20){
                ORG_O20 orgMessage = (ORG_O20) parser.parseForSpecificPackage(currentMessage.getReceivedMessageContent(), "ca.uhn.hl7v2.model.v251.message");
                selectedOrder.setPlacerOrderNumber(orgMessage.getRESPONSE().getORDER().getORC().getPlacerOrderNumber().encode());
                selectedOrder.save(entityManager);
            } else {
                 FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The received message does not use the expected structure");
            }
        }
    }

    @Override
	public void fillOrderRandomly() {
		selectedOrder.fillOrderRandomly(selectedDomain, simulatedActor);
	}

	@Override
	public List<SelectItem> getAvailableOrderStatuses() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(null, ResourceBundle.instance().getString("gazelle.ordermanager.PleaseSelect")));
		items.add(new SelectItem("CM", ResourceBundle.instance().getString("gazelle.ordermanager.OrderCompleted")));
		items.add(new SelectItem("OD", ResourceBundle.instance().getString("gazelle.ordermanager.OrderDiscontinued")));
		items.add(new SelectItem("IP", ResourceBundle.instance().getString("gazelle.ordermanager.OrderInProgress")));
		return items;
	}

	@Override
	public List<SelectItem> listAvailableActions() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(NEW, ResourceBundle.instance().getString(NEW)));
		items.add(new SelectItem(CANCEL, ResourceBundle.instance().getString(CANCEL)));
		if (simulatedActor.getKeyword().equals(OF_KEYWORD)) {
			items.add(new SelectItem(UPDATE, ResourceBundle.instance().getString(UPDATE)));
		} else {
			items.add(new SelectItem(DISCONTINUE, ResourceBundle.instance().getString(DISCONTINUE)));
		}
		return items;
	}

	/**
	 * Getters and Setters
	 */


    public List<HL7V2ResponderSUTConfiguration> getAvailableSUTs() {
        if (availableSUTs == null){
            String usage;
            if (simulatedActor.getKeyword().equals(OP_KEYWORD)) {
                usage = "_RAD2";
            } else {
                usage = "_RAD3";
            }
            usage = selectedDomain.getKeyword() + usage;
            availableSUTs = HL7V2ResponderSUTConfiguration.getAllConfigurationsForUsage(usage);
        }
        return availableSUTs;
    }

	@Override
	public String getUserName(String s) {
		return userService.getUserDisplayNameWithoutException(s);
	}
}
