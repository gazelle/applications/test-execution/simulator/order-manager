package net.ihe.gazelle.ordermanager.mwl.scp;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.admin.UserPreferencesManagerLocal;
import net.ihe.gazelle.ordermanager.dicom.utils.WorklistConverter;
import net.ihe.gazelle.ordermanager.dicom.utils.XMLWorklistBuilder;
import net.ihe.gazelle.ordermanager.filter.EncounterFilter;
import net.ihe.gazelle.ordermanager.filter.OrderFilter;
import net.ihe.gazelle.ordermanager.filter.ScheduledProcedureStepFilter;
import net.ihe.gazelle.ordermanager.hl7.initiators.SelectionType;
import net.ihe.gazelle.ordermanager.model.AccessionNumberGenerator;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStep;
import net.ihe.gazelle.ordermanager.model.WorklistEntry;
import net.ihe.gazelle.ordermanager.utils.RequestedProcedureCreator;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.Map;

@Name("worklistEntryCreator")
@Scope(ScopeType.PAGE)
public class WorklistEntryCreator implements Serializable {

	private static final long serialVersionUID = 6383981321526309260L;

	private static Logger log = LoggerFactory.getLogger(WorklistEntryCreator.class);

	private WorklistEntry selectedWorklist = null;
	private ScheduledProcedureStep selectedProcedureStep = null;
	private Order selectedOrder = null;
	private RequestedProcedure selectedRequestedProcedure = null;

	private ScheduledProcedureStepFilter spsFilter = null;
	private OrderFilter orderFilter;
    private EncounterFilter encounterFilter;

	private SelectionType selectionType = null;
	private Actor simulatedActor;
	private Domain domain;
	private String errors;
	private String creator;

    public void performAnotherTest(){
        selectedWorklist = null;
        selectedProcedureStep = null;
        selectedOrder = null;
        selectedRequestedProcedure = null;
        initializePage();
    }

    @Create
	public void initializePage() {
		simulatedActor = Actor.findActorWithKeyword("OF");
		UserPreferencesManagerLocal preferences = (UserPreferencesManagerLocal) Component
				.getInstance("userPreferencesManagerBean");
		if (preferences != null) {
			if (preferences.getCurrentUser() != null) {
				creator = preferences.getCurrentUser().getUsername();
			} else {
				creator = null;
			}
		}
		// URL params
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		EntityManager entityManager = EntityManagerService.provideEntityManager();
        if (domain == null) {
            if (params.containsKey("domain")) {
                domain = Domain.getDomainByKeyword(params.get("domain"));
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "domain parameter is expected but missing in the URL");
                return;
            }
        }
		if (params.containsKey("encounterId")) {
			Integer encounterId = Integer.valueOf(params.get("encounterId"));
			Encounter encounter = entityManager.find(Encounter.class, encounterId);
			selectEncounter(encounter);
		} else if (params.containsKey("orderId")) {
			Integer orderId = Integer.valueOf(params.get("orderId"));
			Order order = entityManager.find(Order.class, orderId);
			scheduleOrder(order);
		} else if (params.containsKey("spsId")) {
			Integer spsId = Integer.valueOf(params.get("spsId"));
			ScheduledProcedureStep sps = entityManager.find(ScheduledProcedureStep.class, spsId);
			selectScheduledProcedureStep(sps);
		} else {
			listObjects(SelectionType.SPS);
		}
	}

	public void selectScheduledProcedureStep(ScheduledProcedureStep sps) {
		selectedRequestedProcedure = null;
		selectedProcedureStep = sps;
		selectedWorklist = new WorklistEntry(selectedProcedureStep, simulatedActor, domain);
	}

	public void scheduleOrder(Order order) {
		selectedOrder = order;
		scheduleOrder();
	}

	public void scheduleOrder() {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		if ((selectedOrder.getAccessionNumber() == null) || selectedOrder.getAccessionNumber().isEmpty()) {
			selectedOrder.setAccessionNumber(AccessionNumberGenerator.generate());
		}
		selectedOrder = selectedOrder.save(entityManager);
		RequestedProcedureCreator procedureCreator = new RequestedProcedureCreator(
				selectedOrder.getUniversalServiceId(), simulatedActor, domain, creator);
		String orderHierarchyLocation;
		if (domain.getKeyword().equals("EYE")) {
			orderHierarchyLocation = ApplicationConfiguration.getValueOfVariable("eye_order_hierarchy_location");
		} else // Cadiology and Radiology
		{
			orderHierarchyLocation = ApplicationConfiguration.getValueOfVariable("order_hierarchy_location");
		}
		try {
			selectedRequestedProcedure = procedureCreator.createProcedure(orderHierarchyLocation);
			if (selectedRequestedProcedure != null) {
				selectedRequestedProcedure.setOrder(selectedOrder);
			} else {
				FacesMessages.instance().add(
						"The tool has not been able to create the requested procedures for this order");
			}
		} catch (FileNotFoundException e) {
			FacesMessages.instance()
					.add("The order hierarchy file is not available or its location has not be defined");
			log.error(e.getMessage(), e);
		} catch (MalformedURLException e) {
			FacesMessages.instance()
					.add("The order hierarchy file is not available or its location has not be defined");
			log.error(e.getMessage(), e);
		} catch (Exception e) {
			FacesMessages.instance().add(
					"An internal error has prevented the tool from creating the requested procedure");
			log.error(e.getMessage(), e);
		}
		selectedOrder = null;
	}

	public void saveRequestedProcedure() {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		selectedRequestedProcedure = selectedRequestedProcedure.save(entityManager);
	}

	public void selectEncounter(Encounter encounter) {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		selectedOrder = new Order(domain, simulatedActor, encounter, entityManager);
		selectedOrder.setCreator(creator);
		selectedOrder.setOrderControlCode("NW");
		selectedOrder.setOrderStatus("SC");
	}

	public void fillOrderRandomly() {
		selectedOrder.fillOrderRandomly(domain, simulatedActor);
	}

	public void onChangeActionEvent() {
		listObjects(selectionType);
	}

	public void listObjects(SelectionType objectType) {
		selectionType = objectType;
		selectedOrder = null;
		selectedProcedureStep = null;
		selectedRequestedProcedure = null;
        switch (objectType){
            case SPS:
                spsFilter = new ScheduledProcedureStepFilter(domain, simulatedActor, false, true);
                break;
            case ORDER:
                orderFilter = new OrderFilter(domain, simulatedActor, null, false);
                break;
            case ENCOUNTER:
                encounterFilter = new EncounterFilter();
                break;
            default:
                log.error(objectType + " is not an object type defined in this bean");
        }
	}

	public void generateWorklist() {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		UserPreferencesManagerLocal userPreferences = (UserPreferencesManagerLocal) Component
				.getInstance("userPreferencesManagerBean");
		selectedWorklist.setStationAETitle(userPreferences.getStationAETitle());
		selectedWorklist = selectedWorklist.saveWorklist();
		selectedWorklist.setFilepath(selectedWorklist.buildFilePath());
		XMLWorklistBuilder builder = new XMLWorklistBuilder(selectedWorklist);
		if (!builder.build()) {
			errors = "Unable to create the XML file from selected data";
		} else {
			WorklistConverter converter = new WorklistConverter(selectedWorklist);
			boolean success = converter.convertToDicom();
			if (!success) {
				errors = converter.getErrorMessage();
				entityManager.remove(selectedWorklist);
				selectedWorklist.setId(null);
			} else {
				selectedWorklist.saveWorklist();
			}
		}
	}

	public WorklistEntry getSelectedWorklist() {
		return selectedWorklist;
	}

	public void setSelectedWorklist(WorklistEntry selectedWorklist) {
		this.selectedWorklist = selectedWorklist;
	}

	public ScheduledProcedureStep getSelectedProcedureStep() {
		return selectedProcedureStep;
	}

	public void setSelectedProcedureStep(ScheduledProcedureStep selectedProcedureStep) {
		this.selectedProcedureStep = selectedProcedureStep;
	}

	public Order getSelectedOrder() {
		return selectedOrder;
	}

	public void setSelectedOrder(Order selectedOrder) {
		this.selectedOrder = selectedOrder;
	}

	public RequestedProcedure getSelectedRequestedProcedure() {
		return selectedRequestedProcedure;
	}

	public void setSelectedRequestedProcedure(RequestedProcedure selectedRequestProcedure) {
		this.selectedRequestedProcedure = selectedRequestProcedure;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public Domain getDomain() {
		return domain;
	}

    public SelectionType getSelectionType() {
        return selectionType;
    }

    public ScheduledProcedureStepFilter getSpsFilter() {
        return spsFilter;
    }

    public OrderFilter getOrderFilter() {
        return orderFilter;
    }

    public EncounterFilter getEncounterFilter() {
        return encounterFilter;
    }
}
