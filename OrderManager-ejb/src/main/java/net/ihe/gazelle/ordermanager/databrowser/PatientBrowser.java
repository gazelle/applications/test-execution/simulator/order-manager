package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.util.Map;

@Name("patientBrowser")
@Scope(ScopeType.PAGE)
public class PatientBrowser extends DataBrowser<Patient> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3537158311930530918L;

	@Override
	public void modifyQuery(HQLQueryBuilder<Patient> queryBuilder, Map<String, Object> map) {
		if ((patientId != null) && !patientId.isEmpty()) {
			queryBuilder.addLike("patientIdentifiers.identifier", patientId);
		}
	}

    @Override
    public Filter<Patient> getFilter() {
        if (filter == null){
            filter = new Filter<Patient>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return filter;
    }

    @Override
	protected HQLCriterionsForFilter<Patient> getHQLCriterionsForFilter() {
		PatientQuery query = new PatientQuery();
		HQLCriterionsForFilter<Patient> criterions = query.getHQLCriterionsForFilter();
		criterions.addPath("lastChanged", query.creationDate());
		criterions.addPath("creator", query.creator());
        criterions.addPath("firstname", query.firstName());
        criterions.addPath("lastname", query.lastName());
		criterions.addQueryModifier(this);
		return criterions;
	}

	@Override
	public String getPermanentLink(Integer objectId) {
		return "/browsing/patient.seam?id=" + objectId;
	}

    @Override
    public FilterDataModel<Patient> getDataModel() {
        return new FilterDataModel<Patient>(getFilter()) {
            @Override
            protected Object getId(Patient patient) {
                return patient.getId();
            }
        };
    }

}
