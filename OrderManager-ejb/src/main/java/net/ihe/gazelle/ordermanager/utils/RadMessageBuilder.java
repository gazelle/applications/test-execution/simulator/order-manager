package net.ihe.gazelle.ordermanager.utils;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.ErrorCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v231.message.ACK;
import ca.uhn.hl7v2.model.v231.message.ORR_O02;
import ca.uhn.hl7v2.model.v231.segment.ERR;
import ca.uhn.hl7v2.model.v231.segment.MSA;
import ca.uhn.hl7v2.model.v231.segment.MSH;
import ca.uhn.hl7v2.model.v231.segment.OBR;
import ca.uhn.hl7v2.model.v231.segment.ORC;
import ca.uhn.hl7v2.model.v231.segment.PID;
import ca.uhn.hl7v2.model.v231.segment.PV1;
import ca.uhn.hl7v2.model.v251.message.ORG_O20;
import ca.uhn.hl7v2.model.v251.segment.IPC;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.hl7.responders.Receiver;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.ProtocolItem;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStep;
import net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment.ZDS;
import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class description : <b>MessageGenerator</b>
 * This class contains the methods to create the HL7 messages used by the OrderManager simulator
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 * @version 1.0 - 2011, November 17th
 */
public abstract class RadMessageBuilder<T extends AbstractOrder> extends SegmentBuilder {

    protected static final Logger log = LoggerFactory.getLogger(RadMessageBuilder.class);
    private static final String HL7V2_3_1 = "2.3.1";
    private static final String HL7V2_5 = "2.5";
    private static final String HL7V2_5_1 = "2.5.1";

    protected String transactionKeyword;
    protected String sendingActorKeyword;
    protected T order;
    protected HL7V2ResponderSUTConfiguration sut;
    protected String sendingApplication;
    protected String sendingFacility;
    protected SimpleDateFormat sdf;


    /**
     * Constructor
     *
     * @param inTransactionKeyword  is required to determine the type of message to send
     * @param inSendingActorKeyword is required to determine the type of message to send
     * @param inOrder               , the order for which a message needs to be create
     * @param sut                   , the SUT to which sends the message
     * @param sendingApplication    , sending application of the simulator
     * @param sendingFacility       , sending facility of the simulator
     */
    public RadMessageBuilder(String inTransactionKeyword, String inSendingActorKeyword, T inOrder,
                             HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility) {
        this.transactionKeyword = inTransactionKeyword;
        this.sendingApplication = sendingApplication;
        this.sendingFacility = sendingFacility;
        this.sut = sut;
        this.sendingActorKeyword = inSendingActorKeyword;
        this.order = inOrder;
        this.sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    }

    public RadMessageBuilder() {
        this.sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    }

    /**
     * Thanks to the transaction and actor, creates the appropriates message
     *
     * @return the String version of the created message
     *
     * @throws HL7Exception
     */
    public abstract String generateMessage() throws HL7Exception;

    /**
     * Creates an ACK message
     *
     * @param triggerEvent
     * @param ackCode
     * @param userMessage
     * @param errorType
     * @param hl7ErrorCode
     * @param severity
     * @param messageControlId
     * @param hl7Version
     * @param isNACK
     *
     * @return the acknowledgement
     */
    public Message buildACK(String triggerEvent, AcknowledgmentCode ackCode, String userMessage, String errorType,
                            String hl7ErrorCode, String severity, String messageControlId, String hl7Version, boolean isNACK) {
        if (triggerEvent != null) {
            String messageType = "ACK^" + triggerEvent + "^ACK";
            if (HL7V2_3_1.equals(hl7Version)) {
                return buildAckForHL7v231(ackCode, userMessage, hl7ErrorCode, messageControlId, isNACK, messageType);
            } else if (HL7V2_5.equals(hl7Version)) {
                return buildAckForHL7v25(triggerEvent, ackCode, userMessage, hl7ErrorCode, severity, messageControlId, isNACK);
            } else if (HL7V2_5_1.equals(hl7Version)) {
                return buildAckForHL7v251(triggerEvent, ackCode, userMessage, hl7ErrorCode, severity, messageControlId, isNACK);
            }
        }
        return null;
    }

    private Message buildAckForHL7v251(String triggerEvent, AcknowledgmentCode ackCode, String userMessage, String hl7ErrorCode, String severity,
                                       String messageControlId, boolean isNACK) {
        ca.uhn.hl7v2.model.v251.message.ACK ack = new ca.uhn.hl7v2.model.v251.message.ACK();
        try {
            fillMSHSegment(ack.getMSH(), sendingApplication, sendingFacility, sut.getApplication(),
                    sut.getFacility(), "ACK", triggerEvent, "ACK", HL7V2_5_1, sdf, sut.getCharset().getHl7Code());
            fillMSASegment(ack.getMSA(), messageControlId, ackCode);
            if (isNACK) {
                this.populateERRv251(ack.getERR(), hl7ErrorCode, severity, userMessage, null);
            }
        } catch (HL7Exception e) {
            log.error("Unable to fulfill the creation of acknowledgment");
        }
        return ack;
    }

    private Message buildAckForHL7v25(String triggerEvent, AcknowledgmentCode ackCode, String userMessage, String hl7ErrorCode, String severity,
                                      String messageControlId, boolean isNACK) {
        ca.uhn.hl7v2.model.v25.message.ACK ack = new ca.uhn.hl7v2.model.v25.message.ACK();
        try {
            fillMSHSegment(ack.getMSH(), sendingApplication, sendingFacility, sut.getApplication(),
                    sut.getFacility(), "ACK", triggerEvent, "ACK", HL7V2_5, sdf, sut.getCharset().getHl7Code());
            fillMSASegment(ack.getMSA(), messageControlId, ackCode);
            if (isNACK) {
                populateERRv25(ack.getERR(), hl7ErrorCode, severity, userMessage);
            }
        } catch (HL7Exception e) {
            log.error("Unable to fulfill the creation of acknowledgment");
        }
        return ack;
    }

    private Message buildAckForHL7v231(AcknowledgmentCode ackCode, String userMessage, String hl7ErrorCode, String messageControlId,
                                       boolean isNACK,
                                       String messageType) {
        ACK ack = new ACK();
        try {
            populateMSHv231(ack.getMSH(), messageType);
            populateMSAv231(ack.getMSA(), messageControlId, ackCode, false);
            ack.getMSA().getMsa3_TextMessage().setValue(userMessage);
        } catch (HL7Exception e) {
            return ack;
        }
        if (isNACK) {
            populateERRv231(ack.getERR(), hl7ErrorCode);
        }
        return ack;
    }

    protected void populateERRv251(ca.uhn.hl7v2.model.v251.segment.ERR err, String hl7ErrorCode, String severity,
                                   String userMessage, String errorLocation) {
        try {
            err.getHL7ErrorCode().getIdentifier().setValue(hl7ErrorCode);
            err.getSeverity().setValue(severity);
            err.getUserMessage().setValue(userMessage);
        } catch (HL7Exception e) {
            log.error("Cannot populate ERR segment: " + e.getMessage());
        }

    }

    public static void fillMSASegment(ca.uhn.hl7v2.model.v251.segment.MSA msaSegment, String messageControlId,
                                      AcknowledgmentCode ackCode) throws DataTypeException {
        try {
            msaSegment.getAcknowledgmentCode().setValue(ackCode.name());
            msaSegment.getMessageControlID().setValue(messageControlId);
        } catch (DataTypeException e) {
            throw new DataTypeException("An error occurred when filling MSA segment: " + e.getMessage(), e);
        }
    }

    public static void fillMSHSegment(ca.uhn.hl7v2.model.v251.segment.MSH mshSegment, String receivingApplication,
                                      String receivingFacility, String sendingApplication, String sendingFacility, String messageCode,
                                      String triggerEvent, String msgStructure, String hl7Version, SimpleDateFormat sdf, String characterSet)
            throws DataTypeException {
        try {
            mshSegment.getFieldSeparator().setValue("|");
            mshSegment.getEncodingCharacters().setValue("^~\\&");
            mshSegment.getSendingApplication().getNamespaceID().setValue(sendingApplication);
            mshSegment.getSendingFacility().getNamespaceID().setValue(sendingFacility);
            mshSegment.getReceivingApplication().getNamespaceID().setValue(receivingApplication);
            mshSegment.getReceivingFacility().getNamespaceID().setValue(receivingFacility);
            mshSegment.getDateTimeOfMessage().getTime().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getMessageType().getMessageCode().setValue(messageCode);
            mshSegment.getMessageType().getTriggerEvent().setValue(triggerEvent);
            mshSegment.getMessageType().getMessageStructure().setValue(msgStructure);
            mshSegment.getProcessingID().getProcessingID().setValue("P");
            mshSegment.getMessageControlID().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getVersionID().getVersionID().setValue(hl7Version);
            if (characterSet != null) {
                mshSegment.getCharacterSet(0).setValue(characterSet);
            }
        } catch (DataTypeException e) {
            throw new DataTypeException("An error occurred when filling MSH segment: " + e.getMessage(), e);
        }
    }

    /**
     * @param isError
     * @param messageControlId
     * @param hl7ErrorCode
     *
     * @return the acknowledgement
     */
    public Message buildORR(boolean isError, String messageControlId, String hl7ErrorCode, String obrSegment,
                            String orcSegment) {
        ORR_O02 orrMessage = new ORR_O02();
        try {
            populateMSHv231(orrMessage.getMSH(), "ORR^O02^ORR_O02");
            if (!isError) {
                orrMessage.getPIDNTEORCOBRRQDRQ1RXOODSODTNTECTI().getORCOBRRQDRQ1RXOODSODTNTECTI().getORC()
                        .parse(orcSegment);
                orrMessage.getPIDNTEORCOBRRQDRQ1RXOODSODTNTECTI().getORCOBRRQDRQ1RXOODSODTNTECTI().getORC()
                        .getPlacerOrderNumber().parse(order.getPlacerOrderNumber());
                orrMessage.getPIDNTEORCOBRRQDRQ1RXOODSODTNTECTI().getORCOBRRQDRQ1RXOODSODTNTECTI().getORC()
                        .getOrderControl().setValue("NA");
                orrMessage.getPIDNTEORCOBRRQDRQ1RXOODSODTNTECTI().getORCOBRRQDRQ1RXOODSODTNTECTI().getOBR()
                        .parse(obrSegment);
                populateMSAv231(orrMessage.getMSA(), messageControlId, AcknowledgmentCode.AA, false);
            } else {
                populateMSAv231(orrMessage.getMSA(), messageControlId, AcknowledgmentCode.AE, true);
                populateERRv231(orrMessage.getERR(), hl7ErrorCode);
            }
            return orrMessage;
        } catch (Exception e) {
            log.error("Cannot fill the ORR message", e);
            return null;
        }
    }

    /**
     * @param isError
     * @param messageControlId
     * @param hl7ErrorCode
     * @param obrSegment
     * @param orcSegment
     *
     * @return the acknowledgement
     */
    public Message buildORG(boolean isError, String messageControlId, String hl7ErrorCode, String obrSegment,
                            String orcSegment) {
        ORG_O20 orgMessage = new ORG_O20();
        try {
            populateMSHv251(orgMessage.getMSH(), "ORG^O20^ORG_O20");
            if (!isError) {
                orgMessage.getRESPONSE().getORDER().getORC().parse(orcSegment);
                orgMessage.getRESPONSE().getORDER().getORC()
                        .getPlacerOrderNumber().parse(order.getPlacerOrderNumber());
                orgMessage.getRESPONSE().getORDER().getORC()
                        .getOrderControl().setValue("NA");
                orgMessage.getRESPONSE().getORDER().getOBR()
                        .parse(obrSegment);
                fillMSASegment(orgMessage.getMSA(), messageControlId, AcknowledgmentCode.AA);
            } else {
                fillMSASegment(orgMessage.getMSA(), messageControlId, AcknowledgmentCode.AE);
                populateERRv251(orgMessage.getERR(), hl7ErrorCode, Receiver.SEVERITY_ERROR, null, null);
            }
            return orgMessage;
        } catch (Exception e) {
            log.error("Cannot fill the ORG message", e);
            return null;
        }
    }

    /**
     * @param err
     * @param hl7ErrorCode
     */
    protected void populateERRv231(ERR err, String hl7ErrorCode) {
        try {
            err.getErrorCodeAndLocation(0).getEld4_CodeIdentifyingError().getIdentifier().setValue(hl7ErrorCode);
            err.getErr1_ErrorCodeAndLocation(0).getEld4_CodeIdentifyingError().getCe3_NameOfCodingSystem()
                    .setValue("HL70357");
        } catch (HL7Exception e) {
            log.error("Cannot populate ERR segment: " + e.getMessage());
        }

    }

    /**
     * @param err
     * @param hl7ErrorCode
     */
    protected void populateERRv25(ca.uhn.hl7v2.model.v25.segment.ERR err, String hl7ErrorCode, String severity,
                                  String userMessage) {
        try {
            err.getHL7ErrorCode().getIdentifier().setValue(hl7ErrorCode);
            err.getHL7ErrorCode().getNameOfCodingSystem().setValue("HL70357");
            err.getSeverity().setValue(severity);
            err.getUserMessage().setValue(userMessage);
        } catch (HL7Exception e) {
            log.error("Cannot populate ERR segment: " + e.getMessage());
        }

    }


    /**
     * fills ZDS segment (RAD-4 and RAD-13)
     *
     * @param zds
     *
     * @throws HL7Exception
     */
    protected void populateZDSv231(ZDS zds) throws HL7Exception {
        zds.getStudyInstanceUID().getApplicationID().parse("OrderManager");
        zds.getStudyInstanceUID().getTypeOfData().setValue("Application");
        zds.getStudyInstanceUID().getSubtype().setValue("DICOM");
    }

    /**
     * @param tq1Segment
     * @param currentOrder
     *
     * @throws HL7Exception
     */
    protected void populateTQ1v251(ca.uhn.hl7v2.model.v251.segment.TQ1 tq1Segment, T currentOrder) throws HL7Exception {
        if (currentOrder.getStartDateTime() != null) {
            tq1Segment.getStartDateTime().getTime().setValue(sdf.format(currentOrder.getStartDateTime()));
        }
        tq1Segment.getPriority(0).parse(
                ValueSetProvider.getInstance().buildCEElementFromCode(currentOrder.getQuantityTiming(),
                        "quantityTiming"));
    }

    /**
     * fills OBR segment for RAD-4 and RAD-13 transactions
     *
     * @param obr
     * @param sps
     * @param protocol
     *
     * @throws HL7Exception
     */
    protected void populateOBRv231(net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment.OBR obr,
                                   ScheduledProcedureStep sps, ProtocolItem protocol) throws HL7Exception {
        obr.getSetIDOBR().setValue("1");
        obr.getPlacerOrderNumber().parse(order.getPlacerOrderNumber());
        obr.getFillerOrderNumber().parse(order.getFillerOrderNumber());
        String universalServiceIdPart1 = null;
        if (sps.getDomain().getKeyword().equals("EYE")) {
            universalServiceIdPart1 = ValueSetProvider.getInstance().buildCEElementFromCode(
                    order.getUniversalServiceId(), "eyeUniversalServiceId");
        } else {
            universalServiceIdPart1 = ValueSetProvider.getInstance().buildCEElementFromCode(
                    order.getUniversalServiceId(), "radUniversalServiceId");
        }
        if (universalServiceIdPart1 != null) {
            obr.getUniversalServiceID().parse(universalServiceIdPart1);
        } else {
            obr.getUniversalServiceID().getCe1_Identifier().setValue(order.getUniversalServiceId());
        }
        obr.getUniversalServiceID().getCe4_AlternateIdentifier().setValue(protocol.getProtocolCode());
        obr.getUniversalServiceID().getCe5_AlternateText().setValue(protocol.getProtocolCodeMeaning());
        obr.getUniversalServiceID().getCe6_NameOfAlternateCodingSystem().setValue(protocol.getCodingScheme());
        obr.getPriorityOBR().setValue(order.getQuantityTiming());
        if ((order.getDangerCode() != null) && !order.getDangerCode().isEmpty()) {
            String dangerCode = ValueSetProvider.getInstance().buildCEElementFromCode(order.getDangerCode(), "danger");
            if (dangerCode != null) {
                obr.getDangerCode().parse(dangerCode);
            } else {
                obr.getDangerCode().getCe1_Identifier().setValue(order.getDangerCode());
            }
        }
        obr.getRelevantClinicalInfo().setValue(order.getRelevantClinicalInfo());
        if (order.getOrderingProvider() != null) {
            String xcn = order.getOrderingProvider().concat("^")
                    .concat(ValueSet.getDisplayNameForCode("doctor", order.getOrderingProvider()));
            obr.getOrderingProvider(0).parse(xcn);
        }
        obr.getFillerField1().setValue(sps.getScheduledProcedureID());
        obr.getDiagnosticServSectID().setValue(sps.getModality());
        obr.getQuantityTiming(0).getStartDateTime().getTimeOfAnEvent().setValue(sdf.format(sps.getSpsStartDateTime()));
    }

    /**
     * fills OBR segment for RAD-4 and RAD-13 transactions (HL7v2.5.1 option)
     *
     * @param obr
     * @param sps
     * @param protocol
     *
     * @throws HL7Exception
     */
    protected void populateOBRv251(ca.uhn.hl7v2.model.v251.segment.OBR obr,
                                   ScheduledProcedureStep sps, ProtocolItem protocol) throws HL7Exception {
        obr.getSetIDOBR().setValue("1");
        obr.getPlacerOrderNumber().parse(order.getPlacerOrderNumber());
        obr.getFillerOrderNumber().parse(order.getFillerOrderNumber());
        String universalServiceIdPart1 = null;
        if (sps.getDomain().getKeyword().equals("EYE")) {
            universalServiceIdPart1 = ValueSetProvider.getInstance().buildCEElementFromCode(
                    order.getUniversalServiceId(), "eyeUniversalServiceId");
        } else {
            universalServiceIdPart1 = ValueSetProvider.getInstance().buildCEElementFromCode(
                    order.getUniversalServiceId(), "radUniversalServiceId");
        }
        if (universalServiceIdPart1 != null) {
            obr.getUniversalServiceIdentifier().parse(universalServiceIdPart1);
        } else {
            obr.getUniversalServiceIdentifier().getCe1_Identifier().setValue(order.getUniversalServiceId());
        }
        obr.getUniversalServiceIdentifier().getCe4_AlternateIdentifier().setValue(protocol.getProtocolCode());
        obr.getUniversalServiceIdentifier().getCe5_AlternateText().setValue(protocol.getProtocolCodeMeaning());
        obr.getUniversalServiceIdentifier().getCe6_NameOfAlternateCodingSystem().setValue(protocol.getCodingScheme());
        obr.getPriorityOBR().setValue(order.getQuantityTiming());
        if ((order.getDangerCode() != null) && !order.getDangerCode().isEmpty()) {
            String dangerCode = ValueSetProvider.getInstance().buildCEElementFromCode(order.getDangerCode(), "danger");
            if (dangerCode != null) {
                obr.getDangerCode().parse(dangerCode);
            } else {
                obr.getDangerCode().getCe1_Identifier().setValue(order.getDangerCode());
            }
        }
        obr.getRelevantClinicalInformation().setValue(order.getRelevantClinicalInfo());
        if (order.getOrderingProvider() != null) {
            String xcn = order.getOrderingProvider().concat("^")
                    .concat(ValueSet.getDisplayNameForCode("doctor", order.getOrderingProvider()));
            obr.getOrderingProvider(0).parse(xcn);
        }
        obr.getFillerField1().setValue(sps.getScheduledProcedureID());
        obr.getDiagnosticServSectID().setValue(sps.getModality());
    }

    /**
     * fill ORC segment for RAD-4 and RAD-13 transactions
     *
     * @param orc
     * @param sps
     *
     * @throws HL7Exception
     */
    protected void populateORCv231(net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment.ORC orc, ScheduledProcedureStep sps)
            throws HL7Exception {
        if (order.getPlacerGroupNumber() != null) {
            orc.getPlacerGroupNumber().parse(order.getPlacerGroupNumber());
        }
        orc.getFillerOrderNumber().parse(order.getFillerOrderNumber());
        orc.getPlacerOrderNumber().parse(order.getPlacerOrderNumber());
        if (sps.getSpsStartDateTime() != null) {
            orc.getQuantityTiming().getStartDateTime().getTimeOfAnEvent().setValue(sdf.format(sps.getSpsStartDateTime()));
        }
        String organization = ValueSetProvider.getInstance().buildCEElementFromCode(order.getEnteringOrganization(),
                "enteringOrganization");
        orc.getEnteringOrganization().parse(organization);
        if (order.getOrderingProvider() != null) {
            String xcn = order.getOrderingProvider().concat("^")
                    .concat(ValueSet.getDisplayNameForCode("doctor", order.getOrderingProvider()));
            orc.getOrderingProvider().parse(xcn);
        }
        if (order.getEnteredBy() != null) {
            String xcn = order.getEnteredBy().concat("^")
                    .concat(ValueSet.getDisplayNameForCode("doctor", order.getEnteredBy()));
            orc.getEnteredBy().parse(xcn);
        }
    }

    /**
     * fill ORC segment for RAD-4 and RAD-13 transactions
     *
     * @param orc
     * @param sps
     *
     * @throws HL7Exception
     */
    protected void populateORCv251(ca.uhn.hl7v2.model.v251.segment.ORC orc, ScheduledProcedureStep sps)
            throws HL7Exception {
        if (order.getPlacerGroupNumber() != null) {
            orc.getPlacerGroupNumber().parse(order.getPlacerGroupNumber());
        }
        orc.getFillerOrderNumber().parse(order.getFillerOrderNumber());
        orc.getPlacerOrderNumber().parse(order.getPlacerOrderNumber());
        String organization = ValueSetProvider.getInstance().buildCEElementFromCode(order.getEnteringOrganization(),
                "enteringOrganization");
        orc.getEnteringOrganization().parse(organization);
        if (order.getOrderingProvider() != null) {
            String xcn = order.getOrderingProvider().concat("^")
                    .concat(ValueSet.getDisplayNameForCode("doctor", order.getOrderingProvider()));
            orc.getOrderingProvider(0).parse(xcn);
        }
        if (order.getEnteredBy() != null) {
            String xcn = order.getEnteredBy().concat("^")
                    .concat(ValueSet.getDisplayNameForCode("doctor", order.getEnteredBy()));
            orc.getEnteredBy(0).parse(xcn);
        }
    }


    protected void populateIPC(IPC ipc, ScheduledProcedureStep sps, ProtocolItem protocol) throws HL7Exception {
        ipc.getAccessionIdentifier().getEntityIdentifier().setValue("accessionIdentifier");
        ipc.getRequestedProcedureID().getEntityIdentifier().setValue(sps.getRequestedProcedure().getRequestedProcedureID());
        ipc.getStudyInstanceUID().getEntityIdentifier().setValue(sps.getRequestedProcedure().getStudyInstanceUID());
        ipc.getScheduledProcedureStepID().getEntityIdentifier().setValue(sps.getScheduledProcedureID());
        ipc.getModality().getIdentifier().setValue(sps.getModality());
        ipc.getProtocolCode(0).getIdentifier().setValue(protocol.getProtocolCode());
    }

    /**
     * fills the MSH segment of the HL7v2.3.1 messages
     *
     * @param mshSegment
     * @param messageType
     *
     * @throws HL7Exception
     */
    protected void populateMSHv231(MSH mshSegment, String messageType) throws HL7Exception {
        mshSegment.getFieldSeparator().setValue("|");
        mshSegment.getEncodingCharacters().setValue("^~\\&");
        mshSegment.getSendingApplication().getNamespaceID().setValue(sendingApplication);
        mshSegment.getSendingFacility().getNamespaceID().setValue(sendingFacility);
        mshSegment.getReceivingApplication().getNamespaceID().setValue(sut.getApplication());
        mshSegment.getReceivingFacility().getNamespaceID().setValue(sut.getFacility());
        mshSegment.getDateTimeOfMessage().getTimeOfAnEvent().setValue(sdf.format(new Date()));
        mshSegment.getMessageType().parse(messageType);
        mshSegment.getProcessingID().getProcessingID().setValue("P");
        mshSegment.getMessageControlID().setValue(sdf.format(new Date()));
        mshSegment.getVersionID().getVersionID().setValue(HL7V2_3_1);
        if (sut.getCharset() != null) {
            mshSegment.getCharacterSet(0).setValue(sut.getCharset().getHl7Code());
        }
    }

    /**
     * fills the MSH segment of the HL7v2.5.1 messages
     *
     * @param mshSegment
     * @param messageType
     *
     * @throws HL7Exception
     */
    protected void populateMSHv251(ca.uhn.hl7v2.model.v251.segment.MSH mshSegment, String messageType) throws HL7Exception {
        mshSegment.getFieldSeparator().setValue("|");
        mshSegment.getEncodingCharacters().setValue("^~\\&");
        mshSegment.getSendingApplication().getNamespaceID().setValue(sendingApplication);
        mshSegment.getSendingFacility().getNamespaceID().setValue(sendingFacility);
        mshSegment.getReceivingApplication().getNamespaceID().setValue(sut.getApplication());
        mshSegment.getReceivingFacility().getNamespaceID().setValue(sut.getFacility());
        mshSegment.getDateTimeOfMessage().getTime().setValue(sdf.format(new Date()));
        mshSegment.getMessageType().parse(messageType);
        mshSegment.getProcessingID().getProcessingID().setValue("P");
        mshSegment.getMessageControlID().setValue(sdf.format(new Date()));
        mshSegment.getVersionID().getVersionID().setValue(HL7V2_5_1);
        if (sut.getCharset() != null) {
            mshSegment.getCharacterSet(0).setValue(sut.getCharset().getHl7Code());
        }
    }

    /**
     * fills the MSH segment of the HL7v2.3.1 messages
     *
     * @param mshSegment  (from net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment package)
     * @param messageType
     *
     * @throws HL7Exception
     */
    protected void populateMSHv231(net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment.MSH mshSegment, String messageType)
            throws HL7Exception {
        mshSegment.getFieldSeparator().setValue("|");
        mshSegment.getEncodingCharacters().setValue("^~\\&");
        mshSegment.getSendingApplication().getNamespaceID().setValue(sendingApplication);
        mshSegment.getSendingFacility().getNamespaceID().setValue(sendingFacility);
        mshSegment.getReceivingApplication().getNamespaceID().setValue(sut.getApplication());
        mshSegment.getReceivingFacility().getNamespaceID().setValue(sut.getFacility());
        mshSegment.getDateTimeOfMessage().getTimeOfAnEvent().setValue(sdf.format(new Date()));
        mshSegment.getMessageType().parse(messageType);
        mshSegment.getProcessingID().getProcessingID().setValue("P");
        mshSegment.getMessageControlID().setValue(sdf.format(new Date()));
        mshSegment.getVersionID().getVersionID().setValue(HL7V2_3_1);
        if (sut.getCharset() != null) {
            mshSegment.getCharacterSet().setValue(sut.getCharset().getHl7Code());
        }
    }

    /**
     * fills the PID segment of the HL7v2.3.1 messages
     *
     * @param pidSegment
     *
     * @throws HL7Exception
     */
    protected void populatePIDv231(PID pidSegment) throws HL7Exception {
        Patient patient = order.getEncounter().getPatient();
        if (patient.getPatientIdentifiers() != null) {
            int index = 0;
            for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
                pidSegment.getPatientIdentifierList(index).parse(pid.getIdentifier());
                pidSegment.getPatientIdentifierList(index).getIdentifierTypeCode()
                        .setValue(pid.getIdentifierTypeCode());
                index++;
            }
        }
        pidSegment.getPatientName(0).getFamilyLastName().getFamilyName().setValue(patient.getLastName());
        pidSegment.getPatientName(0).getGivenName().setValue(patient.getFirstName());
        if (patient.getSecondName() != null) {
            String otherNames = patient.getSecondName();
            if (patient.getThirdName() != null) {
                otherNames = otherNames.concat(" " + patient.getThirdName());
            }
            pidSegment.getPatientName(0).getMiddleInitialOrName().setValue(otherNames);
        }
        pidSegment.getPatientName(0).getNameTypeCode().setValue("L");
        if (patient.getAlternateFirstName() != null) {
            pidSegment.getPatientName(1).getGivenName().setValue(patient.getAlternateFirstName());
            if (patient.getAlternateSecondName() != null) {
                String otherNames = patient.getAlternateSecondName();
                if (patient.getAlternateThirdName() != null) {
                    otherNames = otherNames.concat(" " + patient.getAlternateThirdName());
                }
                pidSegment.getPatientName(1).getMiddleInitialOrName().setValue(otherNames);
            }
            pidSegment.getPatientName(1).getNameTypeCode().setValue("L");
        }
        if (patient.getAlternateLastName() != null) {
            pidSegment.getPatientName(1).getFamilyLastName().getFamilyName().setValue(patient.getAlternateLastName());
            pidSegment.getPatientName(0).getNameTypeCode().setValue("L");
        }
        pidSegment.getMotherSMaidenName(0).getFamilyLastName().getFamilyName().setValue(patient.getMotherMaidenName());
        if (patient.getMotherMaidenName() != null) {
            pidSegment.getMotherSMaidenName(0).getNameTypeCode().setValue("M");
        }
        if (patient.getAlternateMothersMaidenName() != null) {
            pidSegment.getMotherSMaidenName(1).getNameTypeCode().setValue("M");
            pidSegment.getMotherSMaidenName(1).getFamilyLastName().getFamilyName().setValue(patient.getAlternateMothersMaidenName());
        }
        if (patient.getDateOfBirth() != null) {
            pidSegment.getDateTimeOfBirth().getTimeOfAnEvent().setValue(sdf.format(patient.getDateOfBirth()));
        }
        pidSegment.getSex().setValue(patient.getGenderCode());

        pidSegment.getPatientAddress(0).getStreetAddress().setValue(patient.getStreet());
        pidSegment.getPatientAddress(0).getCity().setValue(patient.getCity());
        pidSegment.getPatientAddress(0).getStateOrProvince().setValue(patient.getState());
        pidSegment.getPatientAddress(0).getZipOrPostalCode().setValue(patient.getZipCode());
        pidSegment.getPatientAddress(0).getCountry().setValue(patient.getCountryCode());
    }

    /**
     * fills the PID segment of the HL7v2.5.1 messages
     *
     * @param pidSegment
     *
     * @throws HL7Exception
     */
    protected void populatePIDv251(ca.uhn.hl7v2.model.v251.segment.PID pidSegment) throws HL7Exception {
        Patient patient = EntityManagerService.provideEntityManager().find(Patient.class, order.getEncounter().getPatient().getId());
        if (patient.getPatientIdentifiers() != null) {
            int index = 0;
            for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
                pidSegment.getPatientIdentifierList(index).parse(pid.getIdentifier());
                pidSegment.getPatientIdentifierList(index).getIdentifierTypeCode()
                        .setValue(pid.getIdentifierTypeCode());
                index++;
            }
        }
        pidSegment.getPatientName(0).getFamilyName().getSurname().setValue(patient.getLastName());
        if (patient.getAlternateLastName() != null && !patient.getAlternateLastName().isEmpty()) {
            pidSegment.getPatientName(1).getFamilyName().getSurname().setValue(patient.getAlternateLastName());
        }
        pidSegment.getPatientName(0).getGivenName().setValue(patient.getFirstName());
        if (patient.getAlternateFirstName() != null && !patient.getAlternateFirstName().isEmpty()) {
            pidSegment.getPatientName(1).getGivenName().setValue(patient.getAlternateFirstName());
        }
        pidSegment.getPatientName(0).getNameTypeCode().setValue("L");
        pidSegment.getMotherSMaidenName(0).getFamilyName().getSurname().setValue(patient.getMotherMaidenName());
        if (patient.getMotherMaidenName() != null) {
            pidSegment.getMotherSMaidenName(0).getNameTypeCode().setValue("M");
        }
        if (patient.getAlternateMothersMaidenName() != null) {
            pidSegment.getMotherSMaidenName(1).getNameTypeCode().setValue("M");
            pidSegment.getMotherSMaidenName(1).getFamilyName().getSurname().setValue(patient.getAlternateMothersMaidenName());
        }
        if (patient.getSecondName() != null) {
            String otherNames = patient.getSecondName();
            if (patient.getThirdName() != null) {
                otherNames = otherNames.concat(" " + patient.getThirdName());
            }
            pidSegment.getPatientName(0).getSecondAndFurtherGivenNamesOrInitialsThereof().setValue(otherNames);
        }
        if (patient.getAlternateFirstName() != null) {
            pidSegment.getPatientName(1).getGivenName().setValue(patient.getAlternateFirstName());
            if (patient.getAlternateSecondName() != null) {
                String otherNames = patient.getAlternateSecondName();
                if (patient.getAlternateThirdName() != null) {
                    otherNames = otherNames.concat(" " + patient.getAlternateThirdName());
                }
                pidSegment.getPatientName(1).getSecondAndFurtherGivenNamesOrInitialsThereof().setValue(otherNames);
            }
            pidSegment.getPatientName(1).getNameTypeCode().setValue("L");
        }
        if (patient.getAlternateLastName() != null) {
            pidSegment.getPatientName(1).getFamilyName().getSurname().setValue(patient.getAlternateLastName());
            pidSegment.getPatientName(0).getNameTypeCode().setValue("L");
        }
        if (patient.getDateOfBirth() != null) {
            pidSegment.getDateTimeOfBirth().getTime().setValue(sdf.format(patient.getDateOfBirth()));
        }
        pidSegment.getAdministrativeSex().setValue(patient.getGenderCode());

        pidSegment.getPatientAddress(0).getStreetAddress().getStreetOrMailingAddress().setValue(patient.getStreet());
        pidSegment.getPatientAddress(0).getCity().setValue(patient.getCity());
        pidSegment.getPatientAddress(0).getStateOrProvince().setValue(patient.getState());
        pidSegment.getPatientAddress(0).getZipOrPostalCode().setValue(patient.getZipCode());
        pidSegment.getPatientAddress(0).getCountry().setValue(patient.getCountryCode());
    }

    /**
     * fills the PID segment of the HL7v2.3.1 messages
     *
     * @param pidSegment (from net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment package)
     *
     * @throws HL7Exception
     */
    protected void populatePIDv231(net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment.PID pidSegment) throws HL7Exception {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Patient patient = entityManager.find(Patient.class, order.getEncounter().getPatient().getId());
        if (patient.getPatientIdentifiers() != null) {
            int index = 0;
            for (PatientIdentifier pid : patient.getPatientIdentifiers()) {
                pidSegment.getPatientIdentifierList(index).parse(pid.getIdentifier());
                pidSegment.getPatientIdentifierList(index).getIdentifierTypeCode()
                        .setValue(pid.getIdentifierTypeCode());
                index++;
            }
        }
        pidSegment.getPatientName(0).getFamilyLastName().getFamilyName().setValue(patient.getLastName());
        pidSegment.getPatientName(0).getGivenName().setValue(patient.getFirstName());
        pidSegment.getPatientName(0).getNameTypeCode().setValue("L");
        pidSegment.getMotherSMaidenName().getFamilyLastName().getFamilyName().setValue(patient.getMotherMaidenName());
        if (patient.getMotherMaidenName() != null) {
            pidSegment.getMotherSMaidenName().getNameTypeCode().setValue("M");
        }
        if (patient.getDateOfBirth() != null) {
            pidSegment.getDateTimeOfBirth().getTimeOfAnEvent().setValue(sdf.format(patient.getDateOfBirth()));
        }
        pidSegment.getSex().setValue(patient.getGenderCode());

        pidSegment.getPatientAddress().getStreetAddress().setValue(patient.getStreet());
        pidSegment.getPatientAddress().getCity().setValue(patient.getCity());
        pidSegment.getPatientAddress().getStateOrProvince().setValue(patient.getState());
        pidSegment.getPatientAddress().getZipOrPostalCode().setValue(patient.getZipCode());
        pidSegment.getPatientAddress().getCountry().setValue(patient.getCountryCode());
    }

    /**
     * fills the PV1 segment of the HL7v2.3.1 messages
     *
     * @param pv1Segment (from net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment package)
     *
     * @throws HL7Exception
     */
    protected void populatePV1v231(net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment.PV1 pv1Segment) throws HL7Exception {
        pv1Segment.getVisitNumber().parse(order.getEncounter().getVisitNumber());
        pv1Segment.getVisitIndicator().setValue("V");
        pv1Segment.getPatientClass().setValue(order.getEncounter().getPatientClassCode());
        if ((order.getEncounter().getReferringDoctorCode() != null)
                && !order.getEncounter().getReferringDoctorCode().isEmpty()) {
            String doctorName = order.getEncounter().getReferringDoctorCode().concat("^")
                    .concat(order.getEncounter().getReferringDoctor());
            pv1Segment.getReferringDoctor().parse(doctorName);
        }
    }

    /**
     * fills the PV1 segment of the HL7v2.3.1 messages
     *
     * @param pv1Segment
     *
     * @throws HL7Exception
     */
    protected void populatePV1v231(PV1 pv1Segment) throws HL7Exception {
        pv1Segment.getVisitNumber().parse(order.getEncounter().getVisitNumber());
        pv1Segment.getVisitIndicator().setValue("V");
        pv1Segment.getPatientClass().setValue(order.getEncounter().getPatientClassCode());
        if ((order.getEncounter().getReferringDoctorCode() != null)
                && !order.getEncounter().getReferringDoctorCode().isEmpty()) {
            String doctorName = order.getEncounter().getReferringDoctorCode().concat("^")
                    .concat(order.getEncounter().getReferringDoctor());
            pv1Segment.getReferringDoctor(0).parse(doctorName);
        }
    }

    /**
     * fills the PV1 segment of the HL7v2.5.1 messages
     *
     * @param pv1Segment
     *
     * @throws HL7Exception
     */
    protected void populatePV1v251(ca.uhn.hl7v2.model.v251.segment.PV1 pv1Segment) throws HL7Exception {
        pv1Segment.getVisitNumber().parse(order.getEncounter().getVisitNumber());
        pv1Segment.getVisitIndicator().setValue("V");
        pv1Segment.getPatientClass().setValue(order.getEncounter().getPatientClassCode());
        if ((order.getEncounter().getReferringDoctorCode() != null)
                && !order.getEncounter().getReferringDoctorCode().isEmpty()) {
            String doctorName = order.getEncounter().getReferringDoctorCode().concat("^")
                    .concat(order.getEncounter().getReferringDoctor());
            pv1Segment.getReferringDoctor(0).parse(doctorName);
        }
    }

    /**
     * fills the ORC segment of the HL7v2.3.1 messages
     *
     * @param orcSegment
     * @param isForNewOrder     indicates if the message is a notification of a new order or not
     * @param valuePlacerNumber indicates whether the placer Order Number field should be set or not
     * @param valueFillerNumber indicates whether the filler Order Number field should be set or not
     *
     * @throws HL7Exception
     */
    protected void populateORCv231(ORC orcSegment, boolean isForNewOrder, boolean valuePlacerNumber,
                                   boolean valueFillerNumber) throws HL7Exception {
        orcSegment.getOrderControl().setValue(order.getOrderControlCode());
        if (valuePlacerNumber && (order.getPlacerOrderNumber() != null)) {
            orcSegment.getPlacerOrderNumber().parse(order.getPlacerOrderNumber());
        }
        if (valueFillerNumber && (order.getFillerOrderNumber() != null)) {
            orcSegment.getFillerOrderNumber().parse(order.getFillerOrderNumber());
        }
        if (order.getOrderStatus() != null) {
            orcSegment.getOrderStatus().setValue(order.getOrderStatus());
        }
        if (order.getPlacerGroupNumber() != null) {
            orcSegment.getPlacerGroupNumber().parse(order.getPlacerGroupNumber());
        }
        if (isForNewOrder) {
            orcSegment.getQuantityTiming().getPriority().setValue(order.getQuantityTiming());
            if (order.getStartDateTime() != null) {
                orcSegment.getQuantityTiming().getStartDateTime().getTimeOfAnEvent()
                        .setValue(sdf.format(order.getStartDateTime()));
            }
            orcSegment.getDateTimeOfTransaction().getTimeOfAnEvent().setValue(sdf.format(order.getTransactionDate()));
            String xcn;
            if (order.getEnteredBy() != null) {
                xcn = order.getEnteredBy().concat("^")
                        .concat(ValueSet.getDisplayNameForCode("doctor", order.getEnteredBy()));
                orcSegment.getEnteredBy(0).parse(xcn);
            }
            if (order.getOrderingProvider() != null) {
                xcn = order.getOrderingProvider().concat("^")
                        .concat(ValueSet.getDisplayNameForCode("doctor", order.getOrderingProvider()));
                orcSegment.getOrderingProvider(0).parse(xcn);
            }
            if (order.getCallBackPhoneNumber() != null) {
                orcSegment.getCallBackPhoneNumber(0).getAnyText()
                        .setValue(order.getCallBackPhoneNumber());
            }
            if (order.getEnteringOrganization() != null) {
                String ce = order
                        .getEnteringOrganization()
                        .concat("^")
                        .concat(ValueSet.getDisplayNameForCode("enteringOrganization", order.getEnteringOrganization()));
                orcSegment.getEnteringOrganization().parse(ce);
            }
        }
    }

    /**
     * fills the ORC segment of the HL7v2.3.1 messages
     *
     * @param orcSegment
     * @param isForNewOrder     indicates if the message is a notification of a new order or not
     * @param valuePlacerNumber indicates whether the placer Order Number field should be set or not
     * @param valueFillerNumber indicates whether the filler Order Number field should be set or not
     *
     * @throws HL7Exception
     */
    protected void populateORCv251(ca.uhn.hl7v2.model.v251.segment.ORC orcSegment, boolean isForNewOrder, boolean valuePlacerNumber,
                                   boolean valueFillerNumber) throws HL7Exception {
        orcSegment.getOrderControl().setValue(order.getOrderControlCode());
        if (valuePlacerNumber && (order.getPlacerOrderNumber() != null)) {
            orcSegment.getPlacerOrderNumber().parse(order.getPlacerOrderNumber());
        }
        if (valueFillerNumber && (order.getFillerOrderNumber() != null)) {
            orcSegment.getFillerOrderNumber().parse(order.getFillerOrderNumber());
        }
        if (order.getOrderStatus() != null) {
            orcSegment.getOrderStatus().setValue(order.getOrderStatus());
        }
        if (order.getPlacerGroupNumber() != null) {
            orcSegment.getPlacerGroupNumber().parse(order.getPlacerGroupNumber());
        }
        if (isForNewOrder) {
            orcSegment.getDateTimeOfTransaction().getTime().setValue(sdf.format(order.getTransactionDate()));
            String xcn;
            if (order.getEnteredBy() != null) {
                xcn = order.getEnteredBy().concat("^")
                        .concat(ValueSet.getDisplayNameForCode("doctor", order.getEnteredBy()));
                orcSegment.getEnteredBy(0).parse(xcn);
            }
            if (order.getOrderingProvider() != null) {
                xcn = order.getOrderingProvider().concat("^")
                        .concat(ValueSet.getDisplayNameForCode("doctor", order.getOrderingProvider()));
                orcSegment.getOrderingProvider(0).parse(xcn);
            }
            if (order.getCallBackPhoneNumber() != null) {
                orcSegment.getCallBackPhoneNumber(0).getAnyText()
                        .setValue(order.getCallBackPhoneNumber());
            }
            if (order.getEnteringOrganization() != null) {
                String ce = order
                        .getEnteringOrganization()
                        .concat("^")
                        .concat(ValueSet.getDisplayNameForCode("enteringOrganization", order.getEnteringOrganization()));
                orcSegment.getEnteringOrganization().parse(ce);
            }
        }
    }

    /**
     * fills the OBR segment of the HL7v2.3.1 messages
     *
     * @param obrSegment
     *
     * @throws HL7Exception
     */
    protected void populateOBRv231(OBR obrSegment) throws HL7Exception {
        if ((order.getPlacerOrderNumber() != null) && !order.getPlacerOrderNumber().isEmpty()) {
            obrSegment.getPlacerOrderNumber().parse(order.getPlacerOrderNumber());
        }
        if ((order.getFillerOrderNumber() != null) && !order.getFillerOrderNumber().isEmpty()) {
            obrSegment.getFillerOrderNumber().parse(order.getFillerOrderNumber());
        }
        if ((order.getOrderResultStatus() != null) && !order.getOrderResultStatus().isEmpty()) {
            obrSegment.getResultStatus().setValue(order.getOrderResultStatus());
        }
        if (order.getUniversalServiceId() != null) {
            if (order.getDomain().getKeyword().equals("EYE")) {
                obrSegment.getUniversalServiceID().parse(
                        ValueSetProvider.getInstance().buildCEElementFromCode(order.getUniversalServiceId(),
                                "eyeUniversalServiceId"));
            } else {
                obrSegment.getUniversalServiceID().parse(
                        ValueSetProvider.getInstance().buildCEElementFromCode(order.getUniversalServiceId(),
                                "radUniversalServiceId"));
            }
        }
        if (order.getDangerCode() != null) {
            obrSegment.getDangerCode().parse(
                    ValueSetProvider.getInstance().buildCEElementFromCode(order.getDangerCode(), "danger"));
        }
        obrSegment.getQuantityTiming(0).getPriority().setValue(order.getQuantityTiming());
        if (order.getStartDateTime() != null) {
            obrSegment.getQuantityTiming(0).getStartDateTime().getTimeOfAnEvent()
                    .setValue(sdf.format(order.getStartDateTime()));
        }
        if (order.getReasonForStudy() != null) {
            obrSegment.getReasonForStudy(0).getText().setValue(order.getReasonForStudy());
        }
        if (order.getOrderingProvider() != null) {
            String xcn = order.getOrderingProvider().concat("^")
                    .concat(ValueSet.getDisplayNameForCode("doctor", order.getOrderingProvider()));
            obrSegment.getOrderingProvider(0).parse(xcn);
        }
        if (order.getTechnician() != null) {
            String xcn = order.getTechnician().concat("&")
                    .concat(ValueSet.getDisplayNameForCode("doctor", order.getOrderingProvider()).replace("^", "&"));
            obrSegment.getTechnician(0).getNdl1_OPName().parse(xcn);
        }
        if (order.getRelevantClinicalInfo() != null) {
            obrSegment.getRelevantClinicalInfo().setValue(order.getRelevantClinicalInfo());
        }
    }

    /**
     * fills the OBR segment of the HL7v2.3.1 messages
     *
     * @param obrSegment
     *
     * @throws HL7Exception
     */
    protected void populateOBRv251(ca.uhn.hl7v2.model.v251.segment.OBR obrSegment) throws HL7Exception {
        if ((order.getPlacerOrderNumber() != null) && !order.getPlacerOrderNumber().isEmpty()) {
            obrSegment.getPlacerOrderNumber().parse(order.getPlacerOrderNumber());
        }
        if ((order.getFillerOrderNumber() != null) && !order.getFillerOrderNumber().isEmpty()) {
            obrSegment.getFillerOrderNumber().parse(order.getFillerOrderNumber());
        }
        if ((order.getOrderResultStatus() != null) && !order.getOrderResultStatus().isEmpty()) {
            obrSegment.getResultStatus().setValue(order.getOrderResultStatus());
        }
        if (order.getUniversalServiceId() != null) {
            if (order.getDomain().getKeyword().equals("EYE")) {
                obrSegment.getUniversalServiceIdentifier().parse(
                        ValueSetProvider.getInstance().buildCEElementFromCode(order.getUniversalServiceId(),
                                "eyeUniversalServiceId"));
            } else if (order.getDomain().getKeyword().equals("RAD")) {
                obrSegment.getUniversalServiceIdentifier().parse(
                        ValueSetProvider.getInstance().buildCEElementFromCode(order.getUniversalServiceId(),
                                "radUniversalServiceId"));
            } else {
                obrSegment.getUniversalServiceIdentifier().parse(
                        ValueSetProvider.getInstance().buildCEElementFromCode(order.getUniversalServiceId(),
                                "troUniversalServiceId"));
            }
        }
        if (order.getDangerCode() != null) {
            obrSegment.getDangerCode().parse(
                    ValueSetProvider.getInstance().buildCEElementFromCode(order.getDangerCode(), "danger"));
        }
        if (order.getReasonForStudy() != null) {
            obrSegment.getReasonForStudy(0).getText().setValue(order.getReasonForStudy());
        }
        if (order.getOrderingProvider() != null) {
            String xcn = order.getOrderingProvider().concat("^")
                    .concat(ValueSet.getDisplayNameForCode("doctor", order.getOrderingProvider()));
            obrSegment.getOrderingProvider(0).parse(xcn);
        }
        if (order.getTechnician() != null) {
            String xcn = order.getTechnician().concat("&")
                    .concat(ValueSet.getDisplayNameForCode("doctor", order.getOrderingProvider()).replace("^", "&"));
            obrSegment.getTechnician(0).getNdl1_Name().parse(xcn);
        }
        if (order.getRelevantClinicalInfo() != null) {
            obrSegment.getRelevantClinicalInformation().setValue(order.getRelevantClinicalInfo());
        }
    }

    /**
     * fill the MSA segment of HL7v2.3.1 messages
     *
     * @param msa
     * @param messageControlId
     * @param ackCode
     * @param fillMSA6
     */
    protected void populateMSAv231(MSA msa, String messageControlId, AcknowledgmentCode ackCode, boolean fillMSA6) {
        try {
            msa.getAcknowledgementCode().setValue(ackCode.name());
            msa.getMessageControlID().setValue(messageControlId);
            if (fillMSA6) {
                msa.getErrorCondition().getIdentifier().setValue(Integer.toString(ErrorCode.UNKNOWN_KEY_IDENTIFIER.getCode()));
                msa.getErrorCondition().getAlternateText().setValue(ErrorCode.UNKNOWN_KEY_IDENTIFIER.getMessage());
            }
        } catch (DataTypeException e) {
            log.error("cannot fill MSA segment: " + e.getMessage());
        }

    }

    /**
     * GETTERS and SETTERS
     */
    public String getSendingActorKeyword() {
        return sendingActorKeyword;
    }

    public void setSendingActorKeyword(String sendingActorKeyword) {
        this.sendingActorKeyword = sendingActorKeyword;
    }

    public T getOrder() {
        return order;
    }

    public void setOrder(T order) {
        this.order = order;
    }

    /**
     * @param transactionKeyword the transactionKeyword to set
     */
    public void setTransactionKeyword(String transactionKeyword) {
        this.transactionKeyword = transactionKeyword;
    }

    /**
     * @return the transactionKeyword
     */
    public String getTransactionKeyword() {
        return transactionKeyword;
    }

    public HL7V2ResponderSUTConfiguration getSut() {
        return sut;
    }

    public void setSut(HL7V2ResponderSUTConfiguration sut) {
        this.sut = sut;
    }

    public String getSendingApplication() {
        return sendingApplication;
    }

    public void setSendingApplication(String sendingApplication) {
        this.sendingApplication = sendingApplication;
    }

    public String getSendingFacility() {
        return sendingFacility;
    }

    public void setSendingFacility(String sendingFacility) {
        this.sendingFacility = sendingFacility;
    }
}
