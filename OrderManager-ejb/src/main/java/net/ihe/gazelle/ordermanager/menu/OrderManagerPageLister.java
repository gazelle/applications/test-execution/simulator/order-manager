package net.ihe.gazelle.ordermanager.menu;

import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.common.pages.PageLister;
import org.kohsuke.MetaInfServices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * <p>OrderManagerPageLister class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@MetaInfServices(PageLister.class)
public class OrderManagerPageLister implements PageLister {

	/** {@inheritDoc} */
	@Override
	public Collection<Page> getPages() {
		Collection<Page> pages = new ArrayList<Page>();
		pages.addAll(Arrays.asList(OrderManagerPages.values()));
		return pages;
	}

}
