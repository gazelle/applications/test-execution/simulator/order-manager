package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.model.ValueSet;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Class description : <b>Encounter</b>
 * This class describes the encounter object
 * This class defines the following attributes
 * <ul>
 * <li><b>id</b> unique identifier in the database</li>
 * <li><b>patient</b> the patient to who is assigned the encounter</li>
 * <li><b>creationDate</b> date of creation of the encounter</li>
 * <li><b>creator</b> creator (user/SUT) of the encounter</li>
 * <li><b>visitNumber</b> visit number</li>
 * <li><b>patientClassCode</b> class code of the patient</li>
 * <li><b>assignedPatientLocation</b> if patient is an inpatient, his/her location (bed/room/facility...) in the hospital</li>
 * <li><b>referringDoctorCode</b> the identifier of the doctor</li>
 * <ul>
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 * @version 1.0 - 2011, November 17th
 */

@Entity
@Name("encounter")
@Table(name = "om_encounter", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_encounter_sequence", sequenceName = "om_encounter_id_seq", allocationSize = 1)
public class Encounter implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5524171929956363278L;

    @Id
    @GeneratedValue(generator = "om_encounter_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    @NotNull
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @OneToMany(mappedBy = "encounter")
    private List<Order> orders;

    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name = "creator")
    private String creator;

    @Column(name = "visit_number")
    private String visitNumber;

    @Column(name = "patient_class_code")
    private String patientClassCode;

    @Column(name = "assigned_patient_location")
    private String assignedPatientLocation;

    @Column(name = "referring_doctor_code")
    private String referringDoctorCode;

    public Encounter() {

    }

    /**
     * @param entityManager
     *
     * @return the saved encounter
     */
    public Encounter save(EntityManager entityManager) {
        Encounter encounter = entityManager.merge(this);
        entityManager.flush();
        return encounter;
    }

    /**
     * @return the patient class code
     */
    public String getPatientClass() {
        return ValueSet.getDisplayNameForCode("patientClass", this.getPatientClassCode());
    }

    /**
     * @return the doctor display name
     */
    public String getReferringDoctor() {
        return ValueSet.getDisplayNameForCode("doctor", this.getReferringDoctorCode());
    }

    public static Encounter getEncounterByVisitNumber(String visitNumber, EntityManager entityManager) {
        HQLQueryBuilder<Encounter> queryBuilder = new HQLQueryBuilder<Encounter>(entityManager, Encounter.class);
        queryBuilder.addEq("visitNumber", visitNumber);
        List<Encounter> encounters = queryBuilder.getList();
        if ((encounters != null) && !encounters.isEmpty()) {
            return encounters.get(0);
        } else {
            return null;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public final void setCreationDate(Date timestamp) {
        if (timestamp != null) {
            this.creationDate = (Date) timestamp.clone();
        } else {
            this.creationDate = null;
        }
    }

    public Date getCreationDate() {
        if (creationDate != null) {
            return (Date) creationDate.clone();
        } else {
            return null;
        }
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getVisitNumber() {
        return visitNumber;
    }

    public void setVisitNumber(String visitNumber) {
        this.visitNumber = visitNumber;
    }

    public String getPatientClassCode() {
        return patientClassCode;
    }

    public void setPatientClassCode(String patientClassCode) {
        this.patientClassCode = patientClassCode;
    }

    public String getAssignedPatientLocation() {
        return assignedPatientLocation;
    }

    public void setAssignedPatientLocation(String assignedPatientLocation) {
        this.assignedPatientLocation = assignedPatientLocation;
    }

    public String getReferringDoctorCode() {
        return referringDoctorCode;
    }

    public void setReferringDoctorCode(String referringDoctorCode) {
        this.referringDoctorCode = referringDoctorCode;
    }
}
